- [NOMAD inputs](#nomad-inputs)
  - [xml files](#xml-files)
    - [Infrastructure xml](#infrastructure-xml)
      - [Walkable areas](#walkable-areas)
      - [Obstacles](#obstacles)
      - [Destinations](#destinations)
      - [Sources](#sources)
      - [Queues](#queues)
    - [Pedestrian parameter sets xml](#pedestrian-parameter-sets-xml)
    - [Scenario definition xml](#scenario-definition-xml)
      - [Demand manager](#demand-manager)
        - [Activities](#activities)
        - [Activity patterns](#activity-patterns)
        - [Demand patterns](#demand-patterns)
        - [Restaurant scheduler](#restaurant-scheduler)
      - [Output managers](#output-managers)
        - [Simulation file output manager](#simulation-file-output-manager)
        - [In-memory output manager](#in-memory-output-manager)
        - [Connectivity file output manager](#connectivity-file-output-manager)


# NOMAD inputs

The nomad input structure consists of three parts:

1. The infrastructure input, defining all the geometry and locations of things such as sources and destination
2. The pedestrian parameter set input, defining the different pedestrian profiles and the accompanying parameter sets
3. The scenario input, defining elements such as the demand, the simulation duration etc.

All these parts will be discussed in more detail below.

## xml files

All three types of input mentioned above need to be provided to NOMAD via xml-files. Every part has its own file and these are discussed in detail below.

### Infrastructure xml

The basis of the xml-file is the infrastructure element:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<infrastructure xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="https://gitlab.tudelft.nl/martijnsparnaa/nomad_xsds/-/raw/master/NOMAD_infrastructure.xsd">
    WALKLEVELS
</infrastructure>
```

The infrastructure element contains 1 or more walkLevel elements. A walkLevel represents a single height layer in a multistory environment. As shown below, every walkLevel needs a unique ID.

```xml
<walkLevel ID="base">
    WALKLEVEL_CONTENT
</walkLevel>
```

The walkLevel contains the following elements:

* A list of walkable areas (you need to define at least one walkableArea)
* A list of obstacles (This list can be empty)
* A list of destinations (you need to define at least one destination)
* A list of sources (you need to define at least one source)
* A list of queues (This list can be empty)

#### Walkable areas

A walkable area defines where pedestrians can potentially walk. One can define multiple non-overlapping walkable areas in one walk level. The areas in the walkable area that are (partly) overlapped by any of the obstacles are removed from the walkable area. As shown below every walkable area needs a unique **ID** and a list of **coordinates** with define the polygon that describes the walkable area. The list is defined as ((x_0,y_0),(x_1,y_1),...(x_n,y_n)) and the coordinates define the absolute locations in meters.

```xml
<walkableAreas>
    <walkableArea ID="base">
        <coords>((0,0),(19,0),(19,12),(0,12),(0,10),(1,10),(1,2),(0,2))</coords>    
    </walkableArea>
</walkableAreas>
```

#### Obstacles

An obstacle defines an area of the walk level that is inaccessible to the pedestrians and which exert a repulsive force on the pedestrians. As shown below, there are four types of obstacles:

1. A line obstacle: A line obstacle is a 1-dimensional obstacle ideal for using as the boundaries of the walkable space (to prevent pedestrians from accidentally leaving the walkable space) or for very thin walls.
2. A polygon obstacle: A polygon obstacle can represent any obstacle. However, in the case of line-like, circular or elliptical obstacles you are advised to use the obstacle types specifically made for those type of obstacles.
3. A circular obstacle: An obstacle in the shape of a circle.
4. An elliptical obstacle: An obstacle in the shape of an ellipse.

Every obstacle has a unique **ID**. The _line_ and _polygon_ need a list of **coordinates** defining, respectively, the line and the polygon. The list has the same structure as the coordinate list of the walkable space.

For both the _circular_ and _elliptical_ obstacles you need to define their **center coordinate** (x,y) in meters. In the case of the _circular_ obstacle you also needs to define its **radius**, again in meters. In the case of the _elliptical_ obstacle you need to define the lengths **semi-major and semi-minor axes** and the **rotation**. In NOMAD it is assumed that the **semi-major axis** is in the x-direction and **semi-minor axis** in the y-direction ([see here](https://en.wikipedia.org/wiki/File:Ellipse-def0.svg)) and that its values are given in meters. The rotation (which is performed based on the center point of the ellipse) is in degrees whereby a positive value results in a counter-clockwise rotation and a negative value in a clockwise rotation.

All obstacles also have an additional option to make them **see through**. By default, obstacles are not see-through but if this option is set to "true" this means the model assumes the obstacles aren't obstructing the sightlines of pedestrians (though it's still preventing pedestrians from accessing the space covered by the obstacle).

```xml
<obstacles> 
    <lineObstacle ID="bottomWall">
        <coords>((1,0), (19,0))</coords>
    </lineObstacle>
    <polygonObstacle ID="bar">
        <seeThrough>true</seeThrough>
        <coords>((7.5,12), (7.5,7.5), (16,7.5), (16,8), (8,8), (8,12))</coords>
    </polygonObstacle>
    <circleObstacle ID="table_2">
        <centerCoord>(4.75,3.75)</centerCoord>
        <radius>0.75</radius>
    </circleObstacle>
    <ellipseObstacle ID="table_3">
        <centerCoord>(13.75,3.75)</centerCoord>
        <semiAxesValues>(1,2)</semiAxesValues>
        <rotation>45</rotation>
    </ellipseObstacle>
</obstacles>
```

#### Destinations

As the name suggest, destinations are the locations that define where a pedestrian goes to perform an activity, the location of a waypoint or the destination of the sinks (the locations where the pedestrians can leave the model).

There are six ways to define a destination:

1. A polygon destination: The geometry of the destination is given by a polygon. When a pedestrian reaches the edge of the polygon (and this is its current destination) it will start the activity linked to this destination (see the activity section for more info).
2. A centroid polygon destination: The geometry of the destination is given by a polygon. When a pedestrian reaches the edge of the polygon (and this is its current destination) it will move to the center of the polygon and start the activity linked to this destination (see the activity section for more info).
3. A line destination: The geometry of the destination is given by a line. When a pedestrian reaches the line (and this is its current destination) it will start the activity linked to this destination (see the activity section for more info).
4. A point destination: The geometry of the destination is given by a single point. When a pedestrian reaches the point (and this is its current destination) it will start the activity linked to this destination (see the activity section for more info).
5. A multi-point in rectangle destination: The geometry of the destination is given by a rectangle. In this rectangle a grid of point destinations is created whereby the points are spaced apart by a provided distance. This destination can, for example, be used to model people standing in front of a stage.
6. A spawn able line destination: This destination is a line destination with the added feature that is can also be used as a line source. This destination is, for example, used to support the use of the restaurant scheduler (see the restaurant scheduler section for more information).

Every destination has a unique **ID**. The _polygon_, _centroid polygon_, _multi point in rectangle_ and _line_ need a list of **coordinates** defining, respectively, the polygon, the rectangle and the line. The list has the same structure as the coordinate lists of the walkable space and obstacles. The _point_ destination only needs a single set of **coordinates**.

Optionally, destinations can have an additional attribute, namely the **groupID** attribute. This optional attribute can be used to group certain destinations. This is, for example, used to support the use of the restaurant scheduler (see the restaurant scheduler section for more information).

Destinations can also set to be **virtual obstacles**. By setting this option to "true", the destination becomes a so-called virtual obstacle which means that for the routing this area is seen as an obstacle. However, unlike a real obstacle the area is still accessible by pedestrians. This option can, for example, be used to model the area taken up by a chair. As a pedestrian should be able to sit on this chair the area should be accessible. However, when the chair is not a pedestrian's destination it should see it as an obstacle.

Furthermore, destinations can also set to be **local destinations**. By setting this option to "true", the destination becomes a local destination of a global destination. This global destination is an obstacle which is identified by the **groupID** attribute. A pedestrian will first walk towards this global destination and when close to the destination (about 2 meters) the pedestrian will follow the route to its specific destination. An example of this can be a table with multiple chairs. The table is the obstacle that is the global destination and the chairs are local destinations. When a pedestrian walks to its chair, the pedestrian will first walk globally towards the table and only in the last few meters walk to its actual destination (the chair). This option can thus enable modelling a slightly different route choice behavior. As an added advantage it also reduces the computation time for the route floor fields.

Lastly, the _multi point in rectangle destination_ has three options to determine how pedestrians choose their destination in the rectangle. Firstly, **distBetweenPoints** which determines the distance between the points in the grid. Each of these points in the grid is a point destination. Secondly, pedestrians can choose their point destination within the rectangle in one of three ways. 1) Randomly, this is the default option whereby a pedestrian selects an unoccupied point destination randomly. 2) One side of the rectangle has preference. In this case pedestrians prefer a destination that is as close to one of the edges of the rectangle. For example, if the rectangle is the area in front of a stage, pedestrians want to find a spot a close to the edge of the rectangle that is next to the stage. To enable this behavior you have to define the **directionVec**. The direction vector is a vector that is orthogonal to and pointing outwards from the edge of the rectangle that you want the pedestrians to prioritize. 3) Around a focal point. In this case the pedestrians try to find an unoccupied destination as close to the given focal point. This focus point is defined by the **focusPoint** parameter. This is a point anywhere in or on the edge of the rectangle. This can, for example, be used to model that people not only want to stand in front of the stage but also as close to the middle as possible. Lastly, the parameter **randomnessFactor** determines, in case the **directionVec** or the **focusPoint** is defined, the fraction of pedestrians that choose a random location instead of their optimal point. If this value is not defined or if this value is 0, it means that all pedestrians choose their optimal destination. When it is 1 all pedestrians will choose a random destination and when it is for example 0.5 half of the pedestrians will choose their optimal point whilst the other half chooses a random point.   


```xml
<destinations>
    <polygonDestination ID="exit">
        <coords>((0,0), (1,0), (1,2), (0,2))</coords>
    </polygonDestination>
    <centroidPolygonDestination ID="char_1_table_2" groupID="table_2">
        <coords>((4.5,4.5), (5,4.5), (5,5), (4.5,5))</coords>
        <isVirtualObstacle>true</isVirtualObstacle>
        <isLocalDestination>true</isLocalDestination>
    </centroidPolygonDestination>
    <lineDestination ID="waypoint_1">
        <coords>((1,5.8), (2,5.8))</coords>
    </lineDestination>
    <pointDestination ID="register">
        <coords>(8,7)</coords>
    </pointDestination>
    <spawnableLineDestination ID="baseArea_1">
        <coords>((1,1), (1,2))</coords>
    </spawnableLineDestination>
    <multiPointInRectangleDestination ID="stageArea_1">
        <coords>((0,0), (10,0), (10,5), (0,5))</coords>
        <distBetweenPoints>0.5</distBetweenPoints>
    </multiPointInRectangleDestination>
    <multiPointInRectangleDestination ID="stageArea_2">
        <coords>((0,0), (10,0), (10,5), (0,5))</coords>
        <distBetweenPoints>0.5</distBetweenPoints>
        <directionVec>((0,0), (0,1))</directionVec>
    </multiPointInRectangleDestination>
    <multiPointInRectangleDestination ID="stageArea_3">
        <coords>((0,0), (10,0), (10,5), (0,5))</coords>
        <distBetweenPoints>0.5</distBetweenPoints>
        <focusPoint>(5,5)</focusPoint>
        <randomnessFactor>0.2</randomnessFactor>
    </multiPointInRectangleDestination>
</destinations>
```

#### Sources

A source is a location where pedestrians can enter the model. As you can see below there are two types of sources:

1. A simple random line source: This is a source, geometrically defined by a line, where all pedestrians that need to enter the model get a random position somewhere along the line as their initial position when they enter the model.
2. A rectangle source: This is a source, geometrically defined by a rectangle, where all pedestrians that need to enter the model get a semi-random position somewhere in the rectangle as their initial position when they enter the model.

Every source has a unique **ID**. The _line_ source needs a list of **coordinates** defining the line. The list has the same structure as all previously discussed coordinate lists. The _rectangle_ source also needs a list of **coordinates**, however this list must define a rectangle by its four corner points. Similar to the _multi point in rectangle destination_ the **directionVec** and **focusPoint** parameters can be used to enable a different behavior than the default random placement. Contrary to the _multi point in rectangle destination_, in case of the _rectangle source_ the **distBetweenPoints** is automatically determined and doesn't have to be provided nor does the **randomnessFactor**

How many pedestrians enter at which source at which times with which properties is determined by the demand manager, so see that section for more info on those things.

```xml
<sources>
    <simpleRandomLineSource ID="entry">
        <coords>((1,10), (1,12))</coords>
    </simpleRandomLineSource>
    <rectangleSource ID="entry_2">
        <coords>((0,8), (1,8), (1,10), (0,10))</coords>
    </rectangleSource>
    <rectangleSource ID="entry_3">
        <coords>((0,8), (1,8), (1,10), (0,10))</coords>
        <directionVec>(1,0)</directionVec>
    </rectangleSource>
    <rectangleSource ID="entry_4">
        <coords>((0,8), (1,8), (1,10), (0,10))</coords>
        <focusPoint>(9,1)</focusPoint>
    </rectangleSource>
</sources>
```

#### Queues

This part defines the physical location of a queue. The scenario definition xml can use any queue to couple it to an activity. A queue is defined by its:
* **ID**: This attribute can later be used by the scenario definition xml to couple this queue to an activity
* **coords**: This is a list of coordinates that defines the line along which pedestrians will queue. This can be a single line segment (i.e. a line defined by 2 points) or multiple joined line segments (3 or more points). 
* **isVirtualObstacle**: This is an optional parameter which by default is false. If set to true, the queue will become a virtual obstacle.

```xml
<queues>
    <queue ID="queue_1">
        <coords>((1,10), (1,12))</coords>
    </queue>
    <queue ID="queue_2">
        <coords>((0,8), (1,8), (1,10), (0,10))</coords>
        <isVirtualObstacle>true</isVirtualObstacle>
    </queue>
</queues>
```

### Pedestrian parameter sets xml

The pedestrian parameter sets xml contains all the different pedestrian profiles and their accompanying parameter sets. For a complete list of the parameters, see the end of this section.

The basis of the xml-file is the pedParameterSetselement:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<pedParameterSets xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="https://gitlab.tudelft.nl/martijnsparnaa/nomad_xsds/-/raw/master/NOMAD_pedParameters.xsd">
    PED_PARAMETER_SETS
</pedParameterSets>
```

This pedParameterSetselement contains one or more pedParameterSet elements. These elements define the pedestrian profiles and are discussed in more detail below.

Every pedestrian parameter set is defined by a unique **ID**. This pedestrian parameter set has one or more parameter elements which define the value of that parameter. If a parameter is not defined the default value will be used by the model. Optionally, you can also define a **base set** by providing the **ID** of another parameter set. This other parameter set now forms the basis of this parameter set. Once can use this method to easily extend another parameter set for example.

```xml
<pedParameterSet ID="base">
    PARAMETERS
</pedParameterSet>
```

```xml
<pedParameterSet ID="slow" baseSet="base">
    PARAMETERS
</pedParameterSet>
```

The general structure of how to define a parameter's value is shown below. To define the parameter, first give it a name (instead of name_paramater_1 etc.). A list of all valid NOMAD parameter names can be found at the end of this section.

A **parameter's value** can be defined as a single number or as a distribution. In the case you define it as a **distribution** you need to provide the:
* **distributionType**: This defines from which of the three supported packages you want to use to define a distribution. Currently, three packages are supported: 1) numpy, 2) scipy and 3) openturns. 
* **distrName**: The name of the distribution from the package defined by the **distributionType** attribute. NOMAD currently supports the distributions from numpy [numpy manual](https://numpy.org/doc/stable/reference/random/generator.html#distributions), scipy stats [scipy stats manual](https://docs.scipy.org/doc/scipy/tutorial/stats.html) and OpenTURNS [OpenTURNS manual](https://openturns.github.io/openturns/latest/user_manual/probabilistic_modelling.html). 
* **distrArgs**: A list of arguments the chosen distribution needs. 
* **lowerTrunc**: An optional parameter which defines the lower truncation boundary in case you define a truncated version of an **OpenTURNS** distribution.
* **upperTrunc**: An optional parameter defining the upper truncation boundary.
* **min** : An optional parameter defining the minimum value. If this value is set, the distribution will return this value if a value is drawn from the distribution that is smaller than this value.
* **max** : An optional parameter defining the maximum value. Works similarly as the minimum value.

```xml
<name_paramater_1>0.5</name_paramater_1> <!-- single value -->
<name_paramater_2>
    <distribution distributionType="numpy">
        <distrName>normal</distrName>
        <distrArgs>(0.6, 0.2)</distrArgs>
    </distribution>
</name_paramater_2>
<name_paramater_3>
    <distribution distributionType="scipy">
        <distrName>normal</distrName>
        <distrArgs>(0.6, 0.2)</distrArgs>
        <min>-0.6</min>
        <max>1.8</max>
    </distribution>
</name_paramater_3>
<name_paramater_4>
    <distribution distributionType="openturns">
        <distrName>Normal</distrName>
        <distrArgs>(0.6, 0.2)</distrArgs>
        <lowerTrunc>-0.6</lowerTrunc>
        <upperTrunc>1.8</upperTrunc>
    </distribution>
</name_paramater_4>
```

Examples:

```xml
<tau>0.5</tau>
<preferredSpeed>
    <distribution distributionType="openturns">
        <distrName>normal</distrName>
        <distrArgs>(1.34, 0.2)</distrArgs>
        <lowerTrunc>0.8</lowerTrunc>
        <upperTrunc>1.8</upperTrunc>
    </distribution>
</preferredSpeed>
```

The following parameters of the model can be adapted (for more info about the parameters see [this thesis](https://doi.org/10.4233/uuid:b65e6e12-a85e-4846-9122-0bb9be47a762)):

* preferredSpeed
* tau
* a_0
* r_0
* a_1
* r_1
* kappa_0
* kappa_1
* a_W
* d_shy
* r_infl
* ie_f
* ie_b
* cPlus_0
* cMinus_0
* t_A
* radius
* noiseFactor

### Scenario definition xml

The basis of the xml-file is the nomadScenario element:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<nomadScenario xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="https://gitlab.tudelft.nl/martijnsparnaa/nomad_xsds/-/raw/master/NOMAD_scenario.xsd">
    SCENARIO DEFINITION
</nomadScenario>
```

The nomadScenario element always contains the following seven required elements:

1) Name: This is the name that identifies the scenario and is used to, for example, name the output files.
2) Label: This is the formatted version of the name that is shown to user in, for example, a gui.
3) Duration: This is the duration of the simulation in seconds. You can provide the duration as a number or as an multiplication equation (only the \* sign is allowed).
4) InfrastructureXmlFlNm: This is the link to the xml-file that will be used to define the infrastructure of the scenario. This can either be an absolute path (e.g. C:\\Some_user\\Nomad\\scen_infra.xml) or a relative path relative to this file.
5) PedParameterSetsXmlFlNm: This is the link to the xml-file that will be used to define the pedestrian parameter sets that can be used in the scenario. This can either be an absolute path (e.g. C:\\Some_user\\Nomad\\scen_ped_param_sets.xml) or a relative path relative to this file.
6) DemandManager: The demand manager defines how many pedestrians enter the simulation where and when and what their properties (e.g. their parameter set or their activity pattern) are.
7) OutputManager: The output manager defines which output manager is used and thus how the results of the simulation are saved.

```xml
<name>activity_scheduler_test</name>
<label>Activity scheduler test</label>
<duration>30*60</duration>
<infrastructureXmlFlNm>activity_scheduler_test_infra.xml</infrastructureXmlFlNm>
<pedParameterSetsXmlFlNm>pedestrian_parameter_sets_example.xml</pedParameterSetsXmlFlNm>
<demandManager>
    DEMAND_MANAGER_CONTENT
</demandManager>
<outputManagers>
    OUTPUT_MANAGER_CONTENT
</outputManagers>
```

Next to the seven required elements, the scenario configuration can have the following optional elements:
* distBetweenQueueingPeds: The distance between pedestrians when they are standing in a queue. The distance is the distance between the center point of the circles that represent the pedestrians. This is by default 0.5 meters.
* gridCellSize: The size of the grid cells that form the spatial grid. This spatial grid is used to quickly obtain the pedestrians and obstacles within a certain distance of a pedestrian. By default, it is 1 meter.
* accCalcFcnType: This sets the function for computing the acceleration of a pedestrian. NOMAD contains 6 acceleration functions:

| Function | Description | Use case|
| -------- | ----------- | ------- | 
| main | The default acceleration function as described in [refer to original NOMAD documentation] | Use as default |
| redPed | This acceleration function ensures the pedestrian always keeps moving to its destination with at least a minimum speed (0.4 m/s) by reducing the influence of the social forces exerted by other pedestrians if these would prevent a pedestrian from moving towards its destination. | Use for scenarios with tight spaces but relatively low densities such as restaurants. As pedestrian will not stop for each other (they will always keep moving towards their destination with a minimum speed) this function should not be used when modelling crowds as people can walk "through" each other if that is the only way to keep moving towards their destination.|
| noNoise | This ignores the randomly generated noise term when computing the acceleration. | Debugging & testing |
| noPed | This ignores the pedestrian force term when computing the acceleration. | Debugging & testing |
| noNoiseAndPed | This ignores both the randomly generated noise term and the pedestrian force term when computing the acceleration. | Debugging & testing |
| onlyPath | This only uses the destination term to compute the acceleration. | Debugging & testing |

* inIsolationTimeStep: The main time step of the model. By default, 0.1 seconds. See [the original NOMAD documentation](https://doi.org/10.4233/uuid:b65e6e12-a85e-4846-9122-0bb9be47a762) for more details.
* inRangeStepsPerInIsolationStep: The number of sub-timesteps NOMAD uses to compute interactions between pedestrian who are close to each other more precisely. By default, 5 (i.e. time steps of inIsolationTimeStep/5 seconds). See [the original NOMAD documentation](https://doi.org/10.4233/uuid:b65e6e12-a85e-4846-9122-0bb9be47a762) for more details.
* inCollisionStepsPerInIsolationStep: The number of sub-timesteps NOMAD uses to compute interactions between pedestrian who are in collision with each other more precisely. By default, 10 (i.e. time steps of inIsolationTimeStep/10 seconds). See [the original NOMAD documentation](https://doi.org/10.4233/uuid:b65e6e12-a85e-4846-9122-0bb9be47a762) for more details.


#### Demand manager

The demand manager defines how many pedestrians enter the simulation where and when and what their properties (e.g. their parameter set or their activity pattern) are.

A demand manager always contains a list of activities and then depending on the use of a scheduler or not, a scheduler or activity patterns and demand patterns (if a scheduler is used the scheduler will create the activity and demand patterns so they do no need to be defined explicitly). These four elements are discussed in more detail below.

```xml
<demandManager>
    <activities>
        ACTVITIES 
    </activities>
    <activityPatterns>
        ACTIVITY_PATTERNS
    </activityPatterns>
    <demandPatterns>
        DEMAND_PATTERNS
    </demandPatterns>
</demandManager>
```

```xml
<demandManager>
    <activities>
        ACTVITIES 
    </activities>
    <restaurantScheduler>
        RESTAURANT_SCHEDULER
    </restaurantScheduler>
</demandManager>
```

##### Activities

Currently, there are five types of activities:

1) Sink: This is a place where pedestrians leave the simulation
2) Waypoint: A waypoint can be use to influence the route a pedestrian takes
3) FixedWaitingTimeActivity: An activity where a pedestrian waits for a given fixed time before moving to the next activity.
4) FixedWaitingTimeNonInteractingActivity: The same as a FixedWaitingTimeActivity but in this case the pedestrian is not interacting with other pedestrians and is also not exerting any influence on other pedestrians (i.e. the pedestrian is invisible to other pedestrians).
5) EndAtTimeWaitingActivity: An activity where a pedestrian waits till a given time before moving to the next activity.

All activities have a unique **ID** and also a **destinationID** that defines which destination it is related to (the destinations are defined in the infrastructure xml). Activities can also optionally be connected to the queues defined in the infrastructure xml. This is done via the **queueID**. Via the **queueDelay** you set a delay between the moment a pedestrian is detected leaving this activity and the next pedestrian moving from the queue to this activity in seconds. This parameter can be used to simulate the behavior of pedestrians to first let someone leave an activity (i.e. let them make room for you) before they move from the queue to the activity. 

Additionally, the _FixedWaitingTimeActivity_, the _FixedWaitingTimeNonInteractingActivity_ and the _EndAtTimeWaitingActivity_ all also need you to provide a **name** for the activity. This should be descriptive of the activity. Furthermore, you should also provide a **duration** or, in the case of the EndAtTimeWaitingActivity, an **end time**. Both are in seconds and both can be given as a multiplication equation (again you can only use the \* sign).

```xml
<activities>
    <sink ID="exit">
        <destinationID>exit</destinationID>
    </sink>
    <waypoint ID="waypoint_1">
        <destinationID>waypoint_1</destinationID>
    </waypoint>
    <fixedWaitingTimeActivity ID="waiting_activity_1">
        <destinationID>char_1</destinationID>
        <name>sitting</name>
        <duration>5*60</duration>
    </fixedWaitingTimeActivity>
    <fixedWaitingTimeNonInteractingActivity ID="waiting_activity_2">
        <destinationID>toilet_1</destinationID>
        <name>toilet</name>
        <duration>3*60</duration>
        <queueID>queue_1</queueID>
        <queueDelay>2</queueDelay>
    </fixedWaitingTimeNonInteractingActivity>
    <endAtTimeWaitingActivity ID="waiting_activity_3">
        <destinationID>char_2</destinationID>
        <name>sitting</name>
        <endTime>15*60</endTime>
    </endAtTimeWaitingActivity>
</activities>
```

##### Activity patterns

Activity patterns define the sequence of activities a pedestrian will perform. Every activity pattern has a unique **ID** and **a list of activity IDs** which define the sequence of activities to be performed.

```xml
<activityPatterns>
    <listActivityPattern ID="customer_1">(waypoint_1, waiting_activity_1, waiting_activity_2, waiting_activity_1, exit)</listActivityPattern>
</activityPatterns>
```

##### Demand patterns

The demand patterns determine when and where how many pedestrians with which properties try to enter the simulation. There are four types of patterns, namely:

1) constantDemandPattern: Provides a constant inflow of a certain number of pedestrians per second for a certain amount of time.
2) poissonDemandPattern: Provides an average inflow of a certain number of pedestrians per second whereby the number of pedestrians entering every timestep is determined using the Poisson distribution.
3) variableDemandPattern : Provides an inflow of a certain number of pedestrians per second for a certain amount of time whereby the inflow in pedestrians per second can vary over time.
4) discreteDemandPattern: Provides a certain inflow of a certain number of pedestrians at a certain time.

Each demand pattern has a unique **ID**. You also need to provide a **source ID** which determines at which source the pedestrians will enter the simulation (the sources are defined in the infrastructure xml). You also need to provide the **ID to the activity pattern** that the pedestrians should use and the **ID of the parameter set** that should be assigned to the pedestrians (the parameter sets are defined in the pedestrian parameter set xml). In the case of the _constantDemandPattern_, the _poissonDemandPattern_ and the _variableDemandPattern_ you can also optionally define the **start time** and **end time**. The time should be provided in seconds and you can use the \* sign. If no start time is provided it is assumed to be 0 and if no end time is provided it is assumed to be equal to the end time of the simulation. In the case _constantDemandPattern_ and the _poissonDemandPattern_ you also have to define the **flow per second** whereby the number is the number of pedestrians that will enter the model every second. The _variableDemandPattern_ needs a **time flow array**. This is a list which defines the flow per second at certain points in time whereby the flow per second is interpolated between every two points using linear interpolation. The format is ((time_1, inflow_1),(time_2, inflow_2),...,(time_n, inflow_n)) whereby the time should be provided in seconds (the \* sign can be used) and the inflow in pedestrians per second. In the case of the _discreteDemandPattern_ you need to provide a **discrete pattern**. The format of this pattern is ((time_1, pedCount_1),(time_2, pedCount_2),...,(time_n, pedCount_n)) whereby the time should be provided in seconds (again, the \* sign can be used) and the pedestrian count should be a positive integer defining the number of pedestrian that will try to enter the simulation at that specific time.

```xml
<demandPatterns>
    <constantDemandPattern ID="demand">
        <sourceID>entry</sourceID>
        <activityPatternID>customer_1</activityPatternID>
        <parameterSetID>base</parameterSetID>
        <startTime>0</startTime>
        <endTime>5*60</endTime>
        <flowPerSecond>0.1</flowPerSecond>
    </constantDemandPattern>
    <poissonDemandPattern ID="demandPoisson">
        <sourceID>entry</sourceID>
        <activityPatternID>customer_1</activityPatternID>
        <parameterSetID>base</parameterSetID>
        <startTime>0</startTime>
        <endTime>5*60</endTime>
        <flowPerSecond>0.2</flowPerSecond>
    </poissonDemandPattern>    
    <variableDemandPattern ID="demand_variable">
        <sourceID>entry</sourceID>
        <activityPatternID>customer_1</activityPatternID>
        <parameterSetID>slow</parameterSetID>
        <startTime>0</startTime>
        <endTime>5*60</endTime>
        <timeFlowArray>((1*60*60,1),(4*60*60,1),(5*60*60,2.5),(5.5*60*60,2.5),(6*60*60,0.5))</timeFlowArray>
    </variableDemandPattern>
    <discreteDemandPattern ID="demand_discrete">
        <sourceID>entry</sourceID>
        <activityPatternID>customer_1</activityPatternID>
        <parameterSetID>base</parameterSetID>
        <discretePattern>((2*60,1),(8*60,2))</discretePattern>
    </discreteDemandPattern>
</demandPatterns>
```

##### Restaurant scheduler

The restaurant scheduler is currently the only activity scheduler that is available. Currently you can also only use one scheduler per scenario.

The restaurant scheduler needs the following input:

* **source ID**: This determines which source should be use as the entrance into the restaurant (the sources are defined in the infrastructure xml)
* **sink ID**: This determines the sink activity which should be used as the exit from the restaurant
* **demand pattern**: This determines how many groups will try to enter the restaurant in which time frame. This is a list with the following format ((startTime_1, endTime_1, groupCnt_1),(startTime_2, endTime_2, groupCnt_2),...,(startTime_n, endTime_n, groupCnt_n)) whereby the start time and end time should be provided in seconds (the \* sign can be used) and the group count should be a positive integer. The size of the groups is determined by the number of seats available at the free tables.
* **sitting destination group IDs**: Defines the collection of activity locations where a single group can sit. The IDs are the group IDs given to the destinations in the infrastructure xml.
* **visit duration**: This determines the visit duration of the guests groups. This can either be a single static value or a distribution. For more details on how to define a static value or a distribution see the [pedestrian parameter xml configuration](#pedestrian-parameter-sets-xml).
* **in group entry distribution**: Determines the time between the arrival of customers of the same group. This can either be a single static value or a distribution.
* **guest pedestrian parameter set distribution**: Determines which pedestrian parameter sets are used with what probability. It is defined as a list with the following format ((ped_parameter_id_1, prob_1), ..., (ped_parameter_id_n, prob_n)). Where the ped_parameter_id_i is the ID that defines the pedestrian parameter set defined in the pedestrian parameter set xml and prob_i is a value between 0 and 1 that defines the probability of this parameter set being used for a customer relative to the other probabilities. The sum of all probabilities should be 1.
    * **staff scheduler**: The staff scheduler provides all the input to the scheduler that dynamically assigns the activities to the staff. You can only use one staff scheduler per restaurant scheduler. The staff scheduler requires the following input:
    * **base area ID**: This determines which spawnable line destination is used as the base area for the staff. This is a location in the restaurant where the staff waits if they have no activity to perform and also the location where they collect the food and drinks that are to be served and/or drop empty plates.
    * **staff count**: The number of waiting staff
    * **Pedestrian parameter set**: The ID of the pedestrian parameter set (defined in the pedestrian parameter set xml) that is used for the staff.
    * **activity count**: The number of activities the waiting staff performs per table per visit of a group of customer. Either a static value or a distribution.

        And the following optional input can be defined

    * **neighborhoods**: Determines if serving neighborhoods are used in the restaurant. A serving neighborhood is a set of tables that is primarily served by one or more appointed members of staff. The neighborhoods are defined by a list formatted as follows: ((table_id_1, table_id_2, ..., table_id_N), ..., (table_id_m, table_id_m+1, ..., table_id_M)).
    * **activity duration**: Determines the duration of each single activity performed at the table by a member of the waiting staff. Either a static value or a distribution.
    * **base first probability**: Determines the probability that for any activity the waiting staff member has to first go to the base area to collect something (e.g. food, drinks, menus) before performing their activity at the table. A value between 0 and 1.
    * **base after probability**: Determines the probability that for any activity the waiting staff member has to go to the base area after an activity at the table (e.g. returning empty plates) before performing their next activity. A value between 0 and 1.
    * **gaps Dirichlet alpha**: The parameter of the Dirichlet distribution which determines how the gaps between activities are distributed (is 1 by default).
    * **after visit activity count**: The number of activities (of of the total number of activities) a member of staff performs, in relation to a visit, after the customers have left the table (e.g. cleaning the table). Either a static value or a distribution.
    * **base activity duration**: The duration of the activity at the base location (e.g. the duration of picking of food at the kitchen). Either a static value or a distribution.

Optionally, the restaurant scheduler can also take the following input:

* **toilets**
    * **toilets destination IDs**: The ID or list of IDs of destinations that identify the location of the toilet or toilets.
    * **toilet visit probability**: The probability that a customer will go to the toilet during their visit. A value between 0 and 1.
    * **toilet visit duration**: The duration of a toilet visit. Either a static value or a distribution.
* **coat rack**
    * **coat rack destination IDs**: The ID or list of IDs of destinations that identify the location of the coat rack or coat racks.
    * **coat rack visit duration**: The duration of a coat rack visit. Either a static value or a distribution.
* **register**
    * **register destination IDs**: The ID or list of IDs of destinations that identify the location of the register or registers.
    * **register visit duration**: The duration of a register visit. Either a static value or a distribution.
* **use tables only once**: A boolean (true/false) that determines if the tables are used multiple times during a time window or only once.
* **intra-table group buffer**: Determines the minimal time between he moment a group leaves a table and the next group arriving at the table. By default, 60 seconds.

```xml
<restaurantScheduler>
    <sourceID>entrance</sourceID>
    <sinkID>exit</sinkID>
    <demandPattern>((0, 8850, 10), (9150, 18000, 15))</demandPattern>
    <sittingDestinationGroupIDs>(table_1, table_2, table_3, table_4, table_5)</sittingDestinationGroupIDs>
    <visitDuration>
        <distribution distributionType="openturns">
            <distrName>Uniform</distrName>
            <distrArgs>(4000, 13200)</distrArgs>
        </distribution>
    </visitDuration>
    <inGroupEntryDistr>0</inGroupEntryDistr>
    <useEntranceTimeSlot>false</useEntranceTimeSlot>
    <guestPedParamSetDistr>((base, 0.8), (slow, 0.2))</guestPedParamSetDistr>
    <toiletDestinationIDs>(toilet_1, toilet_2)</toiletDestinationIDs>
    <toiletVisitProbability>0.4</toiletVisitProbability>
    <toiletVisitDuration>
        <distribution distributionType="openturns">
            <distrName>WeibullMax</distrName>
            <distrArgs>(297501, 5810.93, 297603)</distrArgs>
            <lowerTrunc>18</lowerTrunc>
            <upperTrunc>438</upperTrunc>
            <min>18</min>
            <max>438</max>
        </distribution>            
    </toiletVisitDuration>
    <staffScheduler>
        <baseAreaID>kitchen</baseAreaID>
        <staffCount>3</staffCount>
        <pedParamSetID>staff</pedParamSetID>
        <activityCount>21</activityCount>
        <neighborhoods>((table_1, table_2), (table_3, table_4, table_5))</neighborhoods>
        <parameters>
            <activityDuration>
                <distribution distributionType="openturns">
                    <distrName>InverseNormal</distrName>
                    <distrArgs>(28.8189, 18.3746)</distrArgs>
                    <lowerTrunc>2</lowerTrunc>
                    <upperTrunc>341</upperTrunc>
                    <min>2</min>
                    <max>341</max>
                </distribution>    
            </activityDuration>
            <baseFirstProbability>0.4</baseFirstProbability>
            <baseAfterProbability>0.3</baseAfterProbability>
            <gapsDirichletAlpha>1.0</gapsDirichletAlpha>
            <afterVisitActivityCount>
                <distribution distributionType="numpy">
                    <distrName>integers</distrName>
                    <distrArgs>(2, 4)</distrArgs>
                    <min>2</min>
                    <max>3</max>
                </distribution>
            </afterVisitActivityCount>
            <baseActivityDuration>5.0</baseActivityDuration>
        </parameters>                
    </staffScheduler>
</restaurantScheduler>
```

#### Output managers

Four output managers are included in NOMAD (see [NOMAD Outputs](NOMAD%20Outputs) for more info):

* Simulation file output manager: Saves the basic simulation data in four different files. See the output section for more information
* In-memory output manager: Saves the basic simulation data in memory
* Connectivity file output manager: Saves the connectivity (for every time step which pedestrian were within a certain distance of each other) between pedestrians to file.
* Dummy output manager: Does not save any data to memory or file. Mainly used for debugging or testing purposes.

In every scenario you can define multiple output managers. You always need to define at least one output manager.

```xml
<outputManagers>
    OUTPUT_MANAGER_1
    OUTPUT_MANAGER_2
    ...
    OUTPUT_MANAGER_N
</outputManagers>
```

##### Simulation file output manager

The simulation file output manager needs the following input:

* **ID**: The ID that identifies this output manager
* **base filename**: The output manager will create the files using this name using the following format {base filename}{seed}_{time stamp}.{extension}

You can also define the **output directory** where the files should be saved. This can be both an absolute path or a relative path (to the path of the  scenario xml). If you do not define this the files will be saved in the default output directory: "\<home>/nomad/output/" (On windows the home directory is "C:\Users\\\<username>\ and on linux "/home/\<username>/).

Additionally, you can define **exportGrid** as true. If **exportGrid** is true, the simulation file output manager will output a "*.gm" file that contains the grid data. Subsequent simulations that use exactly the same infrastucture can use this file (see the readme) to load the grid data and speed up the creation of the model significantly. If this is done and **exportGrid** is still set to true, the output manager will not output a new grid file.

```xml
<SimulationFileOutputManager ID="SimulationFileOutput">
    <baseFilename>simulation_output_file_base_name</baseFilename>
    <outputPd>./output</outputPd>
    <exportGrid>True</exportGrid>
</SimulationFileOutputManager>
```

##### In-memory output manager

The in-memory output manager is identified by its **ID**. Optionally, you can set the **save time step**. This option can be used to save the pedestrian trajectory data only every N time steps. Therefore, this value must be a multiple of the **inIsolationTimeStep** (which is 0.1 seconds by default).

```xml
<InMemoryOutputManager ID="InMemoryOutput">
    <saveTimeStep>0.5</saveTimeStep>
</InMemoryOutputManager>
```

##### Connectivity file output manager

The connectivity file output manager needs the following input:

* **ID**: The ID that identifies this output manager
* **base filename**: The output manager will create the files using this name using the following format {base filename}{seed}_{time stamp}.{extension}

And optionally, you can define the **output directory** and the **connectivity cut-off distance**. The **connectivity cut-off distance** determines the maximum distance between two pedestrian whereby their connection is still saved. If this value is not provided the maximum interaction distance, used to compute the pedestrian interaction force is used. 

```xml
<ConnectivityFileOutputManager ID="ConnectivityFileOutput">
    <baseFilename>simulation_output_file_base_name</baseFilename>
    <connectionCutOffDist>5</connectionCutOffDist>
    <outputPd>./output</outputPd>
</ConnectivityFileOutputManager>
```
