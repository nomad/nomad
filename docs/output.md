- [NOMAD outputs](#nomad-outputs)
  - [Simulation file output manager](#simulation-file-output-manager)
    - [Load data from the simulation data files](#load-data-from-the-simulation-data-files)
  - [In-memory output manager](#in-memory-output-manager)
  - [Connectivity file output manager](#connectivity-file-output-manager)
    - [Load data from the connectivity data files](#load-data-from-the-connectivity-data-files)


# NOMAD outputs
NOMAD can output various types of data. What data is included in the output is determined by the output managers defined in the scenario xml (see [the input documentation](input#output-manager) for more details).

## Simulation file output manager

The simulation file output manager saves the simulation data to four different files. These are:
1) Scenario file (\*.scen): A JSON formatted file containing:
    - The name and label of the simulation
    - The timestep size, the duration and the end time index
    - The names of the three other output files (four if a connectivity file output manager is also used)
    - The geometry
    - The pedestrian parameter sets
2) The pedestrian description file (\*.desc): A semi-colon separated file with a line per pedestrian which contains the following information per pedestrian:
    - ID: The unique ID of the pedestrian
    - Start time: The time at which the pedestrian entered the simulation
    - End time: The time at which the pedestrian exited the simulation
    - Radius: The radius of the pedestrian [m]
    - Group ID: The ID of the group the pedestrian belongs to (if any).
    - Parameter set ID: The ID of the pededstrian parameter set from which this pedestrians gets its parameter values (the parameter sets themselves are defined in the scenario output file).
    - Activity pattern ID: The ID of the activity pattern that the pedestrian followed.
    - The activities whereby per activity the following elements are saved:
        - Start time: The time at which the pedestrian started the activity.
        - End time: The time at which the pedestrian ended the activity.
        - Activity type: A descriptive string.
        - Activity ID: The ID of the activity that is being performed.
        - Destination ID: The ID of the destination at which the activity is being performed.
        - Pedestrian state: The state of the pedestrian captured in an integer value (1 = Ped is moving, 2 = Ped is static [performing an activity whilst not moving], 3 = Ped is temporarily outside of the simulation & 4 Ped is static and not interacting [same as static ped but this ped does not influence the pedestrian interaction force of neighboring pedestrians).
3) The trajectory file (\*.traj): A binary file containing the trajectories of all pedestrians. Uses the struct.unpack and struct.pack functions to read and write data from/to the file. The file contains the following data:
    - The first entry is the simulation time step (a float [d])
    - Each next entry is a single data line for a pedestrian which contains the following fields:
        - time (float [d])
        - ID (int [I])
        - pos_x (float [d])
        - pos_y (float [d])
        - vel_x (float [d])
        - vel_y (float [d])
        - velNorm_x (float [d])
        - velNorm_y (float [d])
        - acc_x (float [d])
        - acc_y (float [d])
        - pathForce_x (float [d])
        - pathForce_y (float [d])
        - pedForce_x (float [d])
        - pedForce_y (float [d])
        - obsForce_x (float [d])
        - obsForce_y (float [d])
        - pedIsolationState (int [I])
        - pedIsolationTime (float [d])
        - obsIsolationState (int [I])
        - obsIsolationTime (float [d])
4) The local route choice floor maps file (\*.lrcm.npz): A numpy binary file with the local route choice fields per destination.

### Load data from the simulation data files

```python
from NOMAD import output_manager

# Load all simulation data 
scenario_data, trajactory_data, description_data, lrcm_data = loadScenario(scenario_filename)

# Load simulation data per file type
from NOMAD.output_manager import SimulationFileOutputManager

scenario_data = SimulationFileOutputManager.readScenarioDataFile(scenario_data_filename)
trajactory_data = SimulationFileOutputManager.readTrajactoryDataFile(trajactory_data_filename)
description_data = SimulationFileOutputManager.readDescriptionDataFile(description_data_filename)
lrcm_data = SimulationFileOutputManager.readLrcmDataFile(lrcm_data_filename)
```

## In-memory output manager

The in-memory output manager saves all simulation data in memory. This is only recommended for small scenarios as using this output manager can cause memory overflow errors in larger scenarios. It saves the pedestrian trajectory data in the pedData dictionary which contains an instance of the PedestrianData class as the value whereby the pedestrian's ID is the key. The PedestrianData saves the trajectory data of each pedestrian using the following fields:
- groupID: The group ID of the pedestrian (can also be none).
- startTime: The start time of the pedestrian (i.e. the time at which the pedestrian entered the simulation for the first time).
- endTime: The end time of the pedestrian (i.e. the time at which the pedestrian was in the simulation for the last time).
- startPos: The starting position fo the pedestrian in the simulation.
- times: A Nx1 array with each time step the pedestrian was active.
- positions: A NX2 array with the position (x,y) of the pedestrian for each corresponding time step saved in times.
- velocities: A NX2 array with the velocity (v_x, v_y) of the pedestrian for each corresponding time step saved in times.
- normalizedVelocities: A NX2 array with the normalized velocity (v_x/||v||, v_y/||v||) of the pedestrian for each corresponding time step saved in times.
- activities: The list of activities performed by the pedestrian. Each entry is an instance of the ActivityLogElement class which in turn has the fields:
    - startTime: The time at which the pedestrian started the activity
    - endTime: The time at which the pedestrian ended the activity
    - activityType: A descriptive string 
    - activityID: The ID of the activity that is being performed
    - destinationID: The ID of the destination at which the activity is being performed
    - pedState: The state of the pedestrian captured in an integer value (1 = Ped is moving, 2 = Ped is static [performing an activity whilst not moving], 3 = Ped is temporarily outside of the simulation & 4 Ped is static and not interacting [same as static ped but this ped does not influence the pedestrian interaction force of neighboring pedestrians).

 If the saveTimeStep is set in the input configuration, the in-memory output manager will only save the positions and velocities with the given time step resolution.

## Connectivity file output manager

The connectivity output manager saves the (spatial) connectivity between pedestrians. That is which pedestrian are within a certain radius from each other. The data is saved in a binary numpy file (\*.conn.npz). 

For each time step and for each couple of pedestrians that is within a certain radius of each other (not blocked by obstacles) the connection is saved. This is d

The file contains a dictionary which contains the connection data for each unique pair of pedestrians that have been within the given distance of each other for at least a single time step. The key is a tuple containing the two IDs of the two pedestrians which is determined as follows:
```python
if ID_1 < ID_2:
    return (ID_1, ID_2)
else:
    return (ID_2, ID_1)
```
### Load data from the connectivity data files

```python
from NOMAD.output_manager import ConnectivityFileOutputManager

connection_data = ConnectivityFileOutputManager.readConnectionsDataFile(connection_filename)

```
