'''
NOMAD - A microscopic pedestrian simulation model
Copyright (C) 2023  Martijn Sparnaaij

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from pathlib import Path

from NOMAD.output_manager import loadScenario, SimulationFileOutputManager

example_path = Path(__file__).parent

# Load all simulation data 
scenario_filename = example_path.joinpath('simple_corr_example.scen')  
scenario_data, trajactory_data, description_data, lrcm_data = loadScenario(scenario_filename)

# Load simulation data per file type
scenario_data = SimulationFileOutputManager.readScenarioDataFile(scenario_filename)

trajactory_filename = scenario_filename.parent.joinpath(scenario_data.trajFile)
trajactory_data, _ = SimulationFileOutputManager.readTrajactoryDataFile(trajactory_filename)

description_filename = scenario_filename.parent.joinpath(scenario_data.descFile)
description_data = SimulationFileOutputManager.readDescriptionDataFile(description_filename)
    
lrcm_filename = scenario_filename.parent.joinpath(scenario_data.lrcmFile)
lrcm_data = SimulationFileOutputManager.readLrcmDataFile(lrcm_filename)