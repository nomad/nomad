'''
NOMAD - A microscopic pedestrian simulation model
Copyright (C) 2023  Martijn Sparnaaij

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from pathlib import Path

from NOMAD.input_manager import WalkableAreaInput, LineObstacleInput, \
    LineDestinationInput, SimpleRandomLineSourceInput, WalkLevelInput, \
    NomadSimulationInput, PedParameterSetInput, SinkInput, \
    ListActivityPatternInput, ConstantDemandPatternInput, DemandManagerInput, \
    SimulationFileOutputManagerInput
from NOMAD.nomad_model import createNOMADmodel


seed = None

walkableAreas = [
    WalkableAreaInput('base', ((0,0),(20,0),(20,8),(0,8)))
    ]

obstacles = [
    LineObstacleInput('bottomWall', False, ((0,0), (20,0))),
    LineObstacleInput('rightWall', False, ((20,0), (20,8))),
    LineObstacleInput('topWall', False, ((20,8), (0,8))),
    LineObstacleInput('leftWall', False, ((0,8), (0,0)))
    ]

destinations = [
    LineDestinationInput('exit_left', ((0.5,0), (0.5,8)), None),
    LineDestinationInput('exit_right', ((19.5,0), (19.5,8)), None)
    ]

sources = [
    SimpleRandomLineSourceInput('entry_left',  ((0.5,0), (0.5,8))),
    SimpleRandomLineSourceInput('entry_right',  ((19.5,0), (19.5,8)))
    ]   

queues = []

walkLevels = [
    WalkLevelInput('base', walkableAreas, obstacles, destinations, sources, queues)
    ]

pedParamKwargs = {}
pedParameterSets = [PedParameterSetInput('base', **pedParamKwargs)]

activities = [
    SinkInput('exit_left', 'exit_left', None, None, None),
    SinkInput('exit_right', 'exit_right', None, None, None),
    ]

activityPatterns = [
    ListActivityPatternInput('walk_to_left', ('exit_left',)),
    ListActivityPatternInput('walk_to_right', ('exit_right',))
    ]

demandPatterns = [
    ConstantDemandPatternInput('demand_r2l', 'entry_right', 'walk_to_left', 'base', 1, 0, 10*60),
    ConstantDemandPatternInput('demand_l2r', 'entry_left', 'walk_to_right', 'base', 1, 0, 10*60)
    ]

demandManager =  DemandManagerInput(activities, activityPatterns, demandPatterns)

outputPd = Path.home().joinpath('nomad').joinpath('output')
outputManagers = [SimulationFileOutputManagerInput('fileOutput', outputPd, 'simple_corr_example')]

scenInput = {'name': 'simulation_example',
             'label': 'Simulation example',
             'duration': 10*60,
             'walkLevels': walkLevels,
             'pedParameterSets': pedParameterSets,
             'demandManager': demandManager,
             'outputManagers': outputManagers
             }


nomadSimulationInput = NomadSimulationInput(**scenInput)
scenario = createNOMADmodel(nomadSimulationInput, seed=seed)
scenario.start()
