'''
NOMAD - A microscopic pedestrian simulation model
Copyright (C) 2023  Martijn Sparnaaij

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from pathlib import Path

from NOMAD.input_manager import StaticValueInput, DistributionInput
from NOMAD.nomad_model import createAndRunNOMADmodel, createNOMADmodelInput


example_path = Path(__file__).parent

# Create and run the scenario defined in the xml-file
bottleneck_scenario_filename = example_path.joinpath('bottleneck_scenario.xml')
scenario = createAndRunNOMADmodel(bottleneck_scenario_filename)


# Create and run the scenario defined in the xml-file using the local route choice manager floor fields saved during a previous simulation
# Note: You can only use 
lrcm_load_file = example_path.joinpath("bottleneck.lrcm.npz")
bottleneck_scenario_filename = example_path.joinpath('bottleneck_scenario.xml')
scenario = createAndRunNOMADmodel(bottleneck_scenario_filename,lrcmLoadFile=lrcm_load_file)


# Create and run the scenario defined in the xml-file using a given seed
bottleneck_scenario_filename = example_path.joinpath('bottleneck_scenario.xml')
scenario = createAndRunNOMADmodel(bottleneck_scenario_filename,lrcmLoadFile=lrcm_load_file,seed=10)

# Create, adapt and run the scenario defined in the xml-file
nomad_scenario_input = createNOMADmodelInput(bottleneck_scenario_filename)
nomad_scenario_input.pedParameterSets[0].preferredSpeed = DistributionInput('numpy', 'normal', (1.0, 0.1), min=0.8, max=1.2)
nomad_scenario_input.pedParameterSets[0].r_0 = StaticValueInput(0.1)
nomad_scenario_input.pedParameterSets[0].a_0 = StaticValueInput(1)
createAndRunNOMADmodel(nomad_scenario_input, lrcmLoadFile=lrcm_load_file)
