# NOMAD: A microscopic pedestrian simulation model

This Python package contains the Python implementation of the microscopic pedestrian simulation model NOMAD. For a detailed description of NOMAD please see https://doi.org/10.4233/uuid:b65e6e12-a85e-4846-9122-0bb9be47a762.

## How to install NOMAD

Clone or download NOMAD from the repository. You can then either install NOMAD as a local package using `pip install -e <path to NOMAD>`. The path should be to the folder that contains the setup.cfg file. Alternatively, you can install the required packages using the requirements.txt file via `pip install -r requirements.txt` (make sure your current directory is set to the folder that contains the requirements.txt file).

NOMAD uses custom c-code to speed up parts of the simulation. However, to make advantage of this code you need to compile it first. To do this, install `gcc`. On Windows, [winlibs](http://winlibs.com/) is recommended (make sure to select the same architecture (x86 or x86_64) as the python your using). On Mac or Linux, `gcc` can be installed using a package manager (i.e. [brew](https://brew.sh/) on Mac, APT on Debian/Ubuntu, etc). To compile, go to `NOMAD/C` and run:
```
gcc -c -Wall -Werror -fpic calcPedForces.c
gcc -shared -o calcPedForces.so calcPedForces.o
gcc -c -Wall -Werror -fpic calcAllPedForces.c
gcc -shared -o calcAllPedForces.so calcAllPedForces.o calcPedForces.o
gcc -c -Wall -Werror -fpic convergeCostMatrix.c
gcc -shared -o convergeCostMatrix.so convergeCostMatrix.o
gcc -c -Wall -Werror -fpic getInteractionData.c
gcc -shared -o getInteractionData.so getInteractionData.o
```
**NOTE on Mac, replace `-shared` by `-dynamiclib`**. 
You should now have several *.o and *.so/*.dylib files in that folder.

## How to use NOMAD

### Command line tool
If NOMAD has been installed as a local package NOMAD will be available as a command line tool. The tool can be used for three things:
- Run a nomad model (see next section for more details)
- View simulation results (see next section for more details)
- Open the examples folder `nomad e` or `nomad examples` 

Type `nomad -h` or `nomad --help` to view the command options.   

If NOMAD has not been installed as a local package you can use the following line to run the command line tool:
`python NOMAD_cmd.py <commands>`

### Setup a simulation

A simulation scenario can be defined in one of two ways:
- Via a Python script
- Via 3 xml-files

The simple_cooridor.py file in the examples folder shows how you can setup (and run) a simulation scenario using a Python script.

Setting up a simulation scenario via xml-files involves creating three xml-files. One xml-file defining the infrastucture of the scenario. one xml-file defining the parameters of one or more pedestrian profiles and a xml-file defining the scenario (which links to the other two files). A detailed description of each of these files can be found in the documentation. The main advantage of using the xml-files is that you can easily create multiple scenarios that use the same infrastructure and/or pedestrian parameter profiles without having to define them multiple times. In the examples folder you can find an example of a scenario defined by three xml-files.

### Run a simulation

A simulation defined by a script is run by running the script. You can also create a script to run a simulation from an xml-file:

```python
from NOMAD.nomad_model import createAndRunNOMADmodel
createAndRunNOMADmodel(scenarioDef, lrcmLoadFile=None, seed=None)
```

where the scenarioDef can be either a scenario (see the simple_corridor.py) or the path to a scenario xml-file (see bottleneck_scenario.xml).

You can also run a xml-file defined scenario via the command line tool. 

`nomad run <xml scenario filename>` or `nomad r <xml scenario filename>`

or
 
`python NOMAD_cmd.py run <xml scenario filename>` or `python NOMAD_cmd.py r <xml scenario filename>`

In both cases three additional arguments can be provided, namely:
 - The lrcm file from which previously computed local route choice fields can be loaded (provided the infrastructure is exactly the same). Use "-l <lrcm filename>"
 - The grid manager file from which a previously computed grid can be loaded (provided the infrastructure is exactly the same). Use "-g <grid filename>"
 - The seed. Use "-s <seed>"
 
So for example,
 
`nomad r <xml scenario filename> -l <lrcm filename> -g <grid filename> -s <seed>`
### Process the results

NOMAD provides different output managers to save different outputs to file or memory. The documentation provides a detailed overview. NOMAD also includes a (very) basic animation viewer to display the results. To view the animation use:

`nomad v <scenario output filename (*.scen)>` or `nomad visualize <scenario output filename (*.scen)>`

or 

`python NOMAD_cmd.py v <scenario output filename (*.scen)>` or `python NOMAD_cmd.py visualize <scenario output filename (*.scen)>`

## Documentation
See the docs folder for more documentation

For the input documentation see [input documentation](/docs/input.md)

For the output documentation see [output documentation](/docs/output.md)

## How to cite NOMAD

Sparnaaij, M., Campanella, M. C., Daamen, W., & Hoogendoorn, S. P. (2023). PyNOMAD: A microscopic pedestrian simulation model. https://doi.org/10.4121/543a7eeb-652a-40fe-8a08-2db5ff9e222b.v1. [Software]

## Acknowledgements

Technische Universiteit Delft hereby disclaims all copyright interest in the
program "NOMAD" A microscopic pedestrian simulation model
written by the Author(s).
Jan Dirk Jansen, Dean of Civil Engineering and Geosciences 
