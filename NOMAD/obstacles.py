""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from math import sqrt

from shapely import LineString, Point, Polygon 
import shapely.ops

from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
from NOMAD.vectors import Vector2D
import numpy as np


class Obstacle(ShapelyGeomClassWrapper):
    '''
    Main obstacle class, functions as a superclass
    '''

    __slots__ = ['_seeThrough', '_prepGeometry']

    def __init__(self, ID, seeThrough=False):
        '''
        Constructor
        '''
        super().__init__(ID)
        self._seeThrough = seeThrough
        
    def prepare(self):
        shapely.prepare(self._geometry)

    def getClosestPointOnObstacle(self, pos):
        closestPoints = shapely.ops.nearest_points(Point(pos), self._geometry)
        return Vector2D(closestPoints[1].coords[0][0], closestPoints[1].coords[0][1])

    def getClosestPointOnObstacleNumpy(self, pos):
        raise NotImplementedError('Implement')

    def getVec2closestPointOnObstacleNumpy(self, point):
        x, y = self.getClosestPointOnObstacleNumpy(point)
        return x - point[0], y - point[1]

    def getVec2closestPointOnObstacle(self, pos):
        return self.getClosestPointOnObstacle(pos) - pos

    def isSeeThrough(self):
        return self._seeThrough

    def isSightLineIntersectingObstacle(self, sightLineCoords):
        if self._seeThrough:
            return False

        sightLine = Obstacle.getSightLine(sightLineCoords)

        return sightLine.crosses(self._geometry)
        return self._prepGeometry.intersects(sightLine)

    def isSightLineIntersectingNonSeeThroughObstacle(self, sightLine):
        return self._geometry.intersects(sightLine)

    @staticmethod
    def getSightLine(sightLineCoords):
        return LineString(sightLineCoords)

    # These two methods ensure that seeThrough is a static variable which can only be set at init
    @property
    def seeThrough(self):
        return self._seeThrough

    @seeThrough.setter
    def seeThrough(self, val):
        raise TypeError('seeThrough can only be set at initialization of an obstacle')

# The obstacles are not direct subclasses of the shapely geometry classes due to problems with functions such as buffer()
class LineLikeObstacle(Obstacle):

    __slots__ = ['_lineSegementArray', '_lineSegmentCount']

    def __init__(self, ID, seeThrough=False):
        '''
        Constructor
        '''
        super().__init__(ID, seeThrough)

    def isSightLineIntersectingObstacle(self, sightLineCoords):
        #superAnswer = super().isSightLineIntersectingObstacle(sightLineCoords)
        #numpyAnswer = self.isSightLineBlockedByObstacleNumpy(sightLineCoords, superAnswer)

        #assert(superAnswer == numpyAnswer), 'Answers do not match superAnswer = {}, numpyAnswer = {}'.format(superAnswer, numpyAnswer)

        #return superAnswer
        return super().isSightLineIntersectingObstacle(sightLineCoords)

    def isSightLineBlockedByObstacleNumpy(self, sightLineCoords, superAnswer):
        # A sight line is only blocked by the obstacle if the line actually crosses the obstacle
        # (i.e. a part of the sight line resides witin the obstacle)

        # https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
        # https://ideone.com/PnPJgb
        def calcCrossProducts():
            norm_C_x = self._lineSegementArray[:,0] - sightLineCoords[0][0]
            norm_C_y = self._lineSegementArray[:,1] - sightLineCoords[0][1]
            vec_AB_x = sightLineCoords[1][0] - sightLineCoords[0][0]
            vec_AB_y = sightLineCoords[1][1] - sightLineCoords[0][1]

            cross_AB_C = norm_C_x*vec_AB_y - norm_C_y*vec_AB_x
            cross_CD_C = norm_C_x*self._lineSegementArray[:,5] - norm_C_y*self._lineSegementArray[:,4]
            cross_AB_CD = vec_AB_x*self._lineSegementArray[:,5] - vec_AB_y*self._lineSegementArray[:,4]

            return cross_AB_C, cross_CD_C, cross_AB_CD

        cross_AB_C, cross_CD_C, cross_AB_CD = calcCrossProducts()

        parInd = cross_AB_CD == 0
        if np.sum(parInd) == self._lineSegmentCount:
            assert(superAnswer == False), 'Answers do not match superAnswer = {}, numpyAnswer = {}'.format(superAnswer, False)
            return False

        checkInd = ~parInd & (cross_AB_C != 0)
        # Overlap is in our case not considered to be obstructing view
        if np.sum(checkInd) < 2:
            assert(superAnswer == False), 'Answers do not match superAnswer = {}, numpyAnswer = {}'.format(superAnswer, False)
            return False

        def calcTandU():
            cross_AB_CD_inv = 1/cross_AB_CD[checkInd]
            t = cross_CD_C[checkInd]*cross_AB_CD_inv
            u = cross_AB_C[checkInd]*cross_AB_CD_inv

            return t, u

        t, u = calcTandU()

        crossInd = (t >= 0) & (t <= 1) & (u >= 0) & (u <= 1)
        assert(superAnswer == (np.sum(crossInd) > 1)), 'Answers do not match superAnswer = {}, numpyAnswer = {}'.format(superAnswer, np.sum(crossInd) > 1)
        return np.sum(crossInd) > 1

    def getClosestPointOnObstacleNumpy(self, point):
        point = np.array(point, dtype=float)
        pointBase = point - self._lineSegementArray[:,:2]
        t0 = (self._lineSegementArray[:,4]*pointBase[:,0] + self._lineSegementArray[:,5]*pointBase[:,1])/self._lineSegementArray[:,6]
        t0[t0<0] = 0
        t0[t0>1] = 1

        closestPoints = self._lineSegementArray[:,:2] + t0[:, np.newaxis]*self._lineSegementArray[:,4:6]
        vecs2closestPoints = closestPoints - point
        dist_2 = vecs2closestPoints[:,0]*vecs2closestPoints[:,0] + vecs2closestPoints[:,1]*vecs2closestPoints[:,1]
        minInd = np.argmin(dist_2)

        return closestPoints[minInd,0], closestPoints[minInd,1]

    def createLineSegmentArray(self):
        if isinstance(self._geometry, LineString):
            coords = self._geometry.coords
        elif isinstance(self._geometry, Polygon):
            coords = self._geometry.exterior.coords
        else:
            raise NotImplementedError('This method is not implemented for an object of class "{}"'.format(self._geometry.__class__))

        lineSegmentCount = len(coords) - 1
        self._lineSegmentCount = lineSegmentCount
        self._lineSegementArray = np.zeros((lineSegmentCount,7), dtype=float)

        for ii in range(lineSegmentCount):
            self._lineSegementArray[ii,0] = coords[ii][0]
            self._lineSegementArray[ii,1] = coords[ii][1]
            self._lineSegementArray[ii,2] = coords[ii+1][0]
            self._lineSegementArray[ii,3] = coords[ii+1][1]
            self._lineSegementArray[ii,4] = coords[ii+1][0] - coords[ii][0]
            self._lineSegementArray[ii,5] = coords[ii+1][1] - coords[ii][1]
            self._lineSegementArray[ii,6] = np.dot(self._lineSegementArray[ii,4:6], self._lineSegementArray[ii,4:6])

class LineObstacle(LineLikeObstacle):
    '''
    A line obstacle using the shapely LineString class to represent the obstacle
    '''

    def __init__(self, ID, lineCoords, seeThrough=False):
        '''
        Constructor
        '''
        super().__init__(ID, seeThrough)
        self._geometry = LineString(lineCoords)
        self.createLineSegmentArray()

    def getPolygonRepresentation(self, buffer):
        leftOffset = self._geometry.parallel_offset(buffer, 'left')
        rightOffset = self._geometry.parallel_offset(buffer, 'right')
        
        return Polygon(np.concatenate((leftOffset.coords._coords, np.flip(rightOffset.coords._coords,0))))

class PolygonObstacle(LineLikeObstacle):
    '''
    A polygon obstacle using the shapely Polygon class to represent the obstacle
    '''

    def __init__(self, ID, polygonCoords, seeThrough=False):
        '''
        Constructor
        '''
        super().__init__(ID, seeThrough)
        self._geometry = Polygon(polygonCoords, None)
        self.createLineSegmentArray()

class CircleObstacle(Obstacle):
    '''
    A circular obstacle using the shapely Point class to represent the obstacle
    '''

    __slots__ = ['_radius', '_center_x', '_center_y']

    def __init__(self, ID, centerCoord, radius, seeThrough=False):
        '''
        Constructor
        '''
        super().__init__(ID, seeThrough)
        self._geometry = Point(centerCoord).buffer(radius)
        self._radius = radius
        self._center_x = centerCoord[0]
        self._center_y = centerCoord[1]

    @property
    def centerCoord(self):
        return (self._center_x, self._center_y)

    @property
    def radius(self):
        return self._radius

    def getClosestPointOnObstacleNumpy(self, point):
        diff_x = point[0] - self._center_x
        diff_y = point[1] - self._center_y

        scalingFactor = self._radius/sqrt(diff_x*diff_x + diff_y*diff_y)
        x = self._center_x + diff_x*scalingFactor
        y = self._center_y + diff_y*scalingFactor

        return x, y

class EllipseObstacle(Obstacle):
    '''
    A elliptical obstacle using the shapely Point class to represent the obstacle
    '''

    __slots__ = ['_center_x', '_center_y', '_semiAxesValues', '_rotationInDegrees']

    def __init__(self, ID, centerCoord, semiAxesValues, rotationInDegrees, seeThrough=False):
        '''
        Constructor
        '''
        super().__init__(ID, seeThrough)
        geom = shapely.affinity.scale(Point(centerCoord).buffer(1), semiAxesValues[0], semiAxesValues[1])

        if abs(rotationInDegrees) < 1e-12:
            rotationInDegrees = 0
        else:
            geom = shapely.affinity.rotate(geom, rotationInDegrees)

        self._geometry = geom

        self._semiAxesValues = semiAxesValues
        self._rotationInDegrees = rotationInDegrees
        self._center_x = centerCoord[0]
        self._center_y = centerCoord[1]

    @property
    def centerCoord(self):
        return (self._center_x, self._center_y)

    @property
    def semiAxesValues(self):
        return self._semiAxesValues

    @property
    def rotationInDegrees(self):
        return self._rotationInDegrees

    def getClosestPointOnObstacleNumpy(self, point):
        # See if a more efficient implementation can be found
        closestPoints = shapely.ops.nearest_points(Point(point), self._geometry)
        return closestPoints[1].coords[0][0], closestPoints[1].coords[0][1]

# - Combined (Using difference, intersection, symmetric_difference, union
