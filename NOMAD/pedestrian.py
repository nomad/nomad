""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import ctypes
from dataclasses import dataclass
import logging
from math import exp, sqrt, nan

from shapely.geometry.linestring import LineString

import NOMAD
from NOMAD.constants import TAU, A_0, R_0, A_1, R_1, KAPPA_0, KAPPA_1, A_W, \
    D_SHY, C_PLUS_0, C_MINUS_0, T_A, RADIUS, NOISE_FACTOR, IN_ISOLATION_STATE, \
    IN_COLLISION_STATE, IN_RANGE_STATE, ATTR_PED_CLASS, ACC_MAIN, ACC_NO_NOISE, \
    ACC_ONLY_PATH, ACC_NO_PED_AND_NOISE, ATTR_PED_OPT_CLASS, \
    ATTR_PED_INT_C_CLASS, ATTR_PED_PED_FORCES_C_CLASS, ACC_RED_DEST, DEST_FORCE_REDUCTION_FACTOR, \
    ACC_NO_PED, MIN_SPEED, ACC_RED_PED, WALKING_ACTIVITY_NAME, \
    PED_STATIC_NON_INTERACTING_STATE, PED_OUTSIDE_OF_SIM_STATE, PED_STATIC_STATE, \
    PED_MOVING_STATE, PED_FORCE_MAIN, PED_FORCE_DUMMY
from NOMAD.grid_manager import UNOBSTRUCTED, OBSTRUCTED
from NOMAD.obstacles import Obstacle
from NOMAD.vectors import Vector2D


logger = logging.getLogger(__name__)

#=============================================================================================================
#=============================================================================================================
'''
The base pedestrian class which uses Vector2D and has limited optimization but high readability
'''
#=============================================================================================================
#=============================================================================================================

class Pedestrian():
    '''
    The pedestrian class of NOMAD
    '''
    __slots__ = ['_ID', 'groupID', 'preferredSpeed', 'pos', 'prevPos', 'nextPos', 'vel', 'nextVel',
                 'acc', 'velNorm', 'gridCell', 'walkLevel', 'state', 
                 TAU, A_0, R_0, A_1, R_1, KAPPA_0, KAPPA_1, A_W, D_SHY, 'r_infl_2', 'ie_f_2',
                 'ie_b_2', C_PLUS_0, C_MINUS_0, T_A, RADIUS, NOISE_FACTOR,
                 'activityPattern', 'curActivity', 'tauInv',
                 'pedIsolationTime', 'pedIsolationState', 'obsIsolationTime',
                 'obsIsolationState', 'closestPed', 'closestPedDist', 'vec2otherPeds',
                 'pathFollowingForce', 'pedForce', 'obsForce', 'noise', 'startTime',
                 'endTime', 'anticipatedPos', 'maxDistBehind', 'maxDistInFront',
                 'obscuredPeds', 'unobscuredPeds', 'calcAccelerationFcn', 'calcPedForceFcn', 'spawningPos', 'activityLog',
                 'parameterSetID', 'desDir', 'maxPedForceSize', 'maxPedForceSize_2']

    def __init__(self, ID, pedParameterSet, activityPattern, calcAccFcn, calcPedForceFcn, groupID=None):
        '''
        Constructor
        '''
        self._ID = ID
        self.groupID = groupID

        self.parameterSetID = pedParameterSet.ID

        self.pos = None
        self.nextPos = None
        self.prevPos = None
        self.vel = None
        self.nextVel = None
        self.acc = None
        self.velNorm = None
        self.gridCell = None
        self.walkLevel = None

        self.state = None

        self.preferredSpeed = pedParameterSet.preferredSpeed
        self.tau = pedParameterSet.tau
        self.tauInv = 1/pedParameterSet.tau
        self.a_0 = pedParameterSet.a_0
        self.r_0 = pedParameterSet.r_0
        self.a_1 = pedParameterSet.a_1
        self.r_1 = pedParameterSet.r_1
        self.kappa_0 = pedParameterSet.kappa_0
        self.kappa_1 = pedParameterSet.kappa_1
        self.a_W = pedParameterSet.a_W
        self.d_shy = pedParameterSet.d_shy
        self.r_infl_2 = pedParameterSet.r_infl**2
        self.ie_f_2 = pedParameterSet.ie_f**2
        self.ie_b_2 = pedParameterSet.ie_b**2
        self.cPlus_0 = pedParameterSet.cPlus_0
        self.cMinus_0 = pedParameterSet.cMinus_0
        self.t_A = pedParameterSet.t_A
        self.radius = pedParameterSet.radius
        self.noiseFactor = pedParameterSet.noiseFactor
        
        self.activityPattern = activityPattern
        self.curActivity = None
        self.activityLog = []

        self.pedIsolationTime = None
        self.pedIsolationState = None
        self.obsIsolationTime = None
        self.obsIsolationState = None

        self.closestPed = None
        self.closestPedDist = -1

        self.desDir = None

        self.pathFollowingForce = None
        self.pedForce = None
        self.obsForce = None
        self.noise = None

        self.startTime = None
        self.endTime = None

        self.anticipatedPos = None
        self.maxDistBehind = -max(pedParameterSet.r_infl,  pedParameterSet.ie_b)
        self.maxDistInFront = max(pedParameterSet.r_infl,  pedParameterSet.ie_f)

        self.obscuredPeds = None
        self.unobscuredPeds = None

        self.calcAccelerationFcn = calcAccFcn
        self.calcPedForceFcn = calcPedForceFcn

        self.spawningPos = None

        self.maxPedForceSize = self.tauInv*(self.preferredSpeed - MIN_SPEED)
        self.maxPedForceSize_2 = self.maxPedForceSize*self.maxPedForceSize

    def initialize(self, pos, walkLevel, currentTime):
        self.spawningPos = pos
        self.state = None
        self.walkLevel = walkLevel
        self.activityLog = []        
        self.setInitialActivity(currentTime)
        self.setInitialPosAndVel(pos)
        self.pedIsolationTime = 0
        self.pedIsolationState = IN_ISOLATION_STATE
        self.obsIsolationTime = 0
        self.obsIsolationState = IN_ISOLATION_STATE
        self.acc = Vector2D(nan, nan)
        self.pathFollowingForce = Vector2D(nan, nan)
        self.pedForce = Vector2D(nan, nan)
        self.obsForce = Vector2D(nan, nan)
        self.noise = Vector2D(nan, nan)
        self.vec2otherPeds = {}
        self.anticipatedPos = None
        self.obscuredPeds = set()
        self.unobscuredPeds = set()
        
        if self.calcAccelerationFcn == self.ACC_CALC_FCNS[ACC_NO_PED]:
            self.pedForce = Vector2D(0,0)

    def updatePosition(self, updatePrev=True):
        if updatePrev:
            self.prevPos = self.pos

        self.pos = self.nextPos
        self.nextPos = None
        self.vel = self.nextVel
        self.velNorm = self.vel.normalized()
        self.nextVel = None
        self.clearOtherPedListsAndSets()

    @staticmethod
    def doForwardEuler(base, timeStep, change):
        return base + timeStep*change

    def setInitialPosAndVel(self, pos):
        self.pos = pos
        self.prevPos = pos
        self.velNorm = self.curActivity.getDesiredDirectionForPedVec(self)
        self.vel = self.velNorm*self.preferredSpeed

    def clearOtherPedListsAndSets(self):
        self.vec2otherPeds = {}
        self.obscuredPeds = set()
        self.unobscuredPeds = set()

    def setInitialActivity(self, currentTime):
        self.curActivity, activityName = self.activityPattern.getInitialActivity(self)
        self.addNewActivityToLog(currentTime, activityName, self.curActivity, 
                                 isWalkingActivity=activityName==WALKING_ACTIVITY_NAME)

    def setNextActivity(self, currentTime):
        self.curActivity = self.activityPattern.getNextActivity(self)
        self.addNewActivityToLog(currentTime, WALKING_ACTIVITY_NAME, self.curActivity, isWalkingActivity=True)

    def addNewActivityToLog(self, currentTime, name, activity, isWalkingActivity=False):
        if len(self.activityLog) > 0:
            self.activityLog[-1].setEndTime(currentTime)

        if isWalkingActivity:
            pedState = PED_MOVING_STATE
        else:  
            pedState = activity.pedStateWhilstPerformingActivity
            
        self.activityLog.append(ActivityLogElement(currentTime, name, activity.ID, 
                                                   activity.destination.ID, pedState))

    def dist2OtherPed2(self, otherPed):
        return self.pos.distance_to_2(otherPed.pos)

    #===========================================================================
    # Main functions for calculating the next acceleration, velocity and position
    #===========================================================================

    def calcNextPosition(self, timeStep, timeStepRatio, parameters, pedForce=None):
        self.calcNextVelocity(timeStep, timeStepRatio, parameters, pedForce)
        self.nextPos = self.doForwardEuler(self.pos, timeStep, self.nextVel)

        for obstacle in self.getObstaclesInInfluenceArea():
            posLine = LineString(((self.pos[0],self.pos[1]), (self.nextPos[0],self.nextPos[1])))
            if posLine.crosses(obstacle.geometry):
                logger.debug('Ped {} cannot move without crossing an obstacle!'.format(self.ID))
                logger.debug('Obst = {}, Pos = ({},{}), NextPos = ({},{})'.format(obstacle.ID, self.pos[0], self.pos[1],
                                                                           self.nextPos[0], self.nextPos[1]))
                self.nextPos = self.pos
                self.nextVel = Vector2D((0.0,0.0))
                self.acc = Vector2D((0.0,0.0))

    def calcNextVelocity(self, timeStep, timeStepRatio, parameters, pedForce=None):
        self.calcAcceleration(timeStepRatio, parameters, pedForce)
        self.nextVel = self.doForwardEuler(self.vel, timeStep, self.acc)
        if self.nextVel.length > parameters.MAX_PED_SPEED_2:
            self.nextVel = self.nextVel*parameters.MAX_PED_SPEED/self.nextVel.length
            logger.debug('Ped {} at pos ({},{}) - Unrealistic high speed!'.format(self.ID, self.pos[0], self.pos[1]))
                
    def calcAcceleration(self, timeStepRatio, parameters, pedForce=None):
        self.calcAccelerationFcn(self, timeStepRatio, pedForce)

        if self.acc.length2 > parameters.MAX_PED_ACCELERATION:
            self.acc = self.acc*parameters.MAX_PED_ACCELERATION/self.acc.length
            logger.debug('Ped {} at pos ({},{}) - Unrealistic high acceleration!'.format(self.ID, self.pos[0], self.pos[1]))

    def calcAccelerationMain(self, timeStepRatio, pedForce=None):
        self.pathFollowingForce = self.calcPathFollowingForce()
        self.obsForce = self.calcObstacleForces()
        if pedForce is None:
            self.pedForce = self.calcPedForceFcn(self)
        self.noise = self.calcNoise(timeStepRatio)

        self.acc = self.pathFollowingForce + self.obsForce + self.pedForce + self.noise

    def calcAccelerationNoPed(self, timeStepRatio, pedForce=None):
        self.pathFollowingForce = DEST_FORCE_REDUCTION_FACTOR*self.calcPathFollowingForce()
        self.obsForce = self.calcObstacleForces()
        self.noise = self.calcNoise(timeStepRatio)

        self.acc = self.pathFollowingForce + self.obsForce + self.noise

    def calcAccelerationReducedPed(self, timeStepRatio, pedForce=None):
        self.pathFollowingForce = self.calcPathFollowingForce()
        self.obsForce = self.calcObstacleForces()
        self.pedForce = self.calcPedForceFcn(self)
        self.noise = self.calcNoise(timeStepRatio)

        if self.pedForce.length2 > self.maxPedForceSize_2:            
            reductionFactor = self.maxPedForceSize/self.pedForce.length
            self.pedForce = self.pedForce*reductionFactor
   
        self.acc = self.pathFollowingForce + self.obsForce + self.pedForce + self.noise
    
    def calcAccelerationRedDest(self, timeStepRatio, pedForce=None):
        self.pathFollowingForce = DEST_FORCE_REDUCTION_FACTOR*self.calcPathFollowingForce()
        self.obsForce = self.calcObstacleForces()
        self.pedForce = self.calcPedForceFcn(self)
        self.noise = self.calcNoise(timeStepRatio)

        self.acc = self.pathFollowingForce + self.obsForce + self.pedForce + self.noise

    def calcNoise(self, timeStepRatio):
        std = self.noiseFactor*(0.0056*timeStepRatio*timeStepRatio - 0.1147*timeStepRatio + 1.1091)
        return Vector2D(NOMAD.NOMAD_RNG.normal(0, std), NOMAD.NOMAD_RNG.normal(0, std))

    def calcPathFollowingForce(self):
        try:
            self.desDir = self.curActivity.getDesiredDirectionForPedVec(self)
            return self.tauInv*(self.desDir*self.preferredSpeed - self.vel)
        except:
            logger.debug('Ped {} with pos ({},{}) is outside of the local route choice managers bounds!'.format(self.ID, self.pos[0], self.pos[1]))
            closestPoint = self.curActivity.getClosestPoint(self.pos_x, self.pos_y)
            pos2closestPoint = Vector2D(closestPoint[0].xy[0][0], closestPoint[0].xy[1][0]) - self.pos
            self.desDir = pos2closestPoint.getNormalized()
            return self.tauInv*(self.desDir*self.preferredSpeed - self.vel)

    #===========================================================================
    # Obstacle forces
    #===========================================================================

    def calcObstacleForces(self):
        obsForces = Vector2D(0.0,0.0)

        for obstacle in self.getObstaclesInInfluenceArea():
            vec2closestPoint = obstacle.getVec2closestPointOnObstacle(self.pos)
            dist2obstacle = vec2closestPoint.length - self.radius
            if dist2obstacle > self.d_shy:
                continue
            elif dist2obstacle < 0:
                obsForces = self.addObstaclePhysicalForce(obsForces, vec2closestPoint, dist2obstacle)
            else:
                obsForces += self.calObstaclePsychologicalForce(vec2closestPoint, dist2obstacle)

        return obsForces

    def addObstaclePhysicalForce(self, obsForces, vec2closestPoint, dist2obstacle):
        # dist2obstacle is always negative in this case, hence if the vectors are multiplied by this distance
        # the vector will always point in the opposite direction
        normVec = vec2closestPoint.getNormalized()
        # Force in the normal direction
        obsForces += self.calcObstaclePhysicalForceNormal(normVec, dist2obstacle)
        # Force in the tangential direction
        obsForces += self.calcObstaclePhysicalForceTangential(normVec, dist2obstacle)

        return obsForces

    def calcObstaclePhysicalForceNormal(self, normVec, dist2obstacle):
        return normVec*self.kappa_0*dist2obstacle

    def calcObstaclePhysicalForceTangential(self, normVec, dist2obstacle):
        tangVec = normVec.getTangential()
        return tangVec*self.kappa_1*dist2obstacle*self.vel.dot(tangVec)

    def calObstaclePsychologicalForce(self, vec2closestPoint, dist2obstacle):
        if dist2obstacle <= self.d_shy/2:
            return -vec2closestPoint.getNormalized()*self.a_W*1.0
        return -vec2closestPoint.getNormalized()*self.a_W*2*(1 - dist2obstacle/self.d_shy)

    def getObstaclesInInfluenceArea(self):
        return self.gridCell.obstaclesNear

    #===========================================================================
    # Pedestrian forces
    #===========================================================================

    def updatePedForce(self, pedForce_x, pedForce_y):
        self.pedForces = Vector2D(pedForce_x,pedForce_y)
        
    def dummyPedestrianForces(self):
        return self.pedForces

    def calcPedestrianForces(self):
        pedForces = Vector2D(0,0)

        for otherPedInteractionData in self.getPedsInInfluenceArea():
            if otherPedInteractionData.isTouching:
                pedForces = self.addPedestrianPhysicalForce(pedForces, otherPedInteractionData)
            elif otherPedInteractionData.isStatic:
                pedForces = self.addPedestrianPhysicalForce(pedForces, otherPedInteractionData)
            else:
                pedForces = self.addPedestrianPsychologicalForce(pedForces, otherPedInteractionData)

        return pedForces

    def addPedestrianPhysicalForce(self, pedForces, intData):
        # dist2otherPed is always negative in this case, hence if the vectors are multiplied by this distance
        # the vector will always point in the opposite direction

        # Force in the normal direction
        pedForces += self.calcPedestrianPhysicalForceNormal(intData)
        # Force in the tangential direction
        pedForces += self.calcPedestrianPhysicalForceTangential(intData)

        return pedForces

    def calcPedestrianPhysicalForceNormal(self, intData):
        return intData.dir2otherPedNorm*self.kappa_0*intData.dist2otherPed

    def calcPedestrianPhysicalForceTangential(self, intData):
        tangVec = intData.dir2otherPedNorm.getTangential()
        return tangVec*self.kappa_1*-intData.dist2otherPed*tangVec.dot(intData.otherPed.vel - self.vel)
    
    def addStaticPedestrianPsychologicalForce(self, pedForces, intData):
        dist2otherPed = intData.dist2otherPed - (self.radius + intData.otherPed.radius)
        return pedForces + -intData.dir2otherPedNorm*self.a_0*exp(-dist2otherPed/self.r_0)

    def addPedestrianPsychologicalForce(self, pedForces, intData):
        dist2otherPed = self.calcDist2otherPed(intData)

        # Normal interaction
        pedForces += self.calcPedestrianPsychologicalForceNormal(intData, dist2otherPed)
        if intData.isOpposing:
            # Lateral interaction
            pedForces += self.calcPedestrianPsychologicalForceLateral(intData, dist2otherPed)
           
        return pedForces

    def calcDist2otherPed(self, intData):
        if intData.isInFront:
            c_factor = self.cPlus_0 # Is already squared
        else:
            c_factor = self.cMinus_0 # Is already squared

        return sqrt(c_factor*intData.dir2otherPed_v.length2 + intData.dir2otherPed_w.length2)

    def calcPedestrianPsychologicalForceNormal(self, intData, dist2otherPed):
        dist2otherPed -= (self.radius + intData.otherPed.radius)
        return -intData.dir2otherPedNorm*self.a_0*exp(-dist2otherPed/self.r_0)

    def calcPedestrianPsychologicalForceLateral(self, intData, dist2otherPed):
        return -intData.dir2otherPed_w.getNormalized()*self.a_1*exp(-(dist2otherPed*intData.dir2otherPed_w.length)/self.r_1)

    def getPedsInInfluenceArea(self):
        return getPedsInteractionData(self.gridCell, self)

    #===========================================================================
    # Isolation states
    #===========================================================================

    def updateIsolationState(self, currentTime, timeStep, parameters, forceUpdate=False):
        if forceUpdate or self.pedIsolationTime < currentTime:
            self.updatePedIsolationTime(self, currentTime, timeStep, parameters)

        if forceUpdate or self.obsIsolationTime < currentTime:
            self.updateObsIsolationTime(self, currentTime, timeStep, parameters)

    @staticmethod
    def updatePedIsolationTime(ped, currentTime, timeStep, parameters):
        closestPedDist, otherPed = ped.gridCell.getClosestPedDist(ped, parameters.MAX_PED_ISOLATION_OFFSET)
        if otherPed is None:
            closestPedDist = 2*(parameters.MAX_PED_EXTENT + 2*parameters.MAX_PED_RADIUS)
            otherPedID = None
            otherPedIsStatic = False
        else:
            otherPedID = otherPed.ID
            otherPedIsStatic = otherPed.isStatic
        ped.closestPed = otherPedID
        ped.closestPedDist = closestPedDist

        isolationInterval, isolationState = Pedestrian.getIsolationIntervalAndState(closestPedDist, timeStep, parameters, otherPedIsStatic)

        ped.pedIsolationState = isolationState
        ped.pedIsolationTime = currentTime + isolationInterval

    @staticmethod
    def getIsolationIntervalAndState(dist, timeStep, parameters, otherPedIsStatic):
        if otherPedIsStatic:
            maxApproachingSpeed = parameters.MAX_PED_PREF_SPEED
        else:
            maxApproachingSpeed = parameters.MAX_APPROACHING_SPEED
        
        isolationInterval = (dist - parameters.MAX_PED_EXTENT - 2*parameters.MAX_PED_RADIUS)/maxApproachingSpeed
        
        if isolationInterval <= timeStep:
            isolationInterval = (dist - 2*parameters.MAX_PED_RADIUS)/maxApproachingSpeed
            if isolationInterval <= timeStep:
                isolationState = IN_COLLISION_STATE
                isolationInterval = timeStep*2.1
            else:
                isolationState = IN_RANGE_STATE
        else:
            isolationState = IN_ISOLATION_STATE

        return isolationInterval, isolationState

    @staticmethod
    def updateObsIsolationTime(ped, currentTime, timeStep, parameters):
        closestObsDist = ped.gridCell.getClosestObsDist(ped)
        isolationInterval = (closestObsDist - parameters.MAX_OBS_EXTENT - parameters.MAX_PED_RADIUS)/parameters.MAX_PED_PREF_SPEED
        if isolationInterval <= timeStep:
            isolationInterval = (closestObsDist - parameters.MAX_PED_RADIUS)/parameters.MAX_PED_PREF_SPEED
            if isolationInterval <= timeStep:
                ped.obsIsolationState = IN_COLLISION_STATE
                isolationInterval = timeStep*2.1
            else:
                ped.obsIsolationState = IN_RANGE_STATE
        else:
            ped.obsIsolationState = IN_ISOLATION_STATE

        ped.obsIsolationTime = currentTime + isolationInterval

    def broadcastIsolation(self, currentTime, timeStep, parameters):
        Pedestrian.broadcastPedIsolation(self, currentTime, timeStep, parameters)

    @staticmethod
    def broadcastPedIsolation(ped, currentTime, timeStep, parameters):
        # If broadcast get all peds in InRange range
        # For all peds check if their pedIsolationTime needs to be updated (pedIsolationTime>currentTime+isolationInterval)
        peds = ped.gridCell.getPedsInRange(ped)

        ped.pedIsolationTime = currentTime + parameters.COLLISION_INTERVAL_PED

        for otherPed in peds:
            dist = Vector2D.distance(ped.pos, otherPed.pos)
            isolationInterval, isolationState = Pedestrian.getIsolationIntervalAndState(dist, timeStep, parameters, otherPed.isStatic)
            isolationTime = currentTime + isolationInterval
            if isolationTime < ped.pedIsolationTime:
                ped.pedIsolationTime = isolationTime
                ped.pedIsolationState = isolationState

            if isolationTime < otherPed.pedIsolationTime:
                otherPed.pedIsolationTime = isolationTime
                otherPed.pedIsolationState = isolationState

    #===============================================================================
    # Properties
    #===============================================================================

    def __hash__(self):
        return self._ID.__hash__()

    @property
    def ID(self):
        return self._ID

    @property
    def activityID(self):
        return self.curActivity.ID

    @property
    def destinationID(self):
        return self.curActivity.getDestinationID(self)

    @property
    def pos_x(self):
        return self.pos.x

    @property
    def pos_y(self):
        return self.pos.y

    @property
    def vel_x(self):
        return self.vel.x

    @property
    def vel_y(self):
        return self.vel.y

    @property
    def velNorm_x(self):
        return self.velNorm.x

    @property
    def velNorm_y(self):
        return self.velNorm.y

    @property
    def isStatic(self):
        return self.state == PED_STATIC_STATE
    
    @staticmethod
    def getLabel():
        return ATTR_PED_CLASS

    def __setattr__(self, name, value):
        if name == 'pos':
            if value is not None and not isinstance(value, Vector2D):
                self.pos = Vector2D(value)
        super().__setattr__(name, value)

    #===========================================================================
    # Alternative acceleration functions (primarily for testing purposes)
    #===========================================================================

    def calcAccelerationNoNoise(self, timeStepRatio): #@Unusedvariable
        self.pathFollowingForce = self.calcPathFollowingForce()
        self.pedForce = self.calcPedForceFcn(self)
        self.obsForce = self.calcObstacleForces()

        self.acc = self.pathFollowingForce + self.pedForce + self.obsForce

    def calcAccelerationOnlyPath(self, timeStepRatio): #@Unusedvariable
        self.pathFollowingForce = self.calcPathFollowingForce()

        self.acc = self.pathFollowingForce

    def calcAccelerationNoPedAndNoise(self, timeStepRatio): #@Unusedvariable
        self.pathFollowingForce = self.calcPathFollowingForce()
        self.obsForce = self.calcObstacleForces()

        self.acc = self.pathFollowingForce + self.obsForce

    ACC_CALC_FCNS = {
        ACC_MAIN:calcAccelerationMain,
        ACC_NO_NOISE:calcAccelerationNoNoise,
        ACC_ONLY_PATH:calcAccelerationOnlyPath,
        ACC_NO_PED_AND_NOISE:calcAccelerationNoPedAndNoise,
        ACC_RED_DEST:calcAccelerationRedDest,
        ACC_NO_PED:calcAccelerationNoPed
        }
    
    PED_FORCE_CALC_FCNS = {
        PED_FORCE_MAIN:calcPedestrianForces,
        PED_FORCE_DUMMY:dummyPedestrianForces
        }


#===========================================================================
# Pedestrian interaction data classes
#===========================================================================

class PedInteractionData():
    '''
    A data container class, which contains all information necessary to compute the interaction forces caused by this pedestrian
    '''

    __slots__ = ['dir2otherPedNorm', 'otherPed', 'isOpposing', 'isTouching', 'isInFront']

    def __init__(self, dir2otherPedNorm, otherPed, isTouching=False, isOpposing=False, isInFront=False, isStatic=False):
        '''
        Constructor
        '''
        self.dir2otherPedNorm = dir2otherPedNorm
        self.otherPed = otherPed
        self.isTouching = isTouching
        self.isOpposing = isOpposing
        self.isInFront = isInFront
        self.isStatic = isStatic

class PedInteractionDataTouching(PedInteractionData):

    __slots__ = ['dist2otherPed']

    def __init__(self, dir2otherPedNorm, otherPed, dist2otherPed):
        super().__init__(dir2otherPedNorm, otherPed, isTouching=True, isOpposing=False, isInFront=None)
        self.dist2otherPed = dist2otherPed

class PedInteractionDataStatic(PedInteractionData):

    __slots__ = ['dist2otherPed']

    def __init__(self, dir2otherPedNorm, otherPed, dist2otherPed):
        super().__init__(dir2otherPedNorm, otherPed, isTouching=True, isOpposing=False, isInFront=None, isStatic=True)
        self.dist2otherPed = dist2otherPed

class PedInteractionDataNonOpposing(PedInteractionData):

    __slots__ = ['dir2otherPed_v', 'dir2otherPed_w']

    def __init__(self, dir2otherPedNorm, otherPed, dir2otherPed_v, dir2otherPed_w, isInFront):
        super().__init__(dir2otherPedNorm, otherPed, isTouching=False, isOpposing=False, isInFront=isInFront)
        self.dir2otherPed_v = dir2otherPed_v
        self.dir2otherPed_w = dir2otherPed_w

class PedInteractionDataOpposing(PedInteractionData):

    __slots__ = ['dir2otherPed_v', 'dir2otherPed_w']

    def __init__(self, dir2otherPedNorm, otherPed, dir2otherPed_v, dir2otherPed_w):
        super().__init__(dir2otherPedNorm, otherPed, isTouching=False, isOpposing=True, isInFront=True)
        self.dir2otherPed_v = dir2otherPed_v
        self.dir2otherPed_w = dir2otherPed_w

#===========================================================================
# Pedestrian interaction data functions
#===========================================================================

def getPedsInteractionData(gridCell, ped):
    ped.anticipatedPos = ped.pos + ped.vel*ped.t_A
    otherPedsInCell = gridCell.getOtherPedsInCell(ped)
    pedInteractionData = getPedsInInfluenceArea(otherPedsInCell, ped, gridCell.containsObstacle, gridCell.sightBlockingObstaclesInCell)
    pedsInNeighboringCells = gridCell.getPedsInNeighboringCells()
    pedInteractionData += getPedsInInfluenceArea(pedsInNeighboringCells[UNOBSTRUCTED], ped, False)
    pedInteractionData += getPedsInInfluenceArea(pedsInNeighboringCells[OBSTRUCTED], ped, True, gridCell.obstaclesObstrNeighbors)
    return pedInteractionData

def getPedsInInfluenceArea(pedsInNeighboringCells, ped, doObstacleCheck, obstacles=[]):
    pedInteractionData = []
    for otherPed in pedsInNeighboringCells:
        if otherPed.state in (PED_STATIC_NON_INTERACTING_STATE, PED_OUTSIDE_OF_SIM_STATE):
            continue
        if doObstacleCheck and isObscuredByObstacle(ped, otherPed, obstacles):
            continue

        vec2otherPed, dist2otherPed = calcVec2otherPed(ped, otherPed)

        if vec2otherPed.length == 0:
            continue

        if dist2otherPed < 0:
            # The pedestrian are touching each other so are definitely in the influence area
            pedInteractionData.append(PedInteractionDataTouching(vec2otherPed.getNormalized(), otherPed, dist2otherPed))
            continue

        #if otherPed.state == PED_STATIC_STATE:
        #    pedInteractionData.append(PedInteractionDataStatic(vec2otherPed.getNormalized(), otherPed, dist2otherPed))
        #    continue 

        # Local coordinate system where v is in the direction of the velocity of the ped and w is orthogonal to v
        dist_v = calcDistInVelDirection(ped, vec2otherPed)

        # Anticipation behavior
        if dist_v > 0:
            vec2otherPedAnt = calcAnticipatedPos(otherPed, ped.t_A) - ped.anticipatedPos
            distAnt_v = vec2otherPedAnt.dot(ped.velNorm)
            if distAnt_v > 0:
                # otherPed is still in front
                vec2otherPed = vec2otherPedAnt
                dist2otherPed = vec2otherPed.length - ped.radius - otherPed.radius
                dist_v = distAnt_v

            if vec2otherPed.length == 0:
                continue


        if dist_v < ped.maxDistBehind or dist_v > ped.maxDistInFront:
            continue

        dir2otherPed_v, dir2otherPed_w = calcLocalVecs(ped, vec2otherPed, dist_v)

        if not isInInfluenceArea(dist_v, dir2otherPed_v.length2, dir2otherPed_w.length2, ped.ie_f_2, ped.ie_b_2, ped.r_infl_2):
            continue

        if dist_v > 0 and otherPed.vel.dot(dir2otherPed_v.getNormalized()) < 0:
            pedInteractionData.append(PedInteractionDataOpposing(vec2otherPed.getNormalized(), otherPed, dir2otherPed_v, dir2otherPed_w))
        else:
            pedInteractionData.append(PedInteractionDataNonOpposing(vec2otherPed.getNormalized(), otherPed, dir2otherPed_v, dir2otherPed_w, dist_v > 0))

    return pedInteractionData

def calcAnticipatedPos(ped, t_A):
    return Vector2D(ped.pos.x + ped.vel.x*t_A, ped.pos.y + ped.vel.y*t_A)

def calcVec2otherPed(ped, otherPed):
    if otherPed in ped.vec2otherPeds:
        vec2otherPed = ped.vec2otherPeds[otherPed][0]
        dist2otherPed = ped.vec2otherPeds[otherPed][1]
    else:
        vec2otherPed = otherPed.pos - ped.pos
        dist2otherPed = vec2otherPed.length - ped.radius - otherPed.radius
        otherPed.vec2otherPeds[ped] = (-vec2otherPed, dist2otherPed)
    return vec2otherPed, dist2otherPed

def calcDistInVelDirection(ped, vec2otherPed):
    # Local coordinate system where v is in the direction of the velocity of the ped and w is orthogonal to v
    dist_v = vec2otherPed.dot(ped.velNorm)
    return dist_v

def calcLocalVecs(ped, vec2otherPed, dist_v):
    dir2otherPed_v = ped.velNorm*dist_v
    dir2otherPed_w = vec2otherPed - dir2otherPed_v
    return dir2otherPed_v, dir2otherPed_w

def isObscuredByObstacle(ped, otherPed, obstacles):
    if otherPed in ped.obscuredPeds:
        return True
    elif otherPed in ped.unobscuredPeds:
        return False

    for obstacle in obstacles:
        if obstacle.isSightLineIntersectingObstacle(((ped.pos_x, ped.pos_y), (otherPed.pos_x, otherPed.pos_y))):
            otherPed.obscuredPeds.add(ped)
            return True

    otherPed.unobscuredPeds.add(ped)
    return False

def isObscuredByNonSeeThroughObstacle(ped, otherPed, obstacles):
    if otherPed in ped.obscuredPeds:
        return True
    elif otherPed in ped.unobscuredPeds:
        return False

    sightLine = Obstacle.getSightLine(((ped.pos_x, ped.pos_y), (otherPed.pos_x, otherPed.pos_y)))

    for obstacle in obstacles:
        if obstacle.isSightLineIntersectingNonSeeThroughObstacle(sightLine):
            otherPed.obscuredPeds.add(ped)
            return True

    otherPed.unobscuredPeds.add(ped)
    return False

def removeObscuredPeds(peds, obstacles, ped, onlyNonSeeThroughObstacles=False):
    if onlyNonSeeThroughObstacles:
        fcn = isObscuredByNonSeeThroughObstacle
    else:
        fcn = isObscuredByObstacle
    
    return [otherPed for otherPed in peds if not fcn(ped, otherPed, obstacles)]

def isInInfluenceArea(dist_v, xDist_2, yDist_2, ie_f_2, ie_b_2, r_infl_2):
    if dist_v > 0:
        # In front
        return (xDist_2/ie_f_2 + yDist_2/r_infl_2) <= 1
    else:
        return (xDist_2/ie_b_2 + yDist_2/r_infl_2) <= 1

#=============================================================================================================
#=============================================================================================================
'''
The optimized version of the base pedestrian class which limits the use of Vector2D
'''
#=============================================================================================================
#=============================================================================================================

class PedestrianOpt(Pedestrian):

    __slots__ = ['pos_x', 'pos_y', 'vel_x', 'vel_y', 'acc_x', 'acc_y', 'pathFollowingForce_x',
                'pathFollowingForce_y', 'pedForce_x', 'pedForce_y', 'obsForce_x', 'obsForce_y',
                'noise_x', 'noise_y', 'anticipatedPos_x', 'anticipatedPos_y', 'nextPos_x', 'nextPos_y',
                'prevPos_x', 'prevPos_y','nextVel_x', 'nextVel_y', 'velNorm_x', 'velNorm_y',
                'desDir_x', 'desDir_y']

    def __init__(self, ID, pedParameterSet, activityPattern, calcAccFcn, calcPedForceFcn, groupID=None):
        super().__init__(ID, pedParameterSet, activityPattern, calcAccFcn, calcPedForceFcn, groupID)
        self.prevPos_x = None
        self.prevPos_y = None
        
    def initialize(self, pos, walkLevel, currentTime):
        super().initialize(pos, walkLevel, currentTime)
        self.acc_x = nan
        self.acc_y = nan
        self.pathFollowingForce_x = nan
        self.pathFollowingForce_y = nan
        self.pedForce_x = nan
        self.pedForce_y = nan
        self.obsForce_x = nan
        self.obsForce_y = nan
        self.noise_x = nan
        self.noise_y = nan
        
        if self.calcAccelerationFcn == self.ACC_CALC_FCNS[ACC_NO_PED]:
            self.pedForce_x = 0
            self.pedForce_y = 0

    def setInitialPosAndVel(self, pos):
        self.pos_x = pos.x
        self.pos_y = pos.y
        
        self.prevPos_x = pos.x
        self.prevPos_y = pos.y
        
        self.velNorm_x, self.velNorm_y = self.curActivity.getDesiredDirectionForPed(self)

        self.vel_x = self.velNorm_x*self.preferredSpeed
        self.vel_y = self.velNorm_y*self.preferredSpeed

    def updatePosition(self, updatePrev=True):
        if updatePrev:
            self.prevPos_x = self.pos_x
            self.prevPos_y = self.pos_y

        self.pos_x = self.nextPos_x
        self.pos_y = self.nextPos_y
        self.nextPos_x = None
        self.nextPos_y = None

        self.vel_x = self.nextVel_x
        self.vel_y = self.nextVel_y
        self.nextVel_x = None
        self.nextVel_y = None

        self.velNorm_x, self.velNorm_y = PedestrianOpt.getNormalizedVelocity(self.vel_x, self.vel_y)

        self.clearOtherPedListsAndSets()

    @staticmethod
    def doForwardEuler(base_x, base_y, timeStep, change_x, change_y):
        return base_x + timeStep*change_x, base_y + timeStep*change_y

    def otherPedHasPassed(self, otherPed):
        posDiff_x = otherPed.pos_x - self.pos_x
        posDiff_y = otherPed.pos_y - self.pos_y

        vecLength = sqrt(posDiff_x*posDiff_x + posDiff_y*posDiff_y)

        #angle = arccos((self.desDir_x*posDiff_x + self.desDir_y*posDiff_y)/vecLength)
        #logger.debug('Angle = {}, vecLength = {}'.format(angle/pi*180, vecLength))
        return posDiff_x/vecLength*self.desDir_x + posDiff_y/vecLength*self.desDir_y < (1/sqrt(2))

    def dist2OtherPed2(self, otherPed):
        posDiff_x = otherPed.pos_x - self.pos_x
        posDiff_y = otherPed.pos_y - self.pos_y

        return posDiff_x*posDiff_x + posDiff_y*posDiff_y

    #===========================================================================
    # Main functions for calculating the next acceleration, velocity and position
    #===========================================================================

    def calcNextPosition(self, timeStep, timeStepRatio, parameters):
        self.calcNextVelocity(timeStep, timeStepRatio, parameters)
        #self.calcNextVelocityLimited(timeStep, timeStepRatio, parameters)
        self.nextPos_x, self.nextPos_y = self.doForwardEuler(self.pos_x, self.pos_y, timeStep, self.nextVel_x, self.nextVel_y)

        for obstacle in self.getObstaclesInInfluenceArea():
            posLine = LineString(((self.pos_x, self.pos_y), (self.nextPos_x, self.nextPos_y)))
            if posLine.crosses(obstacle.geometry):
                logger.debug('Ped {} cannot move without crossing an obstacle!'.format(self.ID))
                logger.debug('Obst = {}, Pos = ({},{}), NextPos = ({},{})'.format(obstacle.ID, self.pos_x, self.pos_y,
                                                                           self.nextPos_x, self.nextPos_y))
                self.nextPos_x = self.pos_x
                self.nextPos_y = self.pos_y
                self.nextVel_x = 0
                self.nextVel_y = 0
                self.acc_x = 0
                self.acc_y = 0

    def calcNextVelocity(self, timeStep, timeStepRatio, parameters):
        self.calcAcceleration(timeStepRatio, parameters)
        self.nextVel_x, self.nextVel_y = self.doForwardEuler(self.vel_x, self.vel_y, timeStep, self.acc_x, self.acc_y)

        speed_2 = self.nextVel_x*self.nextVel_x + self.nextVel_y*self.nextVel_y
        if speed_2 > parameters.MAX_PED_SPEED_2:
            scaleFactor = parameters.MAX_PED_SPEED/sqrt(speed_2)
            self.nextVel_x *= scaleFactor
            self.nextVel_y *= scaleFactor
            logger.debug('Ped {} at pos ({},{}) - Unrealistic high speed!'.format(self.ID, self.pos_x, self.pos_y))

    def calcAcceleration(self, timeStepRatio, parameters):
        self.calcAccelerationFcn(self, timeStepRatio)

        accLength_2 = self.acc_x*self.acc_x + self.acc_y*self.acc_y
        if accLength_2 > parameters.MAX_PED_ACCELERATION:
            scaleFactor = parameters.MAX_PED_ACCELERATION/sqrt(accLength_2)
            self.acc_x *= scaleFactor
            self.acc_y *= scaleFactor
            #logger.debug('Ped {} at pos ({},{}) - Unrealistic high acceleration!'.format(self.ID, self.pos_x, self.pos_y))

    def calcAccelerationMain(self, timeStepRatio):
        self.pathFollowingForce_x, self.pathFollowingForce_y = self.calcPathFollowingForce()
        self.obsForce_x, self.obsForce_y = self.calcObstacleForces()
        self.pedForce_x, self.pedForce_y = self.calcPedForceFcn(self)
        
        self.noise_x, self.noise_y = self.calcNoise(timeStepRatio)

        self.acc_x = self.pathFollowingForce_x + self.obsForce_x + self.pedForce_x + self.noise_x
        self.acc_y = self.pathFollowingForce_y + self.obsForce_y + self.pedForce_y + self.noise_y

    def calcAccelerationNoPed(self, timeStepRatio):
        self.pathFollowingForce_x, self.pathFollowingForce_y = self.calcPathFollowingForce()
        self.obsForce_x, self.obsForce_y = self.calcObstacleForces()
        self.noise_x, self.noise_y = self.calcNoise(timeStepRatio)

        self.acc_x = self.pathFollowingForce_x + self.obsForce_x + self.noise_x
        self.acc_y = self.pathFollowingForce_y + self.obsForce_y + self.noise_y

    def calcAccelerationReducedPed(self, timeStepRatio):
        self.pathFollowingForce_x, self.pathFollowingForce_y = self.calcPathFollowingForce()
        self.obsForce_x, self.obsForce_y = self.calcObstacleForces()
        self.pedForce_x, self.pedForce_y = self.calcPedForceFcn(self)
        self.noise_x, self.noise_y = self.calcNoise(timeStepRatio)

        pedForceLength_2 = self.pedForce_x*self.pedForce_x + self.pedForce_y*self.pedForce_y
        if pedForceLength_2 > self.maxPedForceSize_2:            
            reductionFactor = self.maxPedForceSize/sqrt(pedForceLength_2)
            self.pedForce_x = self.pedForce_x*reductionFactor
            self.pedForce_y = self.pedForce_y*reductionFactor

        self.acc_x = self.pathFollowingForce_x + self.obsForce_x + self.pedForce_x + self.noise_x
        self.acc_y = self.pathFollowingForce_y + self.obsForce_y + self.pedForce_y + self.noise_y

    def calcAccelerationRedDest(self, timeStepRatio):
        self.pathFollowingForce_x, self.pathFollowingForce_y = self.calcPathFollowingForce()
        self.pathFollowingForce_x = self.pathFollowingForce_x*DEST_FORCE_REDUCTION_FACTOR
        self.pathFollowingForce_y = self.pathFollowingForce_y*DEST_FORCE_REDUCTION_FACTOR
        self.obsForce_x, self.obsForce_y = self.calcObstacleForces()
        self.pedForce_x, self.pedForce_y = self.calcPedForceFcn(self)
        self.noise_x, self.noise_y = self.calcNoise(timeStepRatio)

        self.acc_x = self.obsForce_x + self.pedForce_x + self.noise_x
        self.acc_y = self.obsForce_y + self.pedForce_y + self.noise_y

    def calcNoise(self, timeStepRatio):
        std = self.noiseFactor*(0.0056*timeStepRatio*timeStepRatio - 0.1147*timeStepRatio + 1.1091)
        return NOMAD.NOMAD_RNG.normal(0, std), NOMAD.NOMAD_RNG.normal(0, std)

    def calcPathFollowingForce(self):
        try:
            self.desDir_x, self.desDir_y = self.curActivity.getDesiredDirectionForPed(self)
            return self.tauInv*(self.desDir_x*self.preferredSpeed - self.vel_x), self.tauInv*(self.desDir_y*self.preferredSpeed - self.vel_y)
        except:
            logger.debug('Ped {} with pos ({},{}) is outside of the local route choice managers bounds!'.format(self.ID, self.pos_x, self.pos_y))
            closestPoint = self.curActivity.getClosestPoint(self.pos_x, self.pos_y)
            pos2closestPoint_x = closestPoint[0].xy[0][0] - self.pos_x
            pos2closestPoint_y = closestPoint[0].xy[1][0] - self.pos_y
            dist2closestPoint = sqrt(pos2closestPoint_x*pos2closestPoint_x + pos2closestPoint_y*pos2closestPoint_y)
            self.desDir_x = pos2closestPoint_x/dist2closestPoint
            self.desDir_y = pos2closestPoint_y/dist2closestPoint
            return self.tauInv*(self.desDir_x*self.preferredSpeed - self.vel_x), self.tauInv*(self.desDir_y*self.preferredSpeed - self.vel_y)

    #===========================================================================
    # Obstacle forces
    #===========================================================================
    def calcObstacleForces(self):
        obsForces_x = 0
        obsForces_y = 0

        for obstacle in self.getObstaclesInInfluenceArea():
            vec2closestPoint_x, vec2closestPoint_y = obstacle.getVec2closestPointOnObstacleNumpy((self.pos_x, self.pos_y))

            point2pointDist = sqrt(vec2closestPoint_x*vec2closestPoint_x + vec2closestPoint_y*vec2closestPoint_y)
            dist2obstacle = point2pointDist - self.radius
            if dist2obstacle > self.d_shy:
                continue
            elif dist2obstacle < 0:
                obsForces_x, obsForces_y = self.addObstaclePhysicalForce(obsForces_x, obsForces_y, vec2closestPoint_x, vec2closestPoint_y, point2pointDist, dist2obstacle)
            else:
                obsForces_x, obsForces_y = self.calObstaclePsychologicalForce(obsForces_x, obsForces_y, vec2closestPoint_x, vec2closestPoint_y, point2pointDist, dist2obstacle)

        return obsForces_x, obsForces_y

    def addObstaclePhysicalForce(self, obsForces_x, obsForces_y, vec2closestPoint_x, vec2closestPoint_y, point2pointDist, dist2obstacle):
        # dist2obstacle is always negative in this case, hence if the vectors are multiplied by this distance
        # the vector will always point in the opposite direction

        vec2closestPointNorm_x = vec2closestPoint_x/point2pointDist
        vec2closestPointNorm_y = vec2closestPoint_y/point2pointDist
        # Force in the normal direction
        obsForces_x, obsForces_y = self.calcObstaclePhysicalForceNormal(obsForces_x, obsForces_y, vec2closestPointNorm_x, vec2closestPointNorm_y, dist2obstacle)
        # Force in the tangential direction
        obsForces_x, obsForces_y = self.calcObstaclePhysicalForceTangential(obsForces_x, obsForces_y, vec2closestPointNorm_x, vec2closestPointNorm_y, dist2obstacle)

        return obsForces_x, obsForces_y

    def calcObstaclePhysicalForceNormal(self, obsForces_x, obsForces_y, vec2closestPointNorm_x, vec2closestPointNorm_y, dist2obstacle):
        scaleFactor = self.kappa_0*dist2obstacle
        return obsForces_x + vec2closestPointNorm_x*scaleFactor, obsForces_y + vec2closestPointNorm_y*scaleFactor

    def calcObstaclePhysicalForceTangential(self, obsForces_x, obsForces_y, vec2closestPointNorm_x, vec2closestPointNorm_y, dist2obstacle):
        scaleFactor = self.kappa_1*dist2obstacle*(-vec2closestPointNorm_y*self.vel_x + vec2closestPointNorm_x*self.vel_y)
        return obsForces_x - vec2closestPointNorm_y*scaleFactor, obsForces_y + vec2closestPointNorm_x*scaleFactor

    def calObstaclePsychologicalForce(self, obsForces_x, obsForces_y, vec2closestPoint_x, vec2closestPoint_y, point2pointDist, dist2obstacle):
        vec2closestPointNorm_x = vec2closestPoint_x/point2pointDist
        vec2closestPointNorm_y = vec2closestPoint_y/point2pointDist

        if dist2obstacle <= self.d_shy/2:
            return obsForces_x - vec2closestPointNorm_x*self.a_W, obsForces_y - vec2closestPointNorm_y*self.a_W
        scaleFactor = self.a_W*2*(1 - dist2obstacle/self.d_shy)
        return obsForces_x - vec2closestPointNorm_x*scaleFactor, obsForces_y - vec2closestPointNorm_y*scaleFactor

    #===========================================================================
    # Pedestrian forces
    #===========================================================================

    def updatePedForce(self, pedForce_x, pedForce_y):
        self.pedForce_x = pedForce_x
        self.pedForce_y = pedForce_y

    def dummyPedestrianForces(self):
        return self.pedForce_x, self.pedForce_y

    def calcPedestrianForces(self):
        pedForces_x = 0
        pedForces_y = 0

        for otherPedInteractionData in self.getPedsInInfluenceArea():
            if otherPedInteractionData.isTouching:
                pedForces_x, pedForces_y = self.addPedestrianPhysicalForce(pedForces_x, pedForces_y, otherPedInteractionData)
            else:
                pedForces_x, pedForces_y = self.addPedestrianPsychologicalForce(pedForces_x, pedForces_y, otherPedInteractionData)

        return pedForces_x, pedForces_y

    def addPedestrianPhysicalForce(self, pedForces_x, pedForces_y, intData):
        # dist2otherPed is always negative in this case, hence if the vectors are multiplied by this distance
        # the vector will always point in the opposite direction

        # Force in the normal direction
        pedForces_x, pedForces_y = self.calcPedestrianPhysicalForceNormal(pedForces_x, pedForces_y, intData)
        # Force in the tangential direction
        pedForces_x, pedForces_y = self.calcPedestrianPhysicalForceTangential(pedForces_x, pedForces_y, intData)

        return pedForces_x, pedForces_y

    def calcPedestrianPhysicalForceNormal(self, pedForces_x, pedForces_y, intData):
        scaleFactor = self.kappa_0*intData.dist2otherPed
        return pedForces_x + intData.dir2otherPedNorm_x*scaleFactor, pedForces_y + intData.dir2otherPedNorm_y*scaleFactor

    def calcPedestrianPhysicalForceTangential(self, pedForces_x, pedForces_y, intData):
        scaleFactor = self.kappa_1*-intData.dist2otherPed*(-intData.dir2otherPedNorm_y*(intData.otherPedVel_x - self.vel_x) + intData.dir2otherPedNorm_x*(intData.otherPedVel_y - self.vel_y))
        return pedForces_x - intData.dir2otherPedNorm_y*scaleFactor, pedForces_y + intData.dir2otherPedNorm_x*scaleFactor

    def addPedestrianPsychologicalForce(self, pedForces_x, pedForces_y, intData):
        dist2otherPed = self.calcDist2otherPed(intData)

        # Normal interaction
        pedForces_x, pedForces_y = self.calcPedestrianPsychologicalForceNormal(pedForces_x, pedForces_y, intData, dist2otherPed)

        if intData.isOpposing:
            # Lateral interaction
            pedForces_x, pedForces_y = self.calcPedestrianPsychologicalForceLateral(pedForces_x, pedForces_y, intData, dist2otherPed)

        return pedForces_x, pedForces_y

    def calcDist2otherPed(self, intData):
        if intData.isInFront:
            c_factor = self.cPlus_0 # Is already squared
        else:
            c_factor = self.cMinus_0 # Is already squared

        return sqrt(c_factor*intData.dir2otherPed_v_length_2 + intData.dir2otherPed_w_length_2)

    def calcPedestrianPsychologicalForceNormal(self, pedForces_x, pedForces_y, intData, dist2otherPed):
        dist2otherPed -= (self.radius + intData.otherPedRadius)
        scaleFactor = -self.a_0*exp(-dist2otherPed/self.r_0)
        return pedForces_x + intData.dir2otherPedNorm_x*scaleFactor, pedForces_y + intData.dir2otherPedNorm_y*scaleFactor

    def calcPedestrianPsychologicalForceLateral(self, pedForces_x, pedForces_y, intData, dist2otherPed):
        #return -intData.dir2otherPed_w.getNormalized()*self.a_1*exp(-(dist2otherPed*intData.dir2otherPed_w.length)/self.r_1)
        dir2otherPed_w_length = sqrt(intData.dir2otherPed_w_length_2)
        try:
            scaleFactor = -self.a_1*exp(-(dist2otherPed*dir2otherPed_w_length)/self.r_1)/dir2otherPed_w_length
        except ZeroDivisionError:
            scaleFactor = 0

        return pedForces_x + intData.dir2otherPed_w_x*scaleFactor, pedForces_y + intData.dir2otherPed_w_y*scaleFactor

    def getPedsInInfluenceArea(self):
        return getPedsInteractionDataOpt(self.gridCell, self)

    #===========================================================================
    # Alternative acceleration functions (primarily for testing purposes)
    #===========================================================================

    def calcAccelerationNoNoise(self, timeStepRatio): #@Unusedvariable
        self.pathFollowingForce_x, self.pathFollowingForce_y = self.calcPathFollowingForce()
        self.obsForce_x, self.obsForce_y = self.calcObstacleForces()
        self.pedForce_x, self.pedForce_y = self.calcPedForceFcn(self)
        self.acc_x = self.pathFollowingForce_x + self.obsForce_x + self.pedForce_x
        self.acc_y = self.pathFollowingForce_y + self.obsForce_y + self.pedForce_y

    def calcAccelerationOnlyPath(self, timeStepRatio): #@Unusedvariable
        self.pathFollowingForce_x, self.pathFollowingForce_y = self.calcPathFollowingForce()

        self.acc_x = self.pathFollowingForce_x
        self.acc_y = self.pathFollowingForce_y

    def calcAccelerationNoPedAndNoise(self, timeStepRatio): #@Unusedvariable
        self.pathFollowingForce_x, self.pathFollowingForce_y = self.calcPathFollowingForce()
        self.obsForce_x, self.obsForce_y = self.calcObstacleForces()

        self.acc_x = self.pathFollowingForce_x + self.obsForce_x
        self.acc_y = self.pathFollowingForce_y + self.obsForce_y

    ACC_CALC_FCNS = {
        ACC_MAIN:calcAccelerationMain,
        ACC_NO_NOISE:calcAccelerationNoNoise,
        ACC_ONLY_PATH:calcAccelerationOnlyPath,
        ACC_NO_PED_AND_NOISE:calcAccelerationNoPedAndNoise,
        ACC_RED_DEST:calcAccelerationRedDest,
        ACC_NO_PED:calcAccelerationNoPed,
        ACC_RED_PED:calcAccelerationReducedPed
        }
    
    PED_FORCE_CALC_FCNS = {
        PED_FORCE_MAIN:calcPedestrianForces,
        PED_FORCE_DUMMY:dummyPedestrianForces
        }

    #===============================================================================
    # Properties
    #===============================================================================

    @property
    def pos(self):
        return Vector2D(self.pos_x, self.pos_y)

    @property
    def vel(self):
        return Vector2D(self.vel_x, self.vel_y)

    @property
    def velNorm(self):
        return Vector2D(self.velNorm_x, self.velNorm_y)

    @property
    def prevPos(self):
        return Vector2D(self.prevPos_x, self.prevPos_y)

    @property
    def nextPos(self):
        return Vector2D(self.nextPos_x, self.nextPos_y)

    @property
    def nextVel(self):
        return Vector2D(self.nextVel_x, self.nextVel_y)

    @property
    def acc(self):
        return Vector2D(self.acc_x, self.acc_y)

    @property
    def pathFollowingForce(self):
        return Vector2D(self.pathFollowingForce_x, self.pathFollowingForce_y)

    @property
    def obsForce(self):
        return Vector2D(self.obsForce_x, self.obsForce_y)

    @property
    def pedForce(self):
        return Vector2D(self.pedForce_x, self.pedForce_y)

    @property
    def noise(self):
        return Vector2D(self.noise_x, self.noise_y)

    @pos.setter
    def pos(self, value):
        self.setXYvalues('pos', value)

    @vel.setter
    def vel(self, value):
        self.setXYvalues('vel', value)

    @velNorm.setter
    def velNorm(self, value):
        self.setXYvalues('velNorm', value)

    @prevPos.setter
    def prevPos(self, value):
        self.setXYvalues('prevPos', value)

    @nextPos.setter
    def nextPos(self, value):
        self.setXYvalues('nextPos', value)

    @nextVel.setter
    def nextVel(self, value):
        self.setXYvalues('nextVel', value)

    @acc.setter
    def acc(self, value):
        self.setXYvalues('acc', value)

    @pathFollowingForce.setter
    def pathFollowingForce(self, value):
        self.setXYvalues('pathFollowingForce', value)

    @obsForce.setter
    def obsForce(self, value):
        self.setXYvalues('obsForce', value)

    @pedForce.setter
    def pedForce(self, value):
        self.setXYvalues('pedForce', value)

    @noise.setter
    def noise(self, value):
        self.setXYvalues('noise', value)

    def setXYvalues(self, attrNm, value):
        if isinstance(value, (tuple, list)):
            setattr(self, attrNm + '_x', value[0])
            setattr(self, attrNm + '_y', value[1])
        else:
            setattr(self, attrNm + '_x', value)
            setattr(self, attrNm + '_y', value)

    @staticmethod
    def getLabel():
        return ATTR_PED_OPT_CLASS

    @staticmethod
    def getNormalizedVelocity(vel_x, vel_y):
        pedSpeed = sqrt(vel_x*vel_x + vel_y*vel_y)
        if pedSpeed > 1e-6:
            velNorm_x = vel_x/pedSpeed
            velNorm_y = vel_y/pedSpeed
        else:
            velNorm_x = 0
            velNorm_y = 0

        return velNorm_x, velNorm_y
    
    def __setattr__(self, name, value):
        object.__setattr__(self, name, value)

#=============================================================================================================
#=============================================================================================================
'''
The version of the base pedestrian class which uses a c-implementation to get the interaction data to speed-up
the calculations
'''
#=============================================================================================================
#=============================================================================================================

if NOMAD.canUseGetInteractionCompiledCode:
    intLib = ctypes.CDLL(str(NOMAD.getInteractionData_so_file))

    class ReturnData(ctypes.Structure):
        _fields_ = [('isInteracting', ctypes.c_bool),
                    ('isTouching', ctypes.c_bool),
                    ('isOpposing', ctypes.c_bool),
                    ('isInFront', ctypes.c_bool),
                    ('dir2otherPedNorm_x', ctypes.c_double),
                    ('dir2otherPedNorm_y', ctypes.c_double),
                    ('dist2otherPed', ctypes.c_double),
                    ('dir2otherPed_v_x', ctypes.c_double),
                    ('dir2otherPed_v_y', ctypes.c_double),
                    ('dir2otherPed_w_x', ctypes.c_double),
                    ('dir2otherPed_w_y', ctypes.c_double),
                    ('dir2otherPed_v_length_2', ctypes.c_double),
                    ('dir2otherPed_w_length_2', ctypes.c_double),
                    ('otherPedVel_x', ctypes.c_double),
                    ('otherPedVel_y', ctypes.c_double),
                    ('otherPedRadius', ctypes.c_double)]

    getInteractionData = intLib.getInteractionData
    getInteractionData.restype = ReturnData
    getInteractionData.argtypes = [ctypes.c_double]*18

class PedestrianIntC(PedestrianOpt):

    def getPedsInInfluenceArea(self):
        return getPedsInteractionDataOpt(self.gridCell, self, useC=True)

    @staticmethod
    def getLabel():
        return ATTR_PED_INT_C_CLASS


#=============================================================================================================
#=============================================================================================================
'''
The interaction data classes and function for both the PedestrianOpt and PedestrianIntC classes
'''
#=============================================================================================================
#=============================================================================================================

#===========================================================================
# Pedestrian interaction data classes
#===========================================================================
class PedInteractionDataOpt():
    '''
    A data container class, which contains all information necessary to compute the interaction forces caused by this pedestrian
    '''

    __slots__ = ['dir2otherPedNorm_x', 'dir2otherPedNorm_y', 'otherPedVel_x', 'otherPedVel_y', 'otherPedRadius', 'isOpposing', 'isTouching', 'isInFront', 'otherPedID']

    def __init__(self, dir2otherPedNorm_x, dir2otherPedNorm_y, otherPed, isTouching=False, isOpposing=False, isInFront=False):
        '''
        Constructor
        '''
        self.dir2otherPedNorm_x = dir2otherPedNorm_x
        self.dir2otherPedNorm_y = dir2otherPedNorm_y
        self.otherPedVel_x = otherPed.vel_x
        self.otherPedVel_y = otherPed.vel_y
        self.otherPedRadius = otherPed.radius
        self.otherPedID = otherPed.ID
        self.isTouching = isTouching
        self.isOpposing = isOpposing
        self.isInFront = isInFront

    @property
    def dir2otherPedNorm(self):
        return (self.dir2otherPedNorm_x, self.dir2otherPedNorm_y)

class PedInteractionDataTouchingOpt(PedInteractionDataOpt):

    __slots__ = ['dist2otherPed']

    def __init__(self, dir2otherPedNorm_x, dir2otherPedNorm_y, otherPed, dist2otherPed):
        super().__init__(dir2otherPedNorm_x, dir2otherPedNorm_y, otherPed, isTouching=True, isOpposing=False, isInFront=None)
        self.dist2otherPed = dist2otherPed

class PedInteractionDataNonOpposingOpt(PedInteractionDataOpt):

    __slots__ = ['dir2otherPed_v_x', 'dir2otherPed_v_y', 'dir2otherPed_w_x', 'dir2otherPed_w_y', 'dir2otherPed_v_length_2', 'dir2otherPed_w_length_2']

    def __init__(self, dir2otherPedNorm_x, dir2otherPedNorm_y, otherPed, dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2, isInFront):
        super().__init__(dir2otherPedNorm_x, dir2otherPedNorm_y, otherPed, isTouching=False, isOpposing=False, isInFront=isInFront)
        self.dir2otherPed_v_x = dir2otherPed_v_x
        self.dir2otherPed_v_y = dir2otherPed_v_y
        self.dir2otherPed_w_x = dir2otherPed_w_x
        self.dir2otherPed_w_y = dir2otherPed_w_y
        self.dir2otherPed_v_length_2 = dir2otherPed_v_length_2
        self.dir2otherPed_w_length_2 = dir2otherPed_w_length_2

    @property
    def dir2otherPed_v(self):
        return (self.dir2otherPed_v_x, self.dir2otherPed_v_y)

    @property
    def dir2otherPed_w(self):
        return (self.dir2otherPed_w_x, self.dir2otherPed_w_y)

class PedInteractionDataOpposingOpt(PedInteractionDataOpt):

    __slots__ = ['dir2otherPed_v_x', 'dir2otherPed_v_y', 'dir2otherPed_w_x', 'dir2otherPed_w_y', 'dir2otherPed_v_length_2', 'dir2otherPed_w_length_2']

    def __init__(self, dir2otherPedNorm_x, dir2otherPedNorm_y, otherPed, dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2):
        super().__init__(dir2otherPedNorm_x, dir2otherPedNorm_y, otherPed, isTouching=False, isOpposing=True, isInFront=True)
        self.dir2otherPed_v_x = dir2otherPed_v_x
        self.dir2otherPed_v_y = dir2otherPed_v_y
        self.dir2otherPed_w_x = dir2otherPed_w_x
        self.dir2otherPed_w_y = dir2otherPed_w_y
        self.dir2otherPed_v_length_2 = dir2otherPed_v_length_2
        self.dir2otherPed_w_length_2 = dir2otherPed_w_length_2

    @property
    def dir2otherPed_v(self):
        return (self.dir2otherPed_v_x, self.dir2otherPed_v_y)

    @property
    def dir2otherPed_w(self):
        return (self.dir2otherPed_w_x, self.dir2otherPed_w_y)


#===========================================================================
# Pedestrian interaction data functions
#===========================================================================

def getPedsInteractionDataOpt(gridCell, ped, useC=False):
    pos_x = ped.pos_x
    pos_y = ped.pos_y
    vel_x = ped.vel_x
    vel_y = ped.vel_y

    radius = ped.radius
    t_A = ped.t_A
    ie_f_2 = ped.ie_f_2
    ie_b_2 = ped.ie_b_2
    r_infl_2 = ped.r_infl_2

    maxDistBehind = ped.maxDistBehind
    maxDistInFront = ped.maxDistInFront

    vec2otherPeds = ped.vec2otherPeds

    anticipatedPos_x = pos_x + vel_x*t_A
    anticipatedPos_y = pos_y + vel_y*t_A
    velNorm_x = ped.velNorm_x
    velNorm_y = ped.velNorm_y

    pedInteractionData = []

    def addPedInteractionData(pedsInNeighboringCells, doObstacleCheck, obstacles=[]):
        for otherPed in pedsInNeighboringCells:
            if doObstacleCheck and isObscuredByObstacle(ped, otherPed, obstacles):
                continue

            otherPos_x = otherPed.pos_x
            otherPos_y = otherPed.pos_y
            otherRadius = otherPed.radius

            x2otherPed, y2otherPed, pos2posDist, dist2otherPed = calcVec2OtherPed(vec2otherPeds, otherPed.ID, otherPos_x, otherPos_y, pos_x, pos_y, radius, otherRadius)

            if pos2posDist == 0:
                continue 

            if dist2otherPed < 0:
                # The pedestrian are touching each other so are definitely in the influence area
                # TODO add also to otherPed and make sure otherPed skips this ped
                pedInteractionData.append(PedInteractionDataTouchingOpt(x2otherPed/pos2posDist, y2otherPed/pos2posDist, otherPed, dist2otherPed))
                continue

            # Local coordinate system where v is in the direction of the velocity of the ped and w is orthogonal to v
            dist_v = x2otherPed*velNorm_x + y2otherPed*velNorm_y

            otherVel_x = otherPed.vel_x
            otherVel_y = otherPed.vel_y

            # Anticipation behavior
            if dist_v > 0:
                x2otherPed, y2otherPed, pos2posDist, dist2otherPed, dist_v = addAnticipationBehaviour(x2otherPed, y2otherPed, pos2posDist, dist2otherPed, dist_v, otherPos_x, otherPos_y, otherVel_x, otherVel_y, t_A, anticipatedPos_x, anticipatedPos_y, velNorm_x, velNorm_y, radius, otherRadius)

            if pos2posDist == 0:
                continue

            if dist_v < maxDistBehind or dist_v > maxDistInFront:
                continue

            dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y = calcLocalVecsXY(velNorm_x, velNorm_y, dist_v, x2otherPed, y2otherPed)

            dir2otherPed_v_length_2 = dir2otherPed_v_x*dir2otherPed_v_x + dir2otherPed_v_y*dir2otherPed_v_y
            dir2otherPed_w_length_2 = dir2otherPed_w_x*dir2otherPed_w_x + dir2otherPed_w_y*dir2otherPed_w_y

            if not isInInfluenceArea(dist_v, dir2otherPed_v_length_2, dir2otherPed_w_length_2, ie_f_2, ie_b_2, r_infl_2):
                continue

            x2otherPedNorm = x2otherPed/pos2posDist
            y2otherPedNorm = y2otherPed/pos2posDist

            dir2otherPed_v_length = sqrt(dir2otherPed_v_length_2)

            if dist_v > 0 and (otherVel_x*dir2otherPed_v_x/dir2otherPed_v_length + otherVel_y*dir2otherPed_v_y/dir2otherPed_v_length ) < 0:
                pedInteractionData.append(PedInteractionDataOpposingOpt(x2otherPedNorm, y2otherPedNorm, otherPed, dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2))
            else:
                pedInteractionData.append(PedInteractionDataNonOpposingOpt(x2otherPedNorm, y2otherPedNorm, otherPed, dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2, dist_v > 0))

    def addPedInteractionDataC(pedsInNeighboringCells, doObstacleCheck, obstacles=[]):
        for otherPed in pedsInNeighboringCells:
            if doObstacleCheck and isObscuredByObstacle(ped, otherPed, obstacles):
                continue

            returnData = getInteractionData(pos_x, pos_y, anticipatedPos_x, anticipatedPos_y, velNorm_x, velNorm_y, radius, t_A, ie_f_2, ie_b_2, r_infl_2, maxDistBehind, maxDistInFront, otherPed.pos_x, otherPed.pos_y, otherPed.vel_x, otherPed.vel_y, otherPed.radius)
            if returnData.isInteracting:
                # Add otherPed ID
                returnData.otherPedID = otherPed.ID
                pedInteractionData.append(returnData)

    otherPedsInCell = gridCell.getOtherPedsInCell(ped)
    pedsInNeighboringCells = gridCell.getPedsInNeighboringCells()
    if useC:
        addPedInteractionDataC(otherPedsInCell, gridCell.containsObstacle, gridCell.sightBlockingObstaclesInCell)
        addPedInteractionDataC(pedsInNeighboringCells[UNOBSTRUCTED], False)
        addPedInteractionDataC(pedsInNeighboringCells[OBSTRUCTED], True, gridCell.obstaclesObstrNeighbors)
    else:
        addPedInteractionData(otherPedsInCell, gridCell.containsObstacle, gridCell.obstaclesObstrNeighbors)
        addPedInteractionData(pedsInNeighboringCells[UNOBSTRUCTED], False)
        addPedInteractionData(pedsInNeighboringCells[OBSTRUCTED], True, gridCell.obstaclesObstrNeighbors)

    return pedInteractionData

def calcVec2OtherPed(vec2otherPeds, otherPedID, otherPos_x, otherPos_y, pos_x, pos_y, radius, otherRadius):
    if otherPedID in vec2otherPeds:
        x2otherPed = vec2otherPeds[otherPedID][0]
        y2otherPed = vec2otherPeds[otherPedID][1]
        pos2posDist = vec2otherPeds[otherPedID][2]
        dist2otherPed = vec2otherPeds[otherPedID][3]
    else:
        x2otherPed = otherPos_x - pos_x
        y2otherPed = otherPos_y - pos_y
        pos2posDist = sqrt(x2otherPed*x2otherPed + y2otherPed*y2otherPed)
        dist2otherPed = pos2posDist - radius - otherRadius
        vec2otherPeds[otherPedID] = (-x2otherPed, -y2otherPed, pos2posDist, dist2otherPed)

    return x2otherPed, y2otherPed, pos2posDist, dist2otherPed

def addAnticipationBehaviour(x2otherPed, y2otherPed, pos2posDist, dist2otherPed, dist_v, otherPos_x, otherPos_y, otherVel_x, otherVel_y, t_A, anticipatedPos_x, anticipatedPos_y, velNorm_x, velNorm_y, radius, otherRadius):
    x2otherPedAnt = otherPos_x + otherVel_x*t_A - anticipatedPos_x
    y2otherPedAnt = otherPos_y + otherVel_y*t_A - anticipatedPos_y
    distAnt_v = x2otherPedAnt*velNorm_x + y2otherPedAnt*velNorm_y

    if distAnt_v > 0:
        # otherPed is still in front
        x2otherPed = x2otherPedAnt
        y2otherPed = y2otherPedAnt
        pos2posDist = sqrt(x2otherPed*x2otherPed + y2otherPed*y2otherPed)
        dist2otherPed = pos2posDist - radius - otherRadius
        dist_v = distAnt_v

    return x2otherPed, y2otherPed, pos2posDist, dist2otherPed, dist_v

def calcLocalVecsXY(velNorm_x, velNorm_y, dist_v, x2otherPed, y2otherPed):
    dir2otherPed_v_x = velNorm_x*dist_v
    dir2otherPed_v_y = velNorm_y*dist_v
    dir2otherPed_w_x = x2otherPed - dir2otherPed_v_x
    dir2otherPed_w_y = y2otherPed - dir2otherPed_v_y

    return dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y

#=============================================================================================================
#=============================================================================================================
'''
The version of the base pedestrian class which uses a c-implementation to get the interaction data to speed-up
the calculations
'''
#=============================================================================================================
#=============================================================================================================
if NOMAD.canUseCalcPedForcesCompiledCode:
    pedForcesLib = ctypes.CDLL(str(NOMAD.calcPedForces_so_file))

    class PedForces(ctypes.Structure):
        _fields_ = [('x', ctypes.c_double),
                    ('y', ctypes.c_double)]

    calcPedForces = pedForcesLib.calcPedForces
    calcPedForces.restype = PedForces
    calcPedForces.argtypes = [ctypes.c_double]*19 + [ctypes.POINTER(ctypes.c_double)]*5  + [ctypes.c_int]

class PedestrianPedForcesC(PedestrianOpt):

    def calcPedestrianForces(self):
        otherPeds = self.gridCell.getOtherPedsInCell(self)
        if self.gridCell.containsSightBlockingObstacle:
            otherPeds = removeObscuredPeds(otherPeds, self.gridCell.sightBlockingObstaclesInCell, self, True)

        pedsInNeighboringCells = self.gridCell.getPedsInNeighboringCells()
        otherPeds += removeObscuredPeds(pedsInNeighboringCells[OBSTRUCTED], self.gridCell.obstaclesObstrNeighbors, self, True)
        otherPeds += pedsInNeighboringCells[UNOBSTRUCTED]

        otherPedCount = len(otherPeds)
        if otherPedCount == 0:
            return 0.0, 0.0
        arrayType = ctypes.c_double*otherPedCount
        
        otherPosArray_x = arrayType(*[otherPed.pos_x for otherPed in otherPeds])
        otherPosArray_y = arrayType(*[otherPed.pos_y for otherPed in otherPeds])
        otherVelArray_x = arrayType(*[otherPed.vel_x for otherPed in otherPeds])
        otherVelArray_y = arrayType(*[otherPed.vel_y for otherPed in otherPeds])
        otherRadiusArray = arrayType(*[otherPed.radius for otherPed in otherPeds])
       
        pedForces = calcPedForces(self.pos_x, self.pos_y, self.vel_x, self.vel_y, self.radius, self.t_A, self.kappa_0,
                                   self.kappa_1, self.cPlus_0, self.cMinus_0, self.a_0, self.r_0, self.a_1, self.r_1,
                                   self.ie_f_2, self.ie_b_2, self.r_infl_2, self.maxDistBehind, self.maxDistInFront,
                                   otherPosArray_x, otherPosArray_y, otherVelArray_x,
                                   otherVelArray_y, otherRadiusArray, otherPedCount)

        return pedForces.x, pedForces.y

    @staticmethod
    def getLabel():
        return ATTR_PED_PED_FORCES_C_CLASS

#=============================================================================================================
#=============================================================================================================
'''
'''
#=============================================================================================================
#=============================================================================================================
@dataclass(frozen=True)
class ActivityLogElement():
    startTime: float
    activityType: str
    activityID: str
    destinationID: str
    pedState: int
    endTime: float = None

    def setEndTime(self, endTime):
        super().__setattr__('endTime', endTime)

#=============================================================================================================
#=============================================================================================================
ATTR_PED_CLASSES = {
    ATTR_PED_CLASS: Pedestrian,
    ATTR_PED_OPT_CLASS: PedestrianOpt,
    ATTR_PED_INT_C_CLASS: PedestrianIntC,
    ATTR_PED_PED_FORCES_C_CLASS: PedestrianPedForcesC,
    }
