""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from math import inf

from shapely.geometry.base import CAP_STYLE
from shapely.geometry.polygon import Polygon

from NOMAD.general_functions import doListCeck, SpawnableLine
from NOMAD.obstacles import LineObstacle
from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
from NOMAD.vectors import Vector2D


class WalkLevel():
    '''
    A walk level
    '''

    def __init__(self, ID):
        '''
        Constructor
        '''
        self._ID = ID
        self._walkableAreas = []
        self._obstacles = []
        self._queues = []
        self._destinations = []
        self._sources = []
        self._connectors = [] # Connectors to other walk levels
        self._elevation = 0 # Default no elevation, float that indicates the elevation of this walk. It can also be a function if the elevation changes over the xy-coordinates
        self._hasConstantElevation = None
        self._extent = Vector2D(0.0,0.0) # The max length in the x and y direction
        self._origin = Vector2D(0.0,0.0) # The min coordinates in the x and y direction
        self._ID2obstacle = {}
        self._ID2destination = {}
        self._routingBuffers = []

    def addWalkableAreas(self, walkableAreas):
        walkableAreas = doListCeck(walkableAreas)

        self._walkableAreas += walkableAreas
        self.updateBounds()

    def addObstacles(self, obstacles):
        obstacles = doListCeck(obstacles)
        WalkLevel.doDuplicateCeck(self._obstacles, obstacles, 'obstacle')

        self._obstacles += obstacles

        for obstacle in obstacles:
            self._ID2obstacle[obstacle.ID] = obstacle
            obstacle.prepare()

        self.updateBounds()

    def addQueues(self, queues):
        queues = doListCeck(queues)
        WalkLevel.doDuplicateCeck(self._queues, queues, 'queue')

        for queue in queues:
            self.checkIfInWalkableArea(queue, 'Queue')           
            
        self._queues += queues

    def addDestinations(self, destinations):
        destinations = doListCeck(destinations)
        WalkLevel.doDuplicateCeck(self._destinations, destinations, 'destination')

        for destination in destinations:
            self.checkIfInWalkableArea(destination, 'Destination')
            self._ID2destination[destination.ID] = destination

        self._destinations += destinations

    def addSources(self, sources):
        sources = doListCeck(sources)
        WalkLevel.doDuplicateCeck(self._sources, sources, 'source')

        for source in sources:
            self.checkIfInWalkableArea(source, 'Source')

        self._sources += sources

    def addRoutingBuffers(self, routingBuffers):
        routingBuffers = doListCeck(routingBuffers)

        for routingBuffer in routingBuffers:
            self.checkIfInWalkableArea(routingBuffer.geometry, 'Routing buffer')

        self._routingBuffers += routingBuffers

    def updateBounds(self):
        self.updateOrigin()
        self.updateExtent()

    def updateOrigin(self):
        xMin = inf
        yMin = inf
        for geomObject in self._walkableAreas + self._obstacles:
            minValues = geomObject.bounds[:2]
            xMin = min(xMin, minValues[0])
            yMin = min(yMin, minValues[1])

        self._origin = Vector2D(xMin, yMin)

    def updateExtent(self):
        xMax = -inf
        yMax = -inf
        for geomObject in self._walkableAreas + self._obstacles:
            maxValues = geomObject.bounds[2:]
            xMax = max(xMax, maxValues[0])
            yMax = max(yMax, maxValues[1])

        self._extent = Vector2D(xMax, yMax) - self._origin

    def cropWalkableAreas(self, buffer):
        for walkableArea in self._walkableAreas:
            walkableArea.crop(self._obstacles, buffer)

    def checkIfInWalkableArea(self, geomObject, geomObjectLabel):
        for walkableArea in self._walkableAreas:
            if walkableArea.contains(geomObject):
                return

        raise ValueError('{} with ID={} lies not within any of the walkable areas!'.format(geomObjectLabel, geomObject.ID))

    def finalizeCreation(self, parameters, gridManager):
        for destination in self.destinations:
            if isinstance(destination, SpawnableLine):
                bufferedGeom = destination._geometry.buffer(parameters.MAX_PED_RADIUS, 32, cap_style=CAP_STYLE.round)
                destination.gridCellsInNeighborhood = gridManager.getGridCellsCoveringPolygon(bufferedGeom, self.ID)
                destination.createBaseLineSegments( parameters, gridManager, self.ID)

    @staticmethod
    def doDuplicateCeck(objectList, objects2addList, objectName):
        baseIDs = [wlObject.ID for wlObject in objectList]
        newIDs = [wlObject.ID for wlObject in objects2addList]
        for newID in newIDs:
            IDcount = newIDs.count(newID)
            if IDcount > 1:
                raise ValueError('The list of new {}s contains {} {}s with ID={}'.format(objectName, IDcount, objectName, newID))
            if newID in baseIDs:
                raise ValueError('The list of {}s already contains a(n) {} with ID={}'.format(objectName, IDcount, objectName, newID))

    def getElevationAtPoint(self, point):
        if self.hasConstantElevation:
            return self.elevation
        else:
            return self.elevation(point)

    def getObstacle(self, ID):
        return self._ID2obstacle[ID]

    def hasObstacle(self, ID):
        return ID in self._ID2obstacle

    def getDestination(self, ID):
        return self._ID2destination[ID]

    def hasDestination(self, ID):
        return ID in self._ID2destination

    # This method ensure the ID is a static variable set at the init of the instance
    @property
    def ID(self):
        return self._ID

    # Getters and setter for private variables
    @property
    def walkableAreas(self):
        return self._walkableAreas

    @property
    def queues(self):
        return self._queues

    @property
    def obstacles(self):
        return self._obstacles

    @property
    def sources(self):
        return self._sources

    @property
    def destinations(self):
        return self._destinations

    @property
    def routingBuffers(self):
        return self._routingBuffers

    @property
    def extent(self):
        return self._extent

    @property
    def origin(self):
        return self._origin

    @property
    def elevation(self):
        return self._elevation

    @property
    def connectors(self):
        return self._connectors

    @property
    def hasConstantElevation(self):
        if self._hasConstantElevation is None:
            if callable(self._elevation):
                self._hasConstantElevation = False
            else:
                self._hasConstantElevation = True

        return self._hasConstantElevation

    @property
    def maxXcoordinate(self):
        return self._origin.x + self._extent.x

    @property
    def maxYcoordinate(self):
        return self._origin.y + self._extent.y

# ================================================================================================
# ================================================================================================

class WalkableArea(ShapelyGeomClassWrapper):

    def __init__(self, ID, polygonCoords):
        '''
        Constructor
        '''
        super().__init__(ID)
        self._geometry = Polygon(polygonCoords)

    def crop(self, obstacles, buffer):
        for obstacle in obstacles:
            if isinstance(obstacle, LineObstacle):
                self._geometry = self.difference(obstacle.getPolygonRepresentation(buffer))
            else:
                self._geometry = self.difference(obstacle)

# ================================================================================================
# ================================================================================================

def getObstacle(obstacleID, walkLevels):
    for walkLevel in walkLevels.values():
        try:
            return walkLevel.getObstacle(obstacleID), walkLevel
        except KeyError: pass
        
    raise KeyError(f'No obstacle with ID {obstacleID} in the provided walk levels')
