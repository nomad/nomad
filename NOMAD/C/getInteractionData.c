/*
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdbool.h>

typedef struct ReturnData {
	bool isInteracting, isTouching, isOpposing, isInFront;
	double dir2otherPedNorm_x, dir2otherPedNorm_y, dist2otherPed, dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2, otherPedVel_x, otherPedVel_y, otherPedRadius;
} ReturnData;

ReturnData getInteractionData(double pos_x, double pos_y, double anticipatedPos_x, double anticipatedPos_y, double velNorm_x, double velNorm_y, double radius, double t_A, double ie_f_2, double ie_b_2, double r_infl_2, double maxDistBehind, double maxDistInFront, double otherPos_x, double otherPos_y, double otherVel_x, double otherVel_y, double otherRadius) {
	ReturnData returnData = {false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0, 0, otherVel_x, otherVel_y, otherRadius};

	double x2otherPed = otherPos_x - pos_x;
	double y2otherPed = otherPos_y - pos_y;
	double pos2posDist = sqrt(x2otherPed*x2otherPed + y2otherPed*y2otherPed);

	if (pos2posDist == 0) {
		return returnData;
	}

	double dist2otherPed = pos2posDist - radius - otherRadius;

	if (dist2otherPed < 0) {
		returnData.isInteracting = true;
		returnData.isTouching = true;
		returnData.dir2otherPedNorm_x = x2otherPed/pos2posDist;
		returnData.dir2otherPedNorm_y = y2otherPed/pos2posDist;
		returnData.dist2otherPed = dist2otherPed;
		return returnData;
	}

	double dist_v = x2otherPed*velNorm_x + y2otherPed*velNorm_y;

	if (dist_v > 0) {
		double x2otherPedAnt = otherPos_x + otherVel_x*t_A - anticipatedPos_x;
		double y2otherPedAnt = otherPos_y + otherVel_y*t_A - anticipatedPos_y;
		double distAnt_v = x2otherPedAnt*velNorm_x + y2otherPedAnt*velNorm_y	;
		if (distAnt_v > 0) {
			x2otherPed = x2otherPedAnt;
			y2otherPed = y2otherPedAnt;
			pos2posDist = sqrt(x2otherPed*x2otherPed + y2otherPed*y2otherPed);
			dist2otherPed = pos2posDist - radius - otherRadius;
			dist_v = distAnt_v;
		}
	}

	if (dist_v < maxDistBehind || dist_v > maxDistInFront) {
		return returnData;
	}

	double dir2otherPed_v_x = velNorm_x*dist_v;
    double dir2otherPed_v_y = velNorm_y*dist_v;
    double dir2otherPed_w_x = x2otherPed - dir2otherPed_v_x;
    double dir2otherPed_w_y = y2otherPed - dir2otherPed_v_y;
	double dir2otherPed_v_length_2 = dir2otherPed_v_x*dir2otherPed_v_x + dir2otherPed_v_y*dir2otherPed_v_y;
    double dir2otherPed_w_length_2 = dir2otherPed_w_x*dir2otherPed_w_x + dir2otherPed_w_y*dir2otherPed_w_y;

	if (dist_v > 0) {
		//In front
		if ((dir2otherPed_v_length_2/ie_f_2 + dir2otherPed_w_length_2/r_infl_2) > 1) {
			return returnData;
		}
    } else if ((dir2otherPed_v_length_2/ie_b_2 + dir2otherPed_w_length_2/r_infl_2) > 1) {
        return returnData;
	}

	double dir2otherPed_v_length = sqrt(dir2otherPed_v_length_2);

	if (dist_v > 0){
		returnData.isInFront = true;
		if ((otherVel_x*dir2otherPed_v_x/dir2otherPed_v_length + otherVel_y*dir2otherPed_v_y/dir2otherPed_v_length ) < 0) {
			returnData.isOpposing = true;
		}
	}
	returnData.isInteracting = true;
	returnData.dir2otherPedNorm_x = x2otherPed/pos2posDist;
	returnData.dir2otherPedNorm_y = y2otherPed/pos2posDist;
	returnData.dir2otherPed_v_x = dir2otherPed_v_x;
	returnData.dir2otherPed_v_y = dir2otherPed_v_y;
	returnData.dir2otherPed_w_x = dir2otherPed_w_x;
	returnData.dir2otherPed_w_y = dir2otherPed_w_y;
	returnData.dir2otherPed_v_length_2 = dir2otherPed_v_length_2;
	returnData.dir2otherPed_w_length_2 = dir2otherPed_w_length_2;

	return returnData;
}
