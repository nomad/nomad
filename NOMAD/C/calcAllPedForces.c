/*
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "struct.c"
#include "calcPedForces.h"

struct PedForces* calcAllPedForces(struct PedForcesInput* in, int pedCount) {
	struct PedForces *pedForces = malloc(sizeof(struct PedForces) * pedCount);
	int i;
	PedForces pedForces_i;
	for (i = 0; i < pedCount; ++i) {
		pedForces_i = calcPedForces(in[i].pos_x, in[i].pos_y, in[i].vel_x, in[i].vel_y, in[i].radius, in[i].t_A, in[i].kappa_0, in[i].kappa_1, in[i].cPlus_0, in[i].cMinus_0, in[i].a_0, in[i].r_0, in[i].a_1, in[i].r_1, in[i].ie_f_2, in[i].ie_b_2, in[i].r_infl_2, in[i].maxDistBehind, in[i].maxDistInFront, in[i].otherPosArray_x, in[i].otherPosArray_y, in[i].otherVelArray_x, in[i].otherVelArray_y, in[i].otherRadiusArray, in[i].otherPedCount);
		//printf("%d: (%f, %f) (x,y)\n", i, pedForces_i.x, pedForces_i.y);
		pedForces[i].x = pedForces_i.x;
		pedForces[i].y = pedForces_i.y;
	}
	return pedForces;
}
