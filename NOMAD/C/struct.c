
typedef struct PedForces {
	double x, y;
} PedForces;


typedef struct PedForcesInput {
	double pos_x, pos_y, vel_x, vel_y, radius,t_A, kappa_0, kappa_1, cPlus_0, cMinus_0, a_0, r_0;
	double a_1, r_1, ie_f_2, ie_b_2, r_infl_2, maxDistBehind, maxDistInFront;
	double* otherPosArray_x;
	double* otherPosArray_y;
	double* otherVelArray_x;
	double* otherVelArray_y;
	double* otherRadiusArray;
	int otherPedCount;
} PedForcesInput;
