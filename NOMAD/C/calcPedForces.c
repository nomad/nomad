/*
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "struct.c"
#include "calcPedForces.h"

PedForces calcPedPhysicalForces(double dist2otherPed, double dir2otherPedNorm_x, double dir2otherPedNorm_y, double otherPedVel_x, double otherPedVel_y, double kappa_0, double kappa_1, double vel_x, double vel_y) {
	double tangForceScalar = kappa_1*(-dist2otherPed*-dir2otherPedNorm_y*(otherPedVel_x - vel_x) + dir2otherPedNorm_x*(otherPedVel_y - vel_y));

	double force_x = dir2otherPedNorm_x*kappa_0*dist2otherPed - dir2otherPedNorm_y*tangForceScalar;
	double force_y = dir2otherPedNorm_y*kappa_0*dist2otherPed + dir2otherPedNorm_x*tangForceScalar;

	PedForces physForces = {force_x, force_y};

	return physForces;
}

PedForces calcPedPsychologicalForces(bool isOpposing, bool isInFront, double dir2otherPedNorm_x, double dir2otherPedNorm_y, double dir2otherPed_w_x, double dir2otherPed_w_y, double dir2otherPed_v_length_2, double dir2otherPed_w_length_2, double cPlus_0, double cMinus_0, double a_0, double r_0, double a_1, double r_1, double radius, double otherRadius) {
	double dist2otherPed;
	if (isInFront){
		dist2otherPed = sqrt(cPlus_0*dir2otherPed_v_length_2 + dir2otherPed_w_length_2);
	} else {
		dist2otherPed = sqrt(cMinus_0*dir2otherPed_v_length_2 + dir2otherPed_w_length_2);
	}

	double normForceScalar = -a_0*exp(-(dist2otherPed - radius - otherRadius)/r_0);

	double force_x = dir2otherPedNorm_x*normForceScalar;
	double force_y = dir2otherPedNorm_y*normForceScalar;

	if (isOpposing){
		double dir2otherPed_w_length = sqrt(dir2otherPed_w_length_2);
		double latForceScalar = -a_1*exp(-(dist2otherPed*dir2otherPed_w_length)/r_1)/dir2otherPed_w_length;
		force_x += dir2otherPed_w_x*latForceScalar;
		force_y += dir2otherPed_w_y*latForceScalar;
	}

	PedForces psychForces = {force_x, force_y};

	return psychForces;
}


PedForces calcPedForces(double pos_x, double pos_y, double vel_x, double vel_y, double radius, double t_A, double kappa_0, double kappa_1, double cPlus_0, double cMinus_0, double a_0, double r_0, double a_1, double r_1, double ie_f_2, double ie_b_2, double r_infl_2, double maxDistBehind, double maxDistInFront, double* otherPosArray_x, double* otherPosArray_y, double* otherVelArray_x, double* otherVelArray_y, double* otherRadiusArray, int otherPedCount) {

	double force_x = 0;
	double force_y = 0;

	double anticipatedPos_x = pos_x + vel_x*t_A;
	double anticipatedPos_y = pos_y + vel_y*t_A;

	double speed = sqrt(vel_x*vel_x + vel_y*vel_y);

	double velNorm_x = 0;
	double velNorm_y = 0;

	if (speed > 1e-6) {
		velNorm_x = vel_x/speed;
		velNorm_y = vel_y/speed;
	}

	PedForces psychForces, physForces;
	double dir2otherPed_x, dir2otherPed_y, pos2posDist, dist2otherPed, dist_v, dir2otherPed_xAnt, dir2otherPed_yAnt, distAnt_v;
	double dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2;
	double dir2otherPed_v_length;
	bool isOpposing, isInFront;

	int i;
	for (i = 0; i < otherPedCount; ++i) {
		dir2otherPed_x = otherPosArray_x[i] - pos_x;
		dir2otherPed_y = otherPosArray_y[i] - pos_y;
		pos2posDist = sqrt(dir2otherPed_x*dir2otherPed_x + dir2otherPed_y*dir2otherPed_y);

		if (pos2posDist == 0) {
			continue;
		}

		dist2otherPed = pos2posDist - radius - otherRadiusArray[i];

		if (dist2otherPed < 0) {
			physForces = calcPedPhysicalForces(dist2otherPed, dir2otherPed_x/pos2posDist, dir2otherPed_y/pos2posDist, otherVelArray_x[i], otherVelArray_y[i], kappa_0, kappa_1, vel_x,  vel_y);
			force_x += physForces.x;
			force_y += physForces.y;
			continue;
		}

		dist_v = dir2otherPed_x*velNorm_x + dir2otherPed_y*velNorm_y;

		if (dist_v > 0) {
			dir2otherPed_xAnt = otherPosArray_x[i] + otherVelArray_x[i]*t_A - anticipatedPos_x;
			dir2otherPed_yAnt = otherPosArray_y[i] + otherVelArray_y[i]*t_A - anticipatedPos_y;
			distAnt_v = dir2otherPed_xAnt*velNorm_x + dir2otherPed_yAnt*velNorm_y	;
			if (distAnt_v > 0) {
				dir2otherPed_x = dir2otherPed_xAnt;
				dir2otherPed_y = dir2otherPed_yAnt;
				pos2posDist = sqrt(dir2otherPed_x*dir2otherPed_x + dir2otherPed_y*dir2otherPed_y);
				dist2otherPed = pos2posDist - radius - otherRadiusArray[i];
				dist_v = distAnt_v;
			}
		}

		if (dist_v < maxDistBehind || dist_v > maxDistInFront) {
			continue;
		}

		dir2otherPed_v_x = velNorm_x*dist_v;
		dir2otherPed_v_y = velNorm_y*dist_v;
		dir2otherPed_w_x = dir2otherPed_x - dir2otherPed_v_x;
		dir2otherPed_w_y = dir2otherPed_y - dir2otherPed_v_y;
		dir2otherPed_v_length_2 = dir2otherPed_v_x*dir2otherPed_v_x + dir2otherPed_v_y*dir2otherPed_v_y;
		dir2otherPed_w_length_2 = dir2otherPed_w_x*dir2otherPed_w_x + dir2otherPed_w_y*dir2otherPed_w_y;

		if (dist_v > 0) {
			//In front
			if ((dir2otherPed_v_length_2/ie_f_2 + dir2otherPed_w_length_2/r_infl_2) > 1) {
				continue;
			}
		} else if ((dir2otherPed_v_length_2/ie_b_2 + dir2otherPed_w_length_2/r_infl_2) > 1) {
			continue;
		}

		dir2otherPed_v_length = sqrt(dir2otherPed_v_length_2);

		if (dist_v > 0){
			if ((otherVelArray_x[i]*dir2otherPed_v_x/dir2otherPed_v_length + otherVelArray_y[i]*dir2otherPed_v_y/dir2otherPed_v_length ) < 0) {
				isOpposing = true;
				isInFront = true;
			} else {
				isOpposing = false;
				isInFront = true;
			}
		}  else {
			isOpposing = false;
			isInFront = false;
		} // End if (dist_v > 0)
		psychForces = calcPedPsychologicalForces(isOpposing, isInFront, dir2otherPed_x/pos2posDist, dir2otherPed_y/pos2posDist, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2, cPlus_0, cMinus_0, a_0, r_0, a_1, r_1, radius, otherRadiusArray[i]);
		force_x += psychForces.x;
		force_y += psychForces.y;
	} // End for loop

	PedForces pedForces = {force_x, force_y};
	return pedForces;
}
