#ifndef CALCPEDFORCES_H   /* Include guard */
#define CALCPEDFORCES_H

PedForces calcPedPhysicalForces(double dist2otherPed, double dir2otherPedNorm_x, double dir2otherPedNorm_y, double otherPedVel_x, double otherPedVel_y, double kappa_0, double kappa_1, double vel_x, double vel_y);
PedForces calcPedPsychologicalForces(bool isOpposing, bool isInFront, double dir2otherPedNorm_x, double dir2otherPedNorm_y, double dir2otherPed_w_x, double dir2otherPed_w_y, double dir2otherPed_v_length_2, double dir2otherPed_w_length_2, double cPlus_0, double cMinus_0, double a_0, double r_0, double a_1, double r_1, double radius, double otherRadius);
PedForces calcPedForces(double pos_x, double pos_y, double vel_x, double vel_y, double radius, double t_A, double kappa_0, double kappa_1, double cPlus_0, double cMinus_0, double a_0, double r_0, double a_1, double r_1, double ie_f_2, double ie_b_2, double r_infl_2, double maxDistBehind, double maxDistInFront, double* otherPosArray_x, double* otherPosArray_y, double* otherVelArray_x, double* otherVelArray_y, double* otherRadiusArray, int otherPedCount);

#endif // CALC_PED_FORCE_H_
