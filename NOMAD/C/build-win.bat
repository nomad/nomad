REM Make sure to have installed winlibs (http://winlibs.com/) and added the winlibs bin folder to your path.
REM For some reason, on some devices you need to add the path to your gcc lib folder to the gcc -shared command: -L <path_to_winbuilds_native>\lib64
REM Example:
REM gcc -shared -o convergeCostMatrix.so convergeCostMatrix.o -L C:\bin\winbuilds_x64_native\lib64

cd %~dp0

gcc -c -Wall -Werror -fpic calcPedForces.c
gcc -shared -o calcPedForces.so calcPedForces.o

gcc -c -Wall -Werror -fpic calcAllPedForces.c
gcc -shared -o calcAllPedForces.so calcAllPedForces.o calcPedForces.o

gcc -c -Wall -Werror -fpic convergeCostMatrix.c
gcc -shared -o convergeCostMatrix.so convergeCostMatrix.o

gcc -c -Wall -Werror -fpic getInteractionData.c
gcc -shared -o getInteractionData.so getInteractionData.o

pause