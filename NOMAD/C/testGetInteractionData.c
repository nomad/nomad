#include <math.h>
#include <stdbool.h> 
#include <stdio.h>

typedef struct ReturnData {
	bool isInteracting, isTouching, isOpposing, isInFront;
	double dir2otherPedNorm_x, dir2otherPedNorm_y, dist2otherPed, dir2otherPed_v_x, dir2otherPed_v_y, dir2otherPed_w_x, dir2otherPed_w_y, dir2otherPed_v_length_2, dir2otherPed_w_length_2, otherVel_x, otherVel_y, otherRadius;	
} ReturnData;

ReturnData getInteractionData(double pos_x, double pos_y, double anticipatedPos_x, double anticipatedPos_y, double velNorm_x, double velNorm_y, double radius, double t_A, double ie_f_2, double ie_b_2, double r_infl_2, double maxDistBehind, double maxDistInFront, double otherPos_x, double otherPos_y, double otherVel_x, double otherVel_y, double otherRadius) {
	ReturnData returnData = {false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0, 0, otherVel_x, otherVel_y, otherRadius};
		
	double x2otherPed = otherPos_x - pos_x;
	double y2otherPed = otherPos_y - pos_y;
	double pos2posDist = sqrt(x2otherPed*x2otherPed + y2otherPed*y2otherPed);
	double dist2otherPed = pos2posDist - radius - otherRadius;

	printf("----- calcVec2OtherPed -----\n");
	printf("x2otherPed = %f\n", x2otherPed);
	printf("y2otherPed = %f\n", y2otherPed);
	printf("pos2posDist = %f\n", pos2posDist);
	printf("dist2otherPed = %f\n\n", dist2otherPed);

	if (dist2otherPed < 0) {
		returnData.isInteracting = true;
		returnData.isTouching = true;
		returnData.dir2otherPedNorm_x = x2otherPed/pos2posDist;
		returnData.dir2otherPedNorm_y = y2otherPed/pos2posDist;
		returnData.dist2otherPed = dist2otherPed;
		return returnData;
	}	
	
	double dist_v = x2otherPed*velNorm_x + y2otherPed*velNorm_y;
	
	printf("----- dist_v -----\n");
	printf("dist_v = %f\n\n", dist_v);

	
	if (dist_v > 0) {
		double x2otherPedAnt = otherPos_x + otherVel_x*t_A - anticipatedPos_x;
		double y2otherPedAnt = otherPos_y + otherVel_y*t_A - anticipatedPos_y;
		double distAnt_v = x2otherPedAnt*velNorm_x + y2otherPedAnt*velNorm_y	;
		if (distAnt_v > 0) {
			x2otherPed = x2otherPedAnt;
			y2otherPed = y2otherPedAnt;
			pos2posDist = sqrt(x2otherPed*x2otherPed + y2otherPed*y2otherPed);
			dist2otherPed = pos2posDist - radius - otherRadius;
			dist_v = distAnt_v;
		}
	}
	
	printf("----- after anticipation -----\n");
	printf("x2otherPed = %f\n", x2otherPed);
	printf("y2otherPed = %f\n", y2otherPed);
	printf("pos2posDist = %f\n", pos2posDist);
	printf("dist2otherPed = %f\n", dist2otherPed);
	printf("dist_v = %f\n\n", dist_v);
	
	if (dist_v < maxDistBehind || dist_v > maxDistInFront) {
		return returnData;
	}
	
	double dir2otherPed_v_x = velNorm_x*dist_v;
    double dir2otherPed_v_y = velNorm_y*dist_v;
    double dir2otherPed_w_x = x2otherPed - dir2otherPed_v_x;
    double dir2otherPed_w_y = y2otherPed - dir2otherPed_v_y;
	double dir2otherPed_v_length_2 = dir2otherPed_v_x*dir2otherPed_v_x + dir2otherPed_v_y*dir2otherPed_v_y;
    double dir2otherPed_w_length_2 = dir2otherPed_w_x*dir2otherPed_w_x + dir2otherPed_w_y*dir2otherPed_w_y;
	
	printf("----- local vecs -----\n");
	printf("dir2otherPed_v_x = %f\n", dir2otherPed_v_x);
	printf("dir2otherPed_v_y = %f\n", dir2otherPed_v_y);
	printf("dir2otherPed_w_x = %f\n", dir2otherPed_w_x);
	printf("dir2otherPed_w_y = %f\n", dir2otherPed_w_y);
	printf("dir2otherPed_v_length_2 = %f\n", dir2otherPed_v_length_2);
	printf("dir2otherPed_w_length_2 = %f\n\n", dir2otherPed_w_length_2);
	
	if (dist_v > 0) {
		//In front
		if ((dir2otherPed_v_length_2/ie_f_2 + dir2otherPed_w_length_2/r_infl_2) > 1) {
			printf("Outside influence area (in front)\n\n");
			return returnData;			
		}
    } else if ((dir2otherPed_v_length_2/ie_b_2 + dir2otherPed_w_length_2/r_infl_2) > 1) {
		printf("Outside influence area (in back)\n\n");
        return returnData;
	}
	
	double dir2otherPed_v_length = sqrt(dir2otherPed_v_length_2);

	if (dist_v > 0){
		returnData.isInFront = true;
		if ((otherVel_x*dir2otherPed_v_x/dir2otherPed_v_length + otherVel_y*dir2otherPed_v_y/dir2otherPed_v_length ) < 0) {
			returnData.isOpposing = true;
		}
	} 
	returnData.isInteracting = true;
	returnData.dir2otherPedNorm_x = x2otherPed/pos2posDist;
	returnData.dir2otherPedNorm_y = y2otherPed/pos2posDist;
	returnData.dir2otherPed_v_x = dir2otherPed_v_x;
	returnData.dir2otherPed_v_y = dir2otherPed_v_y;
	returnData.dir2otherPed_w_x = dir2otherPed_w_x;
	returnData.dir2otherPed_w_y = dir2otherPed_w_y;
	returnData.dir2otherPed_v_length_2 = dir2otherPed_v_length_2;
	returnData.dir2otherPed_w_length_2 = dir2otherPed_w_length_2;
	
	return returnData;
}

int main() {
	double pos_x, pos_y, anticipatedPos_x, anticipatedPos_y, velNorm_x, velNorm_y, radius, t_A, ie_f_2, ie_b_2, r_infl_2, maxDistBehind, maxDistInFront, otherPos_x, otherPos_y, otherVel_x, otherVel_y, otherRadius;
	
	pos_x = 4.5;
	pos_y = 3.25;
	anticipatedPos_x = 4.75;
	anticipatedPos_y = 3.25;
	velNorm_x = 1;
	velNorm_y = 0;
	radius = 0.3;
	t_A = 0.25;
	ie_f_2 = 5*5;
	ie_b_2 = 1*1;
	r_infl_2 = 5*5;
	maxDistBehind = -5;
	maxDistInFront = 5;
	otherPos_x = 4.5;
	//otherPos_x = 2.75;
	otherPos_y = 2.75; 
	otherVel_x = 1;
	otherVel_y = 0.0;
	otherRadius = 0.3;	
	
	printf("----- INPUT -----\n");
	printf("pos_x = %f\n", pos_x);
	printf("pos_y = %f\n", pos_y);
	printf("anticipatedPos_x = %f\n", anticipatedPos_x);
	printf("anticipatedPos_y = %f\n", anticipatedPos_y);
	printf("velNorm_x = %f\n", velNorm_y);
	printf("velNorm_y = %f\n", velNorm_y);
	printf("radius = %f\n", radius);
	printf("t_A = %f\n", t_A);
	printf("ie_f_2 = %f\n", ie_f_2);
	printf("ie_b_2 = %f\n", ie_b_2);
	printf("r_infl_2 = %f\n", r_infl_2);
	printf("otherPos_x = %f\n", otherPos_x);
	printf("otherPos_y = %f\n", otherPos_y);
	printf("otherVel_x = %f\n", otherVel_x);
	printf("otherVel_y = %f\n", otherVel_y);
	printf("otherRadius = %f\n\n", otherRadius);
	
	ReturnData returnData = getInteractionData(pos_x, pos_y, anticipatedPos_x, anticipatedPos_y, velNorm_x, velNorm_y, radius, t_A, ie_f_2, ie_b_2, r_infl_2, maxDistBehind, maxDistInFront, otherPos_x, otherPos_y, otherVel_x, otherVel_y, otherRadius);
	
	if(!returnData.isInteracting){
		printf("----- OUTPUT -----\n");
		printf("Not interacting\n");
		return 0;
	}
	
	printf("----- OUTPUT -----\n");
	printf("isTouching = %s\n", returnData.isTouching ? "true" : "false");
	printf("isOpposing = %s\n", returnData.isOpposing ? "true" : "false");
	printf("isInFront = %s\n", returnData.isInFront ? "true" : "false");
	printf("dir2otherPedNorm_x = %f\n", returnData.dir2otherPedNorm_x);
	printf("dir2otherPedNorm_y = %f\n", returnData.dir2otherPedNorm_y);
	printf("dist2otherPed = %f\n", returnData.dist2otherPed);
	printf("dir2otherPed_v_x = %f\n", returnData.dir2otherPed_v_x);
	printf("dir2otherPed_v_y = %f\n", returnData.dir2otherPed_v_y);
	printf("dir2otherPed_w_x = %f\n", returnData.dir2otherPed_w_x);
	printf("dir2otherPed_w_y = %f\n", returnData.dir2otherPed_w_y);
	printf("dir2otherPed_v_length_2 = %f\n", returnData.dir2otherPed_v_length_2);
	printf("dir2otherPed_w_length_2 = %f\n", returnData.dir2otherPed_w_length_2);
	printf("otherVel_x = %f\n", returnData.otherVel_x);
	printf("otherVel_y = %f\n", returnData.otherVel_y);
	printf("otherRadius = %f\n", returnData.otherRadius);
	
	return 0;
}