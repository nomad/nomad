#!/bin/sh
# Make sure to make this script executable before running:
# chmod +x build-mac.sh
# The build:
# ./build-mac.sh

gcc -c -Wall -Werror -fpic calcPedForces.c
gcc -dynamiclib -o calcPedForces.dylib calcPedForces.o

gcc -c -Wall -Werror -fpic calcAllPedForces.c
gcc -dynamiclib -o calcAllPedForces.dylib calcAllPedForces.o calcPedForces.o

gcc -c -Wall -Werror -fpic convergeCostMatrix.c
gcc -dynamiclib -o convergeCostMatrix.dylib convergeCostMatrix.o

gcc -c -Wall -Werror -fpic getInteractionData.c
gcc -dynamiclib -o getInteractionData.dylib getInteractionData.o