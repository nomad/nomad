/*
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct ConvergenceInfo {
	int iterationCount;
	bool errorIsMinimal;
	double error, dError;
} ConvergenceInfo;

typedef struct ErrorInfo {
	bool errorIsMinimal;
	double error, dError;
} ErrorInfo;


double max(double num1, double num2) {
    return (num1 > num2 ) ? num1 : num2;
}

double min(double num1, double num2) {
    return (num1 < num2 ) ? num1 : num2;
}

int getIndex(int xInd, int yInd, int yCount, int xOrgOffset, int yOrgOffset, int* indSave) {
	int ind = (xInd + xOrgOffset)*yCount + (yInd + yOrgOffset);
	*indSave = ind;
	return ind;
}

ErrorInfo calcError(double* costMatrix, double* previousCostMatrix, int* keys, int keyCount, double previousError, double previousDError, double errorThreshold, double dErrorThreshold, int yCount, int xOrgOffset, int yOrgOffset, int* indSave) {
	double error, dError;

	error = 0;
	int i, ind, xInd, yInd, keyInd;
	for (i = 0; i < keyCount; ++i) {
		keyInd = i*2;
		xInd = keys[keyInd];
		yInd = keys[keyInd+1];
		ind = getIndex(xInd, yInd, yCount, xOrgOffset, yOrgOffset, indSave);
		error += fabs(1 - costMatrix[ind]/previousCostMatrix[ind]);
	}

	error = error/keyCount;

	dError = 0.8*previousDError + 0.2*fabs(error - previousError);
    bool errorIsMinimal = error <  errorThreshold || dError <  dErrorThreshold;

	ErrorInfo errorInfo = {errorIsMinimal, error, dError};
	return errorInfo;
}

void copyArray(double* srcArray, double* destArray, int arrayLength) {
	int i;
	for (i = 0; i < arrayLength; ++i) {
		destArray[i] = srcArray[i];
	}
}

double getCostFromMatrix(double* costArray, int xIndBase, int yIndBase, int xOffset, int yOffset, int xCount, int yCount, int xOrgOffset, int yOrgOffset, int* xIndSave, int* yIndSave, int* indSave) {
	int xInd = xIndBase + xOffset;
	int yInd = yIndBase + yOffset;
	*xIndSave = xInd;
	*yIndSave = yInd;
	if (xInd < -xOrgOffset || xInd > xCount-xOrgOffset || yInd < -yOrgOffset || yInd > yCount-yOrgOffset) {
		return costArray[getIndex(xIndBase, yIndBase, yCount, xOrgOffset, yOrgOffset, indSave)];
	}

	return costArray[getIndex(xInd, yInd, yCount, xOrgOffset, yOrgOffset, indSave)];
}


ConvergenceInfo convergeCostMatrix(double* costMatrix, double* walkingCostMatrix, double* previousCostMatrix, int* keys, int keyCount, int xCount, int yCount, int xOrgOffset, int yOrgOffset, int maxIterationCount, double errorThreshold, double dErrorThreshold, double cellSize, double defDesiredSpeed, double c3, double nu, double eta, double dtt, int* status, int* iterationIndSave, int* xIndSave, int* yIndSave, int* indSave) {
	*status = 1;
	int iterationInd = 0;
	double dError = 1e10;
	double error = 1e10;
	int entryCount = xCount*yCount;
	*status = 2;
	bool errorIsMinimal = false;

	double cellArea = cellSize*cellSize;

	//int* key;
	double cost, leftCost, rightCost, topCost, bottomCost, dx_f, dx_b, dy_f, dy_b, d2x, d2y;
	double u_x_f, u_x_b, u_y_f, u_y_b, u_tot, e_x_f, e_x_b, e_y_f, e_y_b;
	double h_x_f, h_x_b, h_y_f, h_y_b, h_x, h_y;
	int i, xIndBase, yIndBase, ind, keyInd;
	*status = 3;

	while (!errorIsMinimal && iterationInd <= maxIterationCount) {
		*status = 4;
		copyArray(costMatrix, previousCostMatrix, entryCount);
		*status = 5;

		for (i = 0; i < keyCount; ++i) {
			keyInd = i*2;
			xIndBase = keys[keyInd];
			yIndBase = keys[keyInd+1];
			ind = getIndex(xIndBase, yIndBase, yCount, xOrgOffset, yOrgOffset, indSave);
			*status = i*10 + 10;
			cost = previousCostMatrix[ind];
			*status = i*10 + 11;
			leftCost = getCostFromMatrix(previousCostMatrix, xIndBase, yIndBase, -1, 0, xCount, yCount, xOrgOffset, yOrgOffset, xIndSave, yIndSave, indSave);
			*status = i*10 + 12;
			rightCost = getCostFromMatrix(previousCostMatrix, xIndBase, yIndBase, 1, 0, xCount, yCount, xOrgOffset, yOrgOffset, xIndSave, yIndSave, indSave);
			*status = i*10 + 13;
			topCost = getCostFromMatrix(previousCostMatrix, xIndBase, yIndBase, 0, 1, xCount, yCount, xOrgOffset, yOrgOffset, xIndSave, yIndSave, indSave);
			*status = i*10 + 14;
			bottomCost = getCostFromMatrix(previousCostMatrix, xIndBase, yIndBase, 0, -1, xCount, yCount, xOrgOffset, yOrgOffset, xIndSave, yIndSave, indSave);
			*status = i*10 + 15;

			// Calculate the forward and backward differentials
			dx_f = (rightCost - cost)/cellSize;
			dx_b = (cost - leftCost)/cellSize;
			dy_f = (topCost - cost)/cellSize;
			dy_b = (cost - bottomCost)/cellSize;

			// Calculate the central differentials
			d2x = (rightCost - 2*cost + leftCost)/cellArea;
			d2y = (topCost - 2*cost + bottomCost)/cellArea;

			// Calculate the optimal speeds
			u_x_f = (dx_f < 0) ? -dx_f/c3 : 0;
			u_x_b = (dx_b > 0) ? dx_b/c3 : 0;
			u_y_f = (dy_f < 0) ? -dy_f/c3 : 0;
			u_y_b = (dy_b > 0) ? dy_b/c3 : 0;

			u_tot = max(0.001, sqrt((u_x_f + u_x_b)*(u_x_f + u_x_b) + (u_y_f + u_y_b)*(u_y_f + u_y_b)));

			e_x_f = defDesiredSpeed*(u_x_f/u_tot);
			e_x_b = defDesiredSpeed*(u_x_b/u_tot);
			e_y_f = defDesiredSpeed*(u_y_f/u_tot);
			e_y_b = defDesiredSpeed*(u_y_b/u_tot);

			if (e_x_f < u_x_f) {u_x_f = e_x_f;}
			if (e_x_b < u_x_b) {u_x_b = e_x_b;}
			if (e_y_f < u_y_f) {u_y_f = e_y_f;}
			if (e_y_b < u_y_b) {u_y_b = e_y_b;}

			h_x_f = 0.5*c3*u_x_f*u_x_f + u_x_f*dx_f;
			h_x_b = 0.5*c3*u_x_b*u_x_b - u_x_b*dx_b;
			h_y_f = 0.5*c3*u_y_f*u_y_f + u_y_f*dy_f;
			h_y_b = 0.5*c3*u_y_b*u_y_b - u_y_b*dy_b;

			h_x = min(h_x_f, h_x_b);
			h_y = min(h_y_f, h_y_b);

			*status = i*10 + 16;
			costMatrix[ind] = cost + (nu*(d2x + d2y) + walkingCostMatrix[ind] + h_x + h_y - eta*cost)*dtt;
			*status = i*10 + 17;
		}
		*status = 6;

		ErrorInfo errorInfo = calcError(costMatrix, previousCostMatrix, keys, keyCount, error, dError, errorThreshold, dErrorThreshold, yCount, xOrgOffset, yOrgOffset, indSave);
		errorIsMinimal = errorInfo.errorIsMinimal;
		error = errorInfo.error;
		dError = errorInfo.dError;
		*status = 7;
		iterationInd++;
		*iterationIndSave = iterationInd;
	}

	ConvergenceInfo convergenceInfo = {iterationInd, errorIsMinimal, error, dError};
	return convergenceInfo;
}
