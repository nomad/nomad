#!/bin/bash

gcc -c -Wall -Werror -fpic calcPedForces.c
gcc -shared -o calcPedForces.so calcPedForces.o

gcc -c -Wall -Werror -fpic calcAllPedForces.c
gcc -shared -o calcAllPedForces.so calcAllPedForces.o calcPedForces.o

gcc -c -Wall -Werror -fpic convergeCostMatrix.c
gcc -shared -o convergeCostMatrix.so convergeCostMatrix.o

gcc -c -Wall -Werror -fpic getInteractionData.c
gcc -shared -o getInteractionData.so getInteractionData.o