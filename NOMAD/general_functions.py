""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from _collections_abc import dict_values
from dataclasses import dataclass, field
from enum import Enum, auto
from math import sqrt, inf, floor
from pathlib import Path

from shapely import GEOSException, unary_union
from shapely.geometry.linestring import LineString
from shapely.geometry.multilinestring import MultiLineString
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.point import Point
from shapely.geometry.polygon import Polygon

import NOMAD
from NOMAD.constants import OPENTURNS_DISTR_TYPE, SCIPY_DISTR_TYPE, \
    NUMPY_DISTR_TYPE, VALUE_TYPE, STATIC_VALUE, DISTR_VALUE
from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
from NOMAD.vectors import Vector2D
import numpy as np
import openturns as ot
import scipy.stats as stats


# ==============================================================================================
# ==============================================================================================
class DurationType(Enum):
    DAYS = auto()
    HOURS = auto()
    MINUTES = auto()
    SECONDS = auto()
    
SEC2DAY = 1/(3600*24)
SEC2HOUR = 1/3600
SEC2MIN = 1/60

DAY2SEC = 3600*24
HOUR2SEC = 3600
MIN2SEC = 60
    
def getDurationString(durationInSeconds):
    days = getDurationIn(durationInSeconds, DurationType.DAYS)
    hours = getDurationIn(durationInSeconds, DurationType.HOURS, days)
    minutes = getDurationIn(durationInSeconds, DurationType.MINUTES, days*24+hours)
    seconds = getDurationIn(durationInSeconds, DurationType.SECONDS, days*24*60+hours*60+minutes)
    if days > 2:
        return f'{days} days, {hours} hours, {minutes} minutes and {seconds:02.0f} seconds'
    hours = getDurationIn(durationInSeconds, DurationType.HOURS)
    if hours > 2:
        return f'{hours:d}:{minutes:02d}:{seconds:02.0f} [hh:mm:ss]'
    minutes = getDurationIn(durationInSeconds, DurationType.MINUTES)
    if minutes > 2:
        return f'{minutes:02d}:{seconds:02.0f} [mm:ss]'
    
    return f'{durationInSeconds:02.0f} seconds'
    
    
def getDurationIn(durationInSeconds, durationType, parentDuration=0):
    if durationType == DurationType.DAYS:
        return floor(durationInSeconds*SEC2DAY)
    if durationType == DurationType.HOURS:
        return floor((durationInSeconds - parentDuration*DAY2SEC)*SEC2HOUR)
    if durationType == DurationType.MINUTES:
        return floor((durationInSeconds - parentDuration*HOUR2SEC)*SEC2MIN)
    if durationType == DurationType.SECONDS:
        return durationInSeconds - parentDuration*MIN2SEC
    
def doListCeck(objectList):
    if isinstance(objectList, dict):
        objectList = list(objectList.values())
    elif isinstance(objectList, dict_values):
        objectList = list(objectList)
    elif not isinstance(objectList, (list, tuple)):
        objectList = [objectList,]
    if isinstance(objectList, tuple):
        objectList = list(objectList)

    return objectList
    
def get_relative_path_to(main_path, reference_path):
    if reference_path.is_file():
        reference_path = reference_path.parent
    
    if main_path.is_relative_to(reference_path):
        return main_path.relative_to(reference_path)
        
    for parent_ind, parent_dir in enumerate(reference_path.parents):
        if main_path.is_relative_to(parent_dir):
            relative_path = main_path.relative_to(parent_dir)
            for _ in range(parent_ind + 1):
                relative_path = Path('../').joinpath(relative_path)
            return relative_path
                
    raise Exception('The provided paths have no common parent')

# ==============================================================================================
# ==============================================================================================

BUFFER_TRANS = np.asarray([[1,1],[-1,-1]])

class SpawnableLine(ShapelyGeomClassWrapper): 
    
    def __init__(self, ID, lineCoords):
        ShapelyGeomClassWrapper.__init__(self, ID)
        if not isSimpleLine(lineCoords):
            raise ValueError('A line source can only be a simple line which can be described by the equation y = ax + b!')

        self._geometry = LineString(lineCoords)
        
        self.lineGeometry = LineString(lineCoords)
        self.baseLineSegments = None
        self.lineBase_x = lineCoords[0][0]
        self.lineBase_y = lineCoords[0][1]
        self.lineVec_x = lineCoords[1][0] - lineCoords[0][0]
        self.lineVec_y = lineCoords[1][1] - lineCoords[0][1]
        self.lineLength2 = self.lineVec_x*self.lineVec_x + self.lineVec_y*self.lineVec_y
  
    def createBaseLineSegments(self, parameters, gridManager, walkLevelID):
        self._maxPedRadius = parameters.MAX_PED_RADIUS
        self._maxPedRadius2 = parameters.MAX_PED_RADIUS2
        self._maxPedDiameter = parameters.MAX_PED_DIAMETER
        
        bufferedLine = self._geometry.buffer(self._maxPedRadius)
        obstacles2check = []
        obstaclesChecked = []
        gridCells = gridManager.getGridCellsCoveringPolygon(bufferedLine, walkLevelID)
        for gridCell in gridCells:
            for obstacle in gridCell.obstaclesInCell:
                if obstacle in obstaclesChecked:
                    continue
                obstaclesChecked.append(obstacle)
                if obstacle.within(bufferedLine) or obstacle.intersects(bufferedLine):
                    obstacles2check.append(obstacle)

        for obstacle in obstacles2check:
            bufferedObstacle = obstacle.geometry.buffer(self._maxPedRadius)
            self.lineGeometry = self.lineGeometry.difference(bufferedObstacle)

        if self.lineGeometry.is_empty:
            raise Exception('The source {} has no valid points to spawn pedestrians!'.format(self.ID))

        if isinstance(self.lineGeometry, MultiLineString):
            baseLineSegments = list(self.lineGeometry.geoms)
        else:
            baseLineSegments = [self.lineGeometry]

        lines2add = []
        for lineSegment in baseLineSegments:
            xy, _, buffer = getLineVectorAndBuffer(lineSegment, self._maxPedRadius)
            newlineCoords = xy.transpose() +  BUFFER_TRANS*buffer
            lines2add.append(newlineCoords)
        self.baseLineSegments = MultiLineString(lines2add)        

    def getAvailableLineSegments(self, maxPedRadius, maxPedRadius2, maxPedDiameter):
        pedBufferedPoints = []
        for gridCell in self.gridCellsInNeighborhood:
            for ped in gridCell.pedsInCell:
                minDist2 = getMinDist2Line2(ped.pos_x, ped.pos_y, self.lineBase_x, self.lineBase_y, self.lineVec_x, self.lineVec_y, self.lineLength2)
                if minDist2 >= maxPedRadius2:
                    continue
                
                pedBufferedPoints.append(Point((ped.pos_x, ped.pos_y)).buffer(ped.radius))

        pedBufferPointsShape = MultiPolygon(pedBufferedPoints)
        try:
            lineSegments = self.baseLineSegments.difference(pedBufferPointsShape)
        except GEOSException:
            lines = [] 
            for pol in pedBufferPointsShape.geoms:
                lines.append( self.baseLineSegments.difference(pol))
            lineSegments = unary_union(lines)
            
        if isinstance(lineSegments, LineString):
            lineSegments = MultiLineString([lineSegments])
            
        segmentCount = len(lineSegments.geoms)
        lineSegmentArray = np.zeros((segmentCount, 6), dtype=float)

        # Crop lines and put them in the array
        for ii, lineSegment in enumerate(lineSegments.geoms):
            lineCoords, lineVec, buffer = getLineVectorAndBuffer(lineSegment, maxPedRadius)
            lineSegmentArray[ii,0:2] = lineCoords[:,0] + buffer
            lineSegmentArray[ii,2:4] = lineVec - 2*buffer
            lineSegmentArray[ii,4] = lineSegment.length - maxPedDiameter
            lineSegmentArray[ii,5] = lineSegment.length/maxPedDiameter

        returnInd = np.arange(segmentCount)
        NOMAD.NOMAD_RNG.shuffle(returnInd)
        return lineSegmentArray[returnInd,:]
    
    def getPlacementLocations(self, ped2placeCount):
        lineSegmentArray = self.getAvailableLineSegments(self._maxPedRadius, self._maxPedRadius2, self._maxPedDiameter)
        availableLocationCount = int(np.sum(np.floor(lineSegmentArray[:,5])))
        maxPeds2placeCount = min(availableLocationCount, ped2placeCount)

        locations = self.getPlacementLocationsInSegments(lineSegmentArray, maxPeds2placeCount, self._maxPedDiameter)

        return locations, maxPeds2placeCount
    
    def getRandomPlacementLocationsInSegments(self, lineSegmentArray, placementCount, maxPedDiameter):
        lineSegmentCount = lineSegmentArray.shape[0]
        pedsPerSegment = np.zeros(lineSegmentCount, dtype=int)
        if lineSegmentCount == 1:
            pedsPerSegment[0] = placementCount
        else:
            spacePerPedPerSegement = lineSegmentArray[:,4].copy()
            for _ in range(placementCount):
                segmentInd = np.argmax(spacePerPedPerSegement)
                pedsPerSegment[segmentInd] += 1
                spacePerPedPerSegement[segmentInd] = (lineSegmentArray[segmentInd,4] - pedsPerSegment[segmentInd]*maxPedDiameter)/(pedsPerSegment[segmentInd] + 1)

        locations = []
        for ii in range(lineSegmentCount):
            if pedsPerSegment[ii] == 1:
                locations.append(getRandomLocOnLineSegment(lineSegmentArray[ii,0], lineSegmentArray[ii,1], lineSegmentArray[ii,2], lineSegmentArray[ii,3]))
            elif pedsPerSegment[ii] > 1:
                effectiveLengthFactor = (lineSegmentArray[ii,4] - maxPedDiameter*(pedsPerSegment[ii] - 1))/lineSegmentArray[ii,4]
                lineVec_x = lineSegmentArray[ii,2]*effectiveLengthFactor/pedsPerSegment[ii]
                lineVec_y = lineSegmentArray[ii,3]*effectiveLengthFactor/pedsPerSegment[ii]

                maxDiameterfactor = maxPedDiameter/lineSegmentArray[ii,4]
                maxPedDiameter_x = maxDiameterfactor*lineSegmentArray[ii,2]
                maxPedDiameter_y = maxDiameterfactor*lineSegmentArray[ii,3]

                lineBase_x = lineSegmentArray[ii,0]
                lineBase_y = lineSegmentArray[ii,1]

                for _ in range(pedsPerSegment[ii]):
                    locations.append(getRandomLocOnLineSegment(lineBase_x, lineBase_y, lineVec_x, lineVec_y))
                    lineBase_x += lineVec_x + maxPedDiameter_x
                    lineBase_y += lineVec_y + maxPedDiameter_y

        return locations
    
# ==============================================================================================
# ==============================================================================================
class RectangleFilledWithPoints(ShapelyGeomClassWrapper):
    
    def __init__(self, ID, rectangleCoords, directionVec=None, focusPoint=None):
        '''
        Constructor
        '''
        super().__init__(ID)
        if not polygonIsRectangle(rectangleCoords):
            raise ValueError('A RectangleFilledWithPoints can only be a rectangular polygon!')
        self._geometry = Polygon(rectangleCoords, None)
        self._directionVec = directionVec
        self._focusPoint = focusPoint
        
        if directionVec is not None and focusPoint is not None:
            raise ValueError('A RectangleFilledWithPoints can only have either a directionVec or a focusPoint, not both!')
       
    def createPoints(self, distBetweenPoints):
        self._distBetweenPoints = distBetweenPoints
        self._createPoints(distBetweenPoints)                
        
        if self._focusPoint is not None:
            dist2focusPoint2 = (self._centerPoints[:,0] - self._focusPoint[0])**2 + (self._centerPoints[:,1] - self._focusPoint[1])**2 
            self._pointsOrder = tuple(np.argsort(dist2focusPoint2))            
        elif self._directionVec is not None:
            self._pointsOrder = self._getRowBasedPointOrder()
        else:
            self._pointsOrder = None
            
        self._freePoints = set(range(self._maxPointCount))
        self._occupiedPoints = set()
    
    def occupyPoint(self, pointInd):
        self._freePoints.remove(pointInd)
        self._occupiedPoints.add(pointInd)

    def freePoint(self, pointInd):
        self._occupiedPoints.remove(pointInd)
        self._freePoints.add(pointInd)
            
    def getRandomFreePoint(self):
        if len(self._freePoints) == 0:
            return None, None

        return self._getRandomPointFromSet(self._freePoints)

    def _getRandomPointFromSet(self, pointSet):
        listInd = NOMAD.NOMAD_RNG.integers(0, len(pointSet))
        pointInd = list(pointSet)[listInd]
        return self._centerPoints[pointInd,:], pointInd

    def getOptimalFreePoint(self):
        if len(self._freePoints) == 0:
            return None, None
        
        if self._pointsOrder is None:
            return self.getRandomFreePoint()
        
        if self._focusPoint is not None:
            for pointInd in self._pointsOrder:
                if pointInd in self._freePoints:
                    return self._centerPoints[pointInd,:], pointInd
        else:
            return self._getOptimalPoint(self._pointsOrder, 0)
    
    def _getOptimalPoint(self, order, baseInd):
        if isinstance(order[baseInd], tuple):
            point, pointInd = self._getOptimalPoint(order[baseInd], 0)
            if point is None:
                return self._getOptimalPoint(order, baseInd+1)
            else:
                return point, pointInd 
                
        freePoints = self._freePoints.intersection(set(order))
        if len(freePoints) == 0:
            return None, None

        return self._getRandomPointFromSet(freePoints)
    
    def _createPoints(self, distBetweenPoints):
        ll = self._geometry.exterior.coords[0]
        lr = self._geometry.exterior.coords[1]
        ul = self._geometry.exterior.coords[3]
        widthVec = Vector2D(lr[0] - ll[0], lr[1] - ll[1])
        heightVec = Vector2D(ul[0] - ll[0], ul[1] - ll[1])
        
        height = heightVec.length
        width = widthVec.length
        self._heightVec = heightVec.getNormalized()
        self._widthVec = widthVec.getNormalized()

        self._maxRowCount = floor(height/distBetweenPoints) - 1
        self._maxColumnCount = floor(width/distBetweenPoints) - 1
        
        self._maxPointCount = self._maxRowCount*self._maxColumnCount
        self._centerPoints = np.zeros((self._maxPointCount, 2), dtype=float)
        
        heightBuffer = (height - distBetweenPoints*(self._maxRowCount + 1))/2
        widthBuffer = (width - distBetweenPoints*(self._maxColumnCount + 1))/2
       
        startPoint = Vector2D(ll[0] + widthBuffer + distBetweenPoints, 
                              ll[1] + heightBuffer + distBetweenPoints)
        pointInd = 0
        for rowInd in range(self._maxRowCount):
            for colInd in range(self._maxColumnCount): 
                centerPoint = startPoint + self._heightVec*distBetweenPoints*rowInd \
                                + self._widthVec*distBetweenPoints*colInd
                self._centerPoints[pointInd,0] = centerPoint.x
                self._centerPoints[pointInd,1] = centerPoint.y
                pointInd += 1
        
    def _getRowBasedPointOrder(self):
        pointOrder = []
        if not isinstance(self._directionVec, Vector2D):
            self._directionVec = Vector2D(self._directionVec[0], self._directionVec[1])
        self._directionVec = self._directionVec.getNormalized()
        if self._directionVec.hasSameDirectionAsOther(self._widthVec):
            for ii in range(self._maxColumnCount):
                pointOrder.append(tuple(range(self._maxColumnCount - 1 - ii,
                                               self._maxRowCount*self._maxColumnCount - ii, 
                                               self._maxColumnCount)))            
        elif self._directionVec.hasOppositeDirectionAsOther(self._widthVec):
            for ii in range(self._maxColumnCount-1, -1, -1):
                pointOrder.append(set(range(self._maxColumnCount - 1 - ii,
                                               self._maxRowCount*self._maxColumnCount - ii, 
                                               self._maxColumnCount)))            
        elif self._directionVec.hasSameDirectionAsOther(self._heightVec):
            for ii in range(self._maxRowCount-1, -1, -1):
                pointOrder.append(tuple(range(ii*self._maxColumnCount, (ii + 1)*self._maxColumnCount)))            
        elif self._directionVec.hasOppositeDirectionAsOther(self._heightVec):
            for ii in range(self._maxRowCount):
                pointOrder.append(tuple(range(ii*self._maxColumnCount, (ii + 1)*self._maxColumnCount)))            
        else:
            raise ValueError('The direction vector is not orthogonal to any of the rectangle sides!')
        
        return tuple(pointOrder)
    
# ==============================================================================================
# ==============================================================================================


def isSimpleLine(coords):
    if len(coords) != 2:
        return False

    return True

def getLineVectorAndBuffer(lineSegment, bufferSize):
    xy = np.asarray(lineSegment.xy)
    lineVec = xy[:,1] - xy[:,0]
    buffer = lineVec*bufferSize/lineSegment.length
    
    return xy, lineVec, buffer

def polygonIsRectangle(coords):
    if not (len(coords) == 4 or (len(coords) == 5 and coords[0] == coords[-1])):
        return False

    sideLengths = []
    pointIndices = [0,1,2,3,0]
    for ii in range(4):
        p1 = coords[pointIndices[ii]]
        p2 = coords[pointIndices[ii+1]]
        sideLengths.append(sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2))

    return abs(sideLengths[0] - sideLengths[2]) < 1e-10 and abs(sideLengths[1] - sideLengths[3]) < 1e-6

def getRectangleFromLine(lineCoords, buffer, sidesTouchObstacles=(False, False)):
    pass

def getRandomLocOnLineSegment(lineBase_x, lineBase_y, lineVec_x, lineVec_y):
    multiFactor = NOMAD.NOMAD_RNG.uniform()
    return (lineBase_x + lineVec_x*multiFactor, lineBase_y + lineVec_y*multiFactor)

def getMinDist2Line(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2):
    return sqrt(getMinDist2Line2(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2))

def getMinDist2Line2(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2):
    closestPoint_x, closestPoint_y = getClosestPointOnLine(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2)

    return (point_x - closestPoint_x)**2 + (point_y - closestPoint_y)**2

def getClosestPointOnLine(point_x, point_y, lineBase_x, lineBase_y, lineVec_x, lineVec_y, lineLength2):
    t0 = (lineVec_x*(point_x - lineBase_x) + lineVec_y*(point_y - lineBase_y))/lineLength2
    t0 = 0 if t0 < 0 else t0
    t0 = 1 if t0 > 1 else t0

    return lineBase_x + lineVec_x*t0, lineBase_y + lineVec_y*t0

# ===============================================================================================
# ===============================================================================================
@dataclass(frozen=True)
class Parameter():

    def getValue(self, size=None):
        raise NotImplementedError()
    
    def getValues(self, size):
        raise NotImplementedError()
   
    def getMaxValue(self):
        raise NotImplementedError()
         
    def getMinValue(self):
        raise NotImplementedError()

    def getMean(self):
        raise NotImplementedError() 
      

@dataclass(frozen=True)
class StaticValue(Parameter):
    value: float

    def getValue(self, size=None):
        if size is not None:
            return self.getValues(size)
        
        return self.value
    
    def getValues(self, size):
        return np.ones(size)*self.value
        
    def getMaxValue(self):
        return self.value
         
    def getMinValue(self):
        return self.value

    def getMean(self):
        return self.value
    
    def getValueDictRepr(self):
        return {VALUE_TYPE: STATIC_VALUE, 'value':self.value}
        
@dataclass(frozen=True)
class Distribution(Parameter):
    distrType: str
    distrName: str
    distrArgs: tuple
    
    min: float = -inf
    max: float = inf
    
    lowerTrunc: float = None
    upperTrunc: float = None
    
    otDistr: ot.Distribution = None
    scipyDistr: stats._distn_infrastructure.rv_frozen = None
    numpyDistr: callable = None
    
    _getValueFromDistribution: callable = field(init=False)
    _getValuesFromDistribution: callable = field(init=False)
    _getMaxValue: callable = field(init=False)
    _getMinValue: callable = field(init=False)
    _getMeanValue: callable = field(init=False)
    
    def getValueDictRepr(self):
        return {VALUE_TYPE: DISTR_VALUE, 
                'distrType':self.distrType,
                'distrName':self.distrName,
                'distrArgs':self.distrArgs,
                'min':self.min,
                'max':self.max,
                'lowerTrunc':self.lowerTrunc,
                'upperTrunc':self.upperTrunc}
    
    def __post_init__(self):
        if self.distrType == OPENTURNS_DISTR_TYPE:
            self._createOtDistr()
            super().__setattr__('_getValueFromDistribution', self._getValueFromOtDistribution)
            super().__setattr__('_getValuesFromDistribution', self._getValuesFromOtDistribution)
            super().__setattr__('_getMaxValue', self._getMaxFromOtDistribution)
            super().__setattr__('_getMinValue', self._getMinFromOtDistribution)
            super().__setattr__('_getMeanValue', self._getMeanFromOtDistribution)
        elif self.distrType == SCIPY_DISTR_TYPE:
            self._createScipyDistr()
            super().__setattr__('_getValueFromDistribution', self._getValueFromScipyDistribution)
            super().__setattr__('_getValuesFromDistribution', self._getValuesFromScipyDistribution)
            super().__setattr__('_getMaxValue', self._getMaxFromScipyDistribution)
            super().__setattr__('_getMinValue', self._getMinFromScipyDistribution)
            super().__setattr__('_getMeanValue', self._getMeanFromScipyDistribution)
        elif self.distrType == NUMPY_DISTR_TYPE:
            self._createNumpyDistr()
            super().__setattr__('_getValueFromDistribution', self._getValueFromNumpyDistribution)
            super().__setattr__('_getValuesFromDistribution', self._getValuesFromNumpyDistribution)
            super().__setattr__('_getMaxValue', self._getMaxFromNumpyDistribution)
            super().__setattr__('_getMinValue', self._getMinFromNumpyDistribution)
            super().__setattr__('_getMeanValue', self._getMeanFromNumpyDistribution)  
        else:
            raise Exception(f'Unsupported distribution type "{self.distrType}"')
   
   
    def _createOtDistr(self):
        otDistr = getattr(ot, self.distrName)(*self.distrArgs)
        if self.lowerTrunc is not None and self.upperTrunc is not None:
            otDistr = ot.TruncatedDistribution(otDistr, self.lowerTrunc, self.upperTrunc)
        elif self.lowerTrunc is not None:
            otDistr = ot.TruncatedDistribution(otDistr, self.lowerTrunc,ot.TruncatedDistribution.LOWER)
        elif self.upperTrunc is not None:
            otDistr = ot.TruncatedDistribution(otDistr, self.upperTrunc, ot.TruncatedDistribution.UPPER)

        super().__setattr__('otDistr', otDistr)
                              
    def _createScipyDistr(self):
        super().__setattr__('scipyDistr', getattr(stats, self.distrName)(*self.distrArgs))

    def _createNumpyDistr(self):
        super().__setattr__('numpyDistr', getattr(NOMAD.NOMAD_RNG, self.distrName))
                
# ============================================================        
    
    def getValue(self, size=None):
        if size is not None:
            return self.getValues(size)
        
        value = self._getValueFromDistribution()
        return self._boundValue(value)

    def getValues(self, size):
        values = self._getValuesFromDistribution(size)
        return self._boundValues(values)
        
    def _getValueFromOtDistribution(self):
        return self.otDistr.getSample(1).asPoint()[0]

    def _getValuesFromOtDistribution(self, size):
        return np.array(self.otDistr.getSample(size))
            
    def _getValueFromScipyDistribution(self):
        return self.scipyDistr.rvs(random_state=NOMAD.NOMAD_RNG)

    def _getValuesFromScipyDistribution(self, size):
        return self.scipyDistr.rvs(size=size, random_state=NOMAD.NOMAD_RNG)

    def _getValueFromNumpyDistribution(self):
        return self.numpyDistr(*self.distrArgs)

    def _getValuesFromNumpyDistribution(self, size):
        return self.numpyDistr(*self.distrArgs, size=size)

    def _boundValue(self, value):
        if value < self.min:
            return self.min
        if value > self.max:
            return self.max
        return value
  
    def _boundValues(self, values):
        values[values < self.min] = self.min
        values[values > self.max] = self.max
            
        return values
    
    # ============================================================
    def getMaxValue(self):
        return min(self.max, self._getMaxValue())
         
    def getMinValue(self):
        return max(self.min, self._getMinValue())

    def getMean(self):
        return self._getMeanValue()
    
    def _getMaxFromOtDistribution(self):
        otRange = self.otDistr.getRange()
        if otRange.getFiniteUpperBound()[0]:
            return otRange.getUpperBound()[0] 

        return inf

    def _getMinFromOtDistribution(self):
        otRange = self.otDistr.getRange()
        if otRange.getFiniteLowerBound()[0]:
            return otRange.getLowerBound()[0] 

        return -inf

    def _getMeanFromOtDistribution(self):
        return self.otDistr.getMean()[0]
    
    def _getMaxFromScipyDistribution(self):
        return self.scipyDistr.ppf(1)

    def _getMinFromScipyDistribution(self):
        return self.scipyDistr.ppf(0)

    def _getMeanFromScipyDistribution(self):
        return self.scipyDistr.mean()
    
    def _getMaxFromNumpyDistribution(self):
        return inf

    def _getMinFromNumpyDistribution(self):
        return -inf

    def _getMeanFromNumpyDistribution(self):
        return getMeanForNumpyDistr(self.numpyDistr, self.distrArgs)

def getParameter(paramInput):
    if isinstance(paramInput, dict):
        return Distribution(**paramInput)
    else:
        return StaticValue(paramInput)

# ===============================================================================================
# ===============================================================================================
    
def getMeanForNumpyDistr(numpyDistr, numpyDistrArgs):
    rg = NOMAD.NOMAD_RNG
    if isDistrEqualTo(numpyDistr, rg.normal) or \
        isDistrEqualTo(numpyDistr, rg.lognormal):
        if len(numpyDistrArgs):
            return numpyDistrArgs[0]
        else:
            return 0 # Default mean as defined in numpy
    
    if isDistrEqualTo(numpyDistr, rg.poisson):
        if len(numpyDistrArgs):
            return numpyDistrArgs[0]
        else:
            return 1.0 # Default mean as defined in numpy
    
    if isDistrEqualTo(numpyDistr, rg.triangular):
        return (numpyDistrArgs[0] + numpyDistrArgs[1] + numpyDistrArgs[2])/3
    
    if isDistrEqualTo(numpyDistr, rg.uniform):
        low = 0 # # Default as defined in numpy
        high = 1 # # Default as defined in numpy
        
        if len(numpyDistrArgs):
            low = numpyDistrArgs[0]
            if len(numpyDistrArgs) > 1:
                high = numpyDistrArgs[1]
        return (low + high)/2
    
    raise Exception(f'The "{numpyDistr.__name__}" distribution is not supported!')

def isDistrEqualTo(distr, distrOther):
    return distr.__name__ == distrOther.__name__
    