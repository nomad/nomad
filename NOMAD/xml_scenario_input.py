""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from pathlib import Path
from xml.dom.minidom import parse

from NOMAD.constants import FIXED_PLACEMENT_TYPE
from NOMAD.input_manager import createNomadModelFromSimulationInput, \
    NomadSimulationInput, LineObstacleInput, PolygonObstacleInput, \
    CircleObstacleInput, EllipseObstacleInput, WalkLevelInput, \
    WalkableAreaInput, \
    DemandManagerInput, ConstantDemandPatternInput, \
    VariableDemandPatternInput, InMemoryOutputManagerInput, \
    DummyOutputManagerInput , SimpleRandomLineSourceInput, \
    RectangleSourceInput, LineDestinationInput, PolygonDestinationInput, \
    SinkInput, WaypointInput, PointDestinationInput, \
    CentroidPolygonDestinationInput, FixedWaitingTimeActivityInput, \
    DiscreteDemandPatternInput, RestaurantSchedulerInput, \
    FixedWaitingTimeNonInteractingActivityInput, EndAtTimeWaitingActivityInput, \
    DebugOutputManagerInput, ListActivityPatternInput, \
    RestaurantStaffSchedulerInput, SpawnableLineDestinationInput, QueueInput, \
    IS_VIRTUAL_OBS_KEY, DIRECTION_VEC_KEY, FOCUS_POINT_KEY, \
    RANDOMNESS_FACTOR_KEY, DIST_BETWEEN_POINTS_KEY, \
    MultiPointInRectangleDestinationInput, \
    FixedTimeOutsideOfSimulationActivityInput, \
    EventOutsideOfSimulationActivityInput, ConnectivityFileOutputManagerInput, \
    SimulationFileOutputManagerInput, PedParameterSetInput, DistributionInput, \
    StaticValueInput, RestaurantStaffSchedulerParametersInput, IS_LOCAL_DEST_KEY, \
    PoissonDemandPatternInput
import NOMAD.xml_functions as xml
from NOMAD import DEF_OUTPUT_DIR


def createNomadModelFromInputXml(xmlFlNm, seed, lrcmLoadFile=None, gridManagerFile=None, useIndPedManager=False, extraOutputManagers=()):
    simulationInput = parseXml(xmlFlNm)
    return createNomadModelFromSimulationInput(simulationInput, seed, lrcmLoadFile, gridManagerFile, useIndPedManager, extraOutputManagers)

def parseXml(xmlFlNm):
    DOMTree = parse(str(xmlFlNm))
    scenXml = DOMTree.documentElement

    xmlFlNm = Path(xmlFlNm)
    pd = xmlFlNm.parent
    infraXmlFlNm = xml.getFlNm(scenXml, 'infrastructureXmlFlNm', pd=pd)
    walkLevels = parseInfraXml(infraXmlFlNm)
    pedParamSetsXmlFlNm = xml.getFlNm(scenXml, 'pedParameterSetsXmlFlNm', pd=pd)
    pedParameterSets = parsePedParameterSetsXml(pedParamSetsXmlFlNm)

    scenInput = parseScenario(scenXml)
    scenInput['walkLevels'] = walkLevels
    scenInput['pedParameterSets'] = pedParameterSets
    scenInput['demandManager'] = parseDemandManager(scenXml)
    scenInput['outputManagers'] = parseOutputManagers(scenXml, pd)

    nomadSimulationInput = NomadSimulationInput(**scenInput)

    return nomadSimulationInput

# ===============================================================================
# OBSTACLES
# ===============================================================================

def getObstacleID(obstacleXml):
    ID = None
    if xml.hasAttribute(obstacleXml, 'ID'):
        ID = xml.getAttribute(obstacleXml, 'ID')

    return ID

def getSeeThrough(obstacleXml):
    if xml.hasElement(obstacleXml, 'seeThrough'):
        return xml.getBoolean(obstacleXml, 'seeThrough')
    else:
        return None

def parseLineObstacle(obstacleXml):
    return parsePolygonLikeObstacle(obstacleXml, LineObstacleInput)

def parsePolygonObstacle(obstacleXml):
    return parsePolygonLikeObstacle(obstacleXml, PolygonObstacleInput)

def parsePolygonLikeObstacle(obstacleXml, creationFcn):
    ID, seeThrough = parseObstacleBase(obstacleXml)
    coords = xml.getCoordinateTupleEntry(obstacleXml, 'coords')

    return creationFcn(ID, seeThrough, coords)

def parseObstacleBase(obstacleXml):
    ID = getObstacleID(obstacleXml)
    seeThrough = getSeeThrough(obstacleXml)

    return ID, seeThrough

def parseObstacleCicularBase(obstacleXml):
    ID, seeThrough = parseObstacleBase(obstacleXml)
    centerCoord = xml.getCoordinateTupleEntry(obstacleXml, 'centerCoord')
    return ID, seeThrough, centerCoord

def parseCircleObstacle(obstacleXml):
    ID, seeThrough, centerCoord = parseObstacleCicularBase(obstacleXml)
    radius = xml.getFloat(obstacleXml, 'radius')
    return CircleObstacleInput(ID, seeThrough, centerCoord, radius)

def parseEllipseObstacle(obstacleXml):
    ID, seeThrough, centerCoord = parseObstacleCicularBase(obstacleXml)
    semiAxesValues = xml.getCoordinateTupleEntry(obstacleXml, 'semiAxesValues')
    return EllipseObstacleInput(ID, seeThrough, centerCoord, semiAxesValues)

OBSTACLE_TYPES = {
    'lineObstacle':parseLineObstacle,
    'polygonObstacle':parsePolygonObstacle,
    'circleObstacle':parseCircleObstacle,
    'ellipseObstacle':parseEllipseObstacle
    }

# ===============================================================================
# DESTINATIONS
# ===============================================================================

def parseLineDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return LineDestinationInput(ID, coords, groupID, **kwargs)

def parsePointDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return PointDestinationInput(ID, coords, groupID, **kwargs)

def parsePolygonDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return PolygonDestinationInput(ID, coords, groupID, **kwargs)

def parseCentroidPolygonDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return CentroidPolygonDestinationInput(ID, coords, groupID, **kwargs)

def parseSpawnableLineDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    return SpawnableLineDestinationInput(ID, coords, groupID, **kwargs)

def parseMultiPointInRectangleDestination(destinationXml):
    ID, coords, groupID, kwargs = parsDestinationBase(destinationXml)
    if xml.hasElement(destinationXml, DIRECTION_VEC_KEY):
        kwargs[DIRECTION_VEC_KEY] = xml.getCoordinateTupleEntry(destinationXml, DIRECTION_VEC_KEY)
    if xml.hasElement(destinationXml, FOCUS_POINT_KEY):
        kwargs[FOCUS_POINT_KEY] = xml.getCoordinateTupleEntry(destinationXml, FOCUS_POINT_KEY)
    if xml.hasElement(destinationXml, RANDOMNESS_FACTOR_KEY):
        kwargs[RANDOMNESS_FACTOR_KEY] = xml.getFloat(destinationXml, RANDOMNESS_FACTOR_KEY)
    if xml.hasElement(destinationXml, DIST_BETWEEN_POINTS_KEY):
        kwargs[DIST_BETWEEN_POINTS_KEY] = xml.getFloat(destinationXml, DIST_BETWEEN_POINTS_KEY)
    
    return MultiPointInRectangleDestinationInput(ID, coords, groupID, **kwargs)

def parsDestinationBase(destinationXml):
    ID = xml.getAttribute(destinationXml, 'ID')
    coords = xml.getCoordinateTupleEntry(destinationXml, 'coords')

    groupID = None
    if xml.hasAttribute(destinationXml, 'groupID'):
        groupID = xml.getAttribute(destinationXml, 'groupID')

    kwargs = {}

    if xml.hasElement(destinationXml, IS_VIRTUAL_OBS_KEY):
        kwargs[IS_VIRTUAL_OBS_KEY] = xml.getBoolean(destinationXml, IS_VIRTUAL_OBS_KEY)
    if xml.hasElement(destinationXml, IS_LOCAL_DEST_KEY):
        kwargs[IS_LOCAL_DEST_KEY] = xml.getBoolean(destinationXml, IS_LOCAL_DEST_KEY)

    return ID, coords, groupID, kwargs

DESTINATION_TYPES = {
    'lineDestination':parseLineDestination,
    'pointDestination':parsePointDestination,
    'polygonDestination':parsePolygonDestination,
    'centroidPolygonDestination':parseCentroidPolygonDestination,
    'spawnableLineDestination':parseSpawnableLineDestination,
    'multiPointInRectangleDestination':parseMultiPointInRectangleDestination
    }

# ===============================================================================
# SOURCES
# ===============================================================================

def parseSimpleRandomLineSource(sourceXml):
    ID, coords = parsSourceBase(sourceXml)
    return SimpleRandomLineSourceInput(ID, coords)

RECTANGLE_SOURCE_ELEMENTS = (
    xml.ElInfo('directionVec', xml.NUMERIC_TUPLE_TYPE, isOptional=True),
    xml.ElInfo('focusPoint', xml.NUMERIC_TUPLE_TYPE, isOptional=True),
)

def parseRectangleSource(sourceXml):
    ID, coords = parsSourceBase(sourceXml)
    inputDict = xml.getElementDict(sourceXml, RECTANGLE_SOURCE_ELEMENTS)
    
    return RectangleSourceInput(ID, coords, **inputDict)

def parsSourceBase(sourceXml):
    ID = xml.getAttribute(sourceXml, 'ID')
    coords = xml.getCoordinateTupleEntry(sourceXml, 'coords')

    return ID, coords

SOURCE_TYPES = {
    'simpleRandomLineSource':parseSimpleRandomLineSource,
    'rectangleSource':parseRectangleSource
    }

# ===============================================================================
# INFRASTRUCTURE
# ===============================================================================

def parseInfraXml(infraXmlFlNm):
    DOMTree = parse(str(infraXmlFlNm))
    infraXml = DOMTree.documentElement
    walkLevelXmls = xml.getElements(infraXml, 'walkLevel')
    walkLevels = []
    for walkLevelXml in walkLevelXmls:
        walkLevels.append(parseWalkLevel(walkLevelXml))

    return walkLevels

def parseWalkLevel(walkLevelXml):
    ID = xml.getAttribute(walkLevelXml, 'ID')
    # TODO: Elevation
    walkableAreas = parseWalkableAreas(walkLevelXml)
    obstacles = parseObstacles(walkLevelXml)
    queues = parseQueues(walkLevelXml)
    destinations = parseDestinations(walkLevelXml)
    sources = parseSources(walkLevelXml)

    return WalkLevelInput(ID, walkableAreas, obstacles, destinations, sources, queues)

def parseWalkableAreas(walkLevelXml):
    walkableAreas = []
    walkableAreaXmls = xml.getElements(walkLevelXml, 'walkableArea')
    for walkableAreaXml in walkableAreaXmls:
        ID = xml.getAttribute(walkableAreaXml, 'ID')
        coords = xml.getCoordinateTupleEntry(walkableAreaXml, 'coords')
        walkableAreas.append(WalkableAreaInput(ID, coords))

    return walkableAreas

def parseObstacles(walkLevelXml):
    obstacles = []
    for obstacleKey, parseFcn in OBSTACLE_TYPES.items():
        obstacleXmls = xml.getElements(walkLevelXml, obstacleKey)
        for obstacleXml in obstacleXmls:
            obstacles.append(parseFcn(obstacleXml))

    return obstacles

def parseQueues(walkLevelXml):
    queues = []
    if not xml.hasElement(walkLevelXml, 'queues'):
        return queues
                          
    queuesXml = xml.getSingleElement(walkLevelXml, 'queues')
    
    queueXmls = xml.getElements(queuesXml, 'queue')
    for queueXml in queueXmls:        
        ID = xml.getAttribute(queueXml, 'ID')
        coords = xml.getCoordinateTupleEntry(queueXml, 'coords')
        kwargs = {}
        if xml.hasElement(queueXml, IS_VIRTUAL_OBS_KEY):
            kwargs[IS_VIRTUAL_OBS_KEY] = xml.getBoolean(queueXml, IS_VIRTUAL_OBS_KEY)

        queues.append(QueueInput(ID, coords, **kwargs))

    return queues

def parseDestinations(walkLevelXml):
    destinationsXml = xml.getSingleElement(walkLevelXml, 'destinations')
    destinations = []
    for destinationKey, parseFcn in DESTINATION_TYPES.items():
        destinationXmls = xml.getElements(destinationsXml, destinationKey)
        for destinationXml in destinationXmls:
            destinations.append(parseFcn(destinationXml))

    return destinations

def parseSources(walkLevelXml):
    sourcesXml = xml.getSingleElement(walkLevelXml, 'sources')
    sources = []
    for sourceKey, parseFcn in SOURCE_TYPES.items():
        sourceXmls = xml.getElements(sourcesXml, sourceKey)
        for sourceXml in sourceXmls:
            sources.append(parseFcn(sourceXml))

    return sources

# ===============================================================================
# PEDESTRIAN PARAMETERS
# ===============================================================================

PARAMETER_NAMES = ('preferredSpeed', 'tau', 'a_0', 'r_0', 'a_1', 'r_1', 'kappa_0',
    'kappa_1', 'a_W', 'd_shy', 'r_infl', 'ie_f', 'ie_b', 'cPlus_0', 'cMinus_0',
    't_A', 'radius', 'noise')

def parsePedParameterSetsXml(pedParamSetsXmlFlNm):
    DOMTree = parse(str(pedParamSetsXmlFlNm))
    pedParamSetsXml = DOMTree.documentElement
    pedParamSetXmls = xml.getElements(pedParamSetsXml, 'pedParameterSet')
    pedParameterSets = []
    for pedParamSetXml in pedParamSetXmls:
        pedParameterSets.append(parsePedParamaterSet(pedParamSetXml))

    return pedParameterSets

def parsePedParamaterSet(pedParamSetXml):
    ID = xml.getAttribute(pedParamSetXml, 'ID')
    params = {}
    if xml.hasAttribute(pedParamSetXml, 'baseSet'):
        params['baseSet'] = xml.getAttribute(pedParamSetXml, 'baseSet')

    for paramName in PARAMETER_NAMES:
        params[paramName] = parsePedParameter(pedParamSetXml, paramName)

    return PedParameterSetInput(ID, **params)

def parsePedParameter(pedParamSetXml, paramName):
    if not xml.hasElement(pedParamSetXml, paramName, False):
        return
    
    param = xml.getParameter(pedParamSetXml, paramName, raw=True)
    if isinstance(param, dict):
        return DistributionInput(**param)
    
    return StaticValueInput(param)
    
# ===============================================================================
# SCENARIOS
# ===============================================================================

def parseScenario(scenXml):
    SCEN_ELEMENTS = (
        xml.ElInfo('name', xml.STRING_TYPE),
        xml.ElInfo('label', xml.STRING_TYPE),
        xml.ElInfo('duration', xml.FLOAT_TYPE),
        xml.ElInfo('inIsolationTimeStep', xml.FLOAT_TYPE, isOptional=True),
        xml.ElInfo('inRangeStepsPerInIsolationStep', xml.INT_TYPE, isOptional=True),
        xml.ElInfo('inCollisionStepsPerInIsolationStep', xml.INT_TYPE, isOptional=True),
        xml.ElInfo('distBetweenQueueingPeds', xml.FLOAT_TYPE, isOptional=True),
        xml.ElInfo('gridCellSize', xml.FLOAT_TYPE, isOptional=True),
        xml.ElInfo('accCalcFcnType', xml.STRING_TYPE, isOptional=True),
    )

    return xml.getElementDict(scenXml, SCEN_ELEMENTS)

def parseDemandManager(scenXml):
    demandManagerXml = xml.getSingleElement(scenXml, 'demandManager')
    activities = parseActivities(demandManagerXml)
    
    kwargs = {}

    if xml.hasElement(demandManagerXml, 'distBetweenQueueingPeds'):
        kwargs['distBetweenQueueingPeds'] = xml.getFloat(demandManagerXml, 'distBetweenQueueingPeds')

    hasScheduler = False
    for schedulerType, parseFcn in SCHEDULER_TYPES.items():
        if xml.hasElement(demandManagerXml, schedulerType):
            schedulerXml = xml.getSingleElement(demandManagerXml, schedulerType)
            scheduler = parseFcn(schedulerXml)
            kwargs['scheduler'] = scheduler
            hasScheduler = True

    if not hasScheduler:
        kwargs['activityPatterns'] = parseActivityPatterns(demandManagerXml)
        kwargs['demandPatterns'] = parseDemandPatterns(demandManagerXml)

    return DemandManagerInput(activities, **kwargs)

RESTAURANT_SCHEDULER_ELEMENTS = (
    xml.ElInfo('demandPattern', xml.NUMERIC_TUPLE_TYPE),
    xml.ElInfo('sittingDestinationGroupIDs', xml.TUPLE_TYPE),
    xml.ElInfo('visitDuration', xml.PARAM_TYPE),
    xml.ElInfo('inGroupEntryDistr', xml.PARAM_TYPE),
    xml.ElInfo('guestPedParamSetDistr', xml.TUPLE_TYPE),
    xml.ElInfo('toiletDestinationIDs', xml.TUPLE_TYPE, isOptional=True),
    xml.ElInfo('toiletVisitProbability', xml.FLOAT_TYPE, isOptional=True),
    xml.ElInfo('toiletVisitDuration', xml.PARAM_TYPE, isOptional=True),
    xml.ElInfo('coatRackDestinationID', xml.STRING_TYPE, isOptional=True),
    xml.ElInfo('coatRackVisitDuration', xml.PARAM_TYPE, isOptional=True),
    xml.ElInfo('registerDestinationID', xml.STRING_TYPE, isOptional=True),
    xml.ElInfo('registerVisitDuration', xml.PARAM_TYPE, isOptional=True),
    xml.ElInfo('useTablesOnlyOnce', xml.BOOL_TYPE, isOptional=True),
    xml.ElInfo('intraTableGroupBuffer', xml.FLOAT_TYPE, isOptional=True),
    xml.ElInfo('staffScheduler', xml.XML_SUB_PART, isOptional=True),
    )

RESTAURANT_STAFF_PARAMETERS_ELEMENTS = (
    xml.ElInfo('activityDuration', xml.PARAM_TYPE, isOptional=True),
    xml.ElInfo('baseFirstProbability', xml.FLOAT_TYPE, isOptional=True),
    xml.ElInfo('baseAfterProbability', xml.FLOAT_TYPE, isOptional=True),
    xml.ElInfo('gapsDirichletAlpha', xml.FLOAT_TYPE, isOptional=True),
    xml.ElInfo('afterVisitActivityCount', xml.PARAM_TYPE, isOptional=True),
    xml.ElInfo('baseActivityDuration', xml.FLOAT_TYPE, isOptional=True),
    )

RESTAURANT_STAFF_SCHEDULER_ELEMENTS = (
    xml.ElInfo('baseAreaID', xml.STRING_TYPE),
    xml.ElInfo('staffCount', xml.INT_TYPE),
    xml.ElInfo('pedParamSetID', xml.STRING_TYPE),
    xml.ElInfo('activityCount', xml.INT_TYPE, isOptional=True),
    xml.ElInfo('neighborhoods', xml.TUPLE_TYPE, isOptional=True),
    xml.ElInfo('parameters', xml.XML_SUB_PART, isOptional=True),
    )

def parseRestaurantStaffSchedulerParameters(staffSchedulerParametersXml):
    inputDict = xml.getElementDict(staffSchedulerParametersXml, RESTAURANT_STAFF_PARAMETERS_ELEMENTS)
    return RestaurantStaffSchedulerParametersInput(**inputDict)

def parseRestaurantStaffScheduler(staffSchedulerXml):
    inputDict = xml.getElementDict(staffSchedulerXml, RESTAURANT_STAFF_SCHEDULER_ELEMENTS)
    if 'parameters' in inputDict:
        inputDict['parameters'] = parseRestaurantStaffSchedulerParameters(inputDict['parameters'])
 
    return RestaurantStaffSchedulerInput(**inputDict)

def parseRestaurantScheduler(schedulerXml):
    sourceID, sinkID = parseSchedulerBase(schedulerXml)
    inputDict = xml.getElementDict(schedulerXml, RESTAURANT_SCHEDULER_ELEMENTS)

    if 'staffScheduler' in inputDict:
        inputDict['staffScheduler'] = parseRestaurantStaffScheduler(inputDict['staffScheduler'])

    return RestaurantSchedulerInput(sourceID, sinkID, **inputDict)

def parseSchedulerBase(schedulerXml):
    sourceID = xml.getString(schedulerXml, 'sourceID')
    sinkID = xml.getString(schedulerXml, 'sinkID')

    return sourceID, sinkID

SCHEDULER_TYPES = {
    'restaurantScheduler':parseRestaurantScheduler,
    }

def parseSink(activityXml):
    baseArgs = parseActivityBase(activityXml)
    return SinkInput(*baseArgs)

def parseWaypoint(activityXml):
    baseArgs = parseActivityBase(activityXml)
    return WaypointInput(*baseArgs)

def parseFixedWaitingTimeActivity(activityXml):
    baseArgs = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    duration = xml.getFloat(activityXml, 'waitDuration')
    return FixedWaitingTimeActivityInput(*baseArgs, name, duration)

def parseFixedWaitingTimeNonInteractingActivity(activityXml):
    baseArgs = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    duration = xml.getFloat(activityXml, 'waitDuration')
    return FixedWaitingTimeNonInteractingActivityInput(*baseArgs, name, duration)

def parseEndAtTimeWaitingActivity(activityXml):
    baseArgs = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    endtime = xml.getFloat(activityXml, 'endtime')
    return EndAtTimeWaitingActivityInput(*baseArgs, name, endtime)

def parseFixedTimeOutsideOfSimulationActivity(activityXml):
    baseArgs = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    duration = xml.getFloat(activityXml, 'waitDuration')
    return FixedTimeOutsideOfSimulationActivityInput(*baseArgs, name, duration)

def parseEventOutsideOfSimulationActivity(activityXml):
    baseArgs = parseActivityBase(activityXml)
    name = xml.getString(activityXml, 'name')
    return EventOutsideOfSimulationActivityInput(*baseArgs, name)

def parseActivityBase(activityXml):
    ID = xml.getAttribute(activityXml, 'ID')
    destinationID = xml.getString(activityXml, 'destinationID')

    groupID = None
    if xml.hasAttribute(activityXml, 'groupID'):
        groupID = xml.getAttribute(activityXml, 'groupID')

    queueID = None
    if xml.hasElement(activityXml, 'queueID'):
        queueID = xml.getString(activityXml, 'queueID')

    queueDelay = None
    if xml.hasElement(activityXml, 'queueDelay'):
        queueDelay = xml.getFloat(activityXml, 'queueDelay')

    return ID, destinationID, groupID, queueID, queueDelay

ACTIVITY_TYPES = {
    'sink':parseSink,
    'waypoint':parseWaypoint,
    'fixedWaitingTimeActivity':parseFixedWaitingTimeActivity,
    'fixedWaitingTimeNonInteractingActivity':parseFixedWaitingTimeNonInteractingActivity,
    'endAtTimeWaitingActivity':parseEndAtTimeWaitingActivity,
    'fixedTimeOutsideOfSimulationActivity':parseFixedTimeOutsideOfSimulationActivity,
    'eventOutsideOfSimulationActivity':parseEventOutsideOfSimulationActivity,
    }

def parseActivities(demandManagerXml):
    activities = []
    activitiesXml = xml.getSingleElement(demandManagerXml, 'activities')
    for key, parseFcn in ACTIVITY_TYPES.items():
        activityXmls = xml.getElements(activitiesXml, key)
        for activityXml in activityXmls:
            activities.append(parseFcn(activityXml))

    return activities

def parseListActivityPattern(ID, activityPatternXml):
    pattern = xml.getTupleEntry(xml.getSingleElement(activityPatternXml, 'IDlist'))
    return ListActivityPatternInput(ID, pattern)

ACTIVITY_PATTERN_TYPES = {
    'listActivityPattern':parseListActivityPattern,
    }

def parseActivityPatterns(demandManagerXml):
    activityPatterns = []

    activityPatternsXml = xml.getSingleElement(demandManagerXml, 'activityPatterns')
    for key, parseFcn in ACTIVITY_PATTERN_TYPES.items():
        activityPatternXmls = xml.getElements(activityPatternsXml, key)
        for activityPatternXml in activityPatternXmls:
            ID = xml.getAttribute(activityPatternXml, 'ID')
            activityPatterns.append(parseFcn(ID, activityPatternXml))
    
    return activityPatterns


CONSTANT_DEMAND_PATTERN = 'constantDemandPattern'
POISSON_DEMAND_PATTERN = 'poissonDemandPattern'
VARIABLE_DEMAND_PATTERN = 'variableDemandPattern'
DISCRETE_DEMAND_PATTERN = 'discreteDemandPattern'

DEMAND_PATTERN_TYPES = (CONSTANT_DEMAND_PATTERN, POISSON_DEMAND_PATTERN, VARIABLE_DEMAND_PATTERN, DISCRETE_DEMAND_PATTERN)

def parseDemandPatterns(demandManagerXml):
    demandPatterns = []

    demandPatternsXml = xml.getSingleElement(demandManagerXml, 'demandPatterns')
    for patternType in DEMAND_PATTERN_TYPES:
        demandPatternXmls = xml.getElements(demandPatternsXml, patternType)
        for demandPatternXml in demandPatternXmls:
            demandPatterns.append(parseDemandPattern(demandPatternXml, patternType))

    return demandPatterns

def parseDemandPattern(demandPatternXml, patternType):
        ID = xml.getAttribute(demandPatternXml, 'ID')
        sourceID = xml.getString(demandPatternXml, 'sourceID')
        activityPatternID = xml.getString(demandPatternXml, 'activityPatternID')
        parameterSetID = xml.getString(demandPatternXml, 'parameterSetID')

        optKwargs = {}
        if xml.hasElement(demandPatternXml, 'startTime'):
            optKwargs['startTime'] = xml.getFloat(demandPatternXml, 'startTime')
        if xml.hasElement(demandPatternXml, 'endTime'):
            optKwargs['endTime'] = xml.getFloat(demandPatternXml, 'endTime')

        if xml.hasElement(demandPatternXml, 'placement'):
            optKwargs['placement'] = xml.getSingleElement(demandPatternXml, 'placement')
        if xml.hasElement(demandPatternXml, 'placementLocation'):
            optKwargs['placementLocation'] = xml.getCoordinateTupleEntry(demandPatternXml, 'placementLocation')
        else:
            if 'placement' in optKwargs and optKwargs['placement'] == FIXED_PLACEMENT_TYPE:
                raise Exception('When the placement is of the type "fixed", a placement location must be provided!')
                
        if patternType == CONSTANT_DEMAND_PATTERN:
            flowPerSecond = xml.getFloat(demandPatternXml, 'flowPerSecond')
            return ConstantDemandPatternInput(ID, sourceID, activityPatternID, parameterSetID,
                                               flowPerSecond, **optKwargs)
        elif patternType == POISSON_DEMAND_PATTERN:
            flowPerSecond = xml.getFloat(demandPatternXml, 'flowPerSecond')
            return PoissonDemandPatternInput(ID, sourceID, activityPatternID, parameterSetID,
                                               flowPerSecond, **optKwargs)
        elif patternType == VARIABLE_DEMAND_PATTERN:
            timeFlowArray = xml.getNumericTuple(demandPatternXml, 'timeFlowArray')
            return VariableDemandPatternInput(ID, sourceID, activityPatternID, parameterSetID,
                                               timeFlowArray, **optKwargs)
        elif patternType == DISCRETE_DEMAND_PATTERN:
            discretePattern = xml.getNumericTuple(demandPatternXml, 'discretePattern')
            return DiscreteDemandPatternInput(ID, sourceID, activityPatternID, parameterSetID,
                                               discretePattern)
        else:
            raise Exception('Unknown demand pattern type "{}"'.format(patternType))


def parseSimulationFileOutputManager(ID, outputManagerXml, pd):
    baseArgsList = getBasicFileOutputManagerArgsList(outputManagerXml, pd)
    args = tuple(baseArgsList)
    kwargs = {}
    if xml.hasElement(outputManagerXml, 'exportGrid'):
        kwargs["exportGrid"] = xml.getBoolean(outputManagerXml, 'exportGrid')
    return SimulationFileOutputManagerInput(ID, *args, **kwargs) 

def parseDebugOutputManager(ID, outputManagerXml, pd):
    baseArgsList = getBasicFileOutputManagerArgsList(outputManagerXml, pd)
    args = tuple(baseArgsList)
    return DebugOutputManagerInput(ID, *args) 

def parseConnectivityFileOutputManager(ID, outputManagerXml, pd):
    baseArgsList = getBasicFileOutputManagerArgsList(outputManagerXml, pd)
    if xml.hasElement(outputManagerXml, 'connectionCutOffDist'):
        args = tuple(baseArgsList + [xml.getFloat(outputManagerXml, 'connectionCutOffDist')])
    else:
        args = tuple(baseArgsList)
    return ConnectivityFileOutputManagerInput(ID, *args) 

def parseInMemoryOutputManager(ID, outputManagerXml, _):
    if xml.hasElement(outputManagerXml, 'saveTimeStep'):
        args = (xml.getFloat(outputManagerXml, 'saveTimeStep'),)
    else:
        args = ()

    return InMemoryOutputManagerInput(ID, *args)

def parseDummyOutputManager(ID, _, __):
    return DummyOutputManagerInput(ID)

def getBasicFileOutputManagerArgsList(outputManagerXml, pd):
    if xml.hasElement(outputManagerXml, 'outputPd'):
        outputPd = xml.getPd(outputManagerXml, 'outputPd', mainPd=pd, createIfNonExisting=True)
    else:
        outputPd = DEF_OUTPUT_DIR
        if not outputPd.is_dir():
            outputPd.mkdir(parents=True)
    baseFilename = xml.getString(outputManagerXml, 'baseFilename')
    return [outputPd, baseFilename]

OUTPUT_MANAGER_TYPES = {
    'SimulationFileOutputManager': parseSimulationFileOutputManager,
    'DebugOutputManager': parseDebugOutputManager,
    'ConnectivityFileOutputManager': parseConnectivityFileOutputManager,
    'InMemoryOutputManager': parseInMemoryOutputManager,
    'DummyOutputManager': parseDummyOutputManager
    }

def parseOutputManagers(scenXml, pd):
    outputManagers = []

    outputManagersXml = xml.getSingleElement(scenXml, 'outputManagers')
    for key, parseFcn in OUTPUT_MANAGER_TYPES.items():
        outputManagerXmls = xml.getElements(outputManagersXml, key)
        for outputManagerXml in outputManagerXmls:
            if key == 'DummyOutputManager':
                ID = 'Dummy'
            else:
                ID = xml.getAttribute(outputManagerXml, 'ID')
             
            outputManagers.append(parseFcn(ID, outputManagerXml, pd))

    return outputManagers