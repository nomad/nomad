""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from copy import copy
from dataclasses import dataclass, asdict, field
import importlib
from math import inf
from typing import List, Union

from NOMAD.activities import LineDestination, PolygonDestination, Sink, Waypoint, \
    PointDestination, CentroidPolygonDestination, FixedWaitingTimeActivity, \
    FixedWaitingTimeNonInteractingActivity, EndAtTimeWaitingActivity, \
    ListActivityPattern, SpawnableLineDestination, Queue, \
    MultiPointInRectangleDestination, FixedTimeOutsideOfSimulationActivity, \
    EventOutsideOfSimulationActivity
from NOMAD.activity_scheduler import createRestaurantScheduler, \
    INTRA_TABLE_GROUP_BUFFER, ACTIVITY_DURATION_DISTR, BASE_FIRST_PROBABILITY, \
    BASE_AFTER_PROBABILITY, GAPS_DIRICHLET_ALPHA, AFTER_VISIT_ACTIVITY_COUNT, \
    BASE_ACTIVITY_DURATION
from NOMAD.constants import GRID_CELL_SIZE_NM, ACC_CALC_FCN_NM, DIST_BETWEEN_QUEUEING_PEDS, \
    RANDOM_PLACEMENT_TYPE
from NOMAD.demand_manager import DemandManager, RectangleSource, \
    ConstantDemandPattern, VariableDemandPattern, SimpleRandonLineSource, \
    DiscreteDemandPattern, PoissonDemandPattern
from NOMAD.general_functions import Parameter, Distribution, getParameter
from NOMAD.nomad_model import createNomadModel
from NOMAD.nomad_pedestrian_parameters import PedParameterSet
from NOMAD.obstacles import LineObstacle, PolygonObstacle, CircleObstacle, \
    EllipseObstacle
from NOMAD.output_manager import InMemoryOutputManager, \
    DummyOutputManager, SimulationFileOutputManager, \
    ConnectivityFileOutputManager, OutputManager
from NOMAD.walk_level import WalkLevel, WalkableArea


# ===============================================================================
# The data classes that form the basis of the input for NOMAD
# Any input structure should create these data classes and from these data classes
# create the NomadModel instance
# ===============================================================================
@dataclass
class StaticValueInput():
    value: float
       
@dataclass
class DistributionInput():
    distrType: str
    distrName: str
    distrArgs: tuple
    lowerTrunc: float = None
    upperTrunc: float = None
    min: float = -inf
    max: float = inf
        
# ---------------------------------------------------------------------------------
def getParameterInput(inputDict):
    if 'staticValue' in inputDict:
        return StaticValueInput(**inputDict)
    else:
        return DistributionInput(**inputDict)

# ===============================================================================
# ===============================================================================
@dataclass
class WalkableAreaInput():
    ID: str
    coords: tuple

def createWalkableAreas(walkLevelInput):
    walkableAreas = []
    for walkableAreaInput in walkLevelInput.walkableAreas:
            walkableAreas.append(WalkableArea(walkableAreaInput.ID, walkableAreaInput.coords))

    return walkableAreas

# ===============================================================================
# ===============================================================================
@dataclass
class ObstacleInput():
    ID: str
    seeThrough: bool

def createObstacles(walkLevelInput):
    obstacles = []
    for obstacleInput in walkLevelInput.obstacles:
        obstacles.append(OBS_CONFIG_CLASS_2_CREATION_FCN[obstacleInput.__class__](obstacleInput))

    return obstacles

# ===============================================================================
@dataclass
class LineObstacleInput(ObstacleInput):
    coords: tuple

def createLineObstacle(obstacleInput):
    return LineObstacle(obstacleInput.ID, obstacleInput.coords, obstacleInput.seeThrough)

# ===============================================================================
@dataclass
class PolygonObstacleInput(ObstacleInput):
    coords: tuple

def createPolygonObstacle(obstacleInput):
    return PolygonObstacle(obstacleInput.ID, obstacleInput.coords, obstacleInput.seeThrough)

# ===============================================================================
@dataclass
class CircleObstacleInput(ObstacleInput):
    centerCoord: tuple
    radius: float

def createCircleObstacle(obstacleInput):
    return CircleObstacle(obstacleInput.ID, obstacleInput.centerCoord, obstacleInput.radius, obstacleInput.seeThrough)

# ===============================================================================
@dataclass
class EllipseObstacleInput(ObstacleInput):
    centerCoord: tuple
    semiAxesValues: tuple

def createEllipseObstacle(obstacleInput):
    return EllipseObstacle(obstacleInput.ID, obstacleInput.centerCoord, obstacleInput.semiAxesValues, obstacleInput.seeThrough)

# ===============================================================================
OBS_CONFIG_CLASS_2_CREATION_FCN = {
    LineObstacleInput: createLineObstacle,
    PolygonObstacleInput: createPolygonObstacle,
    CircleObstacleInput: createCircleObstacle,
    EllipseObstacleInput: createEllipseObstacle
    }

# ===============================================================================
# ===============================================================================
@dataclass
class QueueInput():
    ID: str
    coords: tuple
    isVirtualObstacle: bool = False

def createQueues(walkLevelInput, walkLevel):
    queues = []
    for queueInput in walkLevelInput.queues:
        queues.append(Queue(queueInput.ID, queueInput.coords, walkLevel, 
                            isVirtualObstacle=queueInput.isVirtualObstacle))
    return queues

# ===============================================================================
# ===============================================================================
@dataclass
class DestinationInput():
    ID: str
    coords: tuple
    groupID: str
    isVirtualObstacle: bool = False
    isLocalDestination: bool = False

def createDestinations(walkLevelInput, walkLevel):
    destinations = []
    for destinationInput in walkLevelInput.destinations:
        destinations.append(DESTINATION_CONFIG_CLASS_2_CREATION_FCN[destinationInput.__class__](destinationInput, walkLevel))

    return destinations

def getBaseDestinationArgs(destinationInput):
    return destinationInput.ID, destinationInput.coords

IS_VIRTUAL_OBS_KEY = 'isVirtualObstacle'
IS_LOCAL_DEST_KEY = 'isLocalDestination'
DIRECTION_VEC_KEY = 'directionVec'
FOCUS_POINT_KEY = 'focusPoint'
RANDOMNESS_FACTOR_KEY = 'randomnessFactor'
DIST_BETWEEN_POINTS_KEY = 'distBetweenPoints'

def getBaseDestinationKwargs(destinationInput):
    baseKwargs = {}
    if destinationInput.groupID is not None:
        baseKwargs['groupID'] = destinationInput.groupID
    if destinationInput.isVirtualObstacle is not None:
        baseKwargs[IS_VIRTUAL_OBS_KEY] = destinationInput.isVirtualObstacle
    if destinationInput.isLocalDestination is not None:
        baseKwargs[IS_LOCAL_DEST_KEY] = destinationInput.isLocalDestination

    return baseKwargs
    
# ===============================================================================
@dataclass
class LineDestinationInput(DestinationInput): pass

def createLineDestination(destinationInput, walkLevel): # @UnusedVariable
    return LineDestination(*getBaseDestinationArgs(destinationInput),
                           **getBaseDestinationKwargs(destinationInput))

# ===============================================================================
@dataclass
class PointDestinationInput(DestinationInput): pass

def createPointDestination(destinationInput, walkLevel): # @UnusedVariable
    return PointDestination(*getBaseDestinationArgs(destinationInput),
                           **getBaseDestinationKwargs(destinationInput))

# ===============================================================================
@dataclass
class PolygonDestinationInput(DestinationInput): pass

def createPolygonDestination(destinationInput, walkLevel): # @UnusedVariable
    return PolygonDestination(*getBaseDestinationArgs(destinationInput),
                           **getBaseDestinationKwargs(destinationInput))

# ===============================================================================
@dataclass
class CentroidPolygonDestinationInput(DestinationInput): pass

def createCentroidPolygonDestination(destinationInput, walkLevel): # @UnusedVariable
    return CentroidPolygonDestination(*getBaseDestinationArgs(destinationInput),
                           **getBaseDestinationKwargs(destinationInput))
    
# ===============================================================================
@dataclass
class SpawnableLineDestinationInput(DestinationInput): pass

def createSpawnableLineDestination(destinationInput, walkLevel): # @UnusedVariable
    return SpawnableLineDestination(*getBaseDestinationArgs(destinationInput),
                           **getBaseDestinationKwargs(destinationInput))
    
# ===============================================================================
@dataclass
class MultiPointInRectangleDestinationInput(DestinationInput):
    distBetweenPoints: float = None
    directionVec: tuple = None
    focusPoint: tuple = None
    randomnessFactor: float = None

def createMultiPointInRectangleDestination(destinationInput, walkLevel):
    args = getBaseDestinationArgs(destinationInput)
    kwargs = getBaseDestinationKwargs(destinationInput)
  
    if destinationInput.directionVec is not None:
        kwargs[DIRECTION_VEC_KEY] = destinationInput.directionVec
    if destinationInput.focusPoint is not None:
        kwargs[FOCUS_POINT_KEY] = destinationInput.focusPoint
    if destinationInput.randomnessFactor is not None:
        kwargs[RANDOMNESS_FACTOR_KEY] = destinationInput.randomnessFactor
    
    return MultiPointInRectangleDestination(*args, destinationInput.distBetweenPoints, 
                                            walkLevel, **kwargs)
    
# ===============================================================================
DESTINATION_CONFIG_CLASS_2_CREATION_FCN = {
    LineDestinationInput: createLineDestination,
    PointDestinationInput: createPointDestination,
    PolygonDestinationInput: createPolygonDestination,
    CentroidPolygonDestinationInput: createCentroidPolygonDestination,
    SpawnableLineDestinationInput: createSpawnableLineDestination,
    MultiPointInRectangleDestinationInput: createMultiPointInRectangleDestination,
    }

# ===============================================================================
# ===============================================================================
@dataclass
class SourceInput():
    ID: str
    coords: tuple

def createSources(walkLevelInputs, walkLevels):
    sourcesPerWalkLevel = {}
    sources = {}
    for walkLevelInput in walkLevelInputs:
        sourcesPerWalkLevel[walkLevels[walkLevelInput.ID]] = []
        for sourceInput in walkLevelInput.sources:
            source = SOURCE_CONFIG_CLASS_2_CREATION_FCN[sourceInput.__class__](sourceInput)
            sourcesPerWalkLevel[walkLevels[walkLevelInput.ID]].append(source)
            sources[source.ID] = source

    return sources, sourcesPerWalkLevel

# ===============================================================================
@dataclass
class SimpleRandomLineSourceInput(SourceInput): pass

def createSimpleRandomLineSource(sourceInput):
    return SimpleRandonLineSource(sourceInput.ID, sourceInput.coords)

# ===============================================================================
@dataclass
class RectangleSourceInput(SourceInput):
    directionVec: tuple = None
    focusPoint: tuple = None

def createRectangleSource(sourceInput):
    return RectangleSource(sourceInput.ID, sourceInput.coords, 
                           sourceInput.directionVec, sourceInput.focusPoint)

# ===============================================================================
SOURCE_CONFIG_CLASS_2_CREATION_FCN = {
    SimpleRandomLineSourceInput: createSimpleRandomLineSource,
    RectangleSourceInput: createRectangleSource
    }

# ===============================================================================
# ===============================================================================
@dataclass
class WalkLevelInput():
    ID: str
    walkableAreas: List[WalkableAreaInput]
    obstacles: List[ObstacleInput]
    destinations: List[DestinationInput]
    sources: List[SourceInput]
    queues: List[QueueInput]
    
def createWalkLevels(walkLevelInputs):
    walkLevels = {}
    destinations = {}
    queues = {}
    for walkLevelInput in walkLevelInputs:
        walkLevel = WalkLevel(walkLevelInput.ID)
        walkLevel.addWalkableAreas(createWalkableAreas(walkLevelInput))
        walkLevel.addObstacles(createObstacles(walkLevelInput))

        queuesWalkLevel = createQueues(walkLevelInput, walkLevel)
        walkLevel.addQueues(queuesWalkLevel)
        for queue in queuesWalkLevel:
            queues[queue.ID] = queue

        destinationsWalkLevel = createDestinations(walkLevelInput, walkLevel)
        for destination in destinationsWalkLevel:
            destinations[destination.ID] = destination
        walkLevel.addDestinations(destinationsWalkLevel)
        walkLevels[walkLevelInput.ID] = walkLevel

    return walkLevels, destinations, queues

# ===============================================================================
# ===============================================================================
# ===============================================================================
# ===============================================================================

@dataclass
class ActivityInput():
    ID: str
    destinationID: str
    groupID: str    
    queueID: str
    queueDelay: float

def createActivities(activityInputs, destinations, queues):
    activities = {}
    activitiesWithQueue = []
    for activityInput in activityInputs:
        activity = ACTIVITY_CONFIG_CLASS_2_CREATION_FCN[activityInput.__class__](activityInput, 
                                                                                 destinations,
                                                                                 queues)
        activities[activity.ID] = activity
        if activity.hasQueue:
            activitiesWithQueue.append(activity)

    return activities, activitiesWithQueue

def getActivityBaseArgs(activityInput, destinations):
    return activityInput.ID, destinations[activityInput.destinationID]

def getActivityBaseKwargs(activityInput, queues):
    baseKwargs = {}
    if activityInput.groupID is not None:
        baseKwargs['groupID'] = activityInput.groupID
    
    if activityInput.queueID is not None:
        baseKwargs['queue'] = queues[activityInput.queueID]

    if activityInput.queueID is not None:
        baseKwargs['queueDelay'] = activityInput.queueDelay


    return baseKwargs

# ===============================================================================
@dataclass
class SinkInput(ActivityInput): pass

def createSink(activityInput, destinations, queues):
    return Sink(*getActivityBaseArgs(activityInput, destinations), 
                **getActivityBaseKwargs(activityInput, queues))

# ===============================================================================
@dataclass
class WaypointInput(ActivityInput): pass

def createWaypoint(activityInput, destinations, queues):
    return Waypoint(*getActivityBaseArgs(activityInput, destinations), 
                **getActivityBaseKwargs(activityInput, queues))
# ===============================================================================
@dataclass
class FixedWaitingTimeActivityInput(ActivityInput):
    name: str
    duration: float

def createFixedWaitingTimeActivity(activityInput, destinations, queues):
    return FixedWaitingTimeActivity(*getActivityBaseArgs(activityInput, destinations), 
                                    activityInput.name, activityInput.duration, 
                                    **getActivityBaseKwargs(activityInput, queues))
    
# ===============================================================================
@dataclass
class FixedWaitingTimeNonInteractingActivityInput(FixedWaitingTimeActivityInput): pass

def createFixedWaitingTimeNonInteractingActivity(activityInput, destinations, queues):
    return FixedWaitingTimeNonInteractingActivity(*getActivityBaseArgs(activityInput, destinations), 
                                                  activityInput.name, activityInput.duration, 
                                    **getActivityBaseKwargs(activityInput, queues))
    
# ===============================================================================
@dataclass
class EndAtTimeWaitingActivityInput(ActivityInput):
    name: str
    endTime: float

def createEndAtTimeWaitingActivity(activityInput, destinations, queues):
    return EndAtTimeWaitingActivity(*getActivityBaseArgs(activityInput, destinations),
                                    activityInput.name, activityInput.endTime, 
                                    **getActivityBaseKwargs(activityInput, queues))
    
# ===============================================================================

@dataclass
class FixedTimeOutsideOfSimulationActivityInput(FixedWaitingTimeActivityInput): pass

def createFixedTimeOutsideOfSimulationActivity(activityInput, destinations, queues):
    return FixedTimeOutsideOfSimulationActivity(*getActivityBaseArgs(activityInput, destinations), 
                                                  activityInput.name, activityInput.duration, 
                                    **getActivityBaseKwargs(activityInput, queues))
    
# ===============================================================================

@dataclass
class EventOutsideOfSimulationActivityInput(ActivityInput): pass

def createEventOutsideOfSimulationActivity(activityInput, destinations, queues):
    return EventOutsideOfSimulationActivity(*getActivityBaseArgs(activityInput, destinations), 
                                                  activityInput.name, **getActivityBaseKwargs(activityInput, queues))

# ===============================================================================
ACTIVITY_CONFIG_CLASS_2_CREATION_FCN = {
    SinkInput: createSink,
    WaypointInput: createWaypoint,
    FixedWaitingTimeActivityInput: createFixedWaitingTimeActivity,
    FixedWaitingTimeNonInteractingActivityInput: createFixedWaitingTimeNonInteractingActivity,
    EndAtTimeWaitingActivityInput: createEndAtTimeWaitingActivity,
    FixedTimeOutsideOfSimulationActivityInput: createFixedTimeOutsideOfSimulationActivity,
    EventOutsideOfSimulationActivityInput: createEventOutsideOfSimulationActivity
    }

# ===============================================================================
# ===============================================================================
@dataclass
class ActivityPatternInput():
    ID: str

def createActivityPatterns(activityPatternInputs, activities):
    activityPatterns = {}

    for activityPatternInput in activityPatternInputs:
        ID = activityPatternInput.ID
        
        activityPatterns[ID] = ACTIVITY_PATTERN_CONFIG_CLASS_2_CREATION_FCN[activityPatternInput.__class__](ID, activities, activityPatternInput)

    return activityPatterns

# ===============================================================================
@dataclass
class ListActivityPatternInput(ActivityPatternInput):
    activityIDs: tuple
    
def createListActivityPattern(ID, activities, activityPatternInput):
        activityList = []
        for activityID in activityPatternInput.activityIDs:
            activityList.append(activities[activityID])
            
        return ListActivityPattern(ID, activityList)
    
# ===============================================================================    
ACTIVITY_PATTERN_CONFIG_CLASS_2_CREATION_FCN = {
    ListActivityPatternInput: createListActivityPattern,
    }    

# ===============================================================================
# ===============================================================================
@dataclass
class DemandPatternInput():
    ID: str
    sourceID: str
    activityPatternID: str
    parameterSetID: str

def createDemandPatterns(demandPatternInputs, pedParameterSets, activityPatterns, sources):
    demandPatterns = {}

    for demandPatternInput in demandPatternInputs:
        ID = demandPatternInput.ID
        creationFcn, addArgs, addKwargs = DEMAND_PATTERN_CONFIG_CLASS_2_CREATION_FCN[demandPatternInput.__class__](demandPatternInput)

        source = sources[demandPatternInput.sourceID]
        pedParamSet = pedParameterSets[demandPatternInput.parameterSetID]
        activityPattern = activityPatterns[demandPatternInput.activityPatternID]

        demandPatterns[ID] = creationFcn(ID, source, pedParamSet, activityPattern,
                                         *addArgs, **addKwargs)

    return demandPatterns

# ===============================================================================
@dataclass
class ConstantDemandPatternInput(DemandPatternInput):
    flowPerSecond: float
    startTime: float = 0
    endTime: float = inf
    placement: str = RANDOM_PLACEMENT_TYPE
    placementLocation: tuple = ()

def getConstantDemandPatternInfo(demandPatternInput):
    addArgs = (demandPatternInput.flowPerSecond, )
    addKwargs = {
        'startTime':demandPatternInput.startTime,
        'endTime':demandPatternInput.endTime,
        'placement':demandPatternInput.placement,
        'placementLocation':demandPatternInput.placementLocation}

    return ConstantDemandPattern, addArgs, addKwargs

# ===============================================================================
@dataclass
class PoissonDemandPatternInput(ConstantDemandPatternInput):
    pass

def getPoissonDemandPatternInfo(demandPatternInput):
    addArgs = (demandPatternInput.flowPerSecond, )
    addKwargs = {
        'startTime':demandPatternInput.startTime,
        'endTime':demandPatternInput.endTime,
        'placement':demandPatternInput.placement,
        'placementLocation':demandPatternInput.placementLocation}

    return PoissonDemandPattern, addArgs, addKwargs

# ===============================================================================
@dataclass
class VariableDemandPatternInput(DemandPatternInput):
    timeFlowArray: tuple
    startTime: float = 0
    endTime: float = inf
    placement: str = RANDOM_PLACEMENT_TYPE
    placementLocation: tuple = ()

def getVariableDemandPatternInfo(demandPatternInput):
    addArgs = (demandPatternInput.timeFlowArray, )
    addKwargs = {
        'startTime':demandPatternInput.startTime,
        'endTime':demandPatternInput.endTime,
        'placement':demandPatternInput.placement,
        'placementLocation':demandPatternInput.placementLocation}

    return VariableDemandPattern, addArgs, addKwargs

# ===============================================================================
@dataclass
class DiscreteDemandPatternInput(DemandPatternInput):
    discretePattern: tuple
    placement: str = RANDOM_PLACEMENT_TYPE
    placementLocation: tuple = ()

def getDiscreteDemandPatternInfo(demandPatternInputs):
    addArgs = (demandPatternInputs.discretePattern, )
    addKwargs = {'placement':demandPatternInputs.placement,
        'placementLocation':demandPatternInputs.placementLocation}

    return DiscreteDemandPattern, addArgs, addKwargs

# ===============================================================================
DEMAND_PATTERN_CONFIG_CLASS_2_CREATION_FCN = {
    ConstantDemandPatternInput: getConstantDemandPatternInfo,
    PoissonDemandPatternInput: getPoissonDemandPatternInfo,
    VariableDemandPatternInput: getVariableDemandPatternInfo,
    DiscreteDemandPatternInput: getDiscreteDemandPatternInfo,
    }

# ===============================================================================
# ===============================================================================
@dataclass
class SchedulerInput():
    sourceID: str
    sinkID: str

# ===============================================================================
@dataclass
class RestaurantStaffSchedulerParametersInput():
    activityDuration: Parameter = getParameter(ACTIVITY_DURATION_DISTR)
    baseFirstProbability: float = BASE_FIRST_PROBABILITY
    baseAfterProbability: float = BASE_AFTER_PROBABILITY
    gapsDirichletAlpha: float = GAPS_DIRICHLET_ALPHA
    afterVisitActivityCount: Parameter = AFTER_VISIT_ACTIVITY_COUNT
    baseActivityDuration: float = BASE_ACTIVITY_DURATION
    
@dataclass
class RestaurantStaffSchedulerInput():
    baseAreaID: str
    staffCount: int
    pedParamSetID: str
    activityCount: int
    neighborhoods: tuple = ()
    parameters: RestaurantStaffSchedulerParametersInput = field(default_factory=RestaurantStaffSchedulerParametersInput())

@dataclass
class RestaurantSchedulerInput(SchedulerInput):
    demandPattern: tuple
    sittingDestinationGroupIDs: tuple
    visitDuration: Parameter
    inGroupEntryDistr: Distribution
    guestPedParamSetDistr: tuple
    toiletDestinationIDs: tuple = None
    toiletVisitProbability: float = None
    toiletVisitDuration: Parameter = None
    coatRackDestinationID: str = None
    coatRackVisitDuration: Parameter = None
    registerDestinationID: str = None
    registerVisitDuration: Parameter = None
    useTablesOnlyOnce: bool = False
    intraTableGroupBuffer: float = INTRA_TABLE_GROUP_BUFFER
    staffScheduler: RestaurantStaffSchedulerInput = None

def addRestaurantScheduler2demandManager(schedulerInput, demandManager, destinations, pedParameterSets, sources, activities, walkLevels):
    scheduler = createRestaurantScheduler(schedulerInput, destinations, pedParameterSets, sources, activities, walkLevels)
    scheduler.initialize()
    demandPatterns = scheduler.createDemandPatterns()
    demandManager.addDemandPatterns(demandPatterns)
    
    if scheduler.staffDynamicScheduler is not None:
        demandManager.addDynamicSchedulers(scheduler.staffDynamicScheduler)
    
@dataclass
class CustomSchedulerInput():
    configFileName: str
    moduleName: str
    package: str = None

def addCustomScheduler2demandManager(schedulerInput, demandManager, destinations, pedParameterSets, sources, activities, walkLevels):
    custom_scheduler_mod = importlib.import_module(schedulerInput.moduleName, 
                                                   package=schedulerInput.package)
    configFlNm = None
    scheduler = custom_scheduler_mod.createScheduler(configFlNm, destinations, pedParameterSets, sources, activities, walkLevels)
    demandManager.addDemandPatterns(scheduler.createDemandPatterns())
    demandManager.addDynamicSchedulers(scheduler.getDynamicSchedulers())
    
# ===============================================================================
# ===============================================================================
@dataclass
class DemandManagerInput():
    activities: List[ActivityInput]
    activityPatterns: List[ActivityPatternInput] = None
    demandPatterns: List[DemandPatternInput] = None
    scheduler: SchedulerInput = None

def createDemandManager(simulationInput, walkLevels, destinations, queues):
    activities, activitiesWithQueue = createActivities(simulationInput.demandManager.activities, 
                                  destinations, queues)

    demandManager = DemandManager(activities, activitiesWithQueue)

    pedParameterSets = createPedParameterSets(simulationInput.pedParameterSets)
    sources, sourcesPerWalkLevel = createSources(simulationInput.walkLevels, walkLevels)
    for walkLevel, sourcesList in sourcesPerWalkLevel.items():
        demandManager.addSources(sourcesList, walkLevel)
        walkLevel.addSources(sourcesList)

    if isinstance(simulationInput.demandManager.scheduler, RestaurantSchedulerInput): 
        addRestaurantScheduler2demandManager(simulationInput.demandManager.scheduler, demandManager, 
                                             destinations, pedParameterSets, sources, activities, walkLevels)
    elif isinstance(simulationInput.demandManager.scheduler, CustomSchedulerInput):
        addCustomScheduler2demandManager(simulationInput.demandManager.scheduler, demandManager,
                                         destinations, pedParameterSets, sources, activities, walkLevels)
    elif simulationInput.demandManager.scheduler is None:
        activityPatterns = createActivityPatterns(simulationInput.demandManager.activityPatterns, activities)
        demandPatterns = createDemandPatterns(simulationInput.demandManager.demandPatterns, pedParameterSets, activityPatterns, sources)
        demandManager.addDemandPatterns(demandPatterns)
    else:
        raise Exception('Unknown scheduler')
            
    return demandManager, pedParameterSets

# ===============================================================================
# ===============================================================================
@dataclass
class OutputManagerInput():
    ID: str 
    
@dataclass
class BasicFileOutputManagerInput(OutputManagerInput):
    outputPd: str
    baseFilename: str

@dataclass
class SimulationFileOutputManagerInput(BasicFileOutputManagerInput): 
    exportGrid: bool = False
    
@dataclass
class ConnectivityFileOutputManagerInput(BasicFileOutputManagerInput):
    connectionCutOffDist: float = None
    
@dataclass
class DebugOutputManagerInput(BasicFileOutputManagerInput): pass

@dataclass
class InMemoryOutputManagerInput(OutputManagerInput):
    saveTimeStep: float = None

@dataclass
class DummyOutputManagerInput(OutputManagerInput): pass

def createOutputManagers(outputManagerInputs, seed, extraOutputManagers):
    outputManager = OutputManager();
    for outputManagerInput in outputManagerInputs:
        kwargs = {}
        if isinstance(outputManagerInput, BasicFileOutputManagerInput):
            args = (outputManagerInput.ID, outputManagerInput.outputPd, outputManagerInput.baseFilename, seed)
            if isinstance(outputManagerInput, SimulationFileOutputManagerInput):
                kwargs["exportGrid"] = outputManagerInput.exportGrid;
                outputManagerInstance = SimulationFileOutputManager(*args, **kwargs)
            elif isinstance(outputManagerInput, ConnectivityFileOutputManagerInput):
                if outputManagerInput.connectionCutOffDist is not None:
                    kwargs['connectionCutOffDist'] = outputManagerInput.connectionCutOffDist
                outputManagerInstance = ConnectivityFileOutputManager(*args, **kwargs)
            else:
                raise Exception(f'Unknown output manager type "{outputManagerInput}"!')
        elif isinstance(outputManagerInput, InMemoryOutputManagerInput):
            saveTimeStep = outputManagerInput.saveTimeStep
            outputManagerInstance = InMemoryOutputManager(outputManagerInput.ID, saveTimeStep)
        elif isinstance(outputManagerInput, DummyOutputManagerInput):
            outputManagerInstance = DummyOutputManager()
        else:
            raise Exception(f'Unknown output manager type "{outputManagerInput}"!')
        
        outputManager.addOuputManager(outputManagerInstance)
    
    for extraOutputManager in extraOutputManagers:
        outputManager.addOuputManager(extraOutputManager)

    outputManager.createFilenames()
    return outputManager

# ===============================================================================
# ===============================================================================

ParameterInput = Union[StaticValueInput, DistributionInput]

@dataclass
class PedParameterSetInput():
    ID: str
    baseSet: str = None
    preferredSpeed: ParameterInput = None
    tau: ParameterInput = None
    a_0: ParameterInput = None
    r_0: ParameterInput = None
    a_1: ParameterInput = None
    r_1: ParameterInput = None
    kappa_0: ParameterInput = None
    kappa_1: ParameterInput = None
    a_W: ParameterInput = None
    d_shy: ParameterInput = None
    r_infl: ParameterInput = None
    ie_f: ParameterInput = None
    ie_b: ParameterInput = None
    cPlus_0: ParameterInput = None
    cMinus_0: ParameterInput = None
    t_A: ParameterInput = None
    radius: ParameterInput = None
    noise: ParameterInput = None

def createPedParameterSets(pedParameterSetInputs):
    pedParameterSets = {}
    pedParameterSetsDicts = {}

    pedParameterSetInputsWithBaseSets = []

    for pedParameterSetInput in pedParameterSetInputs:
        ID = pedParameterSetInput.ID
        if pedParameterSetInput.baseSet is not None:
            pedParameterSetInputsWithBaseSets.append(pedParameterSetInput)
            continue

        paramDict = asdict(pedParameterSetInput)
        paramDict.pop('ID')
        pedParameterSets[ID] = PedParameterSet(ID, **paramDict)
        pedParameterSetsDicts[ID] = paramDict

    addPedParameterSetsWithBaseSet(pedParameterSets, pedParameterSetInputsWithBaseSets, pedParameterSetsDicts)

    return pedParameterSets

def addPedParameterSetsWithBaseSet(pedParameterSets, pedParameterSetInputsWithBaseSets, pedParameterSetsDicts):
    pedParameterSetInputsToCheck = copy(pedParameterSetInputsWithBaseSets)
    while len(pedParameterSetInputsToCheck) > 0:
        pedParameterSetInputsWithBaseSets = copy(pedParameterSetInputsToCheck)
        pedParameterSetInputsToCheck = []
        for pedParameterSetInput in pedParameterSetInputsWithBaseSets:
            if pedParameterSetInput.baseSet not in pedParameterSets:
                pedParameterSetInputsToCheck.append(pedParameterSetInput)
            else:
                addPedParameterSetWithBaseSet(pedParameterSets, pedParameterSetInput, pedParameterSetsDicts)
        if len(pedParameterSetInputsToCheck) == len(pedParameterSetInputsWithBaseSets):
            setStr = '('
            for pedParameterSetInput in pedParameterSetInputsToCheck:
                setStr += '{}, '.format(pedParameterSetInput.ID)
            setStr += ')'
            raise Exception('The base set(s) for pedParameterSet(s) {} does not exist!'.format(setStr))

def addPedParameterSetWithBaseSet(pedParameterSets, pedParameterSetInput, pedParameterSetsDicts):
        ID = pedParameterSetInput.ID

        paramDict = pedParameterSetsDicts[pedParameterSetInput.baseSet]

        paramAddDict = asdict(pedParameterSetInput)
        paramAddDict.pop('ID')
        paramAddDict.pop('baseSet')

        for key, value in paramAddDict.items():
            if value is None and paramDict[key] is not None:
                continue
            paramDict[key] = value

        pedParameterSets[ID] = PedParameterSet(ID, **paramDict)
        pedParameterSetsDicts[ID] = paramDict

# ===============================================================================
# ===============================================================================
@dataclass
class NomadSimulationInput():
    name: str
    label: str
    duration: float
    walkLevels: List[WalkLevelInput]
    demandManager: DemandManagerInput
    outputManagers: List[OutputManagerInput]
    pedParameterSets: List[PedParameterSetInput]
    inIsolationTimeStep: float = None
    inRangeStepsPerInIsolationStep: int = None
    inCollisionStepsPerInIsolationStep: int = None
    distBetweenQueueingPeds: float = None
    gridCellSize: float = None
    pedManagerType: str = None
    accCalcFcnType: str = None
    nomadModelClass: str = None

    _hasBeenValidated: bool = False

    def validateInput(self):
        self.checkAndAssignObstacleIDs()
        self.checkAccFcns()
        self._hasBeenValidated = True

    @property
    def hasBeenValidated(self):
        return self._hasBeenValidated

    def checkAndAssignObstacleIDs(self):
        obstacleIDs = set()
        for walkLevelInput in self.walkLevels:
            for obstacleInput in walkLevelInput.obstacles:
                if obstacleInput.ID is None:
                    continue
                if obstacleInput.ID in obstacleIDs:
                    raise Exception('An obstacle with ID={} already exists!'.format(obstacleInput.ID))
                obstacleIDs.add(obstacleInput.ID)

        for walkLevelInput in self.walkLevels:
            obstacleIDcounter = 0
            for obstacleInput in walkLevelInput.obstacles:
                if obstacleInput.ID is None:
                    obstacleID = createObstacleID(walkLevelInput.ID, obstacleIDcounter)
                    while obstacleID in obstacleIDs:
                        obstacleIDcounter += 1
                        obstacleID = createObstacleID(walkLevelInput.ID, obstacleIDcounter)
                    obstacleInput.ID = obstacleID
                    obstacleIDcounter += 1

    def checkAccFcns(self):
        if self.accCalcFcnType is not None:
            prefix = 'acc_'
            if not self.accCalcFcnType.startswith(prefix):
                self.accCalcFcnType = f'{prefix}{self.accCalcFcnType}'

def createObstacleID(walkableLevelID, obstacleIDcounter):
    return '{}_obs_{:04d}'.format(walkableLevelID, obstacleIDcounter)

# ===============================================================================
# The functions that take an instance of the NomadSimulationInput data class and
# create a NomadModel instance based on its content
# ===============================================================================

def createNomadModelFromSimulationInput(simulationInput, seed, lrcmLoadFile=None, gridManagerFile=None, useIndPedManager=False, extraOutputManagers=()):
    if not simulationInput.hasBeenValidated:
        simulationInput.validateInput()

    walkLevels, destinations, queues = createWalkLevels(simulationInput.walkLevels)
    demandManager, pedParameterSets = createDemandManager(simulationInput, walkLevels, destinations, queues)
    outputManager = createOutputManagers(simulationInput.outputManagers, seed, extraOutputManagers)
    nomadParameterDict = createNomadParameterDict(simulationInput)
    optionalArgs = getOptionalArgs(simulationInput, lrcmLoadFile, gridManagerFile, useIndPedManager)

    scenario = createNomadModel(simulationInput.name, simulationInput.label, seed, simulationInput.duration,
                          walkLevels, demandManager, outputManager, nomadParameterDict, pedParameterSets,
                          **optionalArgs)

    return scenario

# ---------------------------------------------------------------------------------

def createNomadParameterDict(simulationInput):
    parametersDict = {}
    if simulationInput.distBetweenQueueingPeds is not None:
        parametersDict[DIST_BETWEEN_QUEUEING_PEDS] = simulationInput.distBetweenQueueingPeds
    if simulationInput.gridCellSize is not None:
        parametersDict[GRID_CELL_SIZE_NM] = simulationInput.gridCellSize
    if simulationInput.accCalcFcnType is not None:
        parametersDict[ACC_CALC_FCN_NM] = simulationInput.accCalcFcnType

    return parametersDict

# ---------------------------------------------------------------------------------

OPTIONAL_ARGS = ('inIsolationTimeStep', 'inRangeStepsPerInIsolationStep',
    'inCollisionStepsPerInIsolationStep')

def getOptionalArgs(simulationInput, lrcmLoadFile, gridManagerFile, useIndPedManager):
    optionalArgs = {}

    for optionalArg in OPTIONAL_ARGS:
        if getattr(simulationInput, optionalArg) is not None:
            optionalArgs[optionalArg] = getattr(simulationInput, optionalArg)

    if lrcmLoadFile is not None:
        optionalArgs['lrcmLoadFile'] = lrcmLoadFile
    if gridManagerFile is not None:
        optionalArgs['gridManagerFile'] = gridManagerFile
            
    optionalArgs['useIndPedManager'] = useIndPedManager
        

    return optionalArgs
