""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from copy import copy
import logging

from shapely.geometry.linestring import LineString
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.point import Point
from shapely.geometry.polygon import Polygon
import shapely.ops

import NOMAD
from NOMAD.constants import PED_STATIC_STATE, PED_STATIC_NON_INTERACTING_STATE, \
    PED_MOVING_STATE, PED_OUTSIDE_OF_SIM_STATE, WALKING_ACTIVITY_NAME, \
    QUEUEING_ACTIVITY_NAME
from NOMAD.general_functions import doListCeck, SpawnableLine, \
    RectangleFilledWithPoints
from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
from NOMAD.vectors import Vector2D


logger = logging.getLogger(__name__)

# ================================================================================================
# ================================================================================================
class Queue(ShapelyGeomClassWrapper):
    
    def __init__(self, ID, coords, walkLevel, isVirtualObstacle=False):
        ShapelyGeomClassWrapper.__init__(self, ID)
        self._geometry = LineString(coords)
        self._walkLevel = walkLevel
        self._isVirtualObstacle = isVirtualObstacle
        if isVirtualObstacle:
            logger.warn('The isVirtualObstacle attribute is not yet in use!')
        
        self._distBetweenQueueingPeds = None
        self._queueActivity = None
        self._coupledActivity = None
        
        self._waitDestinations = []
        self._arrivalDestination = None
        self._departureDestination = None
        self._firstFreeWaitPointInd = 0
        
        self._ped2destination = {}
        self._ped2destinationInd = {}
        self._pedsInQueue = []
        self._pedAtCoupledActivity = None
                
        self._overflow = []
        
    @property
    def walkLevel(self):
        return self._walkLevel

    @property
    def isVirtualObstacle(self):
        return self._isVirtualObstacle

    @property
    def pedestrians(self):
        return self._ped2destination.keys()
        
    def canMoveToNextActivity(self, ped):
        if self._pedAtCoupledActivity is None and len(self._pedsInQueue) == 0:
            self._pedAtCoupledActivity = ped
        
        return self._pedAtCoupledActivity == ped

    def isMovingToWaitingPoint(self, ped):
        return self._ped2destination[ped] != self._arrivalDestination
        
    def pedIsInQueue(self, ped):
        return ped in self._ped2destination
        
    def isPedWaitingAtArrivalPoint(self, ped):
        if ped ==  self._pedAtCoupledActivity:
            return False
        
        if ped in self._overflow:
            return True
        
        return ped in self._ped2destinationInd and self._ped2destinationInd[ped] == len(self._waitDestinations)
        
    def addPed(self, ped):
        if self._pedAtCoupledActivity is None and len(self._pedsInQueue) == 0:
            self._pedAtCoupledActivity = ped
            return False
        
        if self._firstFreeWaitPointInd >= len(self._waitDestinations):
            self._overflow.append(ped)
            return True
                                        
        self._ped2destination[ped] = self._waitDestinations[self._firstFreeWaitPointInd]
        self._ped2destinationInd[ped] =  self._firstFreeWaitPointInd
        self._pedsInQueue.append(ped)
        self._firstFreeWaitPointInd += 1
        return False
    
    def removePed(self, ped):
        if self._pedAtCoupledActivity == ped:
            return
        self._ped2destination.pop(ped)
        self._ped2destinationInd.pop(ped)
        self._pedsInQueue.remove(ped)
        self._pedAtCoupledActivity = ped
        if len(self._ped2destination) == 0:
            self._firstFreeWaitPointInd = 0
        
    def pedLeftMainActivity(self):
        self._pedAtCoupledActivity = None
    
    def setNextDestinationForPed(self, ped):
        oldIndex = self._ped2destinationInd[ped]
        newIndex = oldIndex - 1
        self._ped2destination[ped] = self._waitDestinations[newIndex]
        self._ped2destinationInd[ped] = newIndex
        if oldIndex + 1 == self._firstFreeWaitPointInd:
            self._firstFreeWaitPointInd -= 1
    
    def getNextPedInQueue(self, ped):
        if ped in self._ped2destination:
            nextInd = self._pedsInQueue.index(ped) + 1
            if nextInd >= len(self._pedsInQueue):
                if len(self._overflow) > 0:
                    self.addPed(self._overflow.pop(0)) 
                    
                return None
            return self._pedsInQueue[nextInd]
        
        if len(self._pedsInQueue) > 0:
            return self._pedsInQueue[0]
        
        return None
        
    def getDesiredDirectionForPed(self, ped):
        if ped in self._ped2destination:
            return Vector2D.getDirVec2otherVec(ped.pos, self._ped2destination[ped].pointCoordsVec)
        else:
            return self._arrivalDestination.getDesiredDirectionForPed(ped)
            
    def getDestinationID(self, ped):
        if ped in self._ped2destination:
            return self._ped2destination[ped].ID
        else:
            return self._arrivalDestination.ID
            
    def isFirstPedInQueue(self, ped):
        return ped == self._pedAtCoupledActivity or \
            (ped in self._ped2destination and self._ped2destination[ped] == self._departureDestination)
    
    def create(self, distBetweenQueueingPeds, demandManager):
        self._distBetweenQueueingPeds = distBetweenQueueingPeds
        self._createWaitPointDestinations()
        # Add last point dest to destinations of walklevel
        self._waitDestinations[-1]._ignoreInLrcc = False
        self._walkLevel.addDestinations(self._waitDestinations)
        # Create queue activity
        self._queueActivity = QueueActivity(f'{self.ID}_act', self._waitDestinations[-1],
                                            self)
        # Insert the queue activity in front of the main activity in the activity patterns
        demandManager.insertQueueActivityInPattern(self._coupledActivity, self._queueActivity)
    
    def _getStartingPoint(self):
        if self._coupledActivity.destination.distance(Point(self._geometry.coords[0])) < self._coupledActivity.destination.distance(Point(self._geometry.coords[-1])):
            return 0, 1
        else:
            return -1, -1
        
    def _createWaitPointDestinations(self):
        startingPointInd, loopDir = self._getStartingPoint()
        basePoint = Vector2D(self._geometry.coords[startingPointInd])
        nextPoint = Vector2D(self._geometry.coords[startingPointInd + loopDir])
        dirVec = Vector2D.getDirVec2otherVec(basePoint, nextPoint)
        dist2nextPoint = Vector2D.distance(basePoint, nextPoint)
        point = Vector2D(basePoint)
        basePointInd = startingPointInd
        while True:
            waitPointDest = self._createWaitPointDestination(point)
            if self._departureDestination is None:
                self._departureDestination = waitPointDest
            self._waitDestinations.append(waitPointDest)
            point = point + dirVec*self._distBetweenQueueingPeds
            distFromBase = Vector2D.distance(point, basePoint)
            if distFromBase >= dist2nextPoint:
                try:
                    offset = distFromBase - dist2nextPoint
                    basePointInd += loopDir
                    basePoint = Vector2D(self._geometry.coords[basePointInd])
                    nextPoint = Vector2D(self._geometry.coords[basePointInd + loopDir])
                    dirVec = Vector2D.getDirVec2otherVec(basePoint, nextPoint)
                    dist2nextPoint = Vector2D.distance(basePoint, nextPoint)
                    point = basePoint + dirVec*offset                    
                except IndexError:
                    break
                
        self._arrivalDestination = waitPointDest
                      
    def _createWaitPointDestination(self, point):        
        return PointDestination(f'{self._ID}_{len(self._waitDestinations):02d}',
                                                       point, groupID=self._ID, ignoreInLrcc=True)
                   
    def coupleActivity(self, activity):
        if self._coupledActivity is not None:
            raise Exception('Multiple activities per queue is not yet supported!')
        
        self._coupledActivity = activity
        
# ================================================================================================
# ================================================================================================
def createDestinationFromGeometry(ID, geometry, *args, **kwargs):
    kwargs['geometry'] = geometry
    if isinstance(geometry, Point):
        return PointDestination(ID, *args, **kwargs)
    elif isinstance(geometry, LineString):
        return LineDestination(ID, *args, **kwargs)
    elif isinstance(geometry, Polygon):
        return PolygonDestination(ID, *args, **kwargs)
    elif isinstance(geometry, MultiPolygon):
        return MultiPolygonDestination(ID, *args, **kwargs)
    else:
        shapelyType = geometry.wkt
        raise TypeError(f'Unsupported shapely type "{shapelyType}"')

# ================================================================================================
# ================================================================================================
class Destination(ShapelyGeomClassWrapper):

    def __init__(self, ID, groupID=None, isVirtualObstacle=False, isLocalDestination=False, geometry=None, 
                 ignoreInLrcc=False, hasSubDestination= False):
        '''
        Constructor
        '''
        ShapelyGeomClassWrapper.__init__(self, ID)
        self._localRouteChoiceManager = None
        self._groupID = groupID
        self.isVirtualObstacle = isVirtualObstacle
        self.isLocalDestination = isLocalDestination
        self._geometry = geometry
        self._ignoreInLrcc = ignoreInLrcc
        self._hasSubDestionation = hasSubDestination

        self._centerPointVec = None
        
    @property
    def groupID(self):
        return self._groupID

    @property
    def hasSubDestination(self):
        return self._hasSubDestionation

    def setSubDestination(self, ped):
        raise NotImplementedError('Implement function')

    def setLocalRouteChoiceManager(self, localRouteChoiceManager):
        self._localRouteChoiceManager = localRouteChoiceManager

    def getDesiredDirectionForPedVec(self, ped):
        return Vector2D(self._localRouteChoiceManager.getDesiredDirectionForPed(ped))

    def getDesiredDirectionForPed(self, ped):
        return self._localRouteChoiceManager.getDesiredDirectionForPed(ped)

    def hasBeenReachedByPed(self, ped):
        # Either the current position point lies within the activity
        # or the line between the the previous and current position crosses the activity
        if Point(ped.pos).within(self._geometry):
            return True

        posLine = LineString((ped.prevPos, ped.pos))
        if posLine.crosses(self._geometry):
            return True

        return False

    def getDestination(self, ped): # @UnusedVariable
        return self
    
    def pedLeavesDest(self, ped): # @UnusedVariable
        pass

    def getActivityLocation(self, ped):
        raise NotImplementedError('Implement function')

    def getCenterPoint(self):
        return self._geometry.centroid

    def getClosestPoint(self, x, y):
        return shapely.ops.nearest_points(self._geometry, Point((x,y)))

    def getVector2centerPoint(self, posVec):
        if self._centerPointVec is None:
            self._centerPointVec = Vector2D(self.getCenterPoint().coords[0]) 
        
        diffVec = self._centerPointVec - posVec
        return diffVec.getNormalized()

class LineDestination(Destination):

    def __init__(self, ID, lineCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if lineCoords is not None:
            self._geometry = LineString(lineCoords)
            
        self._maxPedDistPerStep = None

    def setMaxPedDistPerStep(self, maxPedDistPerStep):
        self._maxPedDistPerStep = maxPedDistPerStep

    def hasBeenReachedByPed(self, ped):
        # Either the current position point lies within the activity
        # or the line between the the previous and current position crosses the activity
        return Point(ped.pos).distance(self._geometry) <= self._maxPedDistPerStep

    def getPolygonRepresentation(self, buffer):
        leftOffset = self._geometry.parallel_offset(buffer, 'left')
        rightOffset = self._geometry.parallel_offset(buffer, 'right')
        return Polygon(list(leftOffset.coords) + list(rightOffset.coords))

    def getActivityLocation(self, ped):
        closestPoints = shapely.ops.nearest_points(self._geometry, Point(ped.pos))
        return list(closestPoints[0].coords)[0]

class PointDestination(Destination):

    def __init__(self, ID, pointCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if pointCoords is not None:
            self._geometry = Point(pointCoords)
        self.pointCoords = list( self._geometry.coords)[0]
        self.pointCoordsVec = Vector2D(self.pointCoords)

    def setMaxPedDistPerStep(self, maxPedDistPerStep):
        self._maxPedDistPerStep = maxPedDistPerStep

    def hasBeenReachedByPed(self, ped):
        # Either the current position point lies within the activity
        # or the line between the the previous and current position crosses the activity
        return Point(ped.pos).distance(self._geometry) <= self._maxPedDistPerStep

    def getActivityLocation(self, ped):  # @UnusedVariable
        return self.pointCoords

class PolygonDestination(Destination):

    def __init__(self, ID, polygonCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if polygonCoords is not None:
            self._geometry = Polygon(polygonCoords, None)

    def getActivityLocation(self, ped):
        return ped.pos

class CentroidPolygonDestination(PolygonDestination):

    def __init__(self, ID, polygonCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, polygonCoords, *args, **kwargs)
        self.center = self._geometry.centroid.coords[0]

    def getActivityLocation(self, ped):  # @UnusedVariable
        return self.center

class MultiPolygonDestination(Destination):

    def __init__(self, ID, multiPolygonCoords=None, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, *args, **kwargs)
        if multiPolygonCoords is not None:
            polygons = []
            for polygonCoords in multiPolygonCoords:
                polygons.append(Polygon(polygonCoords, None))
            self._geometry = MultiPolygon(polygons)

    def getActivityLocation(self, ped):
        return ped.pos

class MultiPointInRectangleDestination(Destination, RectangleFilledWithPoints):

    def __init__(self, ID, rectangleCoords, distBetweenPoints, walkLevel, directionVec=None, focusPoint=None, randomnessFactor=None, *args, **kwargs):
        '''
        Constructor
        '''
        Destination.__init__(self, ID, hasSubDestination=True, *args, **kwargs)
        RectangleFilledWithPoints.__init__(self, ID, rectangleCoords,
                                           directionVec=directionVec, focusPoint=focusPoint)
        self._randomnessFactor = randomnessFactor
        self.createPoints(distBetweenPoints)

        self._createPointDestinations(walkLevel)
                
        self._ped2pointInd = {} 

    def setMaxPedDistPerStep(self, maxPedDistPerStep):
        for pointDestination in self._pointDestinations:
            pointDestination.setMaxPedDistPerStep(maxPedDistPerStep)

    def getDestination(self, ped): # @UnusedVariable
        if ped not in self._ped2pointInd:
            return self
        
        return self._getPointDest(ped)

    def setSubDestination(self, ped):
        if len(self._freePoints) == 0:
            return self
        pointInd = self._selectDestinationPointInd()
        self._ped2pointInd[ped] = pointInd
        
    def getActivityLocation(self, ped):
        return ped.pos

    def _selectDestinationPointInd(self):
        if self._randomnessFactor is None:
            _, pointInd = self.getOptimalFreePoint()
        elif NOMAD.NOMAD_RNG.choice([0,1], p=[1-self._randomnessFactor,self._randomnessFactor]) == 1:
            _, pointInd = self.getRandomFreePoint()
        else:
            _, pointInd =  self.getOptimalFreePoint()

        self.occupyPoint(pointInd)
        return pointInd

    def _getPointDest(self, ped):
        return self._pointDestinations[self._ped2pointInd[ped]]

    def getDesiredDirectionForPed(self, ped):
        if ped in self._ped2pointInd:
            return self._getPointDest(ped).getVector2centerPoint(ped.pos)
        else:
            return self._localRouteChoiceManager.getDesiredDirectionForPed(ped)

    def pedLeavesDest(self, ped): # @UnusedVariable
        pointInd = self._ped2pointInd.pop(ped)
        self.freePoint(pointInd)
          
    def _createPointDestinations(self, walkLevel):
        self._pointDestinations = []
        
        for ii in range(self._maxPointCount):
            self._pointDestinations.append(PointDestination(f'{self._ID}_{ii:02d}',
                                                       self._centerPoints[ii,:], 
                                                       groupID=self._ID, ignoreInLrcc=True))
        
        walkLevel.addDestinations(self._pointDestinations)

# ================================================================================================
# ================================================================================================
class SpawnableLineDestination(LineDestination, SpawnableLine):
    
    def __init__(self, ID, lineCoords=None, *args, **kwargs):
        LineDestination.__init__(self, ID, lineCoords, *args, **kwargs)
        SpawnableLine.__init__(self, ID, lineCoords)
        self._placementLocations = None

    def setPlacementLocationForPeds(self, peds2place):
        self._placementLocations = {}
        locations, maxPeds2placeCount = self.getPlacementLocations(len(peds2place))
        for ii in range(maxPeds2placeCount):
            self._placementLocations[peds2place[ii]] = locations[ii]
        
        return peds2place[maxPeds2placeCount:]

    def getPlacementLocationOfPed(self, ped):
        return Vector2D(self._placementLocations[ped])

    def getPlacementLocationsInSegments(self, lineSegmentArray, placementCount, maxPedDiameter):
        return self.getRandomPlacementLocationsInSegments(lineSegmentArray, placementCount, maxPedDiameter)
  
# ================================================================================================
# ================================================================================================

class Activity():
    '''
    classdocs
    '''

    __slots__ = ['_ID', 'destination', '_name', '_groupID', '_queue', '_queueDelay']

    def __init__(self, ID, destination, name, groupID=None, queue=None, queueDelay=None):
        '''
        Constructor
        '''
        self._ID = ID
        self.destination = destination
        self._name = name
        self._groupID = groupID       
        self._queue = queue
        self._queueDelay = queueDelay
        if queue is not None:
            queue.coupleActivity(self) 
        
    @property
    def ID(self):
        return self._ID

    @property
    def groupID(self):
        return self._groupID

    @property
    def name(self):
        return self._name

    @property
    def queue(self):
        return self._queue

    @property
    def queueDelay(self):
        return self._queueDelay

    @property
    def hasQueue(self):
        return self._queue is not None

    @property
    def pedStateWhilstPerformingActivity(self):
        raise NotImplementedError('Implement function')
    
    def getDestinationID(self, ped): #@UnusedVariable
        return self.destination.getDestination(ped).ID
    
    # --------------------------------------------------------------------
    # Actions
    def actionOnReachingActivity(self, ped, currentTime):
        ped.addNewActivityToLog(currentTime, self._name, self)
        self.actionAfterReachingActivity(ped)
        
        if self.destination.getDestination(ped).hasSubDestination:
            self.destination.setSubDestination(ped)
            return True
        
        return False
            
    def actionAfterReachingActivity(self, ped):
        raise NotImplementedError('Implement function')

    def actionOnLeavingActivity(self, ped, currentTime):
        self.actionBeforeLeavingActivity(ped)
        self.destination.pedLeavesDest(ped)
        ped.setNextActivity(currentTime)
        return self._queue
    
    def actionBeforeLeavingActivity(self, ped):
        raise NotImplementedError('Implement function')

    def actionWhilstPerformingActivity(self, ped):
        raise NotImplementedError('Implement function')

    # --------------------------------------------------------------------
    # Routing
    def getClosestPoint(self, x, y):
        return self.destination.getClosestPoint(x, y)

    def getDesiredDirectionForPedVec(self, ped):
        return self.destination.getDesiredDirectionForPedVec(ped)

    def getDesiredDirectionForPed(self, ped):
        return self.destination.getDesiredDirectionForPed(ped)

# ================================================================================================
# ================================================================================================

class Sink(Activity):

    __slots__ = ['pedsThatReachedThis']

    def __init__(self, ID, destination, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, 'sink', *args, **kwargs)
        self.pedsThatReachedThis = []

    def actionAfterReachingActivity(self, ped):
        self.pedsThatReachedThis.append(ped)

    def empty(self):
        ped2remove = copy(self.pedsThatReachedThis)
        self.pedsThatReachedThis = []
        return ped2remove

    @property
    def pedStateWhilstPerformingActivity(self):
        return None
# ================================================================================================
# ================================================================================================

class Waypoint(Activity):

    def __init__(self, ID, destination, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, 'waypoint', *args, **kwargs)

    def actionAfterReachingActivity(self, ped):
        pass

    def actionBeforeLeavingActivity(self, ped):
        pass

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_MOVING_STATE
# ================================================================================================
# ================================================================================================

class TimeBasedActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    def getEndTime(self, currentTime):
        raise NotImplementedError('Implement function')

class EventBasedActivity(Activity):

    def __init__(self, ID, destination, name):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name)

# ================================================================================================
# ================================================================================================

class StaticInteractingActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_STATIC_STATE

    def actionAfterReachingActivity(self, ped):
        if not self.destination.getDestination(ped).hasSubDestination:
            ped.pos = self.destination.getActivityLocation(ped)
            ped.vel = (0,0)

    def actionBeforeLeavingActivity(self, ped):
        pass

    def actionWhilstPerformingActivity(self, ped):
        pass

class StaticNonInteractingActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)
    
    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_STATIC_NON_INTERACTING_STATE

    def actionAfterReachingActivity(self, ped):
        if not self.destination.getDestination(ped).hasSubDestination:
            ped.pos = self.destination.getActivityLocation(ped)
            ped.vel = (0,0)

    def actionBeforeLeavingActivity(self, ped):
        pass
    
    def actionWhilstPerformingActivity(self, ped):
        pass

class DynamicInteractingActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_MOVING_STATE

    def actionAfterReachingActivity(self, ped):
        pass

    def actionBeforeLeavingActivity(self, ped):
        pass

    def actionWhilstPerformingActivity(self, ped):
        pass

class OutsideOfSimulationActivity(Activity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        '''
        Constructor
        '''
        super().__init__(ID, destination, name, *args, **kwargs)

    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_OUTSIDE_OF_SIM_STATE

    def actionAfterReachingActivity(self, ped):
        pass

    def actionOnLeavingActivity(self, ped, currentTime):
        super().actionOnLeavingActivity(ped, currentTime)
        if ped.state == PED_OUTSIDE_OF_SIM_STATE:
            self.spawnPed(ped)
            
        return self._queue

    def actionBeforeLeavingActivity(self, ped):
        pass

    def actionWhilstPerformingActivity(self, ped):
        pass
    
    def spawnPed(self, ped):
        pos = self.destination.getPlacementLocationOfPed(ped)
        ped.setInitialPosAndVel(pos)

# ================================================================================================
# ================================================================================================

# All activities have two superclasses which have to be inherited in a specific order, namely:
# 1) Either the TimeBasedActivity OR the EventBasedActivity
# 2) Either the StaticInteractingActivity OR the StaticNonInteractingActivity OR the DynamicInteractingActivity
# For example
# class SomeActivity(EventBasedActvity, StaticInteractingActivity):
# class SomeActivity(TimeBasedActivity, DynamicInteractingActivity):

class FixedWaitingTimeActivity(StaticInteractingActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, activityDuration, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self.duration = activityDuration

    def getEndTime(self, currentTime):
        return currentTime + self.duration

class EndAtTimeWaitingActivity(StaticInteractingActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, endTime, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self.endTime = endTime

    def getEndTime(self, _):
        return self.endTime

class FixedWaitingTimeNonInteractingActivity(StaticNonInteractingActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, activityDuration, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self.duration = activityDuration

    def getEndTime(self, currentTime):
        return currentTime + self.duration

class SchedulerEndAtTimeWaitingActivity(EndAtTimeWaitingActivity):

    def __init__(self, ID, destination, name, endTime, dynamicActivityScheduler, *args, **kwargs):
        super().__init__(ID, destination, name, endTime, *args, **kwargs)
        self._scheduler = dynamicActivityScheduler

    def actionAfterReachingActivity(self, ped):
        super().actionAfterReachingActivity(ped)
        self._scheduler.pedArrivedAtActivity(self, ped)
        
class FixedTimeOutsideOfSimulationActivity(OutsideOfSimulationActivity, TimeBasedActivity):

    def __init__(self, ID, destination, name, activityDuration, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self._name = name
        self.duration = activityDuration
        
    def getEndTime(self, currentTime):
        return currentTime + self.duration

class EventOutsideOfSimulationActivity(OutsideOfSimulationActivity, EventBasedActivity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)
        self._name = name
        
class InitialEventOutsideOfSimulationActivity(EventOutsideOfSimulationActivity):

    def __init__(self, ID, destination, name, *args, **kwargs):
        super().__init__(ID, destination, name, *args, **kwargs)

    def getDesiredDirectionForPedVec(self, _):
        return Vector2D(0,0)

    def getDesiredDirectionForPed(self, _):
        return (0,0)   
   
# ================================================================================================
# ================================================================================================
class QueueActivity(Activity):
    
    def __init__(self, ID, destination, queue, *args, **kwargs):
        super().__init__(ID, destination, QUEUEING_ACTIVITY_NAME, *args, **kwargs)
        self._queue = queue # Not via super to prevent coupling of the queue
  
    def getDestinationID(self, ped): #@UnusedVariable
        return self._queue.getDestinationID(ped)
  
    def getDesiredDirectionForPed(self, ped):
        return self._queue.getDesiredDirectionForPed(ped)

    def isMovingToWaitingPoint(self, ped):
        return self._queue.isMovingToWaitingPoint(ped)     

    def canMoveToNextActivity(self, ped):
        return self._queue.canMoveToNextActivity(ped)

    def pedHasWaitingPoint(self, ped):
        return self._queue.pedIsInQueue(ped)

    def actionOnReachingActivity(self, ped, currentTime):
        if self._queue.pedIsInQueue(ped):
            if not self._queue.canMoveToNextActivity(ped):
                ped.vel = (0,0)
                return False
            return True
       
        if self._queue.isPedWaitingAtArrivalPoint(ped):
            return True

        return super().actionOnReachingActivity(ped, currentTime)
            
    def actionAfterReachingActivity(self, ped):
        waitAtArrivalPoint = self._queue.addPed(ped)
        if waitAtArrivalPoint:
            ped.vel = (0,0)
            
    def actionOnLeavingActivity(self, ped, currentTime):
        if self._queue.isFirstPedInQueue(ped):
            super().actionOnLeavingActivity(ped, currentTime)
        else:
            self._queue.setNextDestinationForPed(ped)
        
        return self._queue
 
    def actionBeforeLeavingActivity(self, ped):
        self._queue.removePed(ped)

    def actionWhilstPerformingActivity(self, ped):
        pass
    
    @property
    def pedStateWhilstPerformingActivity(self):
        return PED_STATIC_STATE
 
# ================================================================================================
# ================================================================================================

class ActivityPattern():

    def __init__(self, ID):
        '''
        Constructor
        '''
        self._ID = ID
        self._hasActivitiesWithQueue = False
        self._activitiesWithQueue = []

    @property
    def ID(self):
        return self._ID

    @property
    def hasActivitiesWithQueue(self):
        return self._hasActivitiesWithQueue
    
    @property
    def activitiesWithQueue(self):
        return self._activitiesWithQueue

    def getNextActivity(self, ped):
        raise NotImplementedError('Implement method')

    def getInitialActivity(self, ped):
        # Return the activity and the activity name
        raise NotImplementedError('Implement method')
    

class ListActivityPattern(ActivityPattern):
    
    def __init__(self, ID, activityList):
        '''
        Constructor
        '''
        super().__init__(ID)
        self._activityList = doListCeck(activityList)
        for activity in activityList:
            if activity.hasQueue:
                self._hasActivitiesWithQueue = True
                self._activitiesWithQueue.append(activity)
        self._activityIndPerPed = {}

    def getNextActivity(self, ped):
        newActivityInd = self._activityIndPerPed[ped] + 1
        self._activityIndPerPed[ped] = newActivityInd
        
        return self._activityList[newActivityInd] 
    
    def getInitialActivity(self, ped):
        newActivityInd = 0
        self._activityIndPerPed[ped] = newActivityInd
        return self._activityList[newActivityInd], WALKING_ACTIVITY_NAME 
    
    def insertQueueActivity(self, mainActivity, queueActivity):
        insertIndex = self._activityList.index(mainActivity)
        self._activityList.insert(insertIndex, queueActivity)
        
class SchedulerActivityPattern(ActivityPattern):
    
    def __init__(self, ID, scheduler):
        '''
        Constructor
        '''
        super().__init__(ID)
        self._scheduler = scheduler

    def getNextActivity(self, ped):
        return self._scheduler.getNextActivityForPed(ped)
    
    def getInitialActivity(self, ped):
        return self._scheduler.getInitialActivityForPed(ped)
    
class ActivityPatternScheduler():
    
    def __init__(self, ID):
        '''
        Constructor
        '''
        self._ID = ID

    @property
    def ID(self):
        return self._ID

    def getNextActivityForPed(self, ped):
        raise NotImplementedError('Implement method') 
    