""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import ctypes
from pathlib import Path
import platform
import logging.config
from logging.handlers import RotatingFileHandler

class MakeRotatingFileHandler(RotatingFileHandler):
    def __init__(self, filename, *args, **kwargs):
        filenamePath = Path(filename).resolve()
        filenamePath.parent.mkdir(parents=True, exist_ok=True)
        super().__init__(filename, *args, **kwargs)
        
logging_config_file = Path(__file__).parent.joinpath('logging.ini')
logging.config.fileConfig(logging_config_file, disable_existing_loggers=True)

_package_path = Path(__file__).parent.resolve()
_c_path = _package_path.joinpath('C')

if platform.system() == 'Darwin':
    ext = 'dylib'
else:
    ext = 'so'

getInteractionData_so_file = _c_path.joinpath('getInteractionData.' + ext)
calcPedForces_so_file = _c_path.joinpath('calcPedForces.' + ext)
calcAllPedForces_so_file = _c_path.joinpath('calcAllPedForces.' + ext)
convergeCostMatrix_so_file = _c_path.joinpath('convergeCostMatrix.' + ext)

logger = logging.getLogger(__name__)
try:
    ctypes.CDLL(str(getInteractionData_so_file))
    canUseGetInteractionCompiledCode = True
except:
    canUseGetInteractionCompiledCode = False
    logger.warn('Cannot use compiled code for getInteractionData! NOMAD might be slow!')
    
try:
    ctypes.CDLL(str(calcAllPedForces_so_file))
    canUseCalcAllPedForcesCompiledCode = True
except:
    canUseCalcAllPedForcesCompiledCode = False
    logger.warn('Cannot use compiled code for calcAllPedForces! NOMAD might be slow!')

try:
    ctypes.CDLL(str(calcPedForces_so_file))
    canUseCalcPedForcesCompiledCode = True
except:
    canUseCalcPedForcesCompiledCode = False
    logger.warn('Cannot use compiled code for calcPedForces! NOMAD might be slow!')

try:
    ctypes.CDLL(str(convergeCostMatrix_so_file))
    canUseConvergeCostMatrixCompiledCode = True
except:
    canUseConvergeCostMatrixCompiledCode = False    
    logger.warn('Cannot use compiled code for convergeCostMatrix! NOMAD might be slow!')
    
import numpy as np
NOMAD_RNG = np.random.default_rng()

LOCAL_DIR = Path.home().joinpath('nomad')
DEF_OUTPUT_DIR = LOCAL_DIR.joinpath('output')