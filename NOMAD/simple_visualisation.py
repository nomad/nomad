""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging
from math import floor
import math
from pathlib import Path
import sys

logging.getLogger('matplotlib.font_manager').disabled = True
from matplotlib.lines import Line2D
from matplotlib.patches import Polygon, Circle, Ellipse
from matplotlib.widgets import Button, Slider, TextBox

from NOMAD.local_route_choice_manager import getIDstrFomIDs
from NOMAD.output_manager import SimulationFileOutputManager, loadScenario
import numpy as np

try:
    import matplotlib as mpl
    import matplotlib.animation as animation
    import matplotlib.pyplot as plt
except ImportError:
    pass



class SimpleNomadVisualizer():

    logger = logging.getLogger(__name__)

    def __init__(self, scenDataFlNm, guiBackend=None):
        if guiBackend is not None:
            mpl.use(guiBackend)

        logging.info('Loading scenarios data ...')
        scenDataFlNm = Path(scenDataFlNm)

        self.scenData, self.trajData, self.descData, self.lrcmData = loadScenario(scenDataFlNm)
        self.createUpdateDataDict()

        logging.info('Loading local route fields ...')
        lrcmDataFlNm = scenDataFlNm.parent.joinpath(self.scenData.lrcmFile)
        self.lrcmData = SimulationFileOutputManager.readLrcmDataFile(lrcmDataFlNm)
        self.lrcmSets = []
        for walkLevel in self.scenData.geometry.walkLevels:
            walkLevelID = type(walkLevel).__name__
            for destination in walkLevel.destinations:
                destinationID = type(destination).__name__
                label = '{} - {}'.format(walkLevelID, destinationID)
                lrcmSetID = getIDstrFomIDs(walkLevelID, destinationID)
                self.lrcmSets.append((label, lrcmSetID))

        logging.info('Creating visualization ...')
        self.createPedColorDict()

        self.timeInd = 0

        self.playSpeed = 1

        self.anim = None
        self.animRunning = False

        self.fig = plt.figure('Simple vis')
        self.ax = self.fig.add_axes((0.06, 0.2, 0.9, 0.75))
        self.ax.set_xlim((self.scenData.geometry.extent[0], self.scenData.geometry.extent[2]))
        self.ax.set_ylim((self.scenData.geometry.extent[1], self.scenData.geometry.extent[3]))
        self.ax.set_aspect('equal')

        pauseButtonAx = self.fig.add_axes((0.06, 0.025, 0.1, 0.04))
        self.pauseButton = Button(pauseButtonAx, 'pause', hovercolor='0.975')
        self.pauseButton.on_clicked(self.startPause)

        playSpeedTextBoxAx = self.fig.add_axes((0.3, 0.025, 0.08, 0.04))
        self.playSpeedTextBox = TextBox(playSpeedTextBoxAx, 'Play speed:',initial=str(self.playSpeed))
        self.playSpeedTextBox.on_submit(self.onChangePlaySpeed)

        lrcmSliderAx = self.fig.add_axes((0.45, 0.025, 0.3, 0.04))
        self.lrcmQuiverHandle = None
        self.lrcmSlider = Slider(lrcmSliderAx, label='Lrcm',
                                  valmin=0, valmax=len(self.lrcmSets)-1,
                                  valinit=0, valstep=1)
        self.lrcmSlider.on_changed(self.onLrcmSliderChange)
        self.onLrcmSliderChange(None)

        timeSliderAx = self.fig.add_axes((0.16, 0.09, 0.7, 0.04))
        self.timeSlider = Slider(timeSliderAx, label='Time',
                                  valmin=0, valmax=self.scenData.endTimeInd,
                                  valinit=0, valstep=1)
        self.timeSlider.on_changed(self.onTimeSliderChange)
        self.interval = getInterval(self.scenData.timeStep, self.playSpeed)

        self.pedCircles = {}

        # setup the animation control
        self.timeText = self.ax.set_title(getTimeLabel(self.timeInd, self.scenData.timeStep), horizontalalignment='center',
                                  verticalalignment='center', fontsize=16)
        self.anim = animation.FuncAnimation(self.fig, self.update, init_func=self.initialize,
                                            interval=self.interval, blit=True)
        self.animRunning = True
        plt.show()

    def createUpdateDataDict(self):
        updateDataDict = {}
        for timeInd in range(self.scenData.endTimeInd):
            updateDataDict[timeInd] = {'peds2add':[], 'peds2remove':[], 'peds2update':{}}

        for pedID, pedData in self.trajData.items():
            startInd = getTimeInd(pedData[0,0], self.scenData.timeStep)
            endInd = getTimeInd(pedData[-1,0], self.scenData.timeStep) + 1
            updateDataDict[startInd]['peds2add'].append(pedID)
            if endInd < self.scenData.endTimeInd:
                updateDataDict[endInd]['peds2remove'].append(pedID)
            for ii in range(pedData.shape[0]):
                timeInd = getTimeInd(pedData[ii,0], self.scenData.timeStep)
                updateDataDict[timeInd]['peds2update'][pedID] = (pedData[ii,1], pedData[ii,2])

        self.updateDataDict = updateDataDict

    def createPedColorDict(self):
        colors = ['blue', 'red', 'lime', 'yellow']
        activityPattern2color = {}
        colorIndex = 0
        for pedInfo in self.descData.values():
            if pedInfo.activityPatternID in activityPattern2color:
                continue

            activityPattern2color[pedInfo.activityPatternID] = colors[colorIndex]
            colorIndex += 1
            if colorIndex >= len(colors):
                colorIndex = 0

        self.activityPattern2color = activityPattern2color

    def initialize(self):
        self.drawGeometry()
        self.timeInd = 0
        return [self.timeText]

    def update(self, _):
        if self.timeInd >= self.scenData.endTimeInd:
            self.reset()

        for pedID in self.updateDataDict[self.timeInd]['peds2remove']:
            self.removePed(pedID)

        for pedID in self.updateDataDict[self.timeInd]['peds2add']:
            self.addPed(pedID)

        for pedID, pos in self.updateDataDict[self.timeInd]['peds2update'].items():
            self.pedCircles[pedID].set_center(pos)

        self.timeText.set_text(getTimeLabel(self.timeInd, self.scenData.timeStep))

        self.timeSlider.eventson = False
        self.timeSlider.set_val(self.timeInd)
        self.timeSlider.eventson = True

        self.timeInd += 1
        return list(self.pedCircles.values()) + [self.timeText]

    def reset(self):
        self.timeInd = 0
        for pedID in list(self.pedCircles.keys()):
                self.removePed(pedID)

    def removePed(self, pedID):
        if pedID not in self.pedCircles:
            return
        self.pedCircles[pedID].remove()
        self.pedCircles.pop(pedID)

    def addPed(self, pedID):
        if pedID in self.pedCircles:
            return
        self.pedCircles[pedID] = Circle((np.NaN, np.NaN), radius=self.descData[pedID].radius,
                                            facecolor=self.activityPattern2color[self.descData[pedID].activityPatternID],
                                            edgecolor='k', zorder=7, alpha=0.4)
        self.ax.add_patch(self.pedCircles[pedID])

    def onTimeSliderChange(self, event):
        if self.animRunning:
            self.anim.event_source.stop()
        self.timeInd = int(self.timeSlider.val)

        if self.timeInd > 0:
            for pedID in list(self.pedCircles.keys()):
                self.removePed(pedID)

            for pedID, pos in self.updateDataDict[self.timeInd - 1]['peds2update'].items():
                if pedID not in self.pedCircles:
                    self.addPed(pedID)

                self.pedCircles[pedID].set_center(pos)

        self.fig.canvas.draw_idle()
        if self.animRunning:
            self.anim.event_source.start()

    def onLrcmSliderChange(self, event):
        if self.animRunning:
            self.anim.event_source.stop()
        lrcmInd = int(self.lrcmSlider.val)

        label = self.lrcmSets[lrcmInd][0]
        self.lrcmSlider.valtext.set_text(label)
        lrcmSetID = self.lrcmSets[lrcmInd][1]
        x = self.lrcmData[lrcmSetID][:,0]
        y = self.lrcmData[lrcmSetID][:,1]
        u = self.lrcmData[lrcmSetID][:,2]
        v = self.lrcmData[lrcmSetID][:,3]

        if self.lrcmQuiverHandle is not None:
            self.lrcmQuiverHandle.remove()
        self.lrcmQuiverHandle = self.ax.quiver(x, y, u, v, angles='uv', zorder=5)

        self.fig.canvas.draw_idle()
        if self.animRunning:
            self.anim.event_source.start()

    def onChangePlaySpeed(self, text):
        try:
            playSpeed = float(text)
            if playSpeed <= 0:
                raise Exception('Wrong value')
        except:
            self.playSpeedTextBox.set_val(str(self.playSpeed))
            return

        self.playSpeed = playSpeed
        self.interval = max(1, getInterval(self.scenData.timeStep, playSpeed))
        self.anim.event_source.interval = self.interval

    def startPause(self, event):
        if self.animRunning:
            self.anim.event_source.stop()
            self.animRunning = False
            self.pauseButton.label.set_text('Play')
            self.fig.canvas.draw()
        else:
            self.anim.event_source.start()
            self.animRunning = True
            self.pauseButton.label.set_text('Pause')
            self.fig.canvas.draw()

    def drawGeometry(self):
        for walkLevel in self.scenData.geometry.walkLevels:
            for walkableArea in walkLevel.walkableAreas:
                for coords in walkableArea:
                    pol = Polygon(coords, zorder=1, closed=True, fill=True,facecolor='whitesmoke', edgecolor='k')
                    self.ax.add_patch(pol)

            for obstacle in walkLevel.obstacles:
                if obstacle.type == 'polygon':
                    drawObj = Polygon(obstacle.coords, zorder=3, closed=True, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')
                elif obstacle.type == 'line':
                    drawObj = Line2D(obstacle.coords[0], obstacle.coords[1], zorder=3, color='k')
                elif obstacle.type == 'circle':
                    drawObj = Circle(obstacle.centerCoord, radius=obstacle.radius, zorder=3, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')
                elif obstacle.type == 'ellipse':
                    drawObj = Ellipse(obstacle.centerCoord, obstacle.semiAxesValues[0]*2, obstacle.semiAxesValues[1]*2, obstacle.rotation, zorder=3, fill=True, alpha=0.6, facecolor='darkgray', edgecolor='k')

                if obstacle.type == 'line':
                    self.ax.add_line(drawObj)
                else:
                    self.ax.add_patch(drawObj)

        for destination in walkLevel.destinations:
            drawObj = getDestinationObj(destination)
            if destination.type == 'line':
                self.ax.add_line(drawObj)
            else:
                self.ax.add_patch(drawObj)

        for source in walkLevel.sources:
            if source.type == 'polygon':
                drawObj = Polygon(source.coords, zorder=5, closed=True, fill=True, alpha=0.6, facecolor='0.8', edgecolor='k')
                self.ax.add_patch(drawObj)
            elif source.type == 'line':
                drawObj = Line2D(source.coords[0], source.coords[1], zorder=5, color='0.8')
                self.ax.add_line(drawObj)

def getDestinationObj(destination):
    hatch = 'x'
    color = '0.8'

    if destination.type == 'polygon':
        return Polygon(destination.coords, zorder=4, closed=True, fill=False, alpha=0.6, hatch=hatch, edgecolor=color)
    elif destination.type == 'line':
        return Line2D(destination.coords[0], destination.coords[1], zorder=4, color=color)

def getInterval(timeStep, playSpeed):
    return timeStep*1000/playSpeed

def getTimeInd(time, timeStep):
    return int(floor(time/timeStep))


def getTimeLabel(timeInd, timeStep):
    time = timeInd*timeStep
    isNegative = time < 0
    if isNegative:
        time = -time

    minutes = int(math.floor(time/60))
    seconds = int(math.floor(time - minutes*60))
    thousandsOfAsecond = int((time - minutes*60 - seconds)*10000)
    if isNegative:
        minutes = -float(minutes)

    return '{:-4.0f}:{:02.0f}.{:02.0f} ({:06.0f})'.format(minutes, seconds, thousandsOfAsecond, timeInd)

def visualize(scenDataFlNm, guiBackend=None):
    SimpleNomadVisualizer(scenDataFlNm, guiBackend)

if __name__ == '__main__':
    # Get argument in as filename
    SimpleNomadVisualizer(sys.argv[0])