""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import hashlib
from _collections import defaultdict
from abc import abstractmethod, ABC, abstractproperty
from builtins import staticmethod
from collections import namedtuple
from datetime import datetime
import json
import logging
from math import sqrt
from pathlib import Path
import struct

from shapely.geometry.multipolygon import MultiPolygon

from NOMAD.activities import LineDestination, PolygonDestination, \
    MultiPolygonDestination, PointDestination, MultiPointInRectangleDestination
from NOMAD.constants import PED_STATIC_STATE, PARAM_NMS
from NOMAD.demand_manager import LineSource, RectangleSource
from NOMAD.general_functions import get_relative_path_to
from NOMAD.grid_manager import UNOBSTRUCTED, OBSTRUCTED
from NOMAD.obstacles import LineObstacle, PolygonObstacle, CircleObstacle, \
    EllipseObstacle
from NOMAD.pedestrian import isObscuredByObstacle
import numpy as np


COMMENT_CHAR = '#'
COL_SEP_CHAR = ';'

TYPE = 'type'
INT_TYPE = 'int'
FLOAT_TYPE = 'float'
STR_TYPE = 'str'

TYPE_2_FCN = {
    INT_TYPE:int,
    FLOAT_TYPE:float,
    STR_TYPE:str
    }

COLUMN_NM = 'colNm'
PED_ATTR = 'pedAttr'
ACT_ATTR = 'actAttr'
ATTR_FCN = 'attrFcn'
CLASS_TYPE = 'classType'

PED_ID = 'ID'
TIME = 'time'

CLASS_TYPE_2_C_TYPE = {
    INT_TYPE:'I',
    FLOAT_TYPE:'d',
    }

CLASS_TYPE_2_NUMPY_TYPE = {
    INT_TYPE:'u4',
    FLOAT_TYPE:'f8',
    STR_TYPE:'O'
    }

CLASS_TYPE_2_EMPTY = {
    INT_TYPE:0,
    FLOAT_TYPE:np.NaN,
    }


BUFFER_SIZE = 2**23 # bytes 2^23 bytes = 8 MB

def getXcoord(vector):
    return vector.x

def getYcoord(vector):
    return vector.y

# ==============================================================
# ==============================================================
class OutputManager():
    
    def __init__(self):
        self.outputManagers = []
        self._hasConnectivity = False
        self._hasDebug = False
        self._connectionCutOffDist = None
        self.debugOutputManagers = []
    
    def addOuputManager(self, outputManager):
        # TO DO Allow only one instance per outputmanager type 
        self.outputManagers.append(outputManager)
        if isinstance(outputManager, ConnectivityFileOutputManager):
            self._hasConnectivity = True
            if self._connectionCutOffDist is not None:
                self._connectionCutOffDist =  max(self._connectionCutOffDist, outputManager.connectionCutOffDist) 
            else:
                self._connectionCutOffDist =  outputManager.connectionCutOffDist
                
        self._hasDebug = self._hasDebug or outputManager.isDebug

    def createFilenames(self):
        for outputManager in self.outputManagers:
            if isinstance(outputManager, BasicFileOutputManager):
                outputManager.createFilenames() 
                
    def duringModelCreation(self, nomadModel):
        for outputManager in self.outputManagers:
            outputManager.duringModelCreation(nomadModel)

    def updateGridManagerFile(self, gridManagerFile):
        for outputManager in self.outputManagers:
            if isinstance(outputManager, SimulationFileOutputManager):
                outputManager.updateGridManagerFile(gridManagerFile)

    def updateLrcmLoadFile(self, lrcmLoadFile):
        if lrcmLoadFile is not None:
            lrcmLoadFile = Path(lrcmLoadFile).resolve()
        for outputManager in self.outputManagers:
            if isinstance(outputManager, SimulationFileOutputManager):
                outputManager.updateLrcmLoadFile(lrcmLoadFile)
                
    def initialize(self, nomadModel):
        for outputManager in self.outputManagers:
            if isinstance(outputManager, BasicFileOutputManager):
                outputManager.createFiles()
            outputManager.initialize(nomadModel)

    def finish(self):
        for outputManager in self.outputManagers:
            outputManager.finish()

    def addPeds(self, pedsAdded):
        for outputManager in self.outputManagers:
            outputManager.addPeds(pedsAdded)

    def afterStarting(self, nomadModel):
        for outputManager in self.outputManagers:
            outputManager.afterStarting(nomadModel)
            
    def afterTimeStep(self, nomadModel):
        for outputManager in self.outputManagers:
            outputManager.afterTimeStep(nomadModel)
            
    def afterFinishing(self, nomadModel):
        for outputManager in self.outputManagers:
            outputManager.afterFinishing(nomadModel)
            
    def addCostMatrix2save(self, costMatrix, destinationID, rcmdType):
        for outputManager in self.outputManagers:
            if outputManager.isDebug:
                self.costMatrices2save[DebugOutputManager.getName(destinationID, rcmdType)] = costMatrix
    
    def addCenterCoordsMatrix(self, centerCoordsMatrix):
        for outputManager in self.outputManagers:
            if outputManager.isDebug:
                self.costMatrices2save[CENTER_COORDS_TYPE] = centerCoordsMatrix
    
    def getOutputManagerOfType(self, outputManagerType):
        for outputManager in self.outputManagers:
            if isinstance(outputManager, outputManagerType):
                return outputManager
    
    @property
    def geomHash(self):
        for outputManager in self.outputManagers:
            if isinstance(outputManager, SimulationFileOutputManager):
                return outputManager.geomHash
        
        return None

    @property
    def hasDebug(self):
        return self._hasDebug

    @property
    def hasConnectivity(self):
        return self._hasConnectivity
              
    @property
    def connectionCutOffDist(self):
        return self._connectionCutOffDist
# ==============================================================
# ==============================================================   
class OutputManagerBase(ABC):
    
    @abstractmethod
    def duringModelCreation(self, nomadModel):
        pass

    @abstractmethod
    def initialize(self, nomadModel):
        pass
    
    @abstractmethod
    def finish(self):
        pass
    
    @abstractmethod
    def addPeds(self, pedsAdded):
        pass
    
    @abstractmethod
    def afterStarting(self, nomadModel):
        pass
    
    @abstractmethod
    def afterTimeStep(self, nomadModel):
        pass
    
    @abstractmethod
    def afterFinishing(self, nomadModel):
        pass
    
    @property
    def isDebug(self):
        return False
    
    @abstractproperty
    def type(self):
        pass

# ==============================================================
# ==============================================================
class DummyOutputManager(OutputManagerBase):
    
    def duringModelCreation(self, _,):
        return
    
    def initialize(self, _,):
        return

    def addPeds(self, _):
        return

    def finish(self):
        return

    def afterStarting(self, _):
        return

    def afterTimeStep(self, _):
        return

    def afterFinishing(self, _):
        return

    @property
    def type(self):
        return 'DummyOutputManager'

# ==============================================================
# ==============================================================
class BasicFileOutputManager(OutputManagerBase):
   
    def __init__(self, ID, outputPd, baseFilename, seed):
        self.ID = ID
        self.timeStampStr = BasicFileOutputManager.createTimeStampStr()
        self.outputPd = Path(outputPd).resolve()
        self.baseFilename = baseFilename
        self.seed = seed
        
        self.outputPd.mkdir(parents=True, exist_ok=True)
    
    def duringModelCreation(self, nomadModel):
        raise NotImplementedError

    def createFilenames(self):
        raise NotImplementedError
    
    def createFiles(self):
        raise NotImplementedError
    
    @staticmethod
    def getString(value):
        return str(value)

    def createFile(self, fullFilename, fileType):
        return open(fullFilename, 'w'+fileType)

    def createFilename(self, extension):
        outputPd = Path(self.outputPd)
        filename = f'{self.baseFilename}{f"_{self.seed:04}" if self.seed is not None else ""}_{self.timeStampStr}.{extension}'
        return outputPd.joinpath(filename)
    
    @staticmethod
    def createTimeStampStr():
        return datetime.strftime(datetime.now(), '%Y%m%d%H%M%S')

    @property
    def type(self):
        return 'BasicFileOutputManager'
    
# ==============================================================
# ==============================================================
class InMemoryOutputManager(OutputManagerBase):

    def __init__(self, ID, saveTimeStep=None):
        '''
        Constructor
        '''
        self.ID = ID
        self.saveTimeStep = saveTimeStep
        self._notSavingEveryStep = False
        self._stepsTillNextSave = 0
        self._steps2skip = 0

        self.pedData = {}
        
    def duringModelCreation(self, nomadModel):
        return

    def initialize(self, nomadModel):
        if self.saveTimeStep is not None:
            ratioFactor = self.saveTimeStep/nomadModel.timeInfo.inIsolationTimeStep
            ratioFactorInt = round(ratioFactor)
            if abs(ratioFactor - ratioFactorInt) > 1e-12:
                raise Exception('The save time step in invalid. It should be a multiple of {}'.format(nomadModel.timeInfo.inIsolationTimeStep))

            if ratioFactorInt > 1:
                self._notSavingEveryStep = True
                self._stepsTillNextSave = 0
                self._steps2skip = ratioFactorInt - 1
                
        self.pedData = {}

    def finish(self):
        return

    def afterStarting(self, _):
        return
    
    def addPeds(self, pedsAdded):
        for ped in pedsAdded:
            self.pedData[ped.ID] = self.PedestrianData(ped)

    def afterTimeStep(self, nomadModel):
        for ped in nomadModel.pedsRemoved:
            self.pedData[ped.ID].activities = ped.activityLog
            self.pedData[ped.ID].trimArrays(ped.activityLog[-1].endTime)
            
        if self._notSavingEveryStep:
            if self._stepsTillNextSave > 0:
                self._stepsTillNextSave -= 1
                return

        # Do the actual saving
        time = nomadModel.timeInfo.getCurrentTime()
        for ped in nomadModel.movingPedestrians + nomadModel.newStaticNonInteractingPeds \
                    + nomadModel.newOutsideOfSimulationPeds + nomadModel.newStaticPeds:
            self.addPedTrajectoryData(ped, time)

        if self._notSavingEveryStep:
            self._stepsTillNextSave = self._steps2skip

    def addPedTrajectoryData(self, ped, time):
        self.pedData[ped.ID].addNextPositionAndVelocity(time, ped.pos_x, ped.pos_y,
                                                        ped.vel_x, ped.vel_y,
                                                        ped.velNorm_x, ped.velNorm_y)

    def afterFinishing(self, nomadModel):
        # Trim arrays and set end time of all peds still in the model
        for ped in nomadModel.pedestrians:
            self.pedData[ped.ID].activities = ped.activityLog
            self.pedData[ped.ID].trimArrays(ped.activityLog[-1].endTime)

    def clearPedData(self):
        del self.pedData

    @property
    def type(self):
        return 'InMemoryOutputManager'

    class PedestrianData():

        INIT_ROW_COUNT = 1000

        def __init__(self, ped):
            self.groupID = ped.groupID
            self.startTime = ped.startTime
            self.endTime = None
            self.startPos = ped.spawningPos
            self.times = np.zeros(self.INIT_ROW_COUNT, dtype=float)
            self.positions = np.zeros((self.INIT_ROW_COUNT,2), dtype=float)
            self.velocities = np.zeros((self.INIT_ROW_COUNT,2), dtype=float)
            self.normalizedVelocities = np.zeros((self.INIT_ROW_COUNT,2), dtype=float)
            self.activities = []
            self._rowInd = 0
            self._rowCount = self.INIT_ROW_COUNT

        def addNextPositionAndVelocity(self, time, pos_x, pos_y, vel_x, vel_y, velNorm_x, velNorm_y):
            if self._rowInd >= self._rowCount:
                self.expandArrays()

            self.times[self._rowInd] = time
            self.positions[self._rowInd,:] = (pos_x, pos_y)
            self.velocities[self._rowInd,:] = (vel_x, vel_y)
            self.normalizedVelocities[self._rowInd,:] = (velNorm_x, velNorm_y)

            self._rowInd += 1

        def addStartActivity(self, time, activityType, position):
            pass

        def addEndActivity(self, time):
            pass

        def trimArrays(self, endTime):
            trimIndices = np.argwhere(self.times[1:] == 0)
            if len(trimIndices) == 0:
                return
            trimInd = trimIndices[0][0] + 1
            self.times = self.times[:trimInd]
            self.positions = self.positions[:trimInd]
            self.velocities = self.velocities[:trimInd]
            self.normalizedVelocities = self.normalizedVelocities[:trimInd]
            self.endTime = endTime

        def expandArrays(self):
            newRowCount = self._rowCount + self.INIT_ROW_COUNT

            times = np.zeros(newRowCount, dtype=float)
            positions = np.zeros((newRowCount,2), dtype=float)
            velocities = np.zeros((newRowCount,2), dtype=float)
            normalizedVelocities = np.zeros((newRowCount,2), dtype=float)

            times[:self._rowCount] = self.times
            positions[:self._rowCount,:] = self.positions[:,:]
            velocities[:self._rowCount,:] = self.velocities[:,:]
            normalizedVelocities[:self._rowCount,:] = self.normalizedVelocities[:,:]

            self.times = times
            self.positions = positions
            self.velocities = velocities
            self.normalizedVelocities = normalizedVelocities

            self._rowCount = newRowCount

    class PedestrianAcitivityData():

        def __init__(self):
            self.startTime = None
            self.endTime = None
            self.activityType = None


class SimulationFileOutputManager(BasicFileOutputManager):
    '''
    classdocs
    '''
    TRAJ_EXTENSION = 'traj'
    DESC_EXTENSION = 'desc'
    SCEN_EXTENSION = 'scen'
    LRCM_EXTENSION = 'lrcm.npz'
    GRID_EXTENSION = 'gm'

    PED_DESC_PATTERN = (
        {COLUMN_NM:'ID', PED_ATTR:'ID', TYPE:INT_TYPE},
        {COLUMN_NM:'startTime', PED_ATTR:'startTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'endTime', PED_ATTR:'endTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'radius', PED_ATTR:'radius', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'groupID', PED_ATTR:'groupID', TYPE:STR_TYPE},
        {COLUMN_NM:'parameterSetID', PED_ATTR:'parameterSetID', TYPE:STR_TYPE},
        {COLUMN_NM:'activityPatternID', PED_ATTR:'activityPattern.ID', TYPE:STR_TYPE},
        )

    ACTIVITY_LOG_PATTERN = (
        {COLUMN_NM:'startTime', ACT_ATTR:'startTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'endTime', ACT_ATTR:'endTime', TYPE:FLOAT_TYPE},
        {COLUMN_NM:'activityType', ACT_ATTR:'activityType', TYPE:STR_TYPE},
        {COLUMN_NM:'activityID', ACT_ATTR:'activityID', TYPE:STR_TYPE},
        {COLUMN_NM:'destinationID', ACT_ATTR:'destinationID', TYPE:STR_TYPE},
        {COLUMN_NM:'pedState', ACT_ATTR:'pedState', TYPE:INT_TYPE},
        )

    PED_TRAJ_PATTERN = (
        {COLUMN_NM:'pos_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pos', ATTR_FCN:getXcoord},
        {COLUMN_NM:'pos_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pos', ATTR_FCN:getYcoord},
        {COLUMN_NM:'vel_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'vel', ATTR_FCN:getXcoord},
        {COLUMN_NM:'vel_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'vel', ATTR_FCN:getYcoord},
        {COLUMN_NM:'velNorm_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'velNorm', ATTR_FCN:getXcoord},
        {COLUMN_NM:'velNorm_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'velNorm', ATTR_FCN:getYcoord},
        {COLUMN_NM:'acc_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'acc', ATTR_FCN:getXcoord},
        {COLUMN_NM:'acc_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'acc', ATTR_FCN:getYcoord},
        {COLUMN_NM:'pathForce_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pathFollowingForce', ATTR_FCN:getXcoord},
        {COLUMN_NM:'pathForce_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pathFollowingForce', ATTR_FCN:getYcoord},
        {COLUMN_NM:'pedForce_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pedForce', ATTR_FCN:getXcoord},
        {COLUMN_NM:'pedForce_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pedForce', ATTR_FCN:getYcoord},
        {COLUMN_NM:'obsForce_x', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'obsForce', ATTR_FCN:getXcoord},
        {COLUMN_NM:'obsForce_y', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'obsForce', ATTR_FCN:getYcoord},
        {COLUMN_NM:'pedIsolationState', CLASS_TYPE:INT_TYPE, PED_ATTR:'pedIsolationState'},
        {COLUMN_NM:'pedIsolationTime', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'pedIsolationTime'},
        {COLUMN_NM:'obsIsolationState', CLASS_TYPE:INT_TYPE, PED_ATTR:'obsIsolationState'},
        {COLUMN_NM:'obsIsolationTime', CLASS_TYPE:FLOAT_TYPE, PED_ATTR:'obsIsolationTime'}
        )

    TIME_CLASS = FLOAT_TYPE
    PED_ID_CLASS = INT_TYPE
    TIME_STEP_CLASS = FLOAT_TYPE

    PED_TRAJ_BIN_PATTERN = CLASS_TYPE_2_C_TYPE[TIME_CLASS] + CLASS_TYPE_2_C_TYPE[PED_ID_CLASS] + ''.join(CLASS_TYPE_2_C_TYPE[entry[CLASS_TYPE]] for entry in PED_TRAJ_PATTERN)
    PED_TRAJ_BIN_EMPTY = [CLASS_TYPE_2_EMPTY[entry[CLASS_TYPE]] for entry in PED_TRAJ_PATTERN]

    logger = logging.getLogger(__name__)

    def __init__(self, ID, outputPd, baseFilename, seed, exportGrid=False):
        '''
        Constructor
        '''
        super().__init__(ID, outputPd, baseFilename, seed)
        self.exportGrid = exportGrid
        
    def duringModelCreation(self, nomadModel):
        self.geomDict = getGeometryDict(nomadModel)
        self.geomHash = hashlib.md5(json.dumps(self.geomDict).encode("utf-8")).hexdigest()

    def createFilenames(self):
        self.descriptionFilename = self.createFilename(self.DESC_EXTENSION)
        self.scenarioFilename = self.createFilename(self.SCEN_EXTENSION)
        self.trajectoryFilename = self.createFilename(self.TRAJ_EXTENSION)
        
    def createFiles(self):
        self.descriptionFile = self.createFile(self.descriptionFilename, 't')
        self.scenarioFile = self.createFile(self.scenarioFilename, 't')
        self.trajectoryFile = self.createFile(self.trajectoryFilename, 'b')
        self.logger.debug(f'Output files created in {self.outputPd}')
        
    def updateLrcmLoadFile(self, lrcmLoadFile):
        self.lrcmLoadFlNm = lrcmLoadFile
        self.saveLrcmFile = lrcmLoadFile is None
        if self.saveLrcmFile:
            self.lrcmFilename = self.createFilename(self.LRCM_EXTENSION)
        
    def updateGridManagerFile(self, gridManagerFile):
        if self.exportGrid and gridManagerFile is None:
            self.gridFilename = self.createFilename(self.GRID_EXTENSION)
        else:
            self.exportGrid = False

    def initialize(self, nomadModel):
        self.stepsSavedToTrajectoryFile = 0
        descHeaderStr = self.getDescFileHeaderString()
        self.descriptionFile.write(descHeaderStr)
        self.trajectoryFile.write(struct.pack(CLASS_TYPE_2_C_TYPE[self.TIME_STEP_CLASS], nomadModel.timeInfo.timeStep))
        if self.exportGrid:
            gridExportDict = nomadModel.gridManager.exportDict
            gridExportDict["geomHash"] = self.geomHash
            with open(self.gridFilename, 'wt') as gridFile:
                json.dump(gridExportDict, gridFile)

    @staticmethod
    def getDescFileHeaderString():
        descFirstHeaderStr = COMMENT_CHAR + ' '

        descSecondHeaderStr = COMMENT_CHAR + ' '
        for columnInfo in SimulationFileOutputManager.PED_DESC_PATTERN:
            descSecondHeaderStr += (columnInfo[COLUMN_NM] + COL_SEP_CHAR)
            descFirstHeaderStr += ' '*(len(columnInfo[COLUMN_NM]) + 1)

        baseStr = 'activity'
        appendixList = ['1', '2', '3', '4', 'n']
        for appendix in ['1', '2', '3', '4', 'n']:
            if appendix == appendixList[-1]:
                descFirstHeaderStr += ' ... '
                descSecondHeaderStr += ' ... '

            firstHeaderStrPart = '{}_{}'.format(baseStr, appendix)
            descFirstHeaderStr += firstHeaderStrPart
            secondHeaderStrPart = ''
            for columnInfo in SimulationFileOutputManager.ACTIVITY_LOG_PATTERN:
                secondHeaderStrPart += (columnInfo[COLUMN_NM] + COL_SEP_CHAR)

            descSecondHeaderStr += secondHeaderStrPart
            descFirstHeaderStr += ' '*(len(secondHeaderStrPart) - len(firstHeaderStrPart))

        return descFirstHeaderStr + '\n' + descSecondHeaderStr + '\n'

    def finish(self):
        self.logger.debug('Closing data output files')
        self.descriptionFile.write(COMMENT_CHAR + 'Total step count: {}'.format(self.stepsSavedToTrajectoryFile))
        self.descriptionFile.flush()
        self.descriptionFile.close()

        self.trajectoryFile.flush()
        self.trajectoryFile.close()

    def addPeds(self, _):
        return

    def afterStarting(self, nomadModel):
        self.fillScenFile(nomadModel)
        self.scenarioFile.close()
        if self.saveLrcmFile:
            fillLrcmFile(self.lrcmFilename, nomadModel, self.geomHash)

    def afterTimeStep(self, nomadModel):
        for ped in nomadModel.pedsRemoved:
            self.addPed2DescFile(ped)

        for ped in nomadModel.movingPedestrians + nomadModel.newStaticNonInteractingPeds \
                    + nomadModel.newOutsideOfSimulationPeds + nomadModel.newStaticPeds:
            self.addPedTrajectoryData(ped, nomadModel.timeInfo.currentTime)

        for ped in nomadModel.newOutsideOfSimulationPeds:
            self.addPedTrajectoryData(ped, nomadModel.timeInfo.getNextTime(), isEmptyRow=True)
            
    def afterFinishing(self, nomadModel):
        for ped in nomadModel.pedestrians:
            self.addPed2DescFile(ped)

    def addPed2DescFile(self, ped):
        pedDescStr = ''
        for columnInfo in self.PED_DESC_PATTERN:
            pedDescStr += (str(getAttr(ped, columnInfo[PED_ATTR])) + COL_SEP_CHAR)

        for activityLogElement in ped.activityLog:
            for columnInfo in self.ACTIVITY_LOG_PATTERN:
                pedDescStr += (str(getAttr(activityLogElement, columnInfo[ACT_ATTR])) + COL_SEP_CHAR)

        pedDescStr += '\n'
        self.descriptionFile.write(pedDescStr)

    def addPedTrajectoryData(self, ped, time, isEmptyRow=False):
        pedTrajData = [time, ped.ID]
        
        if isEmptyRow:
            pedTrajData += self.PED_TRAJ_BIN_EMPTY
        else:
            for columnInfo in self.PED_TRAJ_PATTERN:
                attrValue = getattr(ped, columnInfo[PED_ATTR])
                if ATTR_FCN in columnInfo:
                    if attrValue is not None:
                        attrValue = columnInfo[ATTR_FCN](attrValue)
                    else:
                        attrValue = np.NaN
                pedTrajData.append(attrValue)
        self.trajectoryFile.write(struct.pack(self.PED_TRAJ_BIN_PATTERN, *pedTrajData))
        self.stepsSavedToTrajectoryFile += 1

    def fillScenFile(self, nomadModel):
        # Name, Label, Duration, timeStep, timeStepCount
        scenDict = {}
        for attrNm in NOMAD_MODEL_ATTR:
            scenDict[attrNm] = getattr(nomadModel, attrNm)
        scenDict['timeStep'] = nomadModel.timeInfo.inIsolationTimeStep
        scenDict['duration'] = nomadModel.timeInfo.duration
        scenDict['endTimeInd'] = nomadModel.timeInfo.endTimeInd
        # Other output filenames (without pd)
    
        scenDict['descFile'] = str(get_relative_path_to(self.descriptionFilename, self.scenarioFilename))
        scenDict['trajFile'] = str(get_relative_path_to(self.trajectoryFilename, self.scenarioFilename))
        if not self.saveLrcmFile and self.lrcmLoadFlNm is not None:
            scenDict['lrcmFile'] = str(get_relative_path_to(self.lrcmLoadFlNm, self.scenarioFilename))
        else:
            scenDict['lrcmFile'] = str(get_relative_path_to(self.lrcmFilename, self.scenarioFilename))
        
        # TO DO
        # Add connectivity filename
        if nomadModel.outputManager.hasConnectivity:
            connOutputManager = nomadModel.outputManager.getOutputManagerOfType(ConnectivityFileOutputManager)
            scenDict['connFile'] = str(get_relative_path_to(connOutputManager.connectivityFilename, self.scenarioFilename))
        
        # Geometry
        scenDict['geometry'] = self.geomDict
        scenDict['pedParamSets'] = getPedParamSetsDict(nomadModel)
       
        json.dump(scenDict, self.scenarioFile, indent=4)

    @staticmethod
    def readLrcmDataFile(lrcmFlNm):
        return np.load(lrcmFlNm)

    @staticmethod
    def readGridDataFile(gridFilename):
        with open(gridFilename) as f:
            gridData = json.load(f)

        return gridData
    
    @staticmethod
    def readTrajactoryDataFile(flNm):
        from numpy.lib import recfunctions as rfn
        logger = logging.getLogger(__name__)
        logger.debug('Reading trajectory data from file...')
        with open(flNm, "rb") as f:
            dataRead = f.read()

        logger.debug('Processing trajectory data...')
        timeStepSize = struct.calcsize(CLASS_TYPE_2_C_TYPE[SimulationFileOutputManager.TIME_STEP_CLASS])
        timeStep = struct.unpack(CLASS_TYPE_2_C_TYPE[SimulationFileOutputManager.TIME_STEP_CLASS], dataRead[:timeStepSize])[0]
        dTypes = [(TIME, CLASS_TYPE_2_NUMPY_TYPE[SimulationFileOutputManager.TIME_CLASS])] + [(entry[COLUMN_NM], CLASS_TYPE_2_NUMPY_TYPE[entry[CLASS_TYPE]]) for entry in SimulationFileOutputManager.PED_TRAJ_PATTERN]

        # Convert pedestrian data 2 numpy array
        columnCount = 1 + len(SimulationFileOutputManager.PED_TRAJ_PATTERN) # No pedID but include time
        initialRowCount = 10*60*10 # Equals 10 minutes
        rowCountStep = 10*60*5 # Equals 10 minutes
        
        dataUnpackedIter = struct.iter_unpack(SimulationFileOutputManager.PED_TRAJ_BIN_PATTERN, dataRead[timeStepSize:])
        trajData = {}
        rowIndPerPed = {}
        for dataUnpackedIterEntry in dataUnpackedIter:
            time,pedID = dataUnpackedIterEntry[:2]
            if pedID not in trajData:
                trajData[pedID] = np.zeros((initialRowCount, columnCount))
                rowIndPerPed[pedID] = 0
           
            if rowIndPerPed[pedID] > 0 and abs(time - trajData[pedID][rowIndPerPed[pedID]-1,0]) < 1e-10:
                rowIndPerPed[pedID] -= 1 
           
            if trajData[pedID].shape[0] <= rowIndPerPed[pedID]:
                trajData[pedID] = np.append(trajData[pedID], np.zeros((rowCountStep, columnCount)), axis=0)
                            
            trajData[pedID][rowIndPerPed[pedID],0] = time
            trajData[pedID][rowIndPerPed[pedID],1:] = dataUnpackedIterEntry[2:]
            rowIndPerPed[pedID] += 1
        
        logger.debug('Trimming and filling gaps in trajectory data arrays...')
        for pedID, dataArray in trajData.items():
            # Trim
            dataArray = dataArray[:rowIndPerPed[pedID]-1,:]
            # Fill gaps 
            checkIndices = np.where(np.logical_not(np.isclose(np.diff(dataArray[:,0]), timeStep)))[0]
            rowMoveSize = 0
            for checkInd in checkIndices:
                if SimulationFileOutputManager.isEmptyRow(dataArray[checkInd+1+rowMoveSize,1:]):
                    startInd = checkInd + 1 + rowMoveSize
                    endInd = checkInd + 2 + rowMoveSize
                    columnValues = SimulationFileOutputManager.PED_TRAJ_BIN_EMPTY
                else:
                    startInd = checkInd + rowMoveSize
                    endInd = checkInd + 1 + rowMoveSize
                    columnValues = dataArray[startInd,1:]
                
                rowCount = round((dataArray[endInd,0] - dataArray[startInd,0])/timeStep) - 1
                addArray = np.zeros((rowCount, columnCount))
                new_t = np.arange(dataArray[startInd,0] + timeStep, dataArray[endInd,0], timeStep)
                if len(new_t) == rowCount + 1:
                    # Due to numerical rounding error dataArray[endInd,0] is in the array and should be removed
                    new_t = new_t[:-1]
                addArray[:,0] = new_t
                addArray[:,1:] = columnValues
                insertInd = endInd
                
                newRowCount = dataArray.shape[0] + rowCount
                newDataArray = np.zeros((newRowCount, columnCount))
                newDataArray[:insertInd,:] = dataArray[:insertInd,:]
                newDataArray[insertInd:insertInd+rowCount,:] = addArray
                newDataArray[insertInd+rowCount:,:] = dataArray[insertInd:,:]
                
                dataArray = newDataArray
                rowMoveSize += rowCount
                                
            trajData[pedID] = rfn.unstructured_to_structured(dataArray, dTypes)
         
        return trajData, dTypes

    @staticmethod
    def isEmptyRow(rowData):
        nanInd = np.isnan(rowData)
        if not np.any(nanInd):
            return False
        
        return np.all(np.logical_or(nanInd == np.isnan(SimulationFileOutputManager.PED_TRAJ_BIN_EMPTY), np.equal(rowData, SimulationFileOutputManager.PED_TRAJ_BIN_EMPTY)))


    @staticmethod
    def fillTrajectoryGaps(trajData):
        pass

    @staticmethod
    def readDescriptionDataFile(flNm):
        pedestrians = {}
        with open(flNm) as f:
            line = f.readline()
            while line != '':  # The EOF char is an empty string
                if line[0] == COMMENT_CHAR:
                    line = f.readline()
                    continue

                pedInfo = SimulationFileOutputManager.readDescriptionDataFileLine(line)
                pedestrians[pedInfo.ID] = pedInfo
                line = f.readline()

        return pedestrians

    @staticmethod
    def readDescriptionDataFileLine(line):
        fields = line.split(COL_SEP_CHAR)
        if fields[-1] == '\n':
            fields = fields[:-1]
        pedInfo = {}
        fieldInd = 0
        for columnInfo in SimulationFileOutputManager.PED_DESC_PATTERN:
            if fields[fieldInd] == 'None':
                pedInfo[columnInfo[COLUMN_NM]] = None
            else:
                pedInfo[columnInfo[COLUMN_NM]] = TYPE_2_FCN[columnInfo[TYPE]](fields[fieldInd])
            fieldInd += 1

        activityLog = []
        while fieldInd < len(fields):
            activityEl = {}
            for columnInfo in SimulationFileOutputManager.ACTIVITY_LOG_PATTERN:
                if fields[fieldInd] == 'None':
                    activityEl[columnInfo[COLUMN_NM]] = None
                else:
                    activityEl[columnInfo[COLUMN_NM]] = TYPE_2_FCN[columnInfo[TYPE]](fields[fieldInd])
                fieldInd += 1
            activityLog.append(namedtuple('activity', sorted(activityEl))(**activityEl))

        pedInfo['activityLog'] = activityLog

        return namedtuple('pedInfo', sorted(pedInfo))(**pedInfo)

    @staticmethod
    def readScenarioDataFile(flNm):
        with open(flNm) as f:
            scenDataDict = json.load(f)

        return getNamedTupleIter(scenDataDict, 'scenData')

    @staticmethod
    def exportTrajDataToExcel(dataFlNm, xlsxFlNm):
        import xlsxwriter
        trajData = SimulationFileOutputManager.readTrajactoryDataFile(dataFlNm)
        workbook = xlsxwriter.Workbook(xlsxFlNm)

        columns = [TIME] + [columInfo[COLUMN_NM] for columInfo in SimulationFileOutputManager.PED_TRAJ_PATTERN]

        for pedID, pedTrajData in trajData.items():
            worksheet = workbook.add_worksheet('Ped {}'.format(pedID))
            col = 0
            for columnNm in columns:
                row = 0
                worksheet.write(row, col, columnNm)
                row += 1
                for value in pedTrajData[columnNm]:
                    worksheet.write(row, col, value)
                    row += 1
                col += 1

        workbook.close()

    ACTIVITY_SHEETS = [] #TAKING_ORDER_ACTIVITY_NM, COLLECTING_ORDER_ACTIVITY_NM, SERVING_ORDER_ACTIVITY_NM, CHECKING_OUT_ACTIVITY_NM, CLEANING_ACTIVITY_NM]
    
    @staticmethod
    def exportDescriptionDataToExcel(dataFlNm, xlsxFlNm):
        import xlsxwriter
        peds = SimulationFileOutputManager.readDescriptionDataFile(dataFlNm)
        workbook = xlsxwriter.Workbook(xlsxFlNm)

        columns = [columInfo[COLUMN_NM] for columInfo in SimulationFileOutputManager.PED_DESC_PATTERN] + [columInfo[COLUMN_NM] for columInfo in SimulationFileOutputManager.ACTIVITY_LOG_PATTERN]
        worksheet = workbook.add_worksheet('Desc. data')
        row = 0
        for ped in peds.values():
            col = 0
            for columnNm in columns:
                worksheet.write(row, col, columnNm)
                col += 1
            row += 1
            
            col = 0  
            for columInfo in SimulationFileOutputManager.PED_DESC_PATTERN:
                value = getattr(ped, columInfo[COLUMN_NM])
                SimulationFileOutputManager.writeValue2worksheet(worksheet, row, col, value, 
                                                            providedType=columInfo[TYPE], valueNm=columInfo[COLUMN_NM])
                col += 1
            
            for activityEl in ped.activityLog:
                col = len(SimulationFileOutputManager.PED_DESC_PATTERN)
                for columInfo in SimulationFileOutputManager.ACTIVITY_LOG_PATTERN:
                    value = getattr(activityEl, columInfo[COLUMN_NM])
                    SimulationFileOutputManager.writeValue2worksheet(worksheet, row, col, value, 
                                                                providedType=columInfo[TYPE], valueNm=columInfo[COLUMN_NM])
                    col += 1
                row += 1
            
            row += 1 # Empty row between peds
      
        columns = ['ID']  + [columInfo[COLUMN_NM] for columInfo in SimulationFileOutputManager.ACTIVITY_LOG_PATTERN if columInfo[COLUMN_NM] != 'activityType'] 
        for activityType in SimulationFileOutputManager.ACTIVITY_SHEETS:
            worksheet = workbook.add_worksheet(activityType)
            worksheet.write(0, 0, activityType)
            col = 0
            row = 1
            for columnNm in columns:
                worksheet.write(row, col, columnNm)
                col += 1
            
            row = 2
            col = 0
            for ped in peds.values():
                for activityEl in ped.activityLog:
                    if activityEl.activityType == activityType:
                        col = 1
                        worksheet.write(row, 0, ped.ID)
                        for columInfo in SimulationFileOutputManager.ACTIVITY_LOG_PATTERN:
                            if columInfo[COLUMN_NM] == 'activityType':
                                continue
                            value = getattr(activityEl, columInfo[COLUMN_NM])
                            SimulationFileOutputManager.writeValue2worksheet(worksheet, row, col, value, 
                                                                providedType=columInfo[TYPE], valueNm=columInfo[COLUMN_NM]) 
                            col += 1
                        row += 1
        
        workbook.close()

    @staticmethod
    def writeValue2worksheet(worksheet, row, col, value, providedType=None, valueNm=None):
        if value is None:
            worksheet.write_string(row, col, 'None')
        elif valueNm == 'ID':
            worksheet.write_string(row, col, str(value))
        elif providedType in (FLOAT_TYPE, INT_TYPE):
            worksheet.write_number(row, col, value)
        elif providedType == STR_TYPE:
            worksheet.write_string(row, col, value)
        else:
            worksheet.write(row, col, value)
    
    @staticmethod
    def getString(value):
        return str(value)

    @property
    def type(self):
        return 'SimulationOutputManager'

# ==============================================================
# ==============================================================
class ConnectivityFileOutputManager(BasicFileOutputManager):
    
    CONNECTIVITY_EXTENSION = 'conn.npz'
    CONN_CUT_OFF_DIST = np.inf
    
    def __init__(self, ID, outputPd, baseFilename, seed, connectionCutOffDist=CONN_CUT_OFF_DIST):
        super().__init__(ID, outputPd, baseFilename, seed)

        self.connectionCutOffDist = connectionCutOffDist
        self.connectionCutOffDist2 = connectionCutOffDist*connectionCutOffDist
        
        self.connectivityFilename = Path('none')
        self.connections = defaultdict(list)
        self.ID2groupID = {}
        self.pedIDsProcessed = {}
        self.staticPeds = defaultdict(list)
        self.staticPedPairs = {}    
    
    def duringModelCreation(self, nomadModel):
        return

    def createFilenames(self):
        self.connectivityFilename = self.createFilename(self.CONNECTIVITY_EXTENSION)
    
    def createFiles(self):
        pass
    
    def finish(self):
        connectionsOut = {}
        ID2IDtuple = {}
        ID = 0
        
        for IDtuple, value in self.connections.items():
            ID2IDtuple[ID] = IDtuple
            connectionsOut[str(ID)] = value
            ID += 1
            
        connectionsOut['ID2IDtuple'] = ID2IDtuple
        connectionsOut['ID2groupID'] = self.ID2groupID
        
        np.savez(self.connectivityFilename, **connectionsOut)
    
    def afterTimeStep(self, nomadModel):
        self.addConnectionsInfo(nomadModel)
        for ped in nomadModel.pedsRemoved:
            self.ID2groupID[ped.ID] = ped.groupID
    
    def afterFinishing(self, nomadModel):
        for ped in nomadModel.pedestrians:
            self.ID2groupID[ped.ID] = ped.groupID
    
    def addConnectionsInfo(self, nomadModel):
        # Use grid manager to fill the connections
        # All unobstructed cases with dist <= connectionCutOffDist and group != group
        # Save as   key = (ped_1, ped_2) -> see getIDTuple 
        # and     value = [(t_1,dist_1), ..., (t_n,dist_n)]
        self.pedIDsProcessed = []
        for ped in nomadModel.movingPedestrians:
            self.addConnectionsForPed(nomadModel, ped)
        
        for ped in nomadModel.newStaticPeds:
            self.addConnectionsForPed(nomadModel, ped, static=True)
            
        for pedIDtuple, dist in self.staticPedPairs.items(): 
            self.connections[pedIDtuple].append((nomadModel.timeInfo.currentTime, dist))
                                
    def addConnectionsForPed(self, nomadModel, ped, static=False):        
        gridCell = ped.gridCell
        
        pedsInNeigboringCell = gridCell.getPedsInNeighboringCells()
           
        otherPedSets = [
            (gridCell.getOtherPedsInCell(ped), gridCell.containsSightBlockingObstacle, gridCell.sightBlockingObstaclesInCell),
            (pedsInNeigboringCell[UNOBSTRUCTED], False, None),
            (pedsInNeigboringCell[OBSTRUCTED], True, gridCell.obstaclesObstrNeighbors)
        ]    
           
        self.addConnection(nomadModel, static, ped, otherPedSets) 
        
    def addConnection(self, nomadModel, static, ped, otherPedSets):
        def addConn(ped, otherPed):
            pedIDtuple = ConnectivityFileOutputManager.getIDtuple(ped.ID, otherPed.ID)
            if pedIDtuple in self.pedIDsProcessed:
                return
            self.pedIDsProcessed.append(pedIDtuple)
                    
            pedPos_x = ped.pos_x
            otherPedPos_x = otherPed.pos_x
            pedPos_y = ped.pos_y
            otherPedPos_y = otherPed.pos_y    
            dist2 = (pedPos_x - otherPedPos_x)*(pedPos_x - otherPedPos_x) + (pedPos_y - otherPedPos_y)*(pedPos_y - otherPedPos_y) 
            
            if dist2 > self.connectionCutOffDist2 or (checkForObstacles and isObscuredByObstacle(ped, otherPed, obstacles)):
                return
            
            dist = sqrt(dist2)            
            self.connections[pedIDtuple].append((nomadModel.timeInfo.currentTime, dist))
            if static and otherPed.state == PED_STATIC_STATE:
                self.staticPeds[otherPed].append(ped)
                self.staticPeds[ped].append(otherPed)
                self.staticPedPairs[pedIDtuple] = dist
                
        # ====================================================================================
      
        if ped in self.staticPeds:
            for otherPed in self.staticPeds[ped]:
                if otherPed in self.staticPeds:
                    self.staticPeds[otherPed].remove(ped)
                
                pedIDtuple = ConnectivityFileOutputManager.getIDtuple(ped.ID, otherPed.ID)
                if pedIDtuple in self.staticPedPairs:
                    self.staticPedPairs.pop(pedIDtuple)
            
            self.staticPeds.pop(ped)
            
        for otherPeds, checkForObstacles, obstacles in otherPedSets:
            for otherPed in otherPeds:
                addConn(ped, otherPed)
            
    @staticmethod
    def readConnectionsDataFile(flNm):
        connData = np.load(flNm, allow_pickle=True)
        ID2IDtuple = connData['ID2IDtuple'].item()
        ID2groupID = connData['ID2groupID'].item()
        connDataDict = {IDtuple:connData[str(ID)] for ID, IDtuple in ID2IDtuple.items()}
        
        return {'connData':connDataDict, 'ID2groupID':ID2groupID}
        
    @property
    def type(self):
        return 'ConnectivityFileOutputManager'

    @staticmethod
    def getIDtuple(ID_1, ID_2):
        if ID_1 < ID_2:
            return (ID_1, ID_2)
        else:
            return (ID_2, ID_1)

    def initialize(self, _,):
        return

    def addPeds(self, _):
        return

    def afterStarting(self, _):
        return


# ==============================================================
# ==============================================================
NOMAD_MODEL_ATTR = ('name', 'label', 'seed')

RCMD_INIT_TYPE = 'init'
RCMD_FINAL_TYPE = 'final'
CENTER_COORDS_TYPE = 'centerCoords'

class DebugOutputManager(BasicFileOutputManager):
    
    RCMD_EXTENSION = 'npz' # Numpy 
    
    def __init__(self, ID, outputPd, baseFilename, seed):
        '''
        Constructor
        '''
        super().__init__(ID, outputPd, baseFilename, seed)
        self.costMatrixFilename = Path('none')
        self.costMatrices2save = {}
    
    def duringModelCreation(self, nomadModel):
        return

    def initialize(self, _):
        if len(self.costMatrices2save) > 0:
            self.saveMatrices2file()
    
    def createFiles(self):
        self.costMatrixFilename = self.createFilename(self.RCMD_EXTENSION)
    
    def addCostMatrix2save(self, costMatrix, destinationID, rcmdType):
        self.costMatrices2save[DebugOutputManager.getName(destinationID, rcmdType)] = costMatrix
    
    def addCenterCoordsMatrix(self, centerCoordsMatrix):
        self.costMatrices2save[CENTER_COORDS_TYPE] = centerCoordsMatrix
            
    def saveMatrices2file(self):
        np.savez(self.costMatrixFilename, **self.costMatrices2save)
        
    @staticmethod
    def readRcmdDataFile(rcmdFlNm):
        matricesLoaded =  np.load(rcmdFlNm)
        matrices = {RCMD_INIT_TYPE:{}, RCMD_FINAL_TYPE:{}}
        for name, matrix in matricesLoaded.items():
            if name == CENTER_COORDS_TYPE:
                centerCoords = matrix
            elif name.startswith(RCMD_INIT_TYPE):
                matrices[RCMD_INIT_TYPE][DebugOutputManager.stripType(name, RCMD_INIT_TYPE)] = matrix
            elif name.startswith(RCMD_FINAL_TYPE):
                matrices[RCMD_FINAL_TYPE][DebugOutputManager.stripType(name, RCMD_FINAL_TYPE)] = matrix
            else:
                raise Exception(f'Unknown rcmdType for name: {name}')        
    
        return matrices, centerCoords
    
    @staticmethod
    def getName(destinationID, rcmdType):
        if rcmdType == RCMD_INIT_TYPE:
            return f'{RCMD_INIT_TYPE}_{destinationID}'
        elif rcmdType == RCMD_FINAL_TYPE:
            return f'{RCMD_FINAL_TYPE}_{destinationID}'
        else:
            raise Exception(f'Unknown rcmdType: {rcmdType}') 
            
    @staticmethod
    def stripType(name, rcmdType):
        return name.replace(f'{rcmdType}_', '')

    @property
    def isDebug(self):
        return True
            
    @property
    def type(self):
        return 'DebugOutputManager'
            

# ==============================================================
# ==============================================================
def getAttr(baseObject, attrNm):
    if '.' in attrNm:
        attrNmBase, attrNmNext = attrNm.split('.', 1)
        return getAttr(getattr(baseObject, attrNmBase), attrNmNext)
    else:
        return getattr(baseObject, attrNm)

def fillLrcmFile(filename, nomadModel, geomHash):
    from NOMAD.local_route_choice_manager import getIDstrFomIDs
    save_dict = {'geomHash':geomHash}
    for walkLevel in nomadModel.walkLevels:
        for destination in walkLevel.destinations:
            if destination._ignoreInLrcc:
                continue
            keys = destination._localRouteChoiceManager.centerCoords.keys()
            np_array = np.zeros((len(keys),4), dtype=float)
            for ii, key in enumerate(keys):
                np_array[ii,:2] = destination._localRouteChoiceManager.centerCoords[key]
                np_array[ii,2:4] = destination._localRouteChoiceManager.desiredDirections[key]
                
            save_dict[getIDstrFomIDs(walkLevel.ID, destination.ID)] = np_array
            
    np.savez(filename, **save_dict)

def getPedParamSetsDict(nomadModel):
    pedParamDict = {}
    for setID, pedParamSet in nomadModel.pedParameterSets.items():
        pedParamDict[setID] = {}
        for paramName in PARAM_NMS:
            pedParamDict[setID][paramName] = pedParamSet.getValueDict(paramName)

    return pedParamDict

def getGeometryDict(nomadModel):
    geomDict = {}
    walkLevels = {}
    extent = [0,0,0,0]
    for walkLevel in nomadModel.walkLevels:
        walkLevels[walkLevel.ID] = getWalkLevelDict(walkLevel)
        extent = [min(walkLevel.origin[0], extent[0]),
                  min(walkLevel.origin[1], extent[1]),
                  max(walkLevel.extent[0] + walkLevel.origin[0], extent[2]),
                  max(walkLevel.extent[1] + walkLevel.origin[1], extent[3])]

    geomDict['walkLevels'] = walkLevels
    geomDict['extent'] = extent
    return geomDict

def getWalkLevelDict(walkLevel):
    walkableAreas = {}
    for walkableArea in walkLevel.walkableAreas:
        walkableAreas[walkableArea.ID] = getWalkableAreaCoords(walkableArea)
    obstacles = {}
    for obstacle in walkLevel.obstacles:
        obstacles[obstacle.ID] = getObstacleDict(obstacle)
    destinations = {}
    for destination in walkLevel.destinations:
        destinations[destination.ID] = getDestinationDict(destination)
    sources = {}
    for source in walkLevel.sources:
        sources[source.ID] = getSourceDict(source)

    return {'walkableAreas': walkableAreas, 'obstacles':obstacles,
            'destinations':destinations, 'sources':sources}

def getObstacleDict(obstacle):
    if isinstance(obstacle, LineObstacle):
        return {'type':'line', 'coords':getLineCoordsXY(obstacle.geometry.coords)}
    elif isinstance(obstacle, PolygonObstacle):
        return {'type':'polygon', 'coords':list(obstacle.geometry.exterior.coords[:-1])}
    elif isinstance(obstacle, CircleObstacle):
        return {'type':'circle', 'centerCoord':obstacle.centerCoord, 'radius':obstacle.radius}
    elif isinstance(obstacle, EllipseObstacle):
        return {'type':'ellipse', 'centerCoord':obstacle.centerCoord, 'semiAxesValues':obstacle.semiAxesValues, 'rotation':obstacle.rotation}

def getDestinationDict(destination):
    if isinstance(destination, LineDestination):
        return {'type':'line', 'coords':getLineCoordsXY(destination.geometry.coords), 'isVirtualObstacle':destination.isVirtualObstacle}
    elif isinstance(destination,  (PolygonDestination, MultiPointInRectangleDestination)):
        return {'type':'polygon', 'coords':list(destination.geometry.exterior.coords[:-1]), 'isVirtualObstacle':destination.isVirtualObstacle}
    elif isinstance(destination, MultiPolygonDestination):
        return {'type':'multiPolygon', 'coords':[list(polygon.exterior.coords[:-1]) for polygon in destination.geometry], 'isVirtualObstacle':destination.isVirtualObstacle}
    elif isinstance(destination, PointDestination):
        return {'type':'point', 'coords':list(destination.geometry.coords)[0]}
    
def getSourceDict(source):
    if isinstance(source, LineSource):
        return {'type':'line', 'coords':getLineCoordsXY(source.geometry.coords)}
    elif isinstance(source, RectangleSource):
        return {'type':'polygon', 'coords':list(source.geometry.exterior.coords[:-1])}

def getWalkableAreaCoords(walkableArea):
    if isinstance(walkableArea, list):
        return walkableArea
    
    coords = []
    if isinstance(walkableArea.geometry, MultiPolygon):
        for pol in walkableArea.geometry.geoms:
            coords.append(list(pol.exterior.coords[:-1]))
    else:
        coords.append(list(walkableArea.geometry.exterior.coords[:-1]))

    return coords

def getNamedTupleIter(dataDict, name):
    for key, value in dataDict.items():
            if isinstance(value, dict):
                dataDict[key] = getNamedTupleIter(value, key)

    return namedtuple(name, sorted(dataDict))(**dataDict)

def getLineCoordsXY(coords):
    return [[coords[0][0], coords[1][0]], [coords[0][1], coords[1][1]]]

def exportTrajectoriesToMat(scenFlNm):
    from scipy.io import savemat
    scenDataFilePath = Path(scenFlNm)
    matFlNm = scenDataFilePath.parent.joinpath(scenDataFilePath.stem + '.mat')
    scenData = SimulationFileOutputManager.readScenarioDataFile(scenDataFilePath)
    trajDataFilePath = scenDataFilePath.parent.joinpath(scenData.trajFile)
    trajData, _ = SimulationFileOutputManager.readTrajactoryDataFile(trajDataFilePath)

    contentDict = {}
    for ID, pedTrajData in trajData.items():
        time = pedTrajData['time']
        contentMatrix = np.zeros((len(time),5), dtype=float)
        contentMatrix[:,0] = time
        contentMatrix[:,1] = pedTrajData['pos_x']
        contentMatrix[:,2] = pedTrajData['pos_y']
        contentMatrix[:,3] = pedTrajData['vel_x']
        contentMatrix[:,4] = pedTrajData['vel_y']

        contentDict['ped_{}'.format(ID)] = contentMatrix

    savemat(matFlNm, contentDict, format='5')

def loadScenario(scenDataFlNm):
    logger = logging.getLogger(__name__)
    logging.info('Loading scenarios data ...')
    scenDataFlNm = Path(scenDataFlNm)

    scenData = SimulationFileOutputManager.readScenarioDataFile(scenDataFlNm)
    logger.info('Loading trajectories ...')
    trajDataFlNm = scenDataFlNm.parent.joinpath(scenData.trajFile)
    trajData, _ = SimulationFileOutputManager.readTrajactoryDataFile(trajDataFlNm)
    descDataFlNm = scenDataFlNm.parent.joinpath(scenData.descFile)
    descData = SimulationFileOutputManager.readDescriptionDataFile(descDataFlNm)
    
    logger.info('Loading local route fields ...')
    lrcmDataFlNm = scenDataFlNm.parent.joinpath(scenData.lrcmFile)
    lrcmData = SimulationFileOutputManager.readLrcmDataFile(lrcmDataFlNm)

    return scenData, trajData, descData, lrcmData
