""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from _collections import defaultdict
from copy import copy
import ctypes
import logging
from math import ceil, floor, sqrt, inf
import time

from shapely import MultiPolygon, Point, Polygon
import shapely.ops

from NOMAD import convergeCostMatrix_so_file, \
    canUseConvergeCostMatrixCompiledCode
from NOMAD.activities import Destination
from NOMAD.constants import LRCC_FLOODING_TYPE, LRCC_ITERATIVE_TYPE
from NOMAD.general_functions import doListCeck
from NOMAD.obstacles import LineObstacle, Obstacle
from NOMAD.output_manager import RCMD_INIT_TYPE, RCMD_FINAL_TYPE, \
    SimulationFileOutputManager
from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
import numpy as np



DEBUG_LRCM = False

if DEBUG_LRCM:
    try:
        from scipy.ndimage import morphology, filters #@UnresolvedImport
    except Exception as e:
        raise e
        

def getGridCellID(ped, cellSize):
    gridCellIndex = getIndex(ped.pos_x, ped.pos_y, cellSize)
    return (gridCellIndex[0], gridCellIndex[1], ped.walkLevel.ID)

def getIndexFromPos(pos, cellSize):
    return getIndex(pos.x, pos.y, cellSize)

def getIndexFromArray(posArray, cellSize):
    return getIndex(posArray[0], posArray[1], cellSize)

def getIndex(x, y, cellSize):
    return (floor(x/cellSize), floor(y/cellSize))

def getCoordinate(xInd, yInd, cellSize):
    return (xInd*cellSize, yInd*cellSize)

def getNeighbourIndex(base_x, base_y, offset, walkLevelID):
    return (base_x + offset[0], base_y + offset[1], walkLevelID)

# ================================================================================================
# ================================================================================================

class LocalRouteChoiceManager():
    '''
    classdocs
    '''
    logger = logging.getLogger(__name__)

    def __init__(self, ID, cellSize, desiredDirections, centerCoords, destinationID):
        '''
        Constructor
        '''
        self._ID = ID
        self._cellSize = cellSize
        self._desiredDirections = desiredDirections # Start with dict, if too slow try sparse matrix
        self._centerCoords = centerCoords
        self._destinationID = destinationID

    def getDesiredDirectionForPed(self, ped):
        cellID = getGridCellID(ped, self._cellSize)
        try:
            return self.getDesiredDirection(ped.pos_x, ped.pos_y, cellID)
        except:
            self.logger.debug('Ped {} at pos ({},{}) is out of lrcm bounds'.format(ped.ID, ped.pos_x, ped.pos_y))
            return self.getDirectionToClosestCell(ped.pos, cellID, ped.walkLevel.ID)            

    def getDesiredDirection(self, pos_x, pos_y, cellID):
        dirCurCell_x, dirCurCell_y = self._desiredDirections[cellID]

        offSetIndex_x, offSetIndex_y, coordDiff_x, coordDiff_y = self.getOffsetIndex(pos_x, pos_y, cellID)
        dirCellHor_x, dirCellHor_y = self.getDesiredDirectionOfOtherCell(cellID, offSetIndex_x, 0, dirCurCell_x, dirCurCell_y)
        dirCellDiag_x, dirCellDiag_y = self.getDesiredDirectionOfOtherCell(cellID, offSetIndex_x, offSetIndex_y, dirCurCell_x, dirCurCell_y)
        dirCellVert_x, dirCellVert_y = self.getDesiredDirectionOfOtherCell(cellID, 0, offSetIndex_y, dirCurCell_x, dirCurCell_y)

        d_x = abs(coordDiff_x)
        d_y = abs(coordDiff_y)
        dInv_x = 1 - d_x
        dInv_y = 1 - d_y

        desDir_x = dirCurCell_x*dInv_x*dInv_y + dirCellHor_x*d_x*dInv_y + dirCellDiag_x*d_x*d_y + dirCellVert_x*dInv_x*d_y
        desDir_y = dirCurCell_y*dInv_x*dInv_y + dirCellHor_y*d_x*dInv_y + dirCellDiag_y*d_x*d_y + dirCellVert_y*dInv_x*d_y

        desDir_length = sqrt(desDir_x*desDir_x + desDir_y*desDir_y)
        if desDir_length == 0:
            return 0, 0

        return desDir_x/desDir_length, desDir_y/desDir_length

    def getDesiredDirectionOfOtherCell(self, cellInd, offSet_x, offSet_y, dirCurCell_x, dirCurCell_y):
        dirOtherCell = self._desiredDirections.get((cellInd[0] + offSet_x, cellInd[1] + offSet_y))
        if dirOtherCell is None or np.any(np.isnan(dirOtherCell)):
            return dirCurCell_x, dirCurCell_y

        return dirOtherCell

    def getOffsetIndex(self, pos_x, pos_y, cellInd):
        offSetIndex_x = +1
        offSetIndex_y = +1
        centerCoord_x, centerCoord_y = self._centerCoords[cellInd]

        coordDiff_x = (pos_x - centerCoord_x)/self._cellSize
        coordDiff_y = (pos_y - centerCoord_y)/self._cellSize

        if coordDiff_x < 0:
            offSetIndex_x = -1
        if coordDiff_y < 0:
            offSetIndex_y = -1

        return offSetIndex_x, offSetIndex_y, coordDiff_x, coordDiff_y

    def getDirectionToClosestCell(self, pos, cellID, walkLevelID):
        base_x = cellID[0]
        base_y = cellID[1]
        ringOffset = 1
        centerCoords = []
        while len(centerCoords) == 0:
            centerCoords = []
            extent = 2*ringOffset
            offset = [1*ringOffset, 1*ringOffset] # Start at top left corner
            neighborKey = getNeighbourIndex(base_x, base_y, offset, walkLevelID)
            self.add2listIfHasCenterCoord(neighborKey, centerCoords)
            for _ in range(extent): # Go down
                offset[1] -= 1
                neighborKey = getNeighbourIndex(base_x, base_y, offset, walkLevelID)
                self.add2listIfHasCenterCoord(neighborKey, centerCoords)
            
            for _ in range(extent): # Go left
                offset[0] -= 1
                neighborKey = getNeighbourIndex(base_x, base_y, offset, walkLevelID)
                self.add2listIfHasCenterCoord(neighborKey, centerCoords)
            
            for _ in range(extent): # Go up
                offset[1] += 1
                neighborKey = getNeighbourIndex(base_x, base_y, offset, walkLevelID)
                self.add2listIfHasCenterCoord(neighborKey, centerCoords)
            
            for _ in range(extent-1): # Go right
                offset[0] += 1
                neighborKey = getNeighbourIndex(base_x, base_y, offset, walkLevelID)
                self.add2listIfHasCenterCoord(neighborKey, centerCoords)
            
            ringOffset += 1
                  
        minDist_2 = inf
        for centerCoord_x, centerCoord_y in centerCoords:
            diff_x = centerCoord_x - pos.x
            diff_y = centerCoord_y - pos.y 
            dist_2 = diff_x*diff_x + diff_y*diff_y 
            if dist_2 < minDist_2:
                minDist_2 = dist_2
                closestCellInfo = (diff_x, diff_y, dist_2) 

        dist = sqrt(closestCellInfo[2])
        return closestCellInfo[0]/dist, closestCellInfo[1]/dist

    def add2listIfHasCenterCoord(self, index, centerCoordsList):
        if index in self._centerCoords:
            centerCoordsList.append(self._centerCoords[index])
    @property
    def ID(self):
        return self._ID

    @property
    def cellSize(self):
        return self._cellSize

    @property
    def desiredDirections(self):
        return self._desiredDirections

    @property
    def centerCoords(self):
        return self._centerCoords

    @property
    def destinationID(self):
        return self._destinationID

# ================================================================================================
# ================================================================================================

def createLocalRouteChoiceManagers(lrccType, walkLevels, outputManager, *args, **kwargs):
    if lrccType == LRCC_FLOODING_TYPE:
        lrcc = LocalRouteChoiceFloodingComputer(walkLevels, outputManager, *args, **kwargs)
    elif lrccType == LRCC_ITERATIVE_TYPE:
        lrcc = LocalRouteChoiceIterativeComputer(walkLevels, outputManager, *args, **kwargs)
    else:
        raise Exception('Unknown lrcc type "{}"'.format(lrccType))
    lrcc.createLocalRouteChoiceManagers()

# ================================================================================================
# ================================================================================================

CELL_SIZE = 0.1 # [m]
CELL_AREA = CELL_SIZE*CELL_SIZE  # [m2]
BORDER_SIZE = 1
LOCAL_DEST_BUFFER = 2 # [m]

MAX_CONVERGENCE_INTERATIONS = int(2e5)
MAX_INIT_INTERATIONS = 1e4

ERROR_THRESHOLD = 5e-6
D_ERROR_THRESHOLD = 5e-8

MIN_WALKING_COST = 1e-2

DEF_DESIRED_SPEED = 1.34 # [m/s]

C3 = 3
NU = 1e-2
ETA = 1e-3
DTT = CELL_SIZE/4
VIRT_OBS_WALKING_COST = 5

TOP_LOC = 'top'
RIGHT_LOC = 'right'
BOTTOM_LOC = 'bottom'
LEFT_LOC = 'left'
TOP_RIGHT_LOC = 'topRight'
BOTTOM_RIGHT_LOC = 'bottonRight'
BOTTOM_LEFT_LOC = 'bottomLeft'
TOP_LEFT_LOC = 'topLeft'

DESTINATION_COST_TYPE = 'destination'
OBS_COST_TYPE = 'obs'

OFFSET = 'offset'
LOC = 'loc'
DIAG_FACTOR = 'diagFactor'

OFFSET_INFO = (
    {OFFSET:( 0, 1), LOC:TOP_LOC,          DIAG_FACTOR:1},
    {OFFSET:( 1, 0), LOC:RIGHT_LOC,        DIAG_FACTOR:1},
    {OFFSET:( 0,-1), LOC:BOTTOM_LOC,       DIAG_FACTOR:1},
    {OFFSET:(-1, 0), LOC:LEFT_LOC,         DIAG_FACTOR:1},
    {OFFSET:( 1, 1), LOC:TOP_RIGHT_LOC,    DIAG_FACTOR:sqrt(2)},
    {OFFSET:( 1,-1), LOC:BOTTOM_RIGHT_LOC, DIAG_FACTOR:sqrt(2)},
    {OFFSET:(-1,-1), LOC:BOTTOM_LEFT_LOC,  DIAG_FACTOR:sqrt(2)},
    {OFFSET:(-1, 1), LOC:TOP_LEFT_LOC,     DIAG_FACTOR:sqrt(2)}
    )


class LocalRouteChoiceComputer(): # Lrc -> Local route choice

    logger = logging.getLogger(__name__)
    
    def __init__(self, walkLevels, outputManager, pedRadiusMax, **kwargs):
        walkLevels = doListCeck(walkLevels)
        if len(walkLevels) > 1:
            raise Exception("The LocalRouteChoiceComputer doesn't support multiple walk levels yet")

        self.loadFlNm = None
        if 'loadFlNm' in kwargs:
            self.loadFlNm = kwargs['loadFlNm']
            kwargs.pop('loadFlNm')

        self.outputManager = outputManager

        self.walkLevels = walkLevels
        self.destination2walkLevel = getDestination2walkLevel(walkLevels)
        self.bufferSize = ceil((pedRadiusMax)/CELL_SIZE)

        self.xCellCount, self.yCellCount, self.xExtent, self.yExtent = getCellCounts(walkLevels)

        self.maxWalkingCost = self.calcMaxWalkingCost()

        self.walkLevelComputers = {}
        walkLevelRouteChoiceComputer, args = self.getWalkLevelRouteChoiceComputer()
        self.logger.info('Creating the local route choice manager using {}'.format(self))
        self.startTime = time.time()
        
        for walkLevel in walkLevels:
            self.walkLevelComputers[walkLevel.ID] = walkLevelRouteChoiceComputer(walkLevel, self.maxWalkingCost,
                                                                                 self.bufferSize, *args, **kwargs)
            self.logger.info('Creating a walk level local route choice manager using {}'.format(self.walkLevelComputers[walkLevel.ID]))
            self.walkLevelComputers[walkLevel.ID].createWalkableAreaBaseKeys()

    def calcMaxWalkingCost(self):
        raise NotImplementedError('Implement method!')

    def getWalkLevelRouteChoiceComputer(self):
        raise NotImplementedError('Implement method!')

    def createLocalRouteChoiceManagers(self):
        if self.loadFlNm is not None:
            self.createLocalRouteChoiceManagersFromFile()            
            self.logger.info(f'Finished creating local route choice manager in {time.time() - self.startTime:.2f} seconds')
            return
        
        for destination, walkLevel in self.destination2walkLevel.items():
            if isinstance(destination, Destination) and destination._ignoreInLrcc:
                continue
            self.resetMatrices()
            if isinstance(destination, Destination):
                self.createLocalRouteChoiceManagerSingle(destination, walkLevel)
            else:
                self.createLocalRouteChoiceManagerGroup(*destination, walkLevel)
                
        self.logger.info(f'Finished creating local route choice manager in {time.time() - self.startTime:.2f}  seconds') 

    def createLocalRouteChoiceManagerSingle(self, destination, walkLevel):
        self.logger.debug('Creating the local route choice manager for dest. {}'.format(destination.ID))            
        for walkLevelComputer in self.walkLevelComputers.values():
            walkLevelComputer.createWalkableAreaKeys(destination, walkLevelComputer.walkLevel.ID == walkLevel.ID)

            walkLevelComputer.createCostMatrix(destination)

        desiredDirections, centerCoords = self.computeDesiredDirections()

        self.createLocalRouteChoiceManager(destination, desiredDirections, centerCoords)
    
    def createLocalRouteChoiceManagerGroup(self, destinations, globalDest, walkLevel):
        if globalDest is None:
            globalDestGeom = shapely.ops.unary_union([dest._geometry for dest in destinations])
            globalDestIsObs = False
        else:
            globalDestGeom = globalDest._geometry
            globalDestIsObs = isinstance(globalDest, Obstacle)
            
        globalDest = Destination(tuple([dest.ID for dest in destinations]), groupID=destinations[0].groupID) 
        globalDest._geometry = globalDestGeom
        globalDestBufferedGeom = shapely.ops.unary_union([dest._geometry for dest in destinations]).buffer(LOCAL_DEST_BUFFER)
        globalDestFill = Destination(f'globDestFill', groupID=destinations[0].groupID)
        setGeom(globalDestFill, globalDestBufferedGeom, walkLevel, destinations)
            
        globalDestinationKeys = set()
        for walkLevelComputer in self.walkLevelComputers.values():
            walkLevelComputer.createWalkableAreaKeys(globalDestFill, walkLevelComputer.walkLevel.ID == walkLevel.ID)
            globalDestinationKeys = globalDestinationKeys.union(walkLevelComputer.destinationKeys)
            
        self.resetMatrices()
        for walkLevelComputer in self.walkLevelComputers.values():
            walkLevelComputer.createWalkableAreaKeys(globalDest, walkLevelComputer.walkLevel.ID == walkLevel.ID, isDestObs=globalDestIsObs)
            walkLevelComputer.createCostMatrix(globalDest)

        globalDesiredDirections, globalCenterCoords = self.computeDesiredDirections()
        for destination in destinations:
            self.resetMatrices()
            for walkLevelComputer in self.walkLevelComputers.values():
                #walkLevelComputer.createWalkableAreaKeys(destination, walkLevelComputer.walkLevel.ID == walkLevel.ID)
                walkLevelComputer.walkableAreaKeys = copy(globalDestinationKeys)
                walkLevelComputer.removeDestinationKeys(destination, walkLevelComputer.walkLevel.ID == walkLevel.ID)
        
                walkLevelComputer.createCostMatrix(destination)
    
    
            newDesiredDirections = copy(globalDesiredDirections)
            newCenterCoords = copy(globalCenterCoords)
            
            desiredDirections, centerCoords = self.computeDesiredDirections(isLocal=True)
            
            newDesiredDirections.update(desiredDirections)
            newCenterCoords.update(centerCoords)
            
            self.createLocalRouteChoiceManager(destination, newDesiredDirections, newCenterCoords)

    def createLocalRouteChoiceManagersFromFile(self):
        lrcmDataPerDest = SimulationFileOutputManager.readLrcmDataFile(self.loadFlNm)
        if self.outputManager.geomHash is None:
            self.logger.warning("The output manager has no geometry hash. The infrastucture and the local route choice manager might be out of sync!")
        elif 'geomHash' not in lrcmDataPerDest:
            self.logger.warning("The local route choice manager file does not contains a geometry hash. The infrastucture and the local route choice manager might be out of sync!")
        elif self.outputManager.geomHash != str(lrcmDataPerDest['geomHash']):
            self.logger.warning("The infrastucture and the local route choice manager are out of sync! (the hashes are different!)")
                
        for destination, walkLevel in self.destination2walkLevel.items():
            if isinstance(destination, tuple): 
                for subDestination in destination[0]:
                    self.createLocalRouteChoiceManagerFromFile(subDestination, walkLevel, lrcmDataPerDest)
            else:
                self.createLocalRouteChoiceManagerFromFile(destination, walkLevel, lrcmDataPerDest)
        del lrcmDataPerDest
 
    def createLocalRouteChoiceManagerFromFile(self, destination, walkLevel, lrcmDataPerDest):
        if destination._ignoreInLrcc:
            return
        self.logger.debug('Creating the local route choice manager for dest. {}'.format(destination.ID))
        lrcmData = lrcmDataPerDest[getIDstrFomIDs(walkLevel.ID, destination.ID)]
        desiredDirections = {}
        centerCoords = {}           
        for ii in range(len(lrcmData)):
            centerCoord = (lrcmData[ii,0], lrcmData[ii,1])
            localKey = WalkLevelRouteChoiceComputer.getLocalKeyFromCenterCoord(*centerCoord)
            key = WalkLevelRouteChoiceComputer.getGlobalKey(localKey, walkLevel.ID)
            if key in desiredDirections:
                raise Exception('Double!') 
            desiredDirections[key] = (lrcmData[ii,2], lrcmData[ii,3])
            centerCoords[key] = centerCoord
        self.createLocalRouteChoiceManager(destination, desiredDirections, centerCoords)
        
    def createLocalRouteChoiceManager(self, destination, desiredDirections, centerCoords):        
        destination.setLocalRouteChoiceManager(LocalRouteChoiceManager(destination.ID, CELL_SIZE, desiredDirections, centerCoords, destination.ID))
        self.logger.debug('Done creating the local route choice manager for dest. {}'.format(destination.ID))

    def resetMatrices(self):
        for walkLevelComputer in self.walkLevelComputers.values():
            walkLevelComputer.resetMatrices()

    def computeDesiredDirections(self, computeCenterCoords=True, isLocal=False):
        desiredDirections = {}
        centerCoords = {}
        
        for walkLevelComputer in self.walkLevelComputers.values():
            desiredDirectionsOfComputer = walkLevelComputer.getDesiredDirections(isLocal=isLocal)
            desiredDirections.update(desiredDirectionsOfComputer)
            if computeCenterCoords:
                centerCoords.update(walkLevelComputer.getCenterCoords(desiredDirectionsOfComputer))

        self.logger.debug('Finished computing the desired directions')
        
        if computeCenterCoords:
            return desiredDirections, centerCoords
        
        return desiredDirections

#==================================================================================================
#==================================================================================================

class WalkLevelRouteChoiceComputer(): # WlLrc -> Wlk level Local route choice
    
    logger = logging.getLogger(__name__)

    def __init__(self, walkLevel, maxWalkingCost, bufferSize):
        self.walkLevel = walkLevel
        self.maxWalkingCost = maxWalkingCost
        self.bufferSize = bufferSize

        self.xOffset, self.yOffset = getOriginOffsets(walkLevel)
        # Add 1 to the offsets to compensate for the extra border around the costMatrix
        self.xOffset += BORDER_SIZE
        self.yOffset += BORDER_SIZE

        self.walkableAreaBaseKeys = None

        self.costMatrix = None
        self.walkableAreaKeys = None
        self.destinationKeys = None
        
        self.bufferedVirtualObstacleKeys = {}
        self.virtualObstacleKeys = {}
        self.virtualObsCostMatrices = {}
        
        self.routingBufferKeys = defaultdict(set)

        self.xCellCount = ceil(self.walkLevel.extent.x/CELL_SIZE) + 1 + 2*BORDER_SIZE
        self.yCellCount = ceil(self.walkLevel.extent.y/CELL_SIZE) + 1 + 2*BORDER_SIZE
        
        self.minKey_x = -self.xOffset
        self.minKey_y = -self.yOffset
        self.maxKey_x = self.xCellCount - self.xOffset
        self.maxKey_y = self.yCellCount - self.yOffset
        
    def createWalkableAreaBaseKeys(self):
        self.walkableAreaBaseKeys = set() #set
        self.lineObsKeys = set() #set
        for walkableArea in self.walkLevel.walkableAreas:
            WalkLevelRouteChoiceComputer.performActionOnCellOverlap(walkableArea,
                                                                    self.fillWalkableAreaBaseKeysAction,
                                                                    CELL_SIZE)

        for destination in self.walkLevel.destinations:
            if destination.isVirtualObstacle:
                self.virtualObstacleKeys[destination.ID] = set()
                self.bufferedVirtualObstacleKeys[destination.ID] = set()
                WalkLevelRouteChoiceComputer.performActionOnCellOverlap(destination,
                                                                    self.fillVirtualObstacleKeysAction,
                                                                    CELL_SIZE)

        for routingBuffer in self.walkLevel.routingBuffers:
            WalkLevelRouteChoiceComputer.performActionOnCellOverlap(routingBuffer,
                                                                    self.fillRoutingBufferKeysAction,
                                                                    CELL_SIZE)
            
    def fillWalkableAreaBaseKeysAction(self, walkableArea, cellPol, xInd, yInd):
        if walkableArea.contains(cellPol):
            self.walkableAreaBaseKeys.add((xInd, yInd))
        elif walkableArea.intersects(cellPol):
            diffPol = cellPol.difference(walkableArea.geometry)
            if diffPol.area/CELL_AREA < 0.5:
                self.walkableAreaBaseKeys.add((xInd, yInd))

    def fillVirtualObstacleKeysAction(self, destination, cellPol, xInd, yInd):
        if (xInd, yInd) not in self.walkableAreaBaseKeys:
            return

        if destination.contains(cellPol):
            self.virtualObstacleKeys[destination.ID].add((xInd, yInd))
        elif destination.intersects(cellPol):
            diffPol = cellPol.difference(destination.geometry)
            if diffPol.area/CELL_AREA < 0.5:
                self.virtualObstacleKeys[destination.ID].add((xInd, yInd))

        destinationGeom = destination.buffer(self.bufferSize*CELL_SIZE)
        if destinationGeom.contains(cellPol):
            self.bufferedVirtualObstacleKeys[destination.ID].add((xInd, yInd))
        elif destinationGeom.intersects(cellPol):
            diffPol = cellPol.difference(destinationGeom)
            if diffPol.area/CELL_AREA < 0.5:
                self.bufferedVirtualObstacleKeys[destination.ID].add((xInd, yInd))

    def fillRoutingBufferKeysAction(self, routingBuffer, cellPol, xInd, yInd):
        if (xInd, yInd) not in self.walkableAreaBaseKeys:
            return
        
        if routingBuffer.contains(cellPol):
            self.routingBufferKeys[routingBuffer.groupID].add((xInd, yInd))
        elif routingBuffer.intersects(cellPol):
            diffPol = cellPol.difference(routingBuffer.geometry)
            if diffPol.area/CELL_AREA < 0.5:
                self.routingBufferKeys[routingBuffer.groupID].add((xInd, yInd))
                 
    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------

    def resetMatrices(self):
        self.costMatrix = None
        self.walkableAreaKeys = None
        self.destinationKeys = None
        self.virtualObsCostMatrices = {}

    def createWalkableAreaKeys(self, destination, containsDestination, isDestObs=False):
        if not containsDestination and self.walkableAreaKeys is not None:
            return

        self.walkableAreaKeys = copy(self.walkableAreaBaseKeys)

        #self.removeVirtualObstacleKeys(destination)

        if not isDestObs:
            self.removeDestinationKeys(destination, containsDestination)
        else:
            if not containsDestination:
                return

            self.destinationKeys = set()
            self.overlapFactor = 0.99
            while len(self.destinationKeys) == 0:
                WalkLevelRouteChoiceComputer.performActionOnCellOverlap(destination, self.addDestinationKeys, CELL_SIZE)
                self.overlapFactor -= 0.05

    def addDestinationKeys(self, destination, cellPol, xInd, yInd):
        if destination.contains(cellPol):
            self.destinationKeys.add((xInd, yInd))
        elif not isinstance(destination, Point) and destination.intersects(cellPol):
            diffPol = cellPol.difference(destination.geometry)
            if (CELL_AREA - diffPol.area)/CELL_AREA > self.overlapFactor:
                self.destinationKeys.add((xInd, yInd))
        elif isinstance(destination, Point) and cellPol.contains(destination.geometry):
            self.destinationKeys.add((xInd, yInd))

    def removeVirtualObstacleKeys(self, destination):
        for destinationID, virtualObstacleKeys in self.virtualObstacleKeys.items():
            if destinationID == destination.ID:
                continue
            for key in virtualObstacleKeys:
                if key in self.walkableAreaKeys:
                    self.walkableAreaKeys.remove(key)

    def removeDestinationKeys(self, destination, containsDestination):
        baseLength = len( self.walkableAreaKeys)

        if not containsDestination:
            return

        self.destinationKeys = set()
        self.overlapFactor = 0.99
        while len(self.walkableAreaKeys) == baseLength:
            WalkLevelRouteChoiceComputer.performActionOnCellOverlap(destination, self.removeWalkableAreaKeys, 
                                                                    CELL_SIZE, buffer=isinstance(destination.geometry, Point))
            self.overlapFactor -= 0.05

    def removeWalkableAreaKeys(self, destination, cellPol, xInd, yInd):
        if (xInd, yInd) not in self.walkableAreaKeys:
            return
        if destination.contains(cellPol):
            self.walkableAreaKeys.remove((xInd, yInd))
            self.destinationKeys.add((xInd, yInd))
        elif not isinstance(destination.geometry, Point) and destination.intersects(cellPol):
            diffPol = cellPol.difference(destination.geometry)
            if (CELL_AREA - diffPol.area)/CELL_AREA > self.overlapFactor:
                self.walkableAreaKeys.remove((xInd, yInd))
                self.destinationKeys.add((xInd, yInd))
        elif isinstance(destination.geometry, Point) and cellPol.intersects(destination.geometry):
            self.walkableAreaKeys.remove((xInd, yInd))
            self.destinationKeys.add((xInd, yInd))

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------

    def createCostMatrix(self, destination):
        raise NotImplementedError('Implement method!')

    def getValueFromCostMatrix(self, costMatrix, key):
        key = (key[0] + self.xOffset, key[1] + self.yOffset)

        return costMatrix[key]

    def setValueInCostMatrix(self, costMatrix, key, value):
        key = (key[0] + self.xOffset, key[1] + self.yOffset)

        costMatrix[key] = value

    def floodMatrix(self, donatingKeys, costMatrix, costFcn, *args):
        iterationCount = 0
        while len(donatingKeys) > 0 and iterationCount <= MAX_INIT_INTERATIONS:
            nextDonatingKeys = set()
            for donatingKey in donatingKeys:
                self.donate2neighbors(donatingKey, nextDonatingKeys, costMatrix, costFcn, *args)

            donatingKeys = copy(nextDonatingKeys)
            iterationCount += 1

    def donate2neighbors(self, donatingKey, nextDonatingKeys, costMatrix, costFcn, *args):
        donatedCost = self.getValueFromCostMatrix(costMatrix, donatingKey)
        for offsetInfo in OFFSET_INFO:
            offset = offsetInfo[OFFSET]
            diagFactor = offsetInfo[DIAG_FACTOR]
            neighborKey = (donatingKey[0] + offset[0], donatingKey[1] + offset[1])
            costFcn(neighborKey, diagFactor, nextDonatingKeys, donatedCost, *args)

    def floodCostFcn(self, neighborKey, diagFactor, nextDonatingKeys, donatedCost, constantCost):
        if neighborKey not in self.walkableAreaKeys:
            return
        newCost = donatedCost + constantCost*diagFactor

        if newCost < self.getValueFromCostMatrix(self.costMatrix, neighborKey):
            self.setValueInCostMatrix(self.costMatrix, neighborKey, newCost)
            nextDonatingKeys.add(neighborKey)

    def setupCostMatrixForDestination(self):
        donatingKeys = []
        for destinationKey in self.destinationKeys:
            self.setValueInCostMatrix(self.costMatrix, destinationKey, 0)
            donatingKeys.append(destinationKey)

        return donatingKeys

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------

    def createAndFloodVirtualObsCostMatrices(self, destination, constantCost):

        for destinationID in self.virtualObstacleKeys.keys():
            if destinationID == destination.ID:
                continue

            self.curVirtualObsKey = destinationID
            # Per virtual obstacle
            # - Create matrix
            # - Create initial donating keys
            # - Flood the matrix
            self.virtualObsCostMatrices[self.curVirtualObsKey] = WalkLevelRouteChoiceComputer.initCostMatrix(self.xCellCount,
                                                                                                             self.yCellCount,
                                                                                                             baseValue=self.maxWalkingCost)
            # Fill with the connected cell in the walkable area and also obtain the first set of donating keys
            donatingKeys = []
            for key in self.virtualObstacleKeys[destinationID]:
                for offsetInfo in OFFSET_INFO:
                    if offsetInfo[LOC] in [TOP_LEFT_LOC, TOP_RIGHT_LOC, BOTTOM_LEFT_LOC, BOTTOM_RIGHT_LOC]:
                        continue
                    offset = offsetInfo[OFFSET]
                    neighborKey = (key[0] + offset[0], key[1] + offset[1])
                    if neighborKey in self.walkableAreaKeys and neighborKey not in donatingKeys:
                        self.setValueInCostMatrix(self.virtualObsCostMatrices[self.curVirtualObsKey], neighborKey,
                                                  self.getValueFromCostMatrix(self.costMatrix, neighborKey))
                        donatingKeys.append(neighborKey)
                        #print('{} - {}'.format(neighborKey, self.getValueFromCostMatrix(self.costMatrix, neighborKey)))

            self.floodMatrix(donatingKeys, self.virtualObsCostMatrices[self.curVirtualObsKey],
                                                     self.virtualObsCostFcn, constantCost)

    def virtualObsCostFcn(self, neighborKey, diagFactor, nextDonatingKeys, donatedCost, constantCost):
        if neighborKey not in self.virtualObstacleKeys[self.curVirtualObsKey]:
            return

        newCost = donatedCost + constantCost*diagFactor

        if newCost < self.getValueFromCostMatrix(self.virtualObsCostMatrices[self.curVirtualObsKey], neighborKey):
            self.setValueInCostMatrix(self.virtualObsCostMatrices[self.curVirtualObsKey], neighborKey, newCost)
            nextDonatingKeys.add(neighborKey)

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------
    def getDesiredDirections(self, isLocal=False):
        desiredDirections = self.getDesiredDirectionsFromCostMatrix(self.costMatrix,
                                                                    self.walkableAreaKeys, ignoreObs=True, isLocal=isLocal)

        return desiredDirections

    def getDesiredDirectionsFromCostMatrix(self, costMatrix, keys2process, normalized=True, ignoreZero=False, ignoreObs=False, isLocal=False):
        desiredDirections = {}
        if isLocal:
            calcFcn = self.calcDesiredDirectionLocal
        else:
            calcFcn = self.calcDesiredDirection
        
        for key in keys2process:
            desiredDirections[self.getGlobalKey(key, self.walkLevel.ID)] = calcFcn(key, costMatrix, normalized, ignoreZero, ignoreObs)

        for destinationKey in self.destinationKeys:
            desiredDirections[self.getGlobalKey(destinationKey, self.walkLevel.ID)] = (0.0,0.0)

        return desiredDirections

    def calcDesiredDirectionLocal(self, key, costMatrix, normalized=True, ignoreZero=False, ignoreObs=False):
        cost = self.getValueFromCostMatrix(costMatrix, key)
        costDict = {}
        for offsetInfo in OFFSET_INFO:
            offset = offsetInfo[OFFSET]
            loc = offsetInfo[LOC]
            locKey = (key[0] + offset[0], key[1] + offset[1])
            neighborCost = self.getValueFromCostMatrix(costMatrix, locKey)
            if locKey in self.walkableAreaKeys or locKey in self.destinationKeys:
                costDict[loc] = neighborCost
            elif (locKey[0] >= self.minKey_x and locKey[0] <= self.maxKey_x) and (locKey[1] >= self.minKey_y and locKey[1] <= self.maxKey_y):
                if locKey[0] in self.walkableAreaBaseKeys:
                    costDict[loc] = cost
                else:
                    costDict[loc] = neighborCost
                if not ignoreObs and neighborCost == 0:
                    costDict[loc] = cost
                if not ignoreZero and neighborCost == self.maxWalkingCost:
                    costDict[loc] = cost
            else:
                costDict[loc] = cost
        return calcDesiredDirection(costDict, normalized)
        
    def calcDesiredDirection(self, key, costMatrix, normalized=True, ignoreZero=False, ignoreObs=False):
        cost = self.getValueFromCostMatrix(costMatrix, key)
        
        costDict = {}
        for offsetInfo in OFFSET_INFO:
            offset = offsetInfo[OFFSET]
            loc = offsetInfo[LOC]
            locKey = (key[0] + offset[0], key[1] + offset[1])
            neighborCost = self.getValueFromCostMatrix(costMatrix, locKey)
            if locKey in self.walkableAreaBaseKeys:
                costDict[loc] = neighborCost
            elif (locKey[0] >= self.minKey_x and locKey[0] <= self.maxKey_x) and (locKey[1] >= self.minKey_y and locKey[1] <= self.maxKey_y):
                costDict[loc] = neighborCost
                if not ignoreObs and neighborCost == 0:
                    costDict[loc] = cost
                if not ignoreZero and neighborCost == self.maxWalkingCost:
                    costDict[loc] = cost
            else:
                costDict[loc] = cost

        return calcDesiredDirection(costDict, normalized)

    def getCenterCoords(self, keys):
        centerCoords = {}

        for key in keys:
            centerCoords[self.getGlobalKey(key, self.walkLevel.ID)] = WalkLevelRouteChoiceComputer.getCenterCoord(key)

        return centerCoords

    @staticmethod
    def getCenterCoord(key):
        return (key[0]*CELL_SIZE + 0.5*CELL_SIZE, key[1]*CELL_SIZE + 0.5*CELL_SIZE)

    @staticmethod
    def getLocalKeyFromCenterCoord(*args):
        if len(args) == 1:
            return (round((args[0].x - 0.5*CELL_SIZE)/CELL_SIZE), round((args[0].y - 0.5*CELL_SIZE)/CELL_SIZE))
        else:
            return (round((args[0] - 0.5*CELL_SIZE)/CELL_SIZE), round((args[1] - 0.5*CELL_SIZE)/CELL_SIZE))

    @staticmethod
    def getGlobalKey(localKey, walkLevelID):
        return (localKey[0], localKey[1], walkLevelID)

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------
    def __repr__(self):
        raise NotImplementedError('Implement method!')
    
    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------

    @staticmethod
    def performActionOnCellOverlap(overlapObj, actionFcn, cellSize, buffer=0):
        xStart, yStart = getIndexFromArray(overlapObj.bounds[:2], cellSize)
        xStart = xStart - buffer
        yStart = yStart - buffer
        xEnd, yEnd = getIndexFromArray(overlapObj.bounds[2:], cellSize)
        xEnd += buffer
        yEnd += buffer

        for xInd in range(xStart, xEnd + 1):
            for yInd in range(yStart, yEnd + 1):
                llCoord = getCoordinate(xInd, yInd, cellSize)
                cellPol = Polygon((llCoord, (llCoord[0]+cellSize, llCoord[1]),
                                   (llCoord[0]+cellSize, llCoord[1]+cellSize),
                                   (llCoord[0], llCoord[1]+cellSize)))
                actionFcn(overlapObj, cellPol, xInd, yInd)

    @staticmethod
    def initCostMatrix(xCellCount, yCellCount, baseValue=None, baseMatrix=None):
        if baseMatrix is not None:
            return np.copy(baseMatrix)
        elif baseValue is not None:
            return np.ones((xCellCount, yCellCount), dtype=float)*baseValue
        else:
            return np.zeros((xCellCount, yCellCount), dtype=float)

#==================================================================================================
#==================================================================================================

class LocalRouteChoiceFloodingComputer(LocalRouteChoiceComputer):

    def calcMaxWalkingCost(self):
        return (self.xCellCount + self.yCellCount)*2*DEF_DESIRED_SPEED

    def getWalkLevelRouteChoiceComputer(self):
        return WalkLevelRouteChoiceFloodingComputer, ()
    
    def __repr__(self):
        return 'LocalRouteChoiceFloodingComputer'

class WalkLevelRouteChoiceFloodingComputer(WalkLevelRouteChoiceComputer):

    def __init__(self, walkLevel, maxWalkingCost, bufferSize):
        super().__init__(walkLevel, maxWalkingCost, bufferSize)
        self.bufferedObstacleKeys = {}

    def createWalkableAreaBaseKeys(self):
        super().createWalkableAreaBaseKeys()

        for obstacle in self.walkLevel.obstacles:
            self.bufferedObstacleKeys[obstacle.ID] = set()
            WalkLevelRouteChoiceComputer.performActionOnCellOverlap(obstacle,
                                                                 self.fillBufferedObstacleKeysAction,
                                                                 CELL_SIZE, buffer=self.bufferSize)

    def fillBufferedObstacleKeysAction(self, obstacle, cellPol, xInd, yInd):
        if (xInd, yInd) not in self.walkableAreaBaseKeys:
            return

        obstacleGeom = obstacle.buffer(self.bufferSize*CELL_SIZE)
        if obstacleGeom.contains(cellPol):
            self.bufferedObstacleKeys[obstacle.ID].add((xInd, yInd))
        elif obstacleGeom.intersects(cellPol):
            diffPol = cellPol.difference(obstacleGeom)
            if diffPol.area/CELL_AREA < 0.5:
                self.bufferedObstacleKeys[obstacle.ID].add((xInd, yInd))

    def createWalkableAreaKeys(self, destination, containsDestination):
        if not containsDestination and self.walkableAreaKeys is not None:
            return

        self.walkableAreaKeys = copy(self.walkableAreaBaseKeys)

        self.removeVirtualObstacleKeys(destination)
        self.removeBufferedObstacleKeys()

        self.removeDestinationKeys(destination, containsDestination)

    def removeBufferedObstacleKeys(self):
        for bufferedObstacleKeys in self.bufferedObstacleKeys.values():
            for key in bufferedObstacleKeys:
                if key in self.walkableAreaKeys:
                    self.walkableAreaKeys.remove(key)

    def createCostMatrix(self, destination):
        self.logger.debug('Creating the cost matrix using the flooding-only algorithm')
        self.costMatrix = WalkLevelRouteChoiceComputer.initCostMatrix(self.xCellCount,
                                                                      self.yCellCount,
                                                                      baseValue=self.maxWalkingCost)

        donatingKeys = self.setupCostMatrixForDestination()

        constantCost = DEF_DESIRED_SPEED*2*0.5

        self.floodMatrix(donatingKeys, self.costMatrix, self.floodCostFcn, constantCost)

        self.createAndFloodVirtualObsCostMatrices(destination, constantCost)

        self.logger.debug('Done creating the cost matrix')

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------

    def getDesiredDirections(self):
        desiredDirections = super().getDesiredDirections()

        self.addObstacleBufferSpacesDirection(desiredDirections)

        self.addVirtualObstacleBufferSpacesDirection(desiredDirections)

        return desiredDirections

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------

    def addObstacleBufferSpacesDirection(self, desiredDirections):
        for obstacleID, keys in self.bufferedObstacleKeys.items():
            obstacle = self.walkLevel.getObstacle(obstacleID)
            self.addObstacleBufferSpaceDirection(desiredDirections, obstacle, keys)

    def addObstacleBufferSpaceDirection(self, desiredDirections, obstacle, keys):
        for key in keys:
            globalKey = self.getGlobalKey(key)
            if globalKey in desiredDirections:
                continue
            desiredDirections[globalKey] = getDesiredDirectionFromCenter(*self.getCenterCoord(key), obstacle.geometry)

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------

    def addVirtualObstacleBufferSpacesDirection(self, desiredDirections):
        for obstacleID, keys in self.bufferedVirtualObstacleKeys.items():
            destination = self.walkLevel.getDestination(obstacleID)
            self.addVirtualObstacleBufferSpaceDirection(desiredDirections, destination, keys)

    def addVirtualObstacleBufferSpaceDirection(self, desiredDirections, destination, keys):
        isPartlyFilled = False
        for key in self.virtualObstacleKeys[destination.ID]:
            if self.getGlobalKey(key, self.walkLevel.ID) in desiredDirections:
                isPartlyFilled = True
                break

        for key in keys:
            globalKey = self.getGlobalKey(key, self.walkLevel.ID)
            if key in self.virtualObstacleKeys[destination.ID]:
                if globalKey in desiredDirections and not desiredDirections[globalKey].is_null():
                    continue
                if isPartlyFilled:
                    minDist = inf
                    for obstacle in self.walkLevel.obstacles:
                        dist = obstacle.distance(destination)
                        if dist < minDist:
                            baseGeometry = obstacle.geometry
                else:
                    baseGeometry = destination.getCenterPoint()
            else:
                if globalKey in desiredDirections:
                    continue
                baseGeometry = destination.geometry

            desiredDirections[globalKey] = getDesiredDirectionFromCenter(*self.getCenterCoord(key), baseGeometry)

    # ----------------------------------------------------------------------------------------------
    # ----------------------------------------------------------------------------------------------
    def __repr__(self):
        raise 'WalkLevelRouteChoiceFloodingComputer'

#==================================================================================================
#==================================================================================================
if canUseConvergeCostMatrixCompiledCode:
    convergeCostLib = ctypes.CDLL(str(convergeCostMatrix_so_file))
    
    class ConvergenceInfo(ctypes.Structure):
        _fields_ = [('iterationCount', ctypes.c_int),
                    ('errorIsMinimal', ctypes.c_bool),
                    ('error', ctypes.c_double),
                    ('dError', ctypes.c_double)]
    
    convergeCostMatrix = convergeCostLib.convergeCostMatrix
    convergeCostMatrix.restype = ConvergenceInfo
    convergeCostMatrix.argtypes = [ctypes.c_void_p]*4  + [ctypes.c_int]*6 + [ctypes.c_double]*8 + [ctypes.POINTER(ctypes.c_int)]*5

class LocalRouteChoiceIterativeComputer(LocalRouteChoiceComputer):

    def __init__(self, walkLevels, outputManager, pedRadiusMax, useC=True, **kwargs):
        self.useC = useC
        super().__init__(walkLevels, outputManager, pedRadiusMax, **kwargs)

        for walkLevelComputer in self.walkLevelComputers.values():
            walkLevelComputer.createAndFillObsCostMatrix()

    def calcMaxWalkingCost(self):
        return sqrt(self.xExtent**2 + self.yExtent**2)*100

    def getWalkLevelRouteChoiceComputer(self):
        return WalkLevelRouteChoiceIterativeComputer, (self.useC, self.outputManager)

    def __repr__(self):
        if self.useC:
            return 'LocalRouteChoiceIterativeComputer + C'
        else:
            return 'LocalRouteChoiceIterativeComputer' 

class WalkLevelRouteChoiceIterativeComputer(WalkLevelRouteChoiceComputer):

    def __init__(self, walkLevel, maxWalkingCost, bufferSize, useC, outputManager, obstacles2reduce=None, reductionSize=None):
        super().__init__(walkLevel, maxWalkingCost, bufferSize)
        self.useC = useC
        self.outputManager = outputManager

        self.walkingCostMatrixBase = None
        self.walkingCostMatrix = None

        self.previousError = None
        self.dError = None
        self.iterationCount = None
        
        self.obstacles2reduce = obstacles2reduce
        self.reductionSize = reductionSize
        
        if self.obstacles2reduce is None:
            self.obstacles2reduce = [] 
        self.reductionSize = reductionSize
        if len(self.obstacles2reduce) > 0 and not isinstance(self.reductionSize, (int, float)):
            raise TypeError('The reductionSize must be a float or int')
            
        #if outputManager.isDebug:
        #    self.addCenterCoords2file() 
            
    def createWalkableAreaBaseKeys(self):
        super().createWalkableAreaBaseKeys()
        for obstacle in self.obstacles2reduce:
            reducedObstacle = obstacle.buffer(-self.reductionSize)
            obstacleDifference = obstacle.difference(reducedObstacle)
            WalkLevelRouteChoiceComputer.performActionOnCellOverlap(obstacleDifference,
                                                                    self.add2WalkableAreaBaseKeysAction,
                                                                    CELL_SIZE)
            
    def add2WalkableAreaBaseKeysAction(self, obstacleDifference, cellPol, xInd, yInd):
        if obstacleDifference.contains(cellPol):
            self.walkableAreaBaseKeys.add((xInd, yInd))
        elif obstacleDifference.intersects(cellPol):
            diffPol = cellPol.difference(obstacleDifference)
            if diffPol.area/CELL_AREA < 0.5:
                self.walkableAreaBaseKeys.add((xInd, yInd))
         
    def createAndFillObsCostMatrix(self):
        self.walkingCostMatrixBase = WalkLevelRouteChoiceComputer.initCostMatrix(self.xCellCount,
                                                                         self.yCellCount,
                                                                         baseValue=self.maxWalkingCost)
        for key in self.walkableAreaBaseKeys:
            self.setValueInCostMatrix(self.walkingCostMatrixBase, key, 1)
    
    def createCostMatrix(self, destination):
        self.logger.debug('Creating the cost matrix using the iterative algorithm')
        self.walkingCostMatrix = WalkLevelRouteChoiceComputer.initCostMatrix(self.xCellCount,
                                                                    self.yCellCount,
                                                                    baseMatrix=self.walkingCostMatrixBase)
        for destinationID, keys in self.virtualObstacleKeys.items():
            if destinationID == destination.ID or destinationID in destination.ID:
                continue
            
             
            for key in keys.union(self.bufferedVirtualObstacleKeys[destinationID]):
                self.setValueInCostMatrix(self.walkingCostMatrix, key, VIRT_OBS_WALKING_COST)
        
        for groupID, keys in self.routingBufferKeys.items():
            if destination.groupID == groupID:
                continue
            
            for key in keys.union(self.routingBufferKeys[groupID]):
                self.setValueInCostMatrix(self.walkingCostMatrix, key, VIRT_OBS_WALKING_COST)
      
        self.costMatrix = WalkLevelRouteChoiceComputer.initCostMatrix(self.xCellCount,            
                                                                    self.yCellCount,
                                                                    baseValue=self.maxWalkingCost)
        donatingKeys = self.setupCostMatrixForDestination()

        constantCost = 2*CELL_SIZE*DEF_DESIRED_SPEED

        self.floodMatrix(donatingKeys, self.costMatrix, self.floodCostFcn, constantCost)

        if self.outputManager.hasDebug:
            self.outputManager.addCostMatrix2save(self.costMatrix, destination.ID, RCMD_INIT_TYPE)

        self.previousError = 1e10
        self.dError = 1e10

        if canUseConvergeCostMatrixCompiledCode and self.useC:
            self.convergeCostMatrixC()
        else:
            self.convergeCostMatrix()
        self.logger.debug('Done creating the cost matrix. It. count = {},  Err. = {}, dErr. = {}'.format(self.iterationCount,
                                                                                                        self.previousError,
                                                                                                        self.dError))
        
        if DEBUG_LRCM:
            self.checkForLocalOptima(destination)
        
        if self.outputManager.hasDebug:
            self.outputManager.addCostMatrix2save(self.costMatrix, destination.ID, RCMD_FINAL_TYPE)
                
    def convergeCostMatrixC(self):
        xCount = self.costMatrix.shape[0]
        yCount = self.costMatrix.shape[1]

        keys = np.array(list(self.walkableAreaKeys), dtype=int).flatten()
        keyCount = int(len(keys)/2)
        keys = (ctypes.c_int * len(keys))(*keys)

        status = ctypes.c_int(0)
        iterationInd = ctypes.c_int(0)
        xIndSave = ctypes.c_int(-100)
        yIndSave = ctypes.c_int(-100)
        indSave = ctypes.c_int(-100)
        self.logger.debug(f"xCount*yCount = {xCount*yCount}")

        previousCostMatrix = np.zeros(self.walkingCostMatrix.shape, dtype=float)

        try:
            convergenceInfo = convergeCostMatrix(self.costMatrix.ctypes.data_as(ctypes.POINTER(ctypes.c_double)),
                                                self.walkingCostMatrix.ctypes.data_as(ctypes.POINTER(ctypes.c_double)),
                                                previousCostMatrix.ctypes.data_as(ctypes.POINTER(ctypes.c_double)),
                                                keys, keyCount, xCount, yCount, self.xOffset, self.yOffset, MAX_CONVERGENCE_INTERATIONS,
                                                ERROR_THRESHOLD, D_ERROR_THRESHOLD, CELL_SIZE,
                                                DEF_DESIRED_SPEED, C3, NU, ETA, DTT, ctypes.byref(status),
                                                ctypes.byref(iterationInd), ctypes.byref(xIndSave),
                                                ctypes.byref(yIndSave), ctypes.byref(indSave))
        except Exception as e:
            self.logger.debug(f"Status = {status}")
            self.logger.debug(f"indSave = {indSave}")
            self.logger.debug(f"iterationInd = {iterationInd}")
            raise e

        self.previousError = convergenceInfo.error
        self.dError = convergenceInfo.dError
        self.iterationCount = convergenceInfo.iterationCount

    def convergeCostMatrix(self):
        self.iterationCount = 0
        previousCostMatrix = None
        while not self.errorIsMinimal(previousCostMatrix) and self.iterationCount <= MAX_CONVERGENCE_INTERATIONS:
            previousCostMatrix = self.doConvergenceStep()
            self.iterationCount += 1

    def doConvergenceStep(self):
        previousCostMatrix = WalkLevelRouteChoiceComputer.initCostMatrix(self.costMatrix.shape[0],
                                                                         self.costMatrix.shape[1],
                                                                         baseMatrix=self.costMatrix)
        for key in self.walkableAreaKeys:
            cost = self.getValueFromCostMatrix(previousCostMatrix, key)
            costDict = self.getCostNeighboringCells(key, previousCostMatrix)
            self.setValueInCostMatrix(self.costMatrix, key, self.calculateNewCost(key, cost, costDict))

        return previousCostMatrix

    def getCostNeighboringCells(self, key, previousCostMatrix):
        offsetsAndLocs = (
            ((0,1), TOP_LOC), # top
            ((1,0), RIGHT_LOC), # right
            ((0,-1), BOTTOM_LOC), # bottom
            ((-1,0), LEFT_LOC), # left
        )

        costDict = {}
        for offset, loc in offsetsAndLocs:
            locKey = (key[0] + offset[0], key[1] + offset[1])
            if locKey in self.walkableAreaKeys:
                costDict[loc] = self.getValueFromCostMatrix(previousCostMatrix, locKey)
            elif (locKey[0] >= self.minKey_x and locKey[0] <= self.maxKey_x) and (locKey[1] >= self.minKey_y and locKey[1] <= self.maxKey_y):
                costDict[loc] = self.getValueFromCostMatrix(previousCostMatrix, locKey)
            else:
                costDict[loc] = self.getValueFromCostMatrix(previousCostMatrix, key)

        return costDict

    def calculateNewCost(self, key, cost, costDict):
        # Calculate the forward and backward differentials
        dx_f = (costDict[RIGHT_LOC] - cost)/CELL_SIZE
        dx_b = (cost - costDict[LEFT_LOC])/CELL_SIZE
        dy_f = (costDict[TOP_LOC] - cost)/CELL_SIZE
        dy_b = (cost - costDict[BOTTOM_LOC])/CELL_SIZE

        # Calculate the central differentials
        d2x = (costDict[RIGHT_LOC] - 2*cost + costDict[LEFT_LOC])/CELL_AREA
        d2y = (costDict[TOP_LOC] - 2*cost + costDict[BOTTOM_LOC])/CELL_AREA

        # Calculate the optimal speeds
        u_x_f = -dx_f/C3 if dx_f < 0 else 0
        u_x_b = dx_b/C3 if dx_b > 0 else 0
        u_y_f = -dy_f/C3 if dy_f < 0 else 0
        u_y_b = dy_b/C3 if dy_b > 0 else 0

        u_tot = max(0.001, sqrt((u_x_f + u_x_b)**2 + (u_y_f + u_y_b)**2 ))

        e_x_f = DEF_DESIRED_SPEED*(u_x_f/u_tot)
        e_x_b = DEF_DESIRED_SPEED*(u_x_b/u_tot)
        e_y_f = DEF_DESIRED_SPEED*(u_y_f/u_tot)
        e_y_b = DEF_DESIRED_SPEED*(u_y_b/u_tot)

        if e_x_f < u_x_f: u_x_f = e_x_f
        if e_x_b < u_x_b: u_x_b = e_x_b
        if e_y_f < u_y_f: u_y_f = e_y_f
        if e_y_b < u_y_b: u_y_b = e_y_b

        h_x_f = 0.5*C3*u_x_f*u_x_f + u_x_f*dx_f
        h_x_b = 0.5*C3*u_x_b*u_x_b - u_x_b*dx_b
        h_y_f = 0.5*C3*u_y_f*u_y_f + u_y_f*dy_f
        h_y_b = 0.5*C3*u_y_b*u_y_b - u_y_b*dy_b

        h_x = min(h_x_f, h_x_b)
        h_y = min(h_y_f, h_y_b)

        walkingCost = self.getValueFromCostMatrix(self.walkingCostMatrix, key)
        return cost + (NU*(d2x + d2y) + walkingCost + h_x + h_y - ETA*cost)*DTT

    def errorIsMinimal(self, previousCostMatrix):
        if previousCostMatrix is None:
            return False

        error = 0
        for key in self.walkableAreaKeys:
            cost = self.getValueFromCostMatrix(self.costMatrix, key)
            previousCost = self.getValueFromCostMatrix(previousCostMatrix, key)
            error += abs(1 - cost/previousCost)

        error = error/len(self.walkableAreaKeys)
        self.dError = 0.8*self.dError + 0.2*abs(error - self.previousError)
        self.previousError = error
        return error < ERROR_THRESHOLD or self.dError < D_ERROR_THRESHOLD

    def checkForLocalOptima(self, destination):
        neighborhood = morphology.generate_binary_structure(len(self.costMatrix.shape),2)    
        local_min = (filters.minimum_filter(self.costMatrix, footprint=neighborhood)==self.costMatrix)
        background = (self.costMatrix==self.maxWalkingCost)
        eroded_background = morphology.binary_erosion(background, structure=neighborhood, border_value=1)
        detected_minima = (local_min ^ eroded_background)  & (self.costMatrix > 0)
        localMinimaIndices = np.where(detected_minima)
        if len(localMinimaIndices[0]) > 0:
            localOptimaCount = len(localMinimaIndices[0])
            for ii in range(localOptimaCount):
                key = (localMinimaIndices[0][ii], localMinimaIndices[1][ii])
                closestObstacles = self.findClosestObstacles((localMinimaIndices[0][ii], localMinimaIndices[1][ii]))
                self.logger.warning(f'Local optimum detected for destination {destination.ID} at {key}. Nearby obstacle(s):')
                for closestObstacle in closestObstacles:
                    self.logger.warning(f'{closestObstacle.ID}')
            
    def findClosestObstacles(self, localOptimum):
        centerCoord = WalkLevelRouteChoiceComputer.getCenterCoord(localOptimum)
        obstacles = []
        distances = []
        
        for obstacle in self.walkLevel.obstacles:
            if isinstance(obstacle, LineObstacle):
                continue
            vec2closestPoint = obstacle.getVec2closestPointOnObstacleNumpy(centerCoord)
            obstacles.append(obstacle)
            distances.append(sqrt(vec2closestPoint[0]*vec2closestPoint[0] + vec2closestPoint[1]*vec2closestPoint[1]))
           
        if len(distances) == 0:
            return []
        if len(distances) == 1:
            return obstacles
            
        distances = np.array(distances)
        sortInd = np.argsort(distances)
        closestObstacles = [obstacles[sortInd[0]]]
        minDist = distances[sortInd[0]]
        for ii in range(1,len(distances)):
            if distances[sortInd[ii]] > minDist + CELL_SIZE:
                break
            closestObstacles.append(obstacles[sortInd[ii]])
        
        return closestObstacles
    
    def addCenterCoords2file(self):
        centerCoordsMatrix = np.zeros((self.xCellCount, self.yCellCount, 2), dtype=float)
        for xInd in range(self.xCellCount):
            for yInd in range(self.yCellCount):
                centerCoord_x, centerCoord_y = self.getCenterCoord((xInd - self.xOffset, yInd - self.yOffset))
                centerCoordsMatrix[xInd, yInd,0] = centerCoord_x
                centerCoordsMatrix[xInd, yInd,1] = centerCoord_y  
             
        self.outputManager.addCenterCoordsMatrix(centerCoordsMatrix)

    def __repr__(self):
        if self.useC:
            return 'WalkLevelRouteChoiceIterativeComputer + C'
        else:
            return 'WalkLevelRouteChoiceIterativeComputer' 


def getDesiredDirectionFromCenter(centerCoord_x, centerCoord_y, geometry):
        nearestPoints = shapely.ops.nearest_points(geometry, Point((centerCoord_x, centerCoord_y)))
        desiredDirection_x = centerCoord_x - nearestPoints[0].coords[0][0] 
        desiredDirection_y = centerCoord_y - nearestPoints[0].coords[0][1]
        vecLength = sqrt((desiredDirection_x*desiredDirection_x) + (desiredDirection_y*desiredDirection_y))
        return (desiredDirection_x/vecLength, desiredDirection_x/vecLength)

#==================================================================================================
#==================================================================================================

def calcDesiredDirection(costDict, normalized):
    xMagnitude = (costDict[TOP_LEFT_LOC] + costDict[LEFT_LOC] + costDict[BOTTOM_LEFT_LOC])/3 - (costDict[TOP_RIGHT_LOC] + costDict[RIGHT_LOC] + costDict[BOTTOM_RIGHT_LOC])/3
    yMagnitude = (costDict[BOTTOM_LEFT_LOC] + costDict[BOTTOM_LOC] + costDict[BOTTOM_RIGHT_LOC])/3 - (costDict[TOP_LEFT_LOC] + costDict[TOP_LOC] + costDict[TOP_RIGHT_LOC])/3

    if normalized:
        vecLength = sqrt((xMagnitude*xMagnitude) + (yMagnitude*yMagnitude))
        return (xMagnitude/vecLength, yMagnitude/vecLength)
    else:
        return (xMagnitude, yMagnitude)


def getConnectedPolygons(geometry, destinations):
    pols = []
    for pol in geometry.geoms:
        for dest in destinations:
            if pol.contains(dest._geometry) or pol.intersects(dest._geometry):
                pols.append(pol) 
                break
    
    if len(pols) == 1:
        geometry = pols[0]
    elif len(pols) > 1: 
        geometry = MultiPolygon(pols)
    else:
        raise Exception('Empty geometry')

    return geometry

def setGeom(destObj, geomObj, walkLevel, destinations):
    for walkableArea in walkLevel.walkableAreas:
        if walkableArea.intersects(geomObj):
            geometry = walkableArea.intersection(geomObj)
            if isinstance(geometry, MultiPolygon):
                geometry = getConnectedPolygons(geometry, destinations)
                              
        shrinkedGeometry = geometry.buffer(-CELL_SIZE, join_style=2)
        if isinstance(shrinkedGeometry, MultiPolygon):
            shrinkedGeometry = getConnectedPolygons(shrinkedGeometry, destinations)
            geometry = shrinkedGeometry.buffer(CELL_SIZE, join_style=2)
            
        destObj._geometry = geometry  

def getCellCounts(walkLevels):
    walkLevels = doListCeck(walkLevels)
    xMax = -inf
    yMax = -inf
    xMin = inf
    yMin = inf
    for walkLevel in walkLevels:
        xMax = max(xMax, walkLevel.maxXcoordinate)
        yMax = max(yMax, walkLevel.maxYcoordinate)
        xMin = min(xMin, walkLevel.origin.x)
        yMin = min(yMin, walkLevel.origin.y)

    xExtent = xMax - xMin
    yExtent = yMax - yMin

    xCellCount = ceil(xExtent/CELL_SIZE) + 1
    yCellCount = ceil(yExtent/CELL_SIZE) + 1

    return xCellCount, yCellCount, xExtent, yExtent

def getMaxExtent(walkLevels):
    xMax = 0
    yMax = 0
    for walkLevel in walkLevels:
        if xMax < walkLevel.maxXcoordinate:
            xMax = walkLevel.maxXcoordinate
        if yMax < walkLevel.maxYcoordinate:
            yMax = walkLevel.maxYcoordinate

    return xMax, yMax

def getOriginOffsets(walkLevel):
    xOffset = floor(walkLevel.origin[0]/CELL_SIZE)
    yOffset = floor(walkLevel.origin[1]/CELL_SIZE)

    return -xOffset, -yOffset


def getDestination2walkLevel(walkLevels):
    destination2walkLevel = {}
    for walkLevel in walkLevels:
        localDestinations2walkLevel = defaultdict(list)
        for destination in walkLevel.destinations:
            if destination.isLocalDestination:
                localDestinations2walkLevel[destination._groupID].append(destination) 
                continue
            
            destination2walkLevel[destination] = walkLevel

        for destinationList in localDestinations2walkLevel.values():
            try:
                globalDest = walkLevel.getObstacle(destinationList[0].groupID)
            except KeyError:
                globalDest = None
            
            destination2walkLevel[(tuple(destinationList), globalDest)] = walkLevel

    return destination2walkLevel

def getIDstrFomIDs(walkLevelID, destinationID):
    return f'{walkLevelID}_{destinationID}'

#==================================================================================================
#==================================================================================================
class RoutingBuffer(ShapelyGeomClassWrapper):
   
    def __init__(self, ID, geometry, groupID):
        super().__init__(ID)
        self._geometry = geometry
        self._groupID = groupID
    
    @property 
    def groupID(self):
        return self._groupID
    

#==================================================================================================
#==================================================================================================
#==================================================================================================