""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from copy import copy
from dataclasses import dataclass

from NOMAD.constants import NAME_FIELD, PREFERRED_SPEED, \
    VALUE_FIELD, TAU, A_0, R_0, A_1, R_1, KAPPA_0, KAPPA_1, A_W, D_SHY, R_INFL, \
    IE_F, IE_B, C_PLUS_0, C_MINUS_0, T_A, RADIUS, NOISE_FACTOR, PARAM_NMS,\
    STATIC_VALUE
from NOMAD.general_functions import StaticValue, Distribution


DEFAULTS = (
    {NAME_FIELD:PREFERRED_SPEED, VALUE_FIELD:1.34},
    {NAME_FIELD:TAU,             VALUE_FIELD:0.25},
    {NAME_FIELD:A_0,             VALUE_FIELD:10},
    {NAME_FIELD:R_0,             VALUE_FIELD:0.15},
    {NAME_FIELD:A_1,             VALUE_FIELD:0.85},
    {NAME_FIELD:R_1,             VALUE_FIELD:0.6},
    {NAME_FIELD:KAPPA_0,         VALUE_FIELD:1e5},
    {NAME_FIELD:KAPPA_1,         VALUE_FIELD:10},
    {NAME_FIELD:A_W,             VALUE_FIELD:10},
    {NAME_FIELD:D_SHY,           VALUE_FIELD:0.2},
    {NAME_FIELD:R_INFL,          VALUE_FIELD:3},
    {NAME_FIELD:IE_F,            VALUE_FIELD:3},
    {NAME_FIELD:IE_B,            VALUE_FIELD:1},
    {NAME_FIELD:C_PLUS_0,        VALUE_FIELD:0.25},
    {NAME_FIELD:C_MINUS_0,       VALUE_FIELD:0.85},
    {NAME_FIELD:T_A,             VALUE_FIELD:0.25},
    {NAME_FIELD:RADIUS,          VALUE_FIELD:0.25},
    {NAME_FIELD:NOISE_FACTOR,    VALUE_FIELD:0.001}
    )

def getDefaultValue(paramName):
    for paramDict in DEFAULTS:
        if paramDict[NAME_FIELD] == paramName:
            return paramDict[VALUE_FIELD]

@dataclass(frozen=True)
class PedParameter():
    name: str
    
    def setValue(self, value):
        super().__setattr__('value', value)
    
    def getDictRepr(self):
        return {'name':self.name} | self.getValueDictRepr()


@dataclass(frozen=True)
class PedStaticParameter(StaticValue, PedParameter):
    pass

@dataclass(frozen=True)
class PedDistrParameter(Distribution, PedParameter):
    pass


class PedParameterSet():
    '''
    classdocs
    '''

    def __init__(self, ID, *args, **kwargs):
        '''
        Constructor
        '''
        self._ID = ID
        self._uniqueCopy = False

        if len(args) and isinstance(args[0], PedParameterSet):
            self._uniqueCopy = True
            baseParamSet = args[0]
            for paramName in PARAM_NMS:
                setattr(self, '_' + paramName, getattr(baseParamSet, '_' + paramName).getValue())
            return

        for paramDict in DEFAULTS:
            name = paramDict[NAME_FIELD] 
            if name in kwargs and kwargs[name] is not None:
                paramDict = kwargs[name]
            else:
                paramDict = copy(paramDict)
                paramDict.pop(NAME_FIELD)
                
            if VALUE_FIELD in paramDict:
                param = PedStaticParameter(name, **paramDict)
            else:
                param = PedDistrParameter(name, **paramDict)
            
            setattr(self, '_' + param.name, param)

    def _getValue(self, paramName):
        if self._uniqueCopy:
            return getattr(self, '_' + paramName)
        else:
            return getattr(self, '_' + paramName).getValue()

    def _setValue(self, paramName, value):
        if self._uniqueCopy:
            setattr(self, '_' + paramName, value)
        else:
            getattr(self, '_' + paramName).setValue(value)


    def getMaxParamValue(self, paramName):
        return getattr(self, '_' + paramName).getMaxValue()

    def getUniqueCopy(self):
        return PedParameterSet(self.ID, self)

    def updateParamValue(self, paramName, value):
        self._setValue(paramName, value)

    def getValueDict(self, paramName):
        return getattr(self, '_' + paramName).getDictRepr()

    @property
    def ID(self):
        return self._ID

    @property
    def preferredSpeed(self):
        return self._getValue(PREFERRED_SPEED)

    @property
    def tau(self):
        return self._getValue(TAU)

    @property
    def a_0(self):
        return self._getValue(A_0)

    @property
    def r_0(self):
        return self._getValue(R_0)

    @property
    def a_1(self):
        return self._getValue(A_1)

    @property
    def r_1(self):
        return self._getValue(R_1)

    @property
    def kappa_0(self):
        return self._getValue(KAPPA_0)

    @property
    def kappa_1(self):
        return self._getValue(KAPPA_1)

    @property
    def a_W(self):
        return self._getValue(A_W)

    @property
    def d_shy(self):
        return self._getValue(D_SHY)

    @property
    def r_infl(self):
        return self._getValue(R_INFL)

    @property
    def ie_f(self):
        return self._getValue(IE_F)

    @property
    def ie_b(self):
        return self._getValue(IE_B)

    @property
    def cPlus_0(self):
        return self._getValue(C_PLUS_0)

    @property
    def cMinus_0(self):
        return self._getValue(C_MINUS_0)

    @property
    def t_A(self):
        return self._getValue(T_A)

    @property
    def radius(self):
        return  self._getValue(RADIUS)

    @property
    def noiseFactor(self):
        return  self._getValue(NOISE_FACTOR)