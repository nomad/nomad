""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from shapely.geometry.linestring import LineString
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.polygon import Polygon


class ShapelyGeomClassWrapper():
    '''
    Wrapper for the shapely geometry classes
    '''
    __slots__ = ['_ID', '_geometry', '_staticBounds']

    def __init__(self, ID):
        '''
        Constructor
        '''
        self._ID = ID
        self._geometry = None
        self._staticBounds = None

    def difference(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.difference(otherObject._geometry)
        else:
            return self._geometry.difference(otherObject)

    def contains(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.contains(otherObject._geometry)
        else:
            return self._geometry.contains(otherObject)

    def intersects(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.intersects(otherObject._geometry)
        else:
            return self._geometry.intersects(otherObject)

    def distance(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.distance(otherObject._geometry)
        else:
            return self._geometry.distance(otherObject)

    def overlaps(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.overlaps(otherObject._geometry)
        else:
            return self._geometry.overlaps(otherObject)

    def within(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.within(otherObject._geometry)
        else:
            return self._geometry.within(otherObject)

    def touches(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.touches(otherObject._geometry)
        else:
            return self._geometry.touches(otherObject)

    def crosses(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.crosses(otherObject._geometry)
        else:
            return self._geometry.crosses(otherObject)

    def intersection(self, otherObject):
        if isinstance(otherObject, ShapelyGeomClassWrapper):
            return self._geometry.intersection(otherObject._geometry)
        else:
            return self._geometry.intersection(otherObject)

    def buffer(self, *args, **kwargs):
        return self._geometry.buffer(*args, **kwargs)

    def simplify(self, *args, **kwargs):
        return self._geometry.simplify(*args, **kwargs)
    
    # These two methods ensure that ID is a static variable which can only be set at init
    @property
    def ID(self):
        return self._ID

    @property
    def geometry(self):
        return self._geometry

    # These two methods ensure that bounds is a static variable which can only be set at init
    @property
    def bounds(self):
        return self._geometry.bounds

    # These two methods ensure that ID is a static variable which can only be set at init
    @property
    def area(self):
        return self._geometry.area
        # These two methods ensure that ID is a static variable which can only be set at init
    @property
    def isempty(self):
        return self._geometry.isempty

    @property
    def exterior(self):
        return self._geometry.exterior

    @property
    def interiors(self):
        return self._geometry.interiors

    @property
    def centroid(self):
        return self._geometry.centroid

    @property
    def xy(self):
        return self._geometry.xy

    @property
    def boundsStatic(self):
        if self._staticBounds is None:
            self._staticBounds = self.bounds
        return self._staticBounds

    @property
    def coords(self):
        coords = []
        if isinstance(self._geometry, MultiPolygon):
            for pol in self._geometry.geoms:
                coords.append(list(pol.exterior.coords[:-1]))
        elif isinstance(self._geometry, Polygon):
            coords = list(self._geometry.exterior.coords[:-1])
        elif isinstance(self._geometry, LineString):
            lineCoords = self._geometry.coords
            coords = [[lineCoords[0][0], lineCoords[1][0]], [lineCoords[0][1], lineCoords[1][1]]]
        else:
            raise Exception('Cannot handle type "{}"'.format(self._geometry.geom_type))

        return coords
