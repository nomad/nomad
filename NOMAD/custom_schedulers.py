""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from types import SimpleNamespace
from xml.dom.minidom import parse

import NOMAD.xml_functions as xml


def createScheduler(configFlNm, destinations, pedParameterSets, sources, activities, walkLevels):
    raise Exception('Implement function!')

class CustomScheduler():
    '''
    classdocs
    '''

    def createDemandPatterns(self):
        raise Exception('Implement function!')
    
    def getDynamicSchedulers(self):
        raise Exception('Implement function!')
    
    
def getInputFromXml(xmlFlNm, elementDefinition):
    DOMTree = parse(str(xmlFlNm))
    schedulerXml = DOMTree.documentElement
    schedulerInputDict = xml.getElementDict(schedulerXml, elementDefinition)
    return SimpleNamespace(**schedulerInputDict)
    