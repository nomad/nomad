""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import ctypes
from dataclasses import dataclass, field
import logging

import NOMAD
from NOMAD.activities import TimeBasedActivity, Waypoint, ActivityPattern, Sink, \
    OutsideOfSimulationActivity, QueueActivity
from NOMAD.constants import IN_COLLISION_STATE, IN_RANGE_STATE, PED_STATIC_STATE, \
                PED_MOVING_STATE, PED_OUTSIDE_OF_SIM_STATE, PED_STATIC_NON_INTERACTING_STATE, \
    PED_FORCE_MAIN, PED_FORCE_DUMMY, ATTR_PED_OPT_CLASS
from NOMAD.grid_manager import OBSTRUCTED, UNOBSTRUCTED
from NOMAD.nomad_pedestrian_parameters import PedParameterSet
from NOMAD.pedestrian import Pedestrian as PedestrianAttr, ATTR_PED_CLASSES, \
    removeObscuredPeds
from NOMAD.vectors import Vector2D
from NOMAD.walk_level import WalkLevel


class PedestrianManager():
    '''
    Base class
    '''

    logger = logging.getLogger(__name__)
    ped_force_fcn_type = PED_FORCE_MAIN
    def __init__(self, timeInfo, parameters):
        '''
        Constructor
        '''
        self.movingPedestrians = set()
        self.staticPedestrians = set() # All pedestrians that are still in the simulation but not active
        self.outOfSimulationPedestrians = set() # All pedestrians that are temporarily outside of the simulation but can reappear again
        self.staticNonInteractingPedestrians = set() # All pedestrians that are still in the simulation but not active or visible to other peds
        self.pedsPerformingTimeBasedActivities = {}
        self._pedIDcouter = 0
        self.pedsAdded = []

        self.timeInfo = timeInfo
        self.parameters = parameters

        self.pedClass = ATTR_PED_CLASSES[parameters.PED_CLASS]
        self.logger.info(f'Using the {parameters.PED_CLASS} pedestrian class')

    def initialize(self):
        self.movingPedestrians = set()
        self.staticPedestrians = set() # All pedestrians that are still in the simulation but not active
        self.outOfSimulationPedestrians = set() # All pedestrians that are temporarily outside of the simulation but can reappear again
        self.staticNonInteractingPedestrians = set() # All pedestrians that are still in the simulation but not active or visible to other peds
        self.pedsPerformingTimeBasedActivities = {}
        self._pedIDcouter = 0
        self.pedsAdded = []

    def addPedestrians(self, peds2add):
        self.pedsAdded = []
        for pedInfo in peds2add:
            ped = self.createPedestrian(self._pedIDcouter, pedInfo, self.timeInfo.currentTime)
            self.pedsAdded.append(ped)
            self._pedIDcouter += 1

        return self.pedsAdded

    def createPedestrian(self, ID, pedInfo, currentTime):
        kwargs = pedInfo.getGroupIDKwarg()
        ped = self.pedClass(ID, pedInfo.paramSet, pedInfo.activityPattern, self.pedClass.ACC_CALC_FCNS[self.parameters.ACC_CALC_FCN], self.pedClass.PED_FORCE_CALC_FCNS[self.ped_force_fcn_type], **kwargs)
        ped.initialize(Vector2D(pedInfo.initialPos), pedInfo.initialWalkLevel, currentTime)
        ped.startTime = self.timeInfo.currentTime
        return ped

    def createPedestrianOutsideOfSimulation(self, pedInfo):
        ped = self.createPedestrian(self._pedIDcouter, pedInfo, self.timeInfo.currentTime)
        self._pedIDcouter += 1
        self.makePedOutsideOfSim(ped)
        return ped

    def removePedestrians(self, peds2Remove):
        prevTime = self.timeInfo.getPreviousTime()
        for ped in peds2Remove:
            ped.gridCell.removePed(ped)
            ped.endTime = prevTime
            ped.activityLog[-1].setEndTime(prevTime)
            self._popFromPedDict(ped)

    def makePedMoving(self, ped):
        self._popFromPedDict(ped)
        self.movingPedestrians.add(ped)
        ped.state = PED_MOVING_STATE

    def makePedStatic(self, ped):
        self._popFromPedDict(ped)
        self.staticPedestrians.add(ped)
        ped.state = PED_STATIC_STATE

    def makePedOutsideOfSim(self, ped):
        self._popFromPedDict(ped)
        self.outOfSimulationPedestrians.add(ped)
        ped.state = PED_OUTSIDE_OF_SIM_STATE

    def makePedStaticNonInteracting(self, ped):
        self._popFromPedDict(ped)
        self.staticNonInteractingPedestrians.add(ped)
        ped.state = PED_STATIC_NON_INTERACTING_STATE

    def _popFromPedDict(self, ped):
        if ped.state == PED_MOVING_STATE:
            self.movingPedestrians.remove(ped)
        elif ped.state == PED_STATIC_STATE:
            self.staticPedestrians.remove(ped)
        elif ped.state == PED_OUTSIDE_OF_SIM_STATE:
            self.outOfSimulationPedestrians.remove(ped)
        elif ped.state == PED_STATIC_NON_INTERACTING_STATE:
            self.staticNonInteractingPedestrians.remove(ped)
        elif ped.state is None:
            return
        else:
            raise Exception(f'Unknown ped state {ped.state}')

    def updatePedsPerformingActivities(self, pedsLeavingTheirActivity):
        pedsLeavingTheirActivity += self.pedsPerformingTimeBasedActivities.get(self.timeInfo.timeInd, [])
        self.pedsPerformingTimeBasedActivities.pop(self.timeInfo.timeInd, None)

        # Collect those that still need to be placed and place them if possible
        peds2placePerDestination = {}
        for ped in pedsLeavingTheirActivity:
            if isinstance(ped.curActivity, OutsideOfSimulationActivity) and ped.state == PED_OUTSIDE_OF_SIM_STATE:
                if ped.curActivity.destination not in peds2placePerDestination:
                    peds2placePerDestination[ped.curActivity.destination] = []
                peds2placePerDestination[ped.curActivity.destination].append(ped)

        for destination, peds2place in peds2placePerDestination.items():
            pedsNotPlaced = destination.setPlacementLocationForPeds(peds2place)
            nextTimeInd = self.timeInfo.getNextTimeInd()
            # Any ped that cannot be placed should be placed next time step if possible
            for ped in pedsNotPlaced:
                pedsLeavingTheirActivity.remove(ped)
                if nextTimeInd not in self.pedsPerformingTimeBasedActivities:
                    self.pedsPerformingTimeBasedActivities[nextTimeInd] = []
                self.pedsPerformingTimeBasedActivities[nextTimeInd].append(ped)

        pedsInteractingAgain = []
        pedsMovingInQueues = []
        for ped in pedsLeavingTheirActivity:
            isInQueue = isinstance(ped.curActivity, QueueActivity)
            queueDelay = ped.curActivity.queueDelay
            queue = ped.curActivity.actionOnLeavingActivity(ped, self.timeInfo.currentTime)
            if queue is not None:
                # Add all peds 2 move at the next step (or with a certain delay)
                nextPedInQueue = queue.getNextPedInQueue(ped)
                if nextPedInQueue is not None:
                    if not isInQueue and queueDelay is not None:
                        nextTimeInd = self.timeInfo.getTimeInd(self.timeInfo.currentTime + queueDelay)
                    else:
                        nextTimeInd = self.timeInfo.getNextTimeInd()
                    if nextTimeInd not in self.pedsPerformingTimeBasedActivities:
                        self.pedsPerformingTimeBasedActivities[nextTimeInd] = []
                    self.pedsPerformingTimeBasedActivities[nextTimeInd].append(nextPedInQueue)

                if not isInQueue:
                    queue.pedLeftMainActivity()

            oldState, _ = self.changePedState(ped, PED_MOVING_STATE)
            if oldState in (PED_OUTSIDE_OF_SIM_STATE, PED_STATIC_NON_INTERACTING_STATE):
                pedsInteractingAgain.append(ped)

        return pedsInteractingAgain, pedsLeavingTheirActivity+pedsMovingInQueues

    def updatePedsReachingTheirActivities(self, pedsThatReachedTheirDest):
        newStaticNonInteractingPeds = []
        newOutsideOfSimulationPeds = []
        newStaticPeds = []
        for ped in pedsThatReachedTheirDest:
            if isinstance(ped.curActivity, Waypoint):
                ped.curActivity.actionOnLeavingActivity(ped, self.timeInfo.currentTime)
                continue
            if isinstance(ped.curActivity, TimeBasedActivity):
                endTimeInd = self.timeInfo.getTimeInd(ped.curActivity.getEndTime(self.timeInfo.currentTime))
                if endTimeInd <= self.timeInfo.timeInd:
                    ped.curActivity.actionOnLeavingActivity(ped, self.timeInfo.currentTime)
                    continue
                hasSubDestination = ped.curActivity.actionOnReachingActivity(ped, self.timeInfo.currentTime)
                if not hasSubDestination:
                    if endTimeInd not in self.pedsPerformingTimeBasedActivities:
                        self.pedsPerformingTimeBasedActivities[endTimeInd] = []
                    self.pedsPerformingTimeBasedActivities[endTimeInd].append(ped)
            else:
                hasSubDestination = ped.curActivity.actionOnReachingActivity(ped, self.timeInfo.currentTime)
            if isinstance(ped.curActivity, Sink):
                continue

            if isinstance(ped.curActivity, QueueActivity):
                if ped.curActivity.canMoveToNextActivity(ped):
                    nextTimeInd = self.timeInfo.getNextTimeInd()
                    if nextTimeInd not in self.pedsPerformingTimeBasedActivities:
                        self.pedsPerformingTimeBasedActivities[nextTimeInd] = []
                    self.pedsPerformingTimeBasedActivities[nextTimeInd].append(ped)
                    continue
                elif ped.curActivity.pedHasWaitingPoint(ped):
                    continue

            if hasSubDestination:
                continue

            _, newState = self.changePedState(ped, ped.curActivity.pedStateWhilstPerformingActivity)
            if newState == PED_OUTSIDE_OF_SIM_STATE:
                newOutsideOfSimulationPeds.append(ped)
            elif newState == PED_STATIC_NON_INTERACTING_STATE:
                newStaticNonInteractingPeds.append(ped)
            elif newState == PED_STATIC_STATE:
                newStaticPeds.append(ped)

        return newStaticNonInteractingPeds, newOutsideOfSimulationPeds, newStaticPeds

    def calcNewPedestrianPositions(self, pedsLeavingTheirActivity):
        self.updatePedestrianIsolationStates(pedsLeavingTheirActivity)

        for ped in self.pedsAdded:  # Because of broadcast
            self.makePedMoving(ped)

        pedsInIsolation, pedsInRange, pedsInCollision = self.getPedsIsolationStateLists()

        self.calcNextPedestrianPositions(pedsInIsolation, pedsInRange, pedsInCollision)

        return self.movingPedestrians

    def updatePedestrianIsolationStates(self, pedsLeavingTheirActivity):
        for ped in self.pedsAdded:
            ped.broadcastIsolation(self.timeInfo.currentTime, self.timeInfo.inIsolationTimeStep, self.parameters)
            PedestrianAttr.updateObsIsolationTime(ped, self.timeInfo.currentTime, self.timeInfo.inIsolationTimeStep, self.parameters)

        for ped in self.movingPedestrians:
            ped.updateIsolationState(self.timeInfo.currentTime, self.timeInfo.inIsolationTimeStep, self.parameters, forceUpdate=ped in pedsLeavingTheirActivity)

    def getPedsIsolationStateLists(self):
        pedsInIsolation = []
        pedsInRange = []
        pedsInCollision = []
        for ped in self.movingPedestrians:
            if ped.pedIsolationState == IN_COLLISION_STATE or ped.obsIsolationState == IN_COLLISION_STATE:
                pedsInCollision.append(ped)
            elif ped.pedIsolationState == IN_RANGE_STATE or ped.obsIsolationState == IN_RANGE_STATE:
                pedsInRange.append(ped)
            else:
                pedsInIsolation.append(ped)

        return pedsInIsolation, pedsInRange, pedsInCollision

    def changePedState(self, ped, newState):
        if ped.state == newState:
            return ped.state, ped.state
        oldState = ped.state
        if newState == PED_MOVING_STATE:
            self.makePedMoving(ped)
        elif newState == PED_STATIC_STATE:
            self.makePedStatic(ped)
        elif newState == PED_OUTSIDE_OF_SIM_STATE:
            self.makePedOutsideOfSim(ped)
        elif newState == PED_STATIC_NON_INTERACTING_STATE:
            self.makePedStaticNonInteracting(ped)
        else:
            raise Exception(f'Unknown ped state {newState}')

        return oldState, ped.state

    def calcNextPedestrianPositions(self, pedsInIsolation, pedsInRange, pedsInCollision):
        self.calcNextPositions(pedsInIsolation, self.timeInfo.inIsolationTimeStep, 1)

        if len(pedsInCollision) == 0 and len(pedsInRange) == 0:
            self.updatePedestrianPositions(pedsInIsolation, True)
            return

        if len(pedsInCollision) == 0:
            for ii in range(1,self.timeInfo.inRangeStepsPerInIsolationStep+1):
                self.calcNextPositions(pedsInRange, self.timeInfo.inRangeTimeStep, self.timeInfo.inRangeStepsPerInIsolationStep)
                self.updatePedestrianPositions(pedsInRange, ii==1)

            self.updatePedestrianPositions(pedsInIsolation, True)
            return

        inRangeStep = self.timeInfo.inRangeStep
        endInd = self.timeInfo.inCollisionStepsPerInIsolationStep
        for stepNr in range(1, endInd+1):
            self.calcNextPositions(pedsInCollision, self.timeInfo.inCollisionTimeStep, self.timeInfo.inCollisionStepsPerInIsolationStep)

            if stepNr > inRangeStep or stepNr == self.timeInfo.inCollisionStepsPerInIsolationStep:
                self.calcNextPositions(pedsInRange, self.timeInfo.inRangeTimeStep, self.timeInfo.inRangeStepsPerInIsolationStep)
                self.updatePedestrianPositions(pedsInRange, stepNr==1)
                inRangeStep += self.timeInfo.inRangeStep
            else:
                for ped in pedsInRange:
                    ped.clearOtherPedListsAndSets()
            self.updatePedestrianPositions(pedsInCollision, stepNr==1)

        self.updatePedestrianPositions(pedsInIsolation, True)

    def calcNextPositions(self, pedestrians, timeStep, timeStepRatio):
        for ped in pedestrians:
            ped.calcNextPosition(timeStep, timeStepRatio, self.parameters)

    def updatePedestrianPositions(self, pedestrians, updatePrev):
        for ped in pedestrians:
            ped.updatePosition(updatePrev)

    def __repr__(self):
        return 'PedestrianManagerAttr with pedClass = {}'.format(self.pedClass.getLabel())

    @property
    def movingPedestriansList(self):
        return list(self.movingPedestrians)

    @property
    def movingPedestriansIterable(self):
        return self.movingPedestrians

    @property
    def staticPedestriansIterable(self):
        return self.staticPedestrians

    @property
    def pedestrianIterable(self):
        return self.movingPedestrians | self.staticPedestrians | \
                self.outOfSimulationPedestrians | self.staticNonInteractingPedestrians

    @property
    def activePedestrianList(self):
        return list(self.movingPedestrians | self.staticPedestrians)

    @property
    def pedestrianList(self):
        return list(self.pedestrianIterable())

# =================================================================
# =================================================================

class PedForcesInput(ctypes.Structure):
    _fields_ = [('pos_x', ctypes.c_double),
                ('pos_y', ctypes.c_double),
                ('vel_x', ctypes.c_double),
                ('vel_y', ctypes.c_double),
                ('radius', ctypes.c_double),
                ('t_A', ctypes.c_double),
                ('kappa_0', ctypes.c_double),
                ('kappa_1', ctypes.c_double),
                ('cPlus_0', ctypes.c_double),
                ('cMinus_0', ctypes.c_double),
                ('a_0', ctypes.c_double),
                ('r_0', ctypes.c_double),
                ('a_1', ctypes.c_double),
                ('r_1', ctypes.c_double),
                ('ie_f_2', ctypes.c_double),
                ('ie_b_2', ctypes.c_double),
                ('r_infl_2', ctypes.c_double),
                ('maxDistBehind', ctypes.c_double),
                ('maxDistInFront', ctypes.c_double),
                ('otherPosArray_x', ctypes.POINTER(ctypes.c_double)),
                ('otherPosArray_y', ctypes.POINTER(ctypes.c_double)),
                ('otherVelArray_x', ctypes.POINTER(ctypes.c_double)),
                ('otherVelArray_y', ctypes.POINTER(ctypes.c_double)),
                ('otherRadiusArray', ctypes.POINTER(ctypes.c_double)),
                ('otherPedCount', ctypes.c_int)]


if NOMAD.canUseCalcAllPedForcesCompiledCode:
    from NOMAD.pedestrian import PedForces
    pedForcesLib = ctypes.CDLL(str(NOMAD.calcAllPedForces_so_file))
    calcPedForces = pedForcesLib.calcAllPedForces
    calcPedForces.restype = ctypes.POINTER(PedForces)
    calcPedForces.argtypes = [ctypes.POINTER(PedForcesInput), ctypes.c_int]

class PedestrianManagerAll(PedestrianManager):

    ped_force_fcn_type = PED_FORCE_DUMMY

    def __init__(self, timeInfo, parameters, *arg, **kwargs):
        super().__init__(timeInfo, parameters, *arg, **kwargs)
        self.pedClass = ATTR_PED_CLASSES[ATTR_PED_OPT_CLASS]

    def calcNextPositions(self, pedestrians, timeStep, timeStepRatio):
        pedsNoPedForce = set()

        pedInputs = []
        for ped in pedestrians:
            pedInput = PedForcesInput()
            pedInput.pos_x = ped.pos_x;
            pedInput.pos_y = ped.pos_y;
            pedInput.vel_x = ped.vel_x;
            pedInput.vel_y = ped.vel_y;
            pedInput.radius = ped.radius;
            pedInput.t_A = ped.t_A;
            pedInput.kappa_0 = ped.kappa_0;
            pedInput.kappa_1 = ped.kappa_1;
            pedInput.cPlus_0 = ped.cPlus_0;
            pedInput.cMinus_0 = ped.cMinus_0;
            pedInput.a_0 = ped.a_0;
            pedInput.r_0 = ped.r_0;
            pedInput.a_1 = ped.a_1;
            pedInput.r_1 = ped.r_1;
            pedInput.ie_f_2 = ped.ie_f_2;
            pedInput.ie_b_2 = ped.ie_b_2;
            pedInput.r_infl_2 = ped.r_infl_2;
            pedInput.maxDistBehind = ped.maxDistBehind;
            pedInput.maxDistInFront = ped.maxDistInFront;

            otherPeds = ped.gridCell.getOtherPedsInCell(ped)
            if ped.gridCell.containsSightBlockingObstacle:
                otherPeds = removeObscuredPeds(otherPeds, ped.gridCell.sightBlockingObstaclesInCell, ped, True)

            pedsInNeighboringCells = ped.gridCell.getPedsInNeighboringCells()
            otherPeds += removeObscuredPeds(pedsInNeighboringCells[OBSTRUCTED], ped.gridCell.obstaclesObstrNeighbors, ped, True)
            otherPeds += pedsInNeighboringCells[UNOBSTRUCTED]

            otherPedCount = len(otherPeds)
            if otherPedCount == 0:
                pedsNoPedForce.add(ped)
                continue

            arrayType = ctypes.c_double*otherPedCount
            pedInput.otherPosArray_x = arrayType(*[otherPed.pos_x for otherPed in otherPeds])
            pedInput.otherPosArray_y = arrayType(*[otherPed.pos_y for otherPed in otherPeds])
            pedInput.otherVelArray_x = arrayType(*[otherPed.vel_x for otherPed in otherPeds])
            pedInput.otherVelArray_y = arrayType(*[otherPed.vel_y for otherPed in otherPeds])
            pedInput.otherRadiusArray = arrayType(*[otherPed.radius for otherPed in otherPeds])
            pedInput.otherPedCount = otherPedCount

            pedInputs.append(pedInput)

        pedInputArray = (PedForcesInput * len(pedInputs))(*pedInputs)
        pedForces = calcPedForces(pedInputArray, len(pedInputArray))

        pedForceInd = 0;
        for ped in pedestrians:
            if ped in pedsNoPedForce:
                ped.updatePedForce(0,0)
            else:
                ped.updatePedForce(pedForces[pedForceInd].x,pedForces[pedForceInd].y)
                pedForceInd += 1
            ped.calcNextPosition(timeStep, timeStepRatio, self.parameters)

@dataclass
class PedCreationInfo():
    paramSet: PedParameterSet
    activityPattern: ActivityPattern
    initialPos: tuple = field(init=False)
    initialWalkLevel: WalkLevel = field(init=False)
    groupID: str = field(init=False)

    def getGroupIDKwarg(self):
        if hasattr(self, 'groupID'):
            return {'groupID':self.groupID}
        else:
            return {}

    def __eq__(self, other):
        sameGroup = True
        if hasattr(self, 'groupID') and hasattr(self, 'groupID'):
            sameGroup = self.groupID == other.groupID
        elif (hasattr(self, 'groupID') and not hasattr(self, 'groupID')) or (not hasattr(self, 'groupID') and hasattr(self, 'groupID')):
            sameGroup = False

        return self.paramSet == other.paramSet and self.activityPattern == other.activityPattern and sameGroup
