""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from dataclasses import dataclass, field
import logging
from pathlib import Path

from NOMAD.constants import OPENTURNS_DISTR_TYPE, NUMPY_DISTR_TYPE, \
    SCIPY_DISTR_TYPE
from NOMAD.general_functions import Distribution, StaticValue
import numpy as np
import openturns as ot    
import scipy.stats as stats

import NOMAD

def hasAttribute(collection, key):
    return collection.hasAttribute(key)

def getAttribute(collection, key):
    return collection.getAttribute(key)

def hasElement(collection, key, recursive=False):
    if recursive:
        return len(collection.getElementsByTagName(key)) > 0

    return len([element for element in collection.getElementsByTagName(key)
                         if element.parentNode == collection]) > 0

def getElementValue(parentElement, key=None, recursive=False):
    if key is not None:
        if recursive:
            elements = parentElement.getElementsByTagName(key)
        else:
            elements = [element for element in parentElement.getElementsByTagName(key)
                         if element.parentNode == parentElement]
        if len(elements) == 0:
            raise Exception('Element is empty')
        if len(elements) > 1:
            raise Exception('Multiple elements')
        element = elements[0]
    else:
        element = parentElement

    return element.childNodes[0].data

def getElements(collection, key):
    return collection.getElementsByTagName(key)

def getSingleElement(collection, key):
    elXmls = collection.getElementsByTagName(key)
    if len(elXmls) != 1:
        if len(collection.childNodes) == 1:
            return collection
        else:
            raise Exception('The configuration should have exactly one {}!'.format(key))
    return elXmls[0]

def getElementDict(collection, elementsInfo, argDict={}):
    # elInfo = ElInfo dataclass
    #
    elDict = {}
    for elInfo in elementsInfo:
        if elInfo.isOptional and not hasElement(collection, elInfo.key):
            continue
        
        if len(elInfo.args) > 0:
            args = []
            for arg in elInfo.args:
                if arg in argDict:
                    args.append(argDict[arg])
                else:
                    args.append(arg)
            args = tuple(args)
        else:
            args = ()

        if len(elInfo.kwargs) > 0:
            kwargs = {}
            for key, value in elInfo.kwargs.items():
                if key in argDict:
                    kwargs[key] = argDict[key]
                else:
                    kwargs[key] = value
        else:
            kwargs = {}

        elDict[elInfo.key] = EL_TYPE_2_FCN[elInfo.type](collection, elInfo.key, *args, **kwargs)

    return elDict

# ===============================================================================

def getString(collection, key=None):
    return str(getElementValue(collection, key))

def getFlNm(collection, key=None, checkExistence=True, pd=None):
    flNm = Path(getElementValue(collection, key))

    if pd is not None:
        pd = Path(pd)
        flNm = pd.joinpath(flNm).resolve()

    if checkExistence and not flNm.is_file():
        raise FileNotFoundError('The {} file "{}" does not exist!'.format(key, flNm))

    return flNm

def getPd(collection, key=None, createIfNonExisting=False, mainPd=None):
    pd = Path(getElementValue(collection, key))
    if mainPd is not None:
        mainPd = Path(mainPd)
        pd = mainPd.joinpath(pd).resolve()

    if createIfNonExisting and not pd.is_dir():
        pd.mkdir(parents=True)
    elif not pd.is_dir():
        raise NotADirectoryError('The {} directory "{}" does not exist!'.format(key, pd))

    return pd

def getFloat(collection, key=None):
    valueStr = getElementValue(collection, key)
    if '*' in valueStr:
        valueStrs = valueStr.split('*')
        value = np.prod([float(valueStr) for valueStr in valueStrs])
    else:
        value = float(valueStr)

    return value

def getInt(collection, key=None):
    valueStr = getElementValue(collection, key)

    return int(valueStr)

def getBoolean(collection, key=None):
    valueStr = getElementValue(collection, key)
    if valueStr.lower() == 'true':
        return True
    else:
        return False

def getFloatTuple(collection, key=None):
    return getTupleEntry(collection, key, entryCastFcn=float)

def getListEntry(collection, key=None, castFcn=None, returnAsTuple=False, isNumerical=False):
    listStr = getElementValue(collection, key)
    listStr = "".join(listStr.split()) # Remove all whitespaces
    if listStr.startswith('('):
        # Remove all whitespace
        if listStr.count('(') != listStr.count(')'):
            raise Exception('Unbalanced parentheses "("={}, ")"={}\nList string:{}'.format(listStr.count('('), listStr.count(')'), listStr))

    listEntry = string2list(listStr, castFcn, returnAsTuple, isNumerical)

    return listEntry

def getCoordinateTupleEntry(collection, key=None, isIntCoords=False):
    # Accepted formats
    # 1,2
    # 1,2,3
    # (1,2), (2,3), ...
    # ((1,2), (2,3), ...)
    # By default the numbers are cast as floats
    castFcn = float
    if isIntCoords:
        castFcn = int

    listStr = getElementValue(collection, key)
    if listStr.startswith('('):
        # Remove all whitespace
        if listStr.count('(') != listStr.count(')'):
            raise Exception('Unbalanced parentheses "("={}, ")"={}\nList string:{}'.format(listStr.count('('), listStr.count(')'), listStr))

        listStr = "".join(listStr.split())
        if listStr.startswith('(('):
            listStr = listStr[2:-2]
        else:
            listStr = listStr[1:-1]

        listEls = listStr.split('),(')
        if len(listEls) == 1:
            coordList = getListElements(listEls[0], castFcn=castFcn)
        else:
            coordList = []
            for listEl in listEls:
                coordList.append(tuple(getListElements(listEl, castFcn=castFcn)))
    else:
        coordList = getListElements(listStr, castFcn=castFcn)

    return tuple(coordList)

def getNumericTuple(collection, key=None, castFcn=None):
    return getTupleEntry(collection, key, castFcn, True)

def getTupleEntry(collection, key=None, castFcn=None, isNumerical=False):
    return getListEntry(collection, key, castFcn, True, isNumerical)

def getLoggerLevel(collection, key=None):
    loggerLevelName = getElementValue(collection, key)
    for key in logging._nameToLevel:
        if loggerLevelName.lower() == key.lower():
            return key

    # Presume this is a custom level name that will be added by the addLevelName
    return loggerLevelName

def getParameter(collection, key=None, raw=False):
    paramValueXml = getSingleElement(collection, key)
    if hasElement(paramValueXml, 'distribution'):
        return getDistribution(paramValueXml, raw=raw)
    else:
        if raw:
            try:
                return getFloat(paramValueXml)
            except:
                return getFloat(paramValueXml, 'staticValue')
        else:
            try:
                return StaticValue(getFloat(paramValueXml))
            except:
                return StaticValue(getFloat(paramValueXml, 'staticValue'))
        
# ----------------------------------------------------------------------------------------------

def getDistribution(collection, key=None, raw=False):
    if key is not None:
        collection = getSingleElement(collection, key)
    if hasElement(collection, 'distribution'):
        distrXml = getSingleElement(collection, 'distribution')
    else:
        distrXml = collection
    distrType = distrXml.getAttribute('distributionType')
    distrName = getDistributionName(distrXml, distrType)
    if hasElement(distrXml, 'distrArgs'):
        distrArgs = getNumericTuple(distrXml, 'distrArgs')
    else:
        distrArgs = ()

    distrInput = {'distrType':distrType, 'distrName':distrName, 'distrArgs':distrArgs}
    distrInput.update(getElementDict(distrXml, DISTR_OPT_ELEMENTS, distrInput))

    if raw:
        return distrInput
    else:
        return Distribution(**distrInput)

def getDistributionName(distrXml, distrType):
    if distrType == NUMPY_DISTR_TYPE:
        return getNumpyDistributionName(distrXml)
    elif distrType == SCIPY_DISTR_TYPE:
        return getScipyDistributionName(distrXml)
    elif  distrType == OPENTURNS_DISTR_TYPE:
        return getOtDistributionName(distrXml)
    else:
        raise Exception(f'Unknown distribution type "{distrType}"!')

def getOtDistributionName(collection, _=None):
    distrName = getElementValue(collection, 'distrName')
    
    if not hasattr(ot, distrName):
        raise Exception('The distribution type "{}" is not known by openturns!'.format(distrName))

    return distrName

def getScipyDistributionName(collection, _=None):
    distrName = getElementValue(collection, 'distrName')
    
    if not hasattr(stats, distrName):
        raise Exception('The distribution type "{}" is not known by scipy!'.format(distrName))

    return distrName

def getNumpyDistributionName(collection, _=None):
    distrName = getElementValue(collection, 'distrName')

    if not hasattr(NOMAD.NOMAD_RNG, distrName):
        raise Exception('The distribution type "{}" is not known by numpy!'.format(distrName))

    return distrName

# ===============================================================================

@dataclass(frozen=True)
class ElInfo():
    key: str
    type: str
    isOptional: bool = False
    args: tuple = ()
    kwargs: dict = field(default_factory=dict)

    def __post_init__(self):
        if self.type not in EL_TYPE_2_FCN:
            raise Exception('The type "{}" is not recognized!'.format(self.type))

STRING_TYPE = 'str'
FLOAT_TYPE = 'float'
INT_TYPE = 'int'
FLNM_TYPE = 'flNm'
PD_TYPE = 'pd'
BOOL_TYPE = 'bool'
LIST_TYPE = 'list'
TUPLE_TYPE = 'tuple'
COORD_TUPLE_TYPE = 'coordTuple'
NUMERIC_TUPLE_TYPE = 'numericTuple'
NUMPY_DISTR_NM_TYPE = 'numpyDistrNm'
LOGGER_LEVEL_TYPE = 'loggerLevel'
PARAM_TYPE = 'parameter'
DISTR_TYPE = 'distribution'
XML_SUB_PART = 'subPart'

EL_TYPE_2_FCN = {
    STRING_TYPE: getString,
    FLOAT_TYPE: getFloat,
    INT_TYPE: getInt,
    FLNM_TYPE: getFlNm,
    PD_TYPE: getPd,
    BOOL_TYPE: getBoolean,
    LIST_TYPE: getListEntry,
    TUPLE_TYPE: getTupleEntry,
    COORD_TUPLE_TYPE: getCoordinateTupleEntry,
    NUMERIC_TUPLE_TYPE: getNumericTuple,
    NUMPY_DISTR_NM_TYPE: getNumpyDistributionName,
    LOGGER_LEVEL_TYPE: getLoggerLevel,
    PARAM_TYPE: getParameter,
    DISTR_TYPE: getDistribution,
    XML_SUB_PART: getSingleElement
    }
DISTR_OPT_ELEMENTS = (    
    ElInfo('lowerTrunc', FLOAT_TYPE, isOptional=True),
    ElInfo('upperTrunc', FLOAT_TYPE, isOptional=True),
    ElInfo('min', FLOAT_TYPE, isOptional=True),
    ElInfo('max', FLOAT_TYPE, isOptional=True)
    )

# ===============================================================================

def string2list(listStr, castFcn=None, returnAsTuple=False, isNumerical=False):
    if listStr.startswith('(') and listStr.endswith(')'):
        listStr = listStr[1:-1]

    startInd = 0
    entryList = []
    while startInd < len(listStr):
        if listStr[startInd] == '(':
            endInd = listStr.find(')', startInd) + 1
            while listStr[startInd:endInd].count('(') != listStr[startInd:endInd].count(')'):
                endInd = listStr.find(')', endInd) + 1

            entryList.append(string2list(listStr[startInd+1:endInd-1], castFcn, returnAsTuple, isNumerical))
        else:
            endInd = listStr.find(',', startInd)
            if endInd == -1:
                endInd = len(listStr)
            value = listStr[startInd:endInd]
            if isNumerical:
                value = castNumericalEntry(value, castFcn)
            elif castFcn is not None:
                value = castFcn(value)

            entryList.append(value)
        startInd = endInd + 1

    if returnAsTuple:
        entryList = tuple(entryList)
    return entryList

def getListElements(listElStr, delimiter=',', castFcn=None):
    listEntries = [x.strip() for x in listElStr.split(delimiter)]
    if castFcn is not None:
        listEntries = castListEntries(listEntries, castFcn)

    return listEntries

def castListEntries(listEntry, entryCastFcn):
    for ii in range(len(listEntry)):
        if isinstance(listEntry[ii], list):
            listEntry[ii] = castListEntries(listEntry[ii], entryCastFcn)
        else:
            listEntry[ii] = entryCastFcn(listEntry[ii])

    return listEntry

def castNumericalEntry(entryString, castFcn=None):
    if castFcn is not None:
        return castFcn(entryString)
    elif entryString.isdecimal():
        return int(entryString)
    elif '*' in entryString:
        entryStrs = entryString.split('*')
        return np.prod([float(entryStr) for entryStr in entryStrs])
    else:
        return float(entryString)
