""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging
from math import ceil, floor, inf, sqrt

from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.point import Point
from shapely.geometry.polygon import Polygon

from NOMAD.activities import LineDestination, PointDestination
from NOMAD.general_functions import doListCeck
from NOMAD.obstacles import Obstacle
from NOMAD.vectors import Vector2D
from NOMAD.constants import PED_MOVING_STATE, PED_OUTSIDE_OF_SIM_STATE

logger = logging.getLogger(__name__)

def getGridCellID(ped, cellSize):
    gridCellIndex = getIndexFromPos(ped.pos, cellSize)
    return (gridCellIndex[0], gridCellIndex[1], ped.walkLevel.ID)

def getIndexFromPos(pos, cellSize):
    return getIndex(pos.x, pos.y, cellSize)
    #return getIndex(pos.x, pos.y, cellSize)

def getIndexFromArray(posArray, cellSize):
    return getIndex(posArray[0], posArray[1], cellSize)

def getIndex(x, y, cellSize):
    return (floor(x/cellSize), floor(y/cellSize))

def getCoordinate(xInd, yInd, cellSize):
    return (xInd*cellSize, yInd*cellSize)

class GridManager():
    '''
    Manages a grid that starts at (0,0) and presumes that all coordinates in the system are always positive
    '''

    CELL_SIZE = 1
    MAX_OBS_DIST = 0.5 # presuming a shy distance of 0.2 and a radius of 0.3
    MAX_NEIGHBOR_DIST = 6 #Presuming ie_f>=ie_b and ie_f <= 6 meter
    MAX_OBS_OVERLAP = 0.99 #


    def __init__(self, walkLevels, maxPedDistPerStep, cellSize=CELL_SIZE, maxObstacleDist=MAX_OBS_DIST, maxNeighborDistance=MAX_NEIGHBOR_DIST, createCells=True):
        '''
        Constructor
        '''
        self._cellSize = cellSize
        self._maxObstacleDist = maxObstacleDist
        self._maxNeighborDistance = maxNeighborDistance

        self._gridCells = {} # key = (xInd, yInd, walkLevelID), value = GridCell instance
        self._gridCellsWithDestinations = []
        self._gridCellsNearConnectors = [] # All gridCells which either contains (part of a) connector or are maxSpeed/timeStep removed from one taking into account walkable space

        walkLevels = doListCeck(walkLevels)

        self._xCellCount = None
        self._yCellCount = None
        self.calcExtent(walkLevels)
        if createCells:
            self.createCells(walkLevels, maxPedDistPerStep)

    @property
    def exportDict(self):
        return {
            "cellSize":self._cellSize,
            "maxObstacleDist":self._maxObstacleDist,
            "maxNeighborDistance":self._maxNeighborDistance,
            "gridCells": [gridCell.exportDict for gridCell in self._gridCells.values()]
            }
       
    #--------------------------------------------------------------------------------
    def getGridCellsCoveringPolygon(self, polygon, walkLevelID):
        cellsCoveringPolygon = []

        xStart, yStart = getIndex(polygon.bounds[0], polygon.bounds[1], self._cellSize)
        xEnd, yEnd = getIndex(polygon.bounds[2], polygon.bounds[3], self._cellSize)

        for xInd in range(xStart, xEnd + 1):
            for yInd in range(yStart, yEnd + 1):
                gridCell = self._gridCells.get((xInd, yInd, walkLevelID), None)
                if gridCell is not None and gridCell.isCellCoveringPolygon(polygon):
                    cellsCoveringPolygon.append(gridCell)

        return cellsCoveringPolygon

    #--------------------------------------------------------------------------------

    def updateGrid(self, movingPedestrians, staticPedestrians):
        if len(movingPedestrians) == 0:
            return

        for gridCell in self._gridCells.values():
            gridCell.resetGridCell()

        pedsThatPassedConnector = []
        for gridCell in self._gridCellsNearConnectors:
            pedsThatPassedConnector += gridCell.getPedsThatPassedConnectors()

        self.updatePedsWalkLevel(pedsThatPassedConnector)

        for ped in movingPedestrians:
            self.updatePedGridCell(ped)
            
        for ped in staticPedestrians:
            ped.gridCell.addPed(ped)

    def updatePedsWalkLevel(self, pedsThatPassedConnector):
        for ped, connector in pedsThatPassedConnector:  # @UnusedVariable
            # Call connector and ask to which direction the ped crossed it and thus which new walkLevel the ped should get
            pass

    def updatePedGridCell(self, ped):
        gridCellID = getGridCellID(ped, self._cellSize)
        gridCell = self._gridCells.get(gridCellID, None)
        if gridCell is None:
            raise KeyError('No gridCell exists with the index ({},{},{})'.format(*gridCellID))
        gridCell.pedsInCell.append(ped)
        ped.gridCell = gridCell

    def getPedsThatReachedDestination(self):
        pedsThatReachedTheirDest = []
        for gridCell in self._gridCellsWithDestinations:
            gridCell.getPedsThatReachedDestination(pedsThatReachedTheirDest)

        return pedsThatReachedTheirDest

    def getPedsInNeighboringCells(self, cellInd):
        return self._gridCells[cellInd].getPedsInNeighboringCells()

    def removePedFromCell(self, ped):
        ped.gridCell.removePed(ped)
        if ped.state == PED_OUTSIDE_OF_SIM_STATE:
            ped.gridCell = None

    def addPedToCell(self, ped):
        ped.gridCell.addPed(ped)

    #--------------------------------------------------------------------------------

    def calcExtent(self, walkLevels):
        xMax = -inf
        yMax = -inf
        xMin = inf
        yMin = inf
        for walkLevel in walkLevels:
            xMax = max(xMax, walkLevel.maxXcoordinate)
            yMax = max(yMax, walkLevel.maxYcoordinate)
            xMin = min(xMin, walkLevel.origin.x)
            yMin = min(yMin, walkLevel.origin.y)

        self._xCellCount = ceil((xMax - xMin)/self._cellSize) + 1
        self._yCellCount = ceil((yMax - yMin)/self._cellSize) + 1

    def createCells(self, walkLevels, maxPedDistPerStep):
        for walkLevel in walkLevels:
            self.createCellsForWalkLevel(walkLevel, maxPedDistPerStep)
            # Connect the cells

    def createCellsForWalkLevel(self, walkLevel, maxPedDistPerStep):
        logger.debug("Creating cells for walklevel %s", walkLevel.ID)
        for walkableArea in walkLevel.walkableAreas:
            xStart, yStart = getIndexFromArray(walkableArea.bounds[:2], self._cellSize)
            xEnd, yEnd = getIndexFromArray(walkableArea.bounds[2:], self._cellSize)
            for xInd in range(xStart, xEnd + 1):
                for yInd in range(yStart, yEnd + 1):
                    llCoord = getCoordinate(xInd, yInd, self._cellSize)
                    cellPol = Polygon((llCoord, (llCoord[0] + self._cellSize, llCoord[1]), (llCoord[0] + self._cellSize, llCoord[1]+self._cellSize), (llCoord[0], llCoord[1]+self._cellSize)))
                    if self.cellCoversWalkableArea(xInd, yInd, cellPol, walkableArea):
                        gridCellID = (xInd, yInd, walkLevel.ID)
                        gridCell = GridCell(gridCellID, cellPol)
                        GridManager.getObstaclesNearCell(gridCell, walkLevel, self._cellSize, self._maxObstacleDist, cellPol)
                        self._gridCells[gridCellID] = gridCell
                        if self.checkIfCellIsCoveringDestination(gridCell, walkLevel, maxPedDistPerStep):
                            self._gridCellsWithDestinations.append(gridCell)
        logger.debug("Adding neighbours for cells of walklevel %s", walkLevel.ID)
        self.addNeighbors2cells(walkLevel)

    def checkIfCellIsCoveringDestination(self, gridCell, walkLevel, maxPedDistPerStep):
        intersectAtLeastOnce = False
        for destination in walkLevel._destinations:
            intersects = gridCell.polygon.intersects(destination.geometry)
            if intersects:
                diff = gridCell.polygon.difference(destination.geometry)
                if diff.area/gridCell.polygon.area >= self.MAX_OBS_OVERLAP:
                    intersects = False

            if intersects:
                gridCell.destinations[destination.ID] = destination
            intersectAtLeastOnce = intersectAtLeastOnce or intersects

            if not intersects and isinstance(destination, (LineDestination, PointDestination)):
                if destination.distance(gridCell.polygon) <= maxPedDistPerStep*1.05:
                    gridCell.destinations[destination.ID] = destination
                    intersectAtLeastOnce = True

        return intersectAtLeastOnce

    def cellCoversWalkableArea(self, xInd, yInd, cellPol, walkableArea):
        exists = (xInd, yInd) in self._gridCells
        intersects = cellPol.intersects(walkableArea.geometry)
        if intersects:
            diff = cellPol.difference(walkableArea.geometry)
            if diff.area/cellPol.area >= self.MAX_OBS_OVERLAP:
                intersects = False

        return not exists and intersects

    @staticmethod
    def getObstaclesNearCell(gridCell, walkLevel, cellSize, maxObstacleDist, cellPol):
        minDist = inf
        dist2obstacles = {}
        for obstacle in walkLevel.obstacles:
            dist = cellPol.distance(obstacle.geometry)
            if dist == 0 and obstacle.overlaps(cellPol):
                gridCell._containsObstacle = True
                gridCell.obstaclesInCell.append(obstacle)
                if not obstacle.isSeeThrough:
                    gridCell._containsSightBlockingObstacle = True
                    gridCell.sightBlockingObstaclesInCell.append(obstacle)

            dist2obstacles[obstacle] = dist
            if dist <= minDist:
                minDist = dist

            if dist <= maxObstacleDist:
                gridCell.obstaclesNear.append(obstacle)

        cutOffDist = minDist + sqrt(2*cellSize**2)
        for obstacle, dist2obs in dist2obstacles.items():
            if dist2obs <= cutOffDist:
                gridCell.closestObstacles.append(obstacle)

    def addNeighbors2cells(self, walkLevel):
        for gridCell in self._gridCells.values():
            self.getCellNeighbors(gridCell, walkLevel)

    def getCellNeighbors(self, gridCell, walkLevel):
        newCellsAdded = True
        offsetDistance = 1
        while newCellsAdded:
            curNeighborCnt = len(gridCell.obstructedNeighboringCells) + len(gridCell.unobstructedNeighboringCells)
            GridManager.loopOverRingOfNeigboringCells(gridCell, offsetDistance, walkLevel.ID, self.addNeighbor, gridCell, walkLevel)

            offsetDistance += 1
            newCellsAdded = curNeighborCnt < (len(gridCell.obstructedNeighboringCells) + len(gridCell.unobstructedNeighboringCells))

    def addNeighbor(self, neighborKey, gridCell, walkLevel):
        if neighborKey not in self._gridCells:
            return

        if neighborKey in gridCell.unobstructedNeighboringCells or neighborKey in gridCell.obstructedNeighboringCells:
            return

        if gridCell.polygon.distance(self._gridCells[neighborKey].polygon) <= self._maxNeighborDistance:
            neighbor = self._gridCells[neighborKey]
            obstructingObs, isFullyObstructed = GridManager.getObstaclesObstructingNeigbor(gridCell, neighbor, walkLevel)
            if isFullyObstructed:
                gridCell.fullyObstructedNeighboringCells[neighborKey] = neighbor
                neighbor.fullyObstructedNeighboringCells[gridCell.gridIndex] = gridCell
            elif len(obstructingObs) > 0:
                gridCell.obstructedNeighboringCells[neighborKey] = neighbor
                neighbor.obstructedNeighboringCells[gridCell.gridIndex] = gridCell
                gridCell._obstaclesObstrNeighbors = gridCell._obstaclesObstrNeighbors | obstructingObs
                neighbor._obstaclesObstrNeighbors = neighbor._obstaclesObstrNeighbors | obstructingObs
            else:
                gridCell.unobstructedNeighboringCells[neighborKey] = neighbor
                neighbor.unobstructedNeighboringCells[gridCell.gridIndex] = gridCell

    @staticmethod
    def loopOverRingOfNeigboringCells(gridCell, ringOffset, walkLevelID, fcn2perform, *args):
            extent = 2*ringOffset
            offset = [1*ringOffset, 1*ringOffset] # Start at top left corner
            neighborKey = GridManager.getNeighborKey(gridCell, offset, walkLevelID)
            fcn2perform(neighborKey, *args)
            for _ in range(extent): # Go down
                offset[1] -= 1
                neighborKey = GridManager.getNeighborKey(gridCell, offset, walkLevelID)
                fcn2perform(neighborKey, *args)

            for _ in range(extent): # Go left
                offset[0] -= 1
                neighborKey = GridManager.getNeighborKey(gridCell, offset, walkLevelID)
                fcn2perform(neighborKey, *args)

            for _ in range(extent): # Go up
                offset[1] += 1
                neighborKey = GridManager.getNeighborKey(gridCell, offset, walkLevelID)
                fcn2perform(neighborKey, *args)

            for _ in range(extent-1): # Go right
                offset[0] += 1
                neighborKey = GridManager.getNeighborKey(gridCell, offset, walkLevelID)
                fcn2perform(neighborKey, *args)

    @staticmethod
    def getNeighborKey(gridCell, offset, walkLevelID):
        return (gridCell.gridIndex[0] + offset[0], gridCell.gridIndex[1] + offset[1], walkLevelID)

    @staticmethod
    def getObstaclesObstructingNeigbor(gridCell, neighbor, walkLevel):
        obstructingObs = set()
        cellAndNeighborExtent = GridManager.getCellAndNeighborExtent(gridCell, neighbor)

        obstaclesInNeighborhood = []
        for obstacle in walkLevel.obstacles:
            if obstacle.seeThrough or not GridManager.isObstacleInNeighborhood(cellAndNeighborExtent, obstacle): 
                continue
            obstaclesInNeighborhood.append(obstacle)

        if len(obstaclesInNeighborhood) == 0:
            return obstructingObs, False

        for obstacle in obstaclesInNeighborhood:
            if obstacle.contains(gridCell.polygon) or obstacle.contains(neighbor.polygon):
                return obstructingObs, True

        cellPol = gridCell.polygon
        neighborCellPol = neighbor.polygon

        for obstacle in obstaclesInNeighborhood:
            cellPol = cellPol.difference(obstacle.geometry)
            neighborCellPol = neighborCellPol.difference(obstacle.geometry)

        if cellPol.is_empty or neighborCellPol.is_empty:
            return obstructingObs, True

        if isinstance(cellPol, MultiPolygon):
            cellPols = cellPol.geoms
        else:
            cellPols = [cellPol]

        if isinstance(neighborCellPol, MultiPolygon):
            neighborCellPols = neighborCellPol.geoms
        else:
            neighborCellPols = [neighborCellPol]

        nonFullyObstructedPairs = 0

        for cellPolPart in cellPols:
            for neighborCellPart in neighborCellPols:
                anyObsFullyObstructs = False
                for obstacle in obstaclesInNeighborhood:
                    obstructedSightLineCountPart, unobstructedSightLineCountPart = GridManager.getSightLineCounts(cellPolPart, neighborCellPart, obstacle)

                    if unobstructedSightLineCountPart > 0 and obstructedSightLineCountPart > 0:
                        obstructingObs.add(obstacle)
                    if unobstructedSightLineCountPart == 0:
                        anyObsFullyObstructs = True
                        obstructingObs.add(obstacle)
                        
                if not anyObsFullyObstructs:
                    nonFullyObstructedPairs += 1

        ifFullyObstructed = nonFullyObstructedPairs == 0

        if ifFullyObstructed:
            return set(), True

        return obstructingObs, False

    @staticmethod
    def getCellAndNeighborExtent(gridCell, neighbor):
        min_x = min(gridCell.boundsStatic[0], neighbor.boundsStatic[0])
        min_y = min(gridCell.boundsStatic[1], neighbor.boundsStatic[1])
        max_x = max(gridCell.boundsStatic[2], neighbor.boundsStatic[2])
        max_y = max(gridCell.boundsStatic[3], neighbor.boundsStatic[3])

        return (min_x, min_y, max_x, max_y)

    CORNER_INDICES = (
        (0,1),
        (2,1),
        (2,3),
        (0,3)
        )

    @staticmethod
    def isObstacleInNeighborhood(cellAndNeighborExtent, obstacle):
        #for cornerIndex in GridManager.CORNER_INDICES:
        #    if obstacle.boundsStatic[cornerIndex[0]] >= cellAndNeighborExtent[0] and obstacle.boundsStatic[cornerIndex[0]] <= cellAndNeighborExtent[2] \
        #    and  obstacle.boundsStatic[cornerIndex[1]] >= cellAndNeighborExtent[1] and obstacle.boundsStatic[cornerIndex[1]] <= cellAndNeighborExtent[3]:
        #        return True
        extentPol = Polygon((cellAndNeighborExtent[:2], (cellAndNeighborExtent[2], cellAndNeighborExtent[1]), cellAndNeighborExtent[2:], (cellAndNeighborExtent[0], cellAndNeighborExtent[3])))
        return extentPol.contains(obstacle.geometry) or extentPol.overlaps(obstacle.geometry)

    @staticmethod
    def getSightLineCounts(cellPol, neighborPol, obstacle):
        obstructedSightLineCount = 0
        unobstructedSightLineCount = 0
        for cellCoord in cellPol.exterior.coords:
            for neighborCoord in neighborPol.exterior.coords:
                if cellCoord[0] == neighborCoord[0] and cellCoord[1] == neighborCoord[1]:
                    unobstructedSightLineCount += 1
                    continue
                sightLine = Obstacle.getSightLine((cellCoord, neighborCoord))
                if sightLine.within(obstacle.geometry):
                    obstructedSightLineCount += 1
                    continue
                if obstacle.isSightLineIntersectingObstacle((cellCoord, neighborCoord)):
                    obstructedSightLineCount += 1
                else:
                    unobstructedSightLineCount += 1

        return obstructedSightLineCount, unobstructedSightLineCount

    @staticmethod
    def isCellPolNeighborObstructed(cellPol, neighbor, obstacle):
        for cellCoord in cellPol.exterior.coords:
            for neighborCoord in neighbor.polygon.exterior.coords:
                if cellCoord[0] == neighborCoord[0] and cellCoord[1] == neighborCoord[1]:
                    continue
                sightLine = Obstacle.getSightLine((cellCoord, neighborCoord))
                if sightLine.within(obstacle.geometry):
                    return False
                if obstacle.isSightLineIntersectingObstacle((cellCoord, neighborCoord)):
                    return True

        return False

UNOBSTRUCTED = 'Unobstructed'
OBSTRUCTED = 'obstructed'

# ================================================================================================
# ================================================================================================

GRID_CELL_EXPORT_PATTERN = (
    "gridIndex",
    "coordinates",
    "unobstructedNeighboringCellsIds",
    "obstructedNeighboringCellsIds",
    "fullyObstructedNeighboringCellsIds",
    "obstaclesInCellIds",
    "sightBlockingObstaclesInCellIds",
    "obstaclesNearIds",
    "obstaclesObstrNeighborIds",
    "closestObstacleIds",
    "connectorIds",
    "destinationIds"
)

class GridCell():
    '''
    Manages a grid that starts at (0,0) and presumes that all coordinates in the system are always positive
    '''

    __slots__ = ['_gridIndex', '_polygon', '_pedsInCell', '_unobstructedNeighboringCells', '_obstructedNeighboringCells',
                 '_pedsInUnobstructedCells', '_pedsInObstructedCells', '_obstaclesNear', '_obstaclesObstrNeighbors',
                 '_destinations', '_closestObstacles', '_connectors', '_closestPed', '_closestPedDist',
                 '_containsObstacle', '_staticBounds', '_fullyObstructedNeighboringCells', '_obstaclesInCell', 
                 '_sightBlockingObstaclesInCell', '_containsSightBlockingObstacle']

    def __init__(self, gridIndex, polygon):
        '''
        Constructor
        '''
        self._gridIndex = gridIndex
        self._polygon = polygon
        self._staticBounds = polygon.bounds
        self._pedsInCell = []
        self._unobstructedNeighboringCells = {} # All neighboring cells with complete free sight lines
        self._obstructedNeighboringCells = {}  # All neighboring cells with possible obstructed sight lines
        self._fullyObstructedNeighboringCells = {}  # All neighboring cells with completely obstructed sight lines
        self._pedsInUnobstructedCells = None
        self._pedsInObstructedCells = None
        self._obstaclesInCell = []
        self._sightBlockingObstaclesInCell = []
        self._obstaclesNear = []
        self._obstaclesObstrNeighbors = set()
        self._destinations = {}
        self._closestObstacles = []
        self._connectors = []
        self._containsObstacle = False
        self._containsSightBlockingObstacle = False
        self._closestPed = None
        self._closestPedDist = None

    @property
    def exportDict(self):
        return {key: getattr(self, key) for key in GRID_CELL_EXPORT_PATTERN}

    def resetGridCell(self):
        self._pedsInUnobstructedCells = None
        self._pedsInObstructedCells = None
        self._pedsInCell = []

    def removePed(self, ped):
        self._pedsInCell.remove(ped)

    def addPed(self, ped):
        self._pedsInCell.append(ped)

    def getPedsThatPassedConnectors(self):
        # Return the a list of tuples of (ped, connector) of peds who passed one of the connectors
        # It's assumed that a connector is a line and that a ped passes this line if the line connecting the previous pos
        # and the current pos intersects this line
        pass

    def getPedsThatReachedDestination(self, pedsThatReachedTheirDest):
        for ped in self._pedsInCell:
            if ped.state != PED_MOVING_STATE:
                continue
            destination = self._destinations.get(ped.destinationID, None)
            if destination is not None and destination.hasBeenReachedByPed(ped):
                pedsThatReachedTheirDest.append(ped)

    def getOtherPedsInCell(self, ped):
        return [otherPed for otherPed in self._pedsInCell if otherPed != ped]

    def getPedsInNeighboringCells(self):
        if self.pedsInUnobstructedCells is None:
            self._pedsInUnobstructedCells = GridCell.collectPedsInNeighboringCells(self.unobstructedNeighboringCells)
            self._pedsInObstructedCells = GridCell.collectPedsInNeighboringCells(self.obstructedNeighboringCells)

        return {UNOBSTRUCTED:self._pedsInUnobstructedCells, OBSTRUCTED:self._pedsInObstructedCells}

    def getClosestPedDist(self, ped, maxOffset):
        self._closestPed = None
        self._closestPedDist = inf
        offset = 1

        self.findClosestPed(self.pedsInCell, ped)
        while (self._closestPed is None or offset == 1) and offset <= maxOffset:
            GridManager.loopOverRingOfNeigboringCells(self, offset, self.walkLevelID, self.findClosestPedInNeighboringCell, ped)
            offset += 1

        return self._closestPedDist, self._closestPed

    def getPedsInRange(self, ped):
        peds = self.getOtherPedsInCell(ped)
        peds += GridCell.collectPedsInNeighboringCells(self.unobstructedNeighboringCells)
        peds += GridCell.collectPedsInNeighboringCells(self.obstructedNeighboringCells)
        return peds

    def findClosestPedInNeighboringCell(self, neighborKey, ped):
        if neighborKey in self.unobstructedNeighboringCells:
            self.findClosestPed(self.unobstructedNeighboringCells[neighborKey].pedsInCell, ped)
        elif neighborKey in self.obstructedNeighboringCells:
            self.findClosestPed(self.obstructedNeighboringCells[neighborKey].pedsInCell, ped)

    def findClosestPed(self, pedestrians, ped):
        for otherPed in pedestrians:
            if otherPed == ped:
                continue
            dist = Vector2D.distance(ped.pos, otherPed.pos)
            if dist < self._closestPedDist:
                self._closestPedDist = dist
                self._closestPed = otherPed

    def getClosestObsDist(self, ped):
        minDist = inf
        pedPoint = None
        for obs in self._closestObstacles:
            if pedPoint is None:
                pedPoint = Point(ped.pos)
            dist = obs.distance(pedPoint)
            if dist < minDist:
                    minDist = dist

        return minDist

    def isCellCoveringPolygon(self, polygon):
        return self._polygon.within(polygon) or self._polygon.intersects(polygon)

    @staticmethod
    def collectPedsInNeighboringCells(neighboringCells):
        peds = []
        for cell in neighboringCells.values():
            peds += cell._pedsInCell

        return peds

    # These two methods ensure that index is a static variable which can only be set at init
    @property
    def gridIndex(self):
        return self._gridIndex

    @property
    def walkLevelID(self):
        return self._gridIndex[2]

    @property
    def polygon(self):
        return self._polygon

    @property
    def pedsInCell(self):
        return self._pedsInCell

    @property
    def unobstructedNeighboringCells(self):
        return self._unobstructedNeighboringCells

    @property
    def obstructedNeighboringCells(self):
        return self._obstructedNeighboringCells

    @property
    def fullyObstructedNeighboringCells(self):
        return self._fullyObstructedNeighboringCells

    @property
    def pedsInUnobstructedCells(self):
        return self._pedsInUnobstructedCells

    @property
    def pedsInObstructedCells(self):
        return self._pedsInObstructedCells

    @property
    def obstaclesNear(self):
        return self._obstaclesNear

    @property
    def obstaclesInCell(self):
        return self._obstaclesInCell

    @property
    def sightBlockingObstaclesInCell(self):
        return self._sightBlockingObstaclesInCell
    
    @property
    def obstaclesObstrNeighbors(self):
        return self._obstaclesObstrNeighbors

    @property
    def destinations(self):
        return self._destinations

    @property
    def closestObstacles(self):
        return self._closestObstacles

    @property
    def connectors(self):
        return self._connectors

    @property
    def containsObstacle(self):
        return self._containsObstacle
    
    @property
    def containsSightBlockingObstacle(self):
        return self._containsSightBlockingObstacle
    
    @property
    def boundsStatic(self):
        return self._staticBounds

# ==================================================================
# ==================================================================
    @property
    def coordinates(self):
        return list(self._polygon.exterior.coords[:-1])

    @property
    def unobstructedNeighboringCellsIds(self):
        return list(self._unobstructedNeighboringCells.keys())

    @property
    def obstructedNeighboringCellsIds(self):
        return list(self._obstructedNeighboringCells.keys())

    @property
    def fullyObstructedNeighboringCellsIds(self):
        return list(self._fullyObstructedNeighboringCells.keys())

    @property
    def sightBlockingObstaclesInCellIds(self):
        return [obstacle.ID for obstacle in self._sightBlockingObstaclesInCell]
    @property
    def obstaclesInCellIds(self):
        return [obstacle.ID for obstacle in self._obstaclesInCell]
    
    @property
    def obstaclesNearIds(self):
        return [obstacle.ID for obstacle in self._obstaclesNear]
    
    @property
    def obstaclesObstrNeighborIds(self):
        return [obstacle.ID for obstacle in self._obstaclesObstrNeighbors]

    @property
    def closestObstacleIds(self):
        return [obstacle.ID for obstacle in self._closestObstacles]

    @property
    def destinationIds(self):
        return list(self._destinations.keys())

    @property
    def connectorIds(self):
        return [connector.ID for connector in self._connectors]

# ============================================================================================
# ============================================================================================

OBS_KEY_ATTR = {
    "obstaclesInCellIds": "_obstaclesInCell",
    "sightBlockingObstaclesInCellIds": "_sightBlockingObstaclesInCell",
    "obstaclesNearIds": "_obstaclesNear",
    "obstaclesObstrNeighborIds": "_obstaclesObstrNeighbors",
    "closestObstacleIds": "_closestObstacles"
}

GRID_CELL_ATTR = {
    "unobstructedNeighboringCellsIds": "_unobstructedNeighboringCells",
    "obstructedNeighboringCellsIds": "_obstructedNeighboringCells",
    "fullyObstructedNeighboringCellsIds": "_fullyObstructedNeighboringCells",
}

def createGridManagerFromFile(gridManagerFilename, walkLevels, maxPedDistPerStep, geomHash, **kwargs):
    from NOMAD.output_manager import SimulationFileOutputManager
    gridManager = GridManager(walkLevels, maxPedDistPerStep, createCells=False, **kwargs)
    gridData = SimulationFileOutputManager.readGridDataFile(gridManagerFilename)
    if geomHash is None:
        logger.warning("The output manager has no geometry hash. The infrastucture and the grid manager might be out of sync!")
    elif 'geomHash' not in gridData:
        logger.warning("The gridmanager file does not contains a geometry hash. The infrastucture and the grid manager might be out of sync!")
    elif geomHash != gridData['geomHash']:
        logger.warning("The infrastucture and the grid manager are out of sync! (the hashes are different!)")

    if gridManager._cellSize != gridData["cellSize"]:
        logger.warning("The infrastucture and the grid manager are out of sync! (the cell sizes are different!)")
    if gridManager._maxObstacleDist != gridData["maxObstacleDist"]:
        logger.warning("The infrastucture and the grid manager are out of sync! (the max obstacle distances are different!)")
    
    for gridCellInfo in gridData["gridCells"]:
        cellPol = Polygon(gridCellInfo["coordinates"])
        gridIndex = tuple(gridCellInfo["gridIndex"])
        gridCellInfo["gridIndex"] = gridIndex 
        gridCell = GridCell(gridIndex, cellPol)
        gridManager._gridCells[gridIndex] = gridCell

        for key, attrName in OBS_KEY_ATTR.items():
            for obstacleID in gridCellInfo[key]:
                for walklevel in walkLevels:
                    if walklevel.hasObstacle(obstacleID):
                        iteratable = getattr(gridCell, attrName) 
                        if isinstance(iteratable, set):
                            iteratable.add(walklevel.getObstacle(obstacleID))
                        else:
                            iteratable.append(walklevel.getObstacle(obstacleID))
        for destinationID in gridCellInfo['destinationIds']:
            for walklevel in walkLevels:
                if walklevel.hasDestination(destinationID):
                    gridCell.destinations[destinationID] = walklevel.getDestination(destinationID)

        if len(gridCellInfo['destinationIds']) > 0:
            gridManager._gridCellsWithDestinations.append(gridCell)
        

        gridCell._containsObstacle = len(gridCell._obstaclesInCell) > 0
        gridCell._containsSightBlockingObstacle = len(gridCell._sightBlockingObstaclesInCell) > 0

    for gridCellInfo in gridData["gridCells"]:
        gridCell = gridManager._gridCells[gridCellInfo["gridIndex"]]
        for key, attrName in GRID_CELL_ATTR.items():
            for otherGridIndexList in gridCellInfo[key]:
                otherGridIndex = tuple(otherGridIndexList)
                getattr(gridCell, attrName)[otherGridIndex] = gridManager._gridCells[otherGridIndex]
 
    return gridManager
    