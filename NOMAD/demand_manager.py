""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

from copy import copy
import itertools
import logging
from math import floor, inf

from shapely.geometry.base import CAP_STYLE
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.point import Point
from shapely.geometry.polygon import Polygon

import NOMAD
from NOMAD.activities import Sink
from NOMAD.constants import PREFERRED_SPEED, RADIUS, D_SHY, IE_F, IE_B, R_INFL, \
    RANDOM_PLACEMENT_TYPE, FIXED_PLACEMENT_TYPE
from NOMAD.general_functions import doListCeck, SpawnableLine, \
    polygonIsRectangle, getMinDist2Line2, RectangleFilledWithPoints
from NOMAD.shapely_subclass import ShapelyGeomClassWrapper
import numpy as np


logger = logging.getLogger(__name__)

class DemandManager():

    def __init__(self, activities, activitiesWithQueue):
        '''
        Constructor
        '''
        self.sources = set()
        self.activities = activities
        self.activitiesWithQueue = activitiesWithQueue
        self.pedParamaterSets = []
        self.sinks = [activity for activity in activities.values() if isinstance(activity, Sink)]
        self.demandPatterns = []
        self.activityWithQueue2activityPatterns = {}
        self.activeDemandPatterns = []

        self.addList = {}
        self.removeList = {}

        self.sourcesWithQueues = []
        
        self.dynamicSchedulers = []
        
        self.timeInfo = None

    def addDynamicSchedulers(self, dynamicSchedulers):
        dynamicSchedulers = doListCeck(dynamicSchedulers)
        self.dynamicSchedulers += dynamicSchedulers

    def addSources(self, sources, walkLevel):
        sources = doListCeck(sources)
        for source in sources:
            walkLevel.checkIfInWalkableArea(source, 'Source')
            source.walkLevel = walkLevel
            self.sources.update(sources)

    def addDemandPatterns(self, demandPatterns):
        demandPatterns = doListCeck(demandPatterns)
        self.demandPatterns = demandPatterns
        for demandPattern in demandPatterns:
            if demandPattern.source not in self.sources:
                raise ValueError('The demandPattern contains an unknown source')

            if demandPattern.pedParamSet not in self.pedParamaterSets:
                self.pedParamaterSets.append(demandPattern.pedParamSet)

            if demandPattern.activityPattern.hasActivitiesWithQueue:
                for activity in demandPattern.activityPattern.activitiesWithQueue:
                    if activity not in self.activityWithQueue2activityPatterns:
                        self.activityWithQueue2activityPatterns[activity] = []
                    self.activityWithQueue2activityPatterns[activity].append(demandPattern.activityPattern)
            
    def finalizeCreation(self, parameters, timeInfo, gridManager):
        self.timeInfo = timeInfo
        
        for source in self.sources:
            source.finalizeCreation(parameters, gridManager)

        for demandPattern in self.demandPatterns:
            demandPattern.createTimeIndices(timeInfo)
            if isinstance(demandPattern, VariableDemandPattern):
                demandPattern.createFlowFcnArray(timeInfo)
            elif isinstance(demandPattern, DiscreteDemandPattern):
                demandPattern.createPedCountToAddAtTime(timeInfo)

            DemandManager.addSourceToAddRemoveList(demandPattern, demandPattern.startTimeInd, self.addList)
            DemandManager.addSourceToAddRemoveList(demandPattern, demandPattern.endTimeInd, self.removeList)
            
    def initialize(self, pedestrianManager):
        for source in self.sources:
            source.initialize()

        for demandPattern in self.demandPatterns:
            demandPattern.initialize()

        self.activeDemandPatterns = []
        self.sourcesWithQueues = []

        for dynamicScheduler in self.dynamicSchedulers:
            dynamicScheduler.initialize(pedestrianManager, self.timeInfo)

    def getPedsAddedAtInit(self):
        pedAdded = []
        for dynamicScheduler in self.dynamicSchedulers:
            pedAdded += dynamicScheduler.getPedsAddedAtInit()
            
        return  pedAdded

    def getPeds2Add2Simulation(self):
        peds2add = []
        demandPatterns2add = self.addList.get(self.timeInfo.timeInd, None)
        if demandPatterns2add is not None:
            self.activeDemandPatterns += demandPatterns2add

        peds2addPerSource = self.getPeds2addPerSource()

        for source in self.sourcesWithQueues:
            if source not in peds2addPerSource:
                peds2addPerSource[source] = []

        self.sourcesWithQueues = []

        for source, peds2addFromSource in peds2addPerSource.items():
            peds2add += source.getPedsThatCanBeAdded(peds2addFromSource)
            if len(source.waitingList):
                self.sourcesWithQueues.append(source)

        demandPatterns2remove = self.removeList.get(self.timeInfo.timeInd, [])
        for demandPattern in demandPatterns2remove:
            self.activeDemandPatterns.remove(demandPattern)

        return peds2add

    def getPeds2addPerSource(self):
        peds2addPerSource = {}
        for demandPattern in self.activeDemandPatterns:
            demandForSource = peds2addPerSource.get(demandPattern.source, [])
            peds2add, pedCount2Add = demandPattern.getPeds2add(self.timeInfo)
            # To ensure that the getCreationOrder function doesn't throw an error
            if pedCount2Add > 0:
                demandForSource.append(peds2add)
            peds2addPerSource[demandPattern.source] = demandForSource

        # Fix any cases where one source has multiple demands (put peds in correct order)
        for source, demandForSource in peds2addPerSource.items():
            peds2addPerSource[source] = self.getCreationOrder(demandForSource)

        return peds2addPerSource

    def getCreationOrder(self, demandForSource):
        if len(demandForSource) == 0:
            return []
        if len(demandForSource) == 1:
            return demandForSource[0]

        counts = np.asarray([len(peds2add) for peds2add in demandForSource])
        order = np.argsort(counts)
        indices = np.zeros(len(counts), dtype=int)
        factors = np.floor(counts/counts[order[0]]).astype(int)
            
        hasFixed =  [len(peds2add) > 0 and hasattr(peds2add[0], 'initialPos')  for peds2add in demandForSource]
        demandForSource = [x for _, x in sorted(zip(hasFixed, demandForSource), key=lambda item: item[0], reverse=True)]

        peds2add = []
        for ii in range(1,len(counts)+1):
            ind = order[-ii]
            for _ in range(factors[ind]):
                if indices[ind] >= len(demandForSource[ind]):
                    continue
                peds2add.append(demandForSource[ind][indices[ind]])
                indices[ind] += 1

        return peds2add

    def updateEventBasedActivitiesFromSchedulers(self):
        pedsLeavingTheirActivity = []
        for dynamicScheduler in self.dynamicSchedulers:
            pedsLeavingTheirActivity += dynamicScheduler.updateEventBasedActivities()
        
        return pedsLeavingTheirActivity

    def _getMaxValue(self, paramName):
        maxValue = 0
        for demandPattern in self.demandPatterns:
            maxValue = max(maxValue, demandPattern.pedParamSet.getMaxParamValue(paramName))

        return maxValue

    def getMaxSpeed(self):
        return self._getMaxValue(PREFERRED_SPEED)

    def getMaxRadius(self):
        return self._getMaxValue(RADIUS)

    def getMaxDshy(self):
        return self._getMaxValue(D_SHY)

    def getMaxPedExtent(self):
        ie_f_max = self._getMaxValue(IE_F)
        ie_b_max = self._getMaxValue(IE_B)
        r_infl_max = self._getMaxValue(R_INFL)
        return max(ie_f_max, ie_b_max, r_infl_max)

    def insertQueueActivityInPattern(self, mainActivity, queueActivity):
        for activityPattern in self.activityWithQueue2activityPatterns[mainActivity]:
            activityPattern.insertQueueActivity(mainActivity, queueActivity)

    @staticmethod
    def addSourceToAddRemoveList(demandPattern, timeInd, timeList):
            if np.isinf(timeInd):
                return
            listAtTimeInd = timeList.get(timeInd, [])
            listAtTimeInd.append(demandPattern)
            timeList[timeInd] = listAtTimeInd

# ==============================================================================================
# ==============================================================================================

class Source(ShapelyGeomClassWrapper):

    def __init__(self, ID):
        '''
        Constructor
        '''
        ShapelyGeomClassWrapper.__init__(self, ID)

        self.waitingList = []
        self.walkLevel = None
        self.gridCellsInNeighborhood = []

    def initialize(self):
        self.waitingList = []

    def finalizeCreation(self, parameters, gridManager):
        self.setGridCellsInNeighborhood(gridManager, parameters.MAX_PED_RADIUS)

    def setGridCellsInNeighborhood(self, gridManager, maxPedRadius):
        # Add buffer of maxPedRadius to geometry and
        bufferedGeom = self._geometry.buffer(maxPedRadius, 32, cap_style=CAP_STYLE.round)

        self.gridCellsInNeighborhood = gridManager.getGridCellsCoveringPolygon(bufferedGeom, self.walkLevel.ID)

    def getPedsThatCanBeAdded(self, peds2add):
        peds2add = self.waitingList + peds2add
        # Determine which peds can be added and determines and sets their initial position
        maxPeds2placeCount, locationArgs = self.getPlacementInfo(len(peds2add))

        for ii in range(maxPeds2placeCount):
            peds2add[ii].initialPos = self.getInitialLocationForPed(ii, *locationArgs)
            peds2add[ii].initialWalkLevel = self.walkLevel

        self.waitingList = peds2add[maxPeds2placeCount:]

        return peds2add[:maxPeds2placeCount]

    def getPlacementInfo(self, ped2placeCount):
        raise NotImplementedError('Implement')

    def getInitialLocationForPed(self, pedInd):
        raise NotImplementedError('Implement')

class LineSource(Source, SpawnableLine):

    # Presumes a line which can be described by the equation y = ax + b

    def __init__(self, ID, lineCoords):
        Source.__init__(self, ID)
        SpawnableLine.__init__(self, ID, lineCoords)    
        
    def finalizeCreation(self, parameters, gridManager):
        super().finalizeCreation(parameters, gridManager)
        self.createBaseLineSegments(parameters, gridManager, self.walkLevel.ID)

    def getPlacementInfo(self, ped2placeCount):
        locations, maxPeds2placeCount = self.getPlacementLocations(ped2placeCount)
        return maxPeds2placeCount, (locations, )
 
    def getPlacementLocationsInSegments(self, lineSegmentArray, placementCount, maxPedDiameter):
        raise NotImplementedError('Implement')
 
    def getInitialLocationForPed(self, pedInd, locations):
        return locations[pedInd]


class SimpleRandonLineSource(LineSource):

    def __init__(self, ID, lineCoords):
        super().__init__(ID, lineCoords)

    def getPlacementLocationsInSegments(self, lineSegmentArray, placementCount, maxPedDiameter):
        return self.getRandomPlacementLocationsInSegments(lineSegmentArray, placementCount, maxPedDiameter)

class RectangleSource(Source, RectangleFilledWithPoints):

    BUFFER_SIZE = 0.1 #m

    def __init__(self, ID, rectangleCoords, directionVec=None, focusPoint=None, *args, **kwargs):
        '''
        Constructor
        '''
        Source.__init__(self, ID, *args, **kwargs)
        RectangleFilledWithPoints.__init__(self, ID, rectangleCoords,
                                           directionVec=directionVec, focusPoint=focusPoint)

    def finalizeCreation(self, parameters, gridManager):
        super().finalizeCreation(parameters, gridManager)
        self._distBetweenPoints = parameters.MAX_PED_RADIUS*2 + self.BUFFER_SIZE
        self.createPoints(self._distBetweenPoints)

    def getPlacementInfo(self, ped2placeCount):
        availableLocationIndices = self.getAvailableSpawningLocations()
        maxPeds2placeCount = min((len(availableLocationIndices), ped2placeCount))
        locationIndices = NOMAD.NOMAD_RNG.choice(availableLocationIndices, maxPeds2placeCount, False)

        return maxPeds2placeCount, (locationIndices, )

    def getAvailableSpawningLocations(self):
        self.availableLocationIndices = set(range(0, self.maxPedCount))
        for gridCell in self.gridCellsInNeighborhood:
            for ped in gridCell.pedsInCell:
                self.filterPedsInArea(ped)

        return list(self.availableLocationIndices)

    def filterPedsInArea(self, ped):
        # Check with which spawning locations it overlaps
        overlapInd = np.argwhere(np.linalg.norm(self._centerPoints - np.asarray(ped.pos), axis=1) < self._distBetweenPoints)
        if len(overlapInd) == 0 or len(overlapInd[0]) == 0:
            return False

        overlapInd = overlapInd[0]
        for ii in overlapInd:
            if isinstance(ii,np.int64):
                self.availableLocationIndices.discard(ii)
            else:
                self.availableLocationIndices.discard(ii[0])

        return True

    def getInitialLocationForPed(self, pedInd, locationIndices):
        return tuple(self.centerPoints[locationIndices[pedInd],:])

class RandomRectangleSource(Source): #Untested
    
    def __init__(self, ID, polygonCoords):
        '''
        Constructor
        '''
        super().__init__(ID)

        # Can only be regtangular
        if not polygonIsRectangle(polygonCoords):
            raise ValueError('A source can only be a rectangular polygon!')
        self._geometry = Polygon(polygonCoords, None)
        self._placementGeometry = None
    
    def finalizeCreation(self, parameters, gridManager):
        super().finalizeCreation(parameters, gridManager)
        self.cropPlacementArea(parameters, gridManager)
        self._maxPedRadius = parameters.MAX_PED_RADIUS
        self._maxPedRadius2 = parameters.MAX_PED_RADIUS2
    
    def cropPlacementArea(self, parameters, gridManager):
        bufferedPolygon = self._geometry.buffer(parameters.MAX_PED_RADIUS)
        obstacles2check = []
        obstaclesChecked = []
        gridCells = gridManager.getGridCellsCoveringPolygon(bufferedPolygon, self.walkLevel.ID)
        for gridCell in gridCells:
            for obstacle in gridCell.obstaclesInCell:
                if obstacle in obstaclesChecked:
                    continue
                obstaclesChecked.append(obstacle)
                if obstacle.within(bufferedPolygon) or obstacle.intersects(bufferedPolygon):
                    obstacles2check.append(obstacle)

        placementGeometry = copy(self._geometry)
        for obstacle in obstacles2check:
            bufferedObstacle = obstacle.geometry.buffer(parameters.MAX_PED_RADIUS)
            placementGeometry = placementGeometry.difference(bufferedObstacle)
    
        self._placementGeometry = placementGeometry
    
    def getInitialLocationForPeds(self, pedCount):
        pedGeoms = self.getPedsInNeighbourhood()
        placementPolygons = copy(self._placementGeometry)
        for pedGeom in pedGeoms:
            placementPolygons = placementPolygons.difference(pedGeom)
        
        initLocations = []
        for _ in range(pedCount):
            if isinstance(placementPolygons, MultiPolygon):
                maxArea = 0
                placementPolygon = None
                for subPolygon in placementPolygons.geoms:
                    if subPolygon.area > maxArea:
                        placementPolygon = subPolygon
                        maxArea = subPolygon.area
            else:
                placementPolygon = placementPolygons
            
            initLoc = list(placementPolygon.centroid.coords)[0]
            
            initLocations.append(initLoc)
            placementPolygons = placementPolygons.difference(Point((initLoc[0], initLoc[1])).buffer(self._maxPedRadius))
            
        return initLocations
        
    
    def getPedsInNeighbourhood(self):
        pedGeoms = []
        for gridCell in self.gridCellsInNeighborhood:
            for ped in gridCell.pedsInCell:
                minDist2 = getMinDist2Line2(ped.pos_x, ped.pos_y, self.lineBase_x, self.lineBase_y, self.lineVec_x, self.lineVec_y, self.lineLength2)
                if minDist2 >= self._maxPedRadius2:
                    continue
    
                pedGeoms.append(Point((ped.pos_x, ped.pos_y)).buffer(ped.radius))
        
        return pedGeoms
       

# ==============================================================================================
# ==============================================================================================
class DemandPattern():

    def __init__(self, ID, source, pedParamSet, activityPattern, startTime=0, endTime=inf, groupID=None, placement=RANDOM_PLACEMENT_TYPE, placementLocation=()):
        self._ID = ID
        self.source = source
        self.pedParamSet = pedParamSet
        self.activityPattern = activityPattern
        self.groupID = groupID

        self.startTime = startTime
        self.endTime = endTime

        self.placement = placement
        self.placementLocation = itertools.cycle(placementLocation)

        self.startTimeInd = None
        self.endTimeInd = None

        self.added = 0

    def createTimeIndices(self, timeInfo):
        self.startTimeInd = timeInfo.getTimeInd(self.startTime)
        if np.isinf(self.endTime):
            self.endTimeInd = self.endTime
        else:
            self.endTimeInd = timeInfo.getTimeIndCeiled(self.endTime)

    def initialize(self):
        self.added = 0

    def getPeds2add(self, timeInfo):
        from NOMAD.pedestrian_manager import PedCreationInfo
        peds2add = []
        pedCount2Add = self.getPedCount2Add(timeInfo)
        self.added += pedCount2Add
        for _ in range(pedCount2Add):
            pedCreationInfo = PedCreationInfo(self.pedParamSet.getUniqueCopy(), self.activityPattern)
            if self.groupID is not None:
                pedCreationInfo.groupID = self.groupID
            if self.placement == FIXED_PLACEMENT_TYPE:
                pedCreationInfo.initialPos = self.placementLocation.__next__()
            peds2add.append(pedCreationInfo)
        return peds2add, pedCount2Add

    def getPedCount2Add(self, timeInfo):
        pedCount2Add = self.getTotalPedCountAtTime(timeInfo) - self.added
        return pedCount2Add

    def getTotalPedCountAtTime(self, timeInfo):
        raise NotImplementedError('Implement method!')

    @property
    def demandType(self):
        raise NotImplementedError('Implement method!')

    # These two methods ensure that ID is a static variable which can only be set at init
    @property
    def ID(self):
        return self._ID

class ConstantDemandPattern(DemandPattern):

    def __init__(self, ID, source, pedParamSet, activityPattern, flowPerSecond, *args, **kargs):
        super().__init__(ID, source, pedParamSet, activityPattern, *args, **kargs)

        self.flowPerSecond = flowPerSecond

    def getTotalPedCountAtTime(self, timeInfo):
        return floor(self.flowPerSecond*timeInfo.getTime(timeInfo.timeInd - self.startTimeInd))

    @property
    def demandType(self):
        return 'constant'

class PoissonDemandPattern(DemandPattern):

    def __init__(self, ID, source, pedParamSet, activityPattern, flowPerSecond, *args, **kargs):
        super().__init__(ID, source, pedParamSet, activityPattern, *args, **kargs)

        self.flowPerSecond = flowPerSecond

    def getPedCount2Add(self, timeInfo):
        return NOMAD.NOMAD_RNG.poisson(self.flowPerSecond*timeInfo.timeStep)

    @property
    def demandType(self):
        return 'poisson'

class VariableDemandPattern(DemandPattern):

    def __init__(self, ID, source, pedParamSet, activityPattern, flowArrayList, *args, **kargs):
        super().__init__(ID, source, pedParamSet, activityPattern, *args, **kargs)
        self.flowArrayList = flowArrayList
        self.flowFcnArray = None

    def getTotalPedCountAtTime(self, timeInfo):
        def getSurfaceOfLinFcn(a,b,x):
            return np.sum(0.5*a*np.power(x,2) + b*x)
        t = timeInfo.currentTime
        binIndices = np.where(t >= self.flowFcnArray[0,:])[0]
        if len(binIndices) == 1:
            totalPed = 0
        else:
            totalPed = getSurfaceOfLinFcn(self.flowFcnArray[3,binIndices[:-1]],
                                          self.flowFcnArray[4,binIndices[:-1]],
                                          self.flowFcnArray[2,binIndices[:-1]])

        totalPed += getSurfaceOfLinFcn(self.flowFcnArray[3,binIndices[-1]],
                                      self.flowFcnArray[4,binIndices[-1]],
                                      t - self.flowFcnArray[0,binIndices[-1]])

        return floor(totalPed)

    def createFlowFcnArray(self, timeStepInfo):
        # Variable flow whereby the flow between to point is either stable or linearly
        # increasing or decreasing (all can be described by y = a*x + b)
        # flowFcnArray = [
        #    start time
        #    end time
        #    a
        #    b
        #  ]
        stepCount = self.endTimeInd - self.startTimeInd + 1

        doesNotContainStart = self.flowArrayList[0][0] != 0
        doesNotContainEnd = self.flowArrayList[-1][0] != timeStepInfo.getTime(stepCount - 1)

        partCount = len(self.flowArrayList) + doesNotContainStart + doesNotContainEnd

        flowArray = np.zeros((2,partCount))

        if doesNotContainStart:
            flowArray[1,0] = self.flowArrayList[0][1]
            startInd = 1
        else:
            startInd = 0

        for ii in range(len(self.flowArrayList)):
            flowArray[0,ii+startInd] = self.flowArrayList[ii][0]
            flowArray[1,ii+startInd] = self.flowArrayList[ii][1]

        if doesNotContainEnd:
            flowArray[0,-1] = timeStepInfo.getTime(stepCount - 1)
            flowArray[1,-1] = flowArray[1,-2]

        # Capture the case of a sudden peak
        breakPoints = np.concatenate((np.zeros(1, dtype=int),
                                      np.where((flowArray[0,1:] - flowArray[0,:-1]) < 1e-6)[0] + 1,
                                      np.ones(1, dtype=int)*flowArray.shape[1]))


        flowFcnArray = np.zeros((5, 0))

        for ii in range(len(breakPoints) - 1):
            flowArray_ii = flowArray[:,breakPoints[ii]:breakPoints[ii + 1]]
            flowFcnArray_ii = np.zeros((5, flowArray_ii.shape[1] - 1))
            flowFcnArray_ii[0,:] = flowArray_ii[0,:-1]
            flowFcnArray_ii[1,:] = flowArray_ii[0,1:]
            flowFcnArray_ii[2,:] = flowArray_ii[0,1:] - flowArray_ii[0,:-1]
            flowFcnArray_ii[3,:] = (flowArray_ii[1,1:] - flowArray_ii[1,:-1])/flowFcnArray_ii[2,:]
            flowFcnArray_ii[4,:] = flowArray_ii[1,:-1]

            # Set to zero in case small rounding error causes it to get a slop
            flowFcnArray_ii[3,np.abs(flowArray_ii[1,1:] - flowArray_ii[1,:-1]) < 1e-6] = 0

            flowFcnArray = np.concatenate((flowFcnArray, flowFcnArray_ii), axis=1)

        self.flowFcnArray = flowFcnArray

    @property
    def demandType(self):
        return 'variable'

class DiscreteDemandPattern(DemandPattern):

    def __init__(self, ID, source, pedParamSet, activityPattern, discretePattern, *args, **kargs):
        super().__init__(ID, source, pedParamSet, activityPattern, *args, **kargs)

        # discretePattern is a list which contains one or more lists with have two elements:
        # 1.) A time in seconds
        # 2.) A number of peds that should be created at that time
        # So for example [[10, 2], [80, 1], [100, 5]]
        self.discretePattern = discretePattern
        self.pedCountToAddAtTime = {}

    def createTimeIndices(self, timeInfo):
        self.startTimeInd = timeInfo.getTimeInd(self.discretePattern[0][0])
        self.endTimeInd = timeInfo.getTimeIndCeiled(self.discretePattern[-1][0])

    def createPedCountToAddAtTime(self, timeInfo):
        for el in self.discretePattern:
            elTimeInd = timeInfo.getTimeInd(el[0])
            self.pedCountToAddAtTime[elTimeInd] = el[1]

    def getPedCount2Add(self, timeInfo):
        return self.pedCountToAddAtTime.get(timeInfo.timeInd, 0)

    @property
    def demandType(self):
        return 'discrete'

# ==============================================================================================
# ==============================================================================================

