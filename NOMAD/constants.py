""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

ATTR_PED_CLASS = 'attrPed'
ATTR_PED_OPT_CLASS = 'attrPedOpt'
ATTR_PED_INT_C_CLASS = 'attrPedIntC'
ATTR_PED_PED_FORCES_C_CLASS = 'attrPedPedForcesC'

ACC_MAIN = 'acc_main'
ACC_NO_NOISE = 'acc_noNoise'
ACC_ONLY_PATH = 'acc_onlyPath'
ACC_NO_PED_AND_NOISE = 'acc_noNoiseAndPed'
ACC_RED_DEST = 'acc_redDest'
ACC_NO_PED = 'acc_noPed'
ACC_RED_PED = 'acc_redPed'

PED_FORCE_MAIN = 'pedForce_main'
PED_FORCE_DUMMY = 'pedForce_dummy'

IN_ISOLATION_STATE = 1
IN_RANGE_STATE = 2
IN_COLLISION_STATE = 3

IN_ISOLATION_TIME_STEP = 0.1
IN_RANGE_STEPS_PER_IN_ISOLATION_STEP = 5
IN_COLLISION_STEPS_PER_IN_ISOLATION_STEP = 10

MAX_PED_SPEED_NM = 'MAX_PED_SPEED'
MAX_PED_SPEED_2_NM = 'MAX_PED_SPEED_2'
MAX_PED_PREF_SPEED_NM = 'MAX_PED_PREF_SPEED'
MAX_PED_DIST_PER_STEP_NM = 'MAX_PED_DIST_PER_STEP'
MAX_PED_RADIUS_NM = 'MAX_PED_RADIUS'
MAX_PED_D_SHY_NM = 'MAX_PED_D_SHY'
MAX_PED_EXTENT_NM = 'MAX_PED_EXTENT'
MAX_PED_ACCELERATION_NM = 'MAX_PED_ACCELERATION'
DIST_BETWEEN_QUEUEING_PEDS = 'DIST_BETWEEN_QUEUEING_PEDS'
GRID_CELL_SIZE_NM = 'GRID_CELL_SIZE'
ACC_CALC_FCN_NM = 'ACC_CALC_FCN'

PREFERRED_SPEED = 'preferredSpeed'
TAU = 'tau'
A_0 = 'a_0'
R_0 = 'r_0'
A_1 = 'a_1'
R_1 = 'r_1'
KAPPA_0 = 'kappa_0'
KAPPA_1 = 'kappa_1'
A_W = 'a_W'
D_SHY = 'd_shy'
R_INFL = 'r_infl'
IE_F = 'ie_f'
IE_B = 'ie_b'
C_PLUS_0 = 'cPlus_0'
C_MINUS_0 = 'cMinus_0'
T_A = 't_A'
RADIUS = 'radius'
NOISE_FACTOR = 'noiseFactor'


NAME_FIELD = 'name'
VALUE_FIELD = 'value'
PARAM_NMS = (PREFERRED_SPEED, TAU, A_0, R_0, A_1, R_1, KAPPA_0, KAPPA_1, A_W, D_SHY,
             R_INFL, IE_F, IE_B, C_PLUS_0, C_MINUS_0, T_A, RADIUS, NOISE_FACTOR)

STATIC_VALUE = 'staticValue'
DISTR_VALUE = 'distributionValue'
VALUE_TYPE = 'valueType'

DEST_FORCE_REDUCTION_FACTOR = 0.45
MIN_SPEED = 0.4 # m/s

LRCC_FLOODING_TYPE = 'lrcmFlooding'
LRCC_ITERATIVE_TYPE = 'lrcmIterative'

PED_MOVING_STATE = 1
PED_STATIC_STATE = 2
PED_OUTSIDE_OF_SIM_STATE = 3
PED_STATIC_NON_INTERACTING_STATE = 4

WALKING_ACTIVITY_NAME = 'Walking'
QUEUEING_ACTIVITY_NAME = 'Queueing'

NUMPY_DISTR_TYPE = 'numpy'
SCIPY_DISTR_TYPE = 'scipy'
OPENTURNS_DISTR_TYPE = 'openturns'

FIXED_PLACEMENT_TYPE = 'fixed'
RANDOM_PLACEMENT_TYPE = 'random'
