""" 
Copyright (C) 2023 Martijn Sparnaaij - All Rights Reserved

This file is part of NOMAD.

NOMAD is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

NOMAD is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with NOMAD. If not, see <https://www.gnu.org/licenses/>. 
"""

import logging
from math import sqrt
import math


EPSILON = 0
EPSILON2 = 0

logger = logging.getLogger(__name__)

class Vector2D(tuple):

    def __new__(self, *args):
        if len(args) == 1:
            return tuple.__new__(Vector2D, ((args[0][0] * 1.0, args[0][1] * 1.0)))
        elif len(args) == 2:
            return tuple.__new__(Vector2D, ((args[0] * 1.0, args[1] * 1.0)))

    def __str__(self):
        """Concise string representation."""
        return "Vector2D(%.2f, %.2f)" % self

    def __repr__(self):
        """Precise string representation."""
        return "Vector2D(%r, %r)" % self

    @property
    def x(self):
        """The horizontal coordinate."""
        return self[0]

    @property
    def y(self):
        """The vertical coordinate."""
        return self[1]

    @property
    def length(self):
        """The length or scalar magnitude of the vector."""
        #try:
        #    return self.__dict__['length']
        #except:
        #    self.__dict__['length'] = self.calcLength()
        #    return self.__dict__['length']
        x, y = self
        return sqrt(x*x + y*y)

    def calcLength(self):
        x, y = self
        return sqrt(x*x + y*y)

    @property
    def length2(self):
        """The square of the length of the vector."""
#         try:
#             return self.__dict__['length2']
#         except:
#             x, y = self
#             l2 = x*x + y*y
#             self.__dict__['length2'] = l2
#             return l2
        x, y = self
        return x*x + y*y

    def is_null(self):
        """Flag indicating if the vector is effectively zero-length.

        :return: True if the vector length < EPSILON.
        """
        return self.length2 < EPSILON2

    def __nonzero__(self):
        """A vector is True if it is not the null vector."""
        return self[0] != 0.0 or self[1] != 0.0

    def almost_equals(self, other):
        """Compare vectors for approximate equality.

        :param other: Vector being compared.
        :type other: Vector2D
        :return: True if distance between the vectors < ``EPSILON``.
        """
        ox, oy = other
        dx = self[0] - ox
        dy = self[1] - oy
        return (dx*dx + dy*dy) < EPSILON2

    def normalized(self):
        """Return the vector scaled to unit length. If the vector
        is null, the null vector is returned.

        :rtype: Vector2D
        """
        L = self.length
        if L > EPSILON:
            v = tuple.__new__(Vector2D, (self[0] / L, self[1] / L))
            v.__dict__['length'] = v.__dict__['length2'] = 1.0
            return v
        else:
            return null

    def getNormalized(self):
        try:
            return self.__dict__['normalized'] # @IgnoreException
        except:
            self.__dict__['normalized'] = self.normalized()
            return self.__dict__['normalized']

    def perpendicular(self):
        """Compute the perpendicular vector.

        :rtype: Vector2D
        """
        return tuple.__new__(Vector2D, (-self[1], self[0]))

    def getTangential(self):
        return self.perpendicular()

    def dot(self, other):
        """Compute the dot product with another vector.

        :param other: The vector with which to compute the dot product.
        :type other: Vector2D
        :rtype: float
        """
        ox, oy = other
        return self[0] * ox + self[1] * oy

    def cross(self, other):
        """Compute the cross product with another vector.

        :param other: The vector with which to compute the cross product.
        :type other: Vector2D
        :return: The length of the cross-product vector
        :rtype: float
        """
        ox, oy = other
        return self[0] * oy - self[1] * ox

    @property
    def angle(self):
        """The angle the vector makes to the positive x axis in the range
        ``(-180, 180]``.
        """
        try:
            return self.__dict__['angle']
        except:
            angle = math.degrees(math.atan2(self[1], self[0]))
            self.__dict__['angle'] = angle
        return angle


    @property
    def angle_rad(self):
        """The angle the vector makes to the positive x axis in the range
        ``(-pi, pi]``.
        """
        try:
            return self.__dict__['angle']
        except:
            angle = math.atan2(self[1], self[0])
            self.__dict__['angle'] = angle
        return angle

    def angle_to(self, other):
        """Compute the smallest angle from this vector to another.

        :param other: Vector to compute the angle to.
        :type other: Vector2D
        :return: Angle in degrees in the range ``(-180, 180]``.
        :rtype: float
        """
        return other.angle - self.angle

    def angle_to_rad(self, other):
        """Compute the smallest angle from this vector to another.

        :param other: Vector to compute the angle to.
        :type other: Vector2D
        :return: Angle in degrees in the range ``(-pi, pi]``.
        :rtype: float
        """
        return other.angle_rad - self.angle_rad


    def distance_to(self, other):
        """Compute the distance to another point vector.

        :param other: The point vector to which to compute the distance.
        :type other: Vector2D
        :rtype: float
        """
        ox, oy = other
        return math.hypot(self[0] - ox, self[1] - oy)

    def distance_to_2(self, otherVec):
        ox, oy = otherVec
        diff_x = self[0] - ox
        diff_y = self[1] - oy

        return diff_x*diff_x + diff_y*diff_y

    def scaled_to(self, length):
        """Compute the vector scaled to a given length. If the
        vector is null, the null vector is returned.

        :param length: The length of the vector returned, unless
            the vector is null.
        :type length: float
        :rtype: Vector2D
        """
        L = self.length
        if L > EPSILON:
            vx, vy = self
            s = length / L
            v = tuple.__new__(Vector2D, (vx * s, vy * s))
            v.__dict__['length'] = length
            return v
        else:
            return null

    def project(self, other):
        """Compute the projection of another vector onto this one.

        :param other: The vector to project.
        :type other: Vector2D
        :rtype: Vector2D
        """
        L = self.length2
        if L > EPSILON2:
            s = self.dot(other) / L
            return tuple.__new__(Vector2D, (self[0] * s, self[1] * s))
        else:
            return null

    def reflect(self, other):
        """Compute the reflection of this vector against another.

        :param other: The vector to reflect against.
        :type other: Vector2D
        :rtype: Vector2D
        """
        x1, y1 = self
        x2, y2 = other
        L = (x2 * x2 + y2 * y2)
        if L > EPSILON2:
            temp = 2 * (x1 * x2 + y1 * y2) / L
            return tuple.__new__(Vector2D, (x2 * temp - x1, y2 * temp - y1))
        else:
            return null

    def clamped(self, min_length=None, max_length=None):
        """Compute a vector in the same direction with a bounded
        length. If ``min_length`` <= ``self.length`` <= ``max_length``
        then the original vector is returned.

        :param min_length: Minimum length of computed vector. Note if
            the input vector is null, the null vector is always returned.
        :type min_length: float
        :param max_length: Maximum length of computed vector. Must
            be >= ``min_length``.
        :type max_length: float
        :rtype: Vector2D
        """
        if (min_length is not None and max_length is not None
            and min_length > max_length):
            raise ValueError(
                "Vector2D.clamped: expected min_length <= max_length")
        L2 = self.length2
        if min_length is not None and L2 < min_length**2:
            return self.scaled_to(min_length)
        if max_length is not None and L2 > max_length**2:
            return self.scaled_to(max_length)
        return self

    def lerp(self, other, bias):
        """Compute a vector by linear interpolation between
        this vector and another.

        :param other: The vector to interpolate to. its value
            is returned when ``bias == 1.0``.
        :type other: Vector2D
        :param bias: Interpolation value when in the range [0, 1]. Becomes
            an extrapolation value outside this range.
        :type bias: float
        :rtype: Vector2D
        """
        ox, oy = other
        b1 = 1.0 - bias
        return tuple.__new__(Vector2D,
            (self[0] * b1 + ox * bias, self[1] * b1 + oy * bias))

    def __eq__(self, other):
        try:
            return (self[0] == other[0] and self[1] == other[1]
                and len(other) == 2)
        except (TypeError, IndexError):
            return False

    def __ne__(self, other):
        try:
            return (self[0] != other[0] or self[1] != other[1]
                or len(other) != 2)
        except (TypeError, IndexError):
            return True

    def __add__(self, other):
        """Add the vectors componentwise.

        :param other: The vector to add.
        :type other: Vector2D
        """
        try:
            ox, oy = other
        except Exception:
            return NotImplemented
        return tuple.__new__(Vector2D, (self[0] + ox, self[1] + oy))

    __iadd__ = __add__

    def __sub__(self, other):
        """Subtract the vectors componentwise.

        :param other: The vector to substract.
        :type other: Vector2D
        """
        try:
            ox, oy = other
        except Exception:
            return NotImplemented
        return tuple.__new__(Vector2D, (self[0] - ox, self[1] - oy))

    __isub__ = __sub__

    def __mul__(self, other):
        """Either multiply the vector by a scalar or componentwise
        with another vector.

        :param other: The object to multiply by.
        :type other: Vector2D or float
        """
        try:
            other = float(other)
            return tuple.__new__(Vector2D, (self[0] * other, self[1] * other))
        except TypeError:
            try:
                ox, oy = other
            except Exception:
                return NotImplemented
            return tuple.__new__(Vector2D, (self[0] * ox, self[1] * oy))

    __rmul__ = __imul__ = __mul__

    def __truediv__(self, other):
        """Divide the vector by a scalar or componentwise
        by another vector.

        :param other: The value to divide by.
        :type other: Vector2D or float
        """
        try:
            other = float(other)
            return tuple.__new__(Vector2D, (self[0] / other, self[1] / other))
        except TypeError:
            try:
                ox, oy = other
            except Exception:
                return NotImplemented
            return tuple.__new__(Vector2D, (self[0] / ox, self[1] / oy))

    __itruediv__ = __truediv__

    def __rtruediv__(self, other):
        """Divide a scalar or vector by this vector componentwise.

        :param other: The value to divide into.
        :type other: Vector2D or float
        """
        try:
            other = float(other)
            return tuple.__new__(Vector2D, (other / self[0], other / self[1]))
        except TypeError:
            try:
                ox, oy = other
            except Exception:
                return NotImplemented
            return tuple.__new__(Vector2D, (ox / self[0], oy / self[1]))

    def __floordiv__(self, other):
        """Divide the vector by a scalar or componentwise by
        another vector, rounding down.

        :param other: The value to divide by.
        :type other: Vector2D or float
        """
        try:
            other = float(other)
            return tuple.__new__(Vector2D, (self[0] // other, self[1] // other))
        except TypeError:
            try:
                ox, oy = other
            except Exception:
                return NotImplemented
            return tuple.__new__(Vector2D, (self[0] // ox, self[1] // oy))

    __ifloordiv__ = __floordiv__

    def __rfloordiv__(self, other):
        """Divide a scalar or vector by this vector componentwise,
        rounding down.

        :param other: The value to divide into.
        :type other: Vector2D or float
        """
        try:
            other = float(other)
            return tuple.__new__(Vector2D, (other // self[0], other // self[1]))
        except TypeError:
            try:
                ox, oy = other
            except Exception:
                return NotImplemented
            return tuple.__new__(Vector2D, (ox // self[0], oy // self[1]))

    def __pos__(self):
        return self

    def __neg__(self):
        """Compute the unary negation of the vector."""
        return tuple.__new__(Vector2D, (-self[0], -self[1]))

    def __abs__(self):
        """Compute the absolute magnitude of the vector."""
        return self.length

    def hasSameDirectionAsOther(self, otherVector):
        normVec = self.getNormalized()
        otherNormVec = otherVector.getNormalized()
        return abs(normVec[0] - otherNormVec[0]) < 1e-10 and abs(normVec[1] - otherNormVec[1]) < 1e-10

    def hasOppositeDirectionAsOther(self, otherVector):
        normVec = self.getNormalized()
        otherNormVec = otherVector.getNormalized()
        return abs(normVec[0] + otherNormVec[0]) < 1e-10 and abs(normVec[1] + otherNormVec[1]) < 1e-10

    def getOpposite(self):
        return self.__neg__()

    @staticmethod
    def distance(vec, otherVec):
        return vec.distance_to(otherVec)

    @staticmethod
    def getDirVec2otherVec(vec, otherVec):
        dirVec = otherVec - vec
        return dirVec.getNormalized()


    def round(self, decimalCount):
        return tuple.__new__(Vector2D, (round(self[0],decimalCount), round(self[1],decimalCount)))

    @staticmethod
    def roundStat(vec, decimalCount):
        return vec.round(decimalCount)
    
    

    __hash__ = tuple.__hash__ # hash is not inherited in Py 3


null = Vector2D(0, 0)

